#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc,char **argv)
{
  int N;	//number of files
  int nEvents;	//number of events for each process
  char filename1[40];	//filename1
  char filename2[40];	//filename2
  char buffer[200];	//string buffer
  ofstream macro_file;	//output macro file
  ofstream script_file;	//output shell script file
  ofstream submit_file;
  char *Data_Directory;
  int RunID;

  if (argc!= 4){
    cout <<"Usage: ./ScriptGenerator Number_of_process(1 to 999) Number_of_events(for each) RunID\n";
    return 0;
  }

  //Get data directory
  Data_Directory=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (Data_Directory==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  N = atoi(argv[1]);
  nEvents = atoi(argv[2]);
  RunID = atoi(argv[3]);

  submit_file.open("../SubmitJobs2.sh",ios::out);

  for (int i=1;i<=N;i++)
  {
    //set file names
    sprintf(filename1,"run%03d.mac",i);
    sprintf(filename2,"submit%03d.sh",i);
    //open files
    macro_file.open(filename1,ios::out);
    script_file.open(filename2,ios::out);
    //write to files
    macro_file<<"/Setup/SetDataDirect "<<Data_Directory<<endl;
    macro_file<<"/Setup/SetRandomGen Internal"<<endl;
    macro_file<<"/Setup/SetGeneratorID "<<i<<endl;
//    macro_file<<"/Generator/SetSourceType Diffuse"<<endl;
//    macro_file<<"/Generator/SetDiffuse_R 59.0"<<endl;
//    macro_file<<"/Generator/SetDiffuse_H 81.166"<<endl;
//    macro_file<<"/Generator/SetMOT_pos 0.0 0.0 -10.834"<<endl;
    macro_file<<"/Generator/SetSourceType MOT"<<endl;
    macro_file<<"/Generator/SetIsotope He6"<<endl;
    macro_file<<"/Generator/SetMOT_pos -0.390 -0.311 -3.391"<<endl;
    macro_file<<"/Generator/SetMOT_sigma 0.730 0.649 0.730"<<endl;
    macro_file<<"/Generator/Set_b 0.0"<<endl;
    macro_file<<"/Generator/Set_a 0.3333333333333"<<endl;
    macro_file<<"/Generator/SetSolidAngle 0.0 0.0 180"<<endl;
    macro_file<<"/Setup/SetRunID "<<RunID<<endl;
    macro_file<<"/run/setCut 0.1"<<endl;
    macro_file<<"/run/setCutForRegion MWPC 0.05 mm"<<endl;
    macro_file<<"/run/beamOn "<<nEvents<<endl;

    sprintf(buffer,"%03d",i);
    script_file<<"#!/bin/bash"<<endl;
    script_file<<"#PBS -e Cluster2/Output/STDErr"<<buffer<<".txt"<<endl;
    script_file<<"#PBS -o Cluster2/Output/STDOut"<<buffer<<".txt"<<endl;
    script_file<<"#PBS -l walltime=26:00:00"<<endl;
    if (i==1) script_file<<"#PBS -M hongran@uw.edu"<<endl;
    script_file<<"#PBS -m abe"<<endl;
    script_file<<"cd $PBS_O_WORKDIR"<<endl;
//    script_file<<"cd BetaTracking"<<endl;
    script_file<<". /share/apps/root/bin/thisroot.sh"<<endl;
    script_file<<". /share/apps/geant4/bin/geant4.sh"<<endl;
    script_file<<". $HE6_SIMULATION_DIRECTORY/setup.sh"<<endl;
    script_file<<"$HE6_SIMULATION_DIRECTORY/BetaTracking/BetaTracking $HE6_SIMULATION_DIRECTORY/Cluster2/run"<<buffer<<".mac"<<endl;
    script_file<<"echo G4 Simulation Done!"<<endl;
//    script_file<<"cd ../IonTracking"<<endl;
//    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"1 "<<"InterpF "<<"FullRegionAppxNom4kV.dat " << "None "<<"9.2 "<<"Li6 " << "0.104"<<endl;
//    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"1 "<<"InterpF "<<"Nom4kVztop72zbot-93.dat " << "None "<<"9.2 "<<"Li6 " << "0.104"<<endl;
//    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"2 "<<"InterpF "<<"CurrentSpacingsVoltages.dat " << "None "<<"9.2 "<<"Li6 " << "0.104"<<endl;
//    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"1 "<<"InterpF "<<"ScaledField_Oct19to26CalibratedVoltages_-1000_-2000_2246_6198_10295_14279_18373_23069.dat " << "None "<<"9.1 "<<"Li6 " << "0.0942"<<endl;
    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"1 "<<"InterpF "<<"ScaledField_Oct19to26CalibratedVoltagesCORRECTEDRatio_-1000_-2000_2247_6201_10300_14287_18382_23080_-2482.dat " << "None "<<"9.1 "<<"Li6 " << "0.0942"<<endl;
    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"2 "<<"InterpF "<<"ScaledField_Oct19to26CalibratedVoltagesCORRECTEDRatio_-1000_-2000_2247_6201_10300_14287_18382_23080_-2482.dat " << "None "<<"9.15 "<<"Li6 " << "0.0942 -0.5"<<endl;
    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"3 "<<"InterpF "<<"ScaledField_Oct19to26CalibratedVoltagesCORRECTEDRatio_-1000_-2000_2247_6201_10300_14287_18382_23080_-2482.dat " << "None "<<"9.2 "<<"Li6 " << "0.0942 -1.0"<<endl;
//    script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking "<<i<<" "<<RunID<<" "<<"2 "<<"UF" << " -1.5661 " << "9.1 "<<"Li6 " << "0.0942"<<endl;
//    script_file<<"echo IonTracking Done"<<endl;
    // Post Processor
    script_file<<"$HE6_SIMULATION_DIRECTORY/PostProcessor/PostProcessor "<<i<<" "<<RunID<<" " << "1 "<<"1 "<<"1 "<<endl;
    script_file<<"$HE6_SIMULATION_DIRECTORY/PostProcessor/PostProcessor "<<i<<" "<<RunID<<" " << "2 "<<"1 "<<"1 "<<endl;
    script_file<<"$HE6_SIMULATION_DIRECTORY/PostProcessor/PostProcessor "<<i<<" "<<RunID<<" " << "3 "<<"1 "<<"1 "<<endl;
    script_file<<"echo Processing Done"<<endl;

    submit_file<<"qsub $HE6_SIMULATION_DIRECTORY/Cluster2/"<<filename2<<endl;
    //close files
    macro_file.close();
    script_file.close();
  }
  submit_file.close();
  return 0;
}
