#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc,char **argv)
{
  int N;	//number of files
  int nEvents;	//number of events for each process
  char filename1[40];	//filename1
  char filename2[40];	//filename2
  char buffer[200];	//string buffer
  ofstream macro_file;	//output macro file
  ofstream script_file;	//output shell script file
  ofstream submit_file;
  char *Data_Directory;
  int RunID;

  if (argc!= 4){
    cout <<"Usage: ./ScriptGenerator Number_of_process(1 to 999) Number_of_events(for each) RunID\n";
    return 0;
  }

  //Get data directory
  Data_Directory=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (Data_Directory==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  N = atoi(argv[1]);
  nEvents = atoi(argv[2]);
  RunID = atoi(argv[3]);

  submit_file.open("../SubmitJobs.sh",ios::out);

  for (int i=1;i<=N;i++)
  {
    //set file names
    sprintf(filename1,"run%03d.mac",i);
    sprintf(filename2,"submit%03d.sh",i);
    //open files
    macro_file.open(filename1,ios::out);
    script_file.open(filename2,ios::out);
    //write to files
    macro_file<<"/Setup/SetDataDirect "<<Data_Directory<<endl;
    macro_file<<"/Setup/SetRandomGen Internal"<<endl;
    macro_file<<"/Setup/SetGeneratorID "<<i<<endl;
//    macro_file<<"/Generator/SetSourceType Diffuse"<<endl;
//    macro_file<<"/Generator/SetDiffuse_R 59.0"<<endl;
//    macro_file<<"/Generator/SetDiffuse_H 81.166"<<endl;
//    macro_file<<"/Generator/SetMOT_pos 0.0 0.0 -10.834"<<endl;
    macro_file<<"/Generator/SetSourceType MOT"<<endl;
//    macro_file<<"/Generator/SetIsotope He6"<<endl;
    macro_file<<"/Generator/SetMOT_pos -1.95 1.95 -2.86"<<endl;
    macro_file<<"/Generator/SetMOT_sigma 1.25 1.25 0.01"<<endl;

    //Set Isotope after setting the MOT Position
    macro_file<<"/Generator/SetIsotope Fe55"<<endl;
    macro_file<<"/Generator/Set_b 0.0"<<endl;
    macro_file<<"/Generator/Set_a -0.3333333333333"<<endl;
    macro_file<<"/Generator/SetSolidAngle 0.0 0.0 30"<<endl;
    macro_file<<"/Setup/SetRunID "<<RunID<<endl;
    macro_file<<"/run/setCut 0.1"<<endl;
    macro_file<<"/run/setCutForRegion MWPC 0.05 mm"<<endl;
    macro_file<<"/run/beamOn "<<nEvents<<endl;

    sprintf(buffer,"%03d",i);
    script_file<<"#!/bin/bash"<<endl;
    script_file<<"#PBS -e Cluster/Output/STDErr"<<buffer<<".txt"<<endl;
    script_file<<"#PBS -o Cluster/Output/STDOut"<<buffer<<".txt"<<endl;
    script_file<<"#PBS -l walltime=26:00:00"<<endl;
    if (i==1) script_file<<"#PBS -M hongran@uw.edu"<<endl;
    script_file<<"#PBS -m abe"<<endl;
    script_file<<"cd $PBS_O_WORKDIR"<<endl;
//    script_file<<"cd BetaTracking"<<endl;
    script_file<<". /share/apps/root/bin/thisroot.sh"<<endl;
    script_file<<". /share/apps/geant4/bin/geant4.sh"<<endl;
    script_file<<". $HE6_SIMULATION_DIRECTORY/setup.sh"<<endl;
//    script_file<<"$HE6_SIMULATION_DIRECTORY/BetaTracking/BetaTracking $HE6_SIMULATION_DIRECTORY/Cluster/run"<<buffer<<".mac"<<endl;
    script_file<<"echo G4 Simulation Done!"<<endl;
    // Post Processor
    script_file<<"$HE6_SIMULATION_DIRECTORY/PostProcessor/PostProcessor "<<i<<" "<<RunID<<" " << "1 "<<"1 "<<"1 BetaOnly"<<endl;
    script_file<<"echo Processing Done"<<endl;

    submit_file<<"qsub $HE6_SIMULATION_DIRECTORY/Cluster/"<<filename2<<endl;
    //close files
    macro_file.close();
    script_file.close();
  }
  submit_file.close();
  return 0;
}
