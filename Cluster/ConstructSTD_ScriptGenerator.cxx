#include <iostream>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

using namespace std;

int main(int argc,char **argv)
{
  int N;	//number of files
  int nEvents;	//number of events for each process
  char filename1[40];	//filename1
  char filename2[40];	//filename2
  char filename3[40];	//filename3
  
  char buffer[200];	//string buffer
  ofstream beta_macro_file, ion_macro_file;	//output macro file
  ofstream script_file;	//output shell script file
  ofstream submit_file;
  ofstream command_log;
  char *Data_Directory;
  int aPlus_RunID, aMinus_RunID, RunID, IonID;
  string std_string;
  double a;

  if (argc!= 5){
    cout <<"Usage: ./ConstructSTD_ScriptGenerator Number_of_events(for each) aMinus_RunID aPlus_RunID IonID\n";
    return 0;
  }

  //Get data directory
  Data_Directory=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (Data_Directory==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }

  N = 100;
  nEvents = atoi(argv[1]);
  aMinus_RunID = atoi(argv[2]);
  aPlus_RunID = atoi(argv[3]);
  IonID = atoi(argv[4]);

  submit_file.open("../SubmitJobs.sh",ios::out);
  
  for (int j=1;j<=2;j++){
	  if(j==1){
		  std_string = "aMinus";
		  a = -.3333333333;
		  RunID = aMinus_RunID;
	  }else{
		  std_string = "aPlus";
		  a = +.3333333333;
		  RunID = aPlus_RunID; 
	  }
	  for (int i=1;i<=N;i++){
		  //set file names
		  sprintf(filename1,"run_%s%03d.mac",std_string.c_str(),i);
		  sprintf(filename2,"ion_%s%03d.in",std_string.c_str(),i);
		  sprintf(filename3,"submit_%s%03d.sh",std_string.c_str(),i);

		  //open files
		  beta_macro_file.open(filename1,ios::out);
		  ion_macro_file.open(filename2,ios::out);
		  script_file.open(filename3,ios::out);

		  //write to files
		  beta_macro_file<<"/Setup/SetDataDirect "<<Data_Directory<<endl;
		  beta_macro_file<<"/Setup/SetRandomGen Internal"<<endl;
		  beta_macro_file<<"/Setup/SetGeneratorID "<<i<<endl;    
		  beta_macro_file<<"/Setup/SetRunID "<<RunID<<endl;
		  //    beta_macro_file<<"/Generator/SetSourceType Diffuse"<<endl;
		  //    beta_macro_file<<"/Generator/SetDiffuse_R 59.0"<<endl;
		  //    beta_macro_file<<"/Generator/SetDiffuse_H 81.166"<<endl;
		  beta_macro_file<<"/Generator/SetMOT_pos -0.377 -0.278 -0.687"<<endl;
		  beta_macro_file<<"/Generator/SetMOT_sigma 0.609 0.577 0.609"<<endl;
		  beta_macro_file<<"/Generator/SetSourceType MOT"<<endl;
		  beta_macro_file<<"/Generator/SetIsotope He6"<<endl;    
		  //beta_macro_file<<"/Generator/SetMOT_pos -1.95 1.95 -2.86"<<endl;
		  //beta_macro_file<<"/Generator/SetMOT_sigma 1.25 1.25 0.01"<<endl;
		  //Set Isotope after setting the MOT Position
		  //beta_macro_file<<"/Generator/SetIsotope Bi207"<<endl;
		  beta_macro_file<<"/Generator/SetSolidAngle 0.0 0.0 20"<<endl;
		  beta_macro_file<<"/Generator/Set_b 0.0"<<endl;
		  beta_macro_file<<"/Generator/Set_a "<<a<<endl;
		  //beta_macro_file<<"/process/inactivate msc "<<endl;
		  //beta_macro_file<<"/process/inactivate eBrem "<<endl;
		  //beta_macro_file<<"/process/inactivate CoulombScat "<<endl;
		  beta_macro_file<<"/run/setCut 0.1"<<endl;
		  beta_macro_file<<"/run/setCutForRegion MWPC 0.05 mm"<<endl;
		  beta_macro_file<<"/run/beamOn "<<nEvents<<endl;


		  ion_macro_file<<"SetGeneratorID "<<i<<endl;
		  ion_macro_file<<"SetRunID "<<RunID<<endl; 
		  ion_macro_file<<"SetIonID "<<IonID<<endl;
		  ion_macro_file<<"SetModule InterpF"<<endl; 
		  ion_macro_file<<"SetIon Li6"<<endl; 
		  ion_macro_file<<"SetShakeoffProb 0.0962 0.00104"<<endl;			
		  ion_macro_file<<"SetZShift 0"<<endl; 
		  ion_macro_file<<"SetEFieldFile "<<"ScaledField_Jan2016ZOpti155Settings_-1000_-2000_2247_6201_10300_14287_18382_23080_-2482.dat"<<endl;
		  ion_macro_file<<"SetRegionOfFlight 81 -91 70.5"<<endl;	 //r0,zMCP,ztop 
		  ion_macro_file<<"SetMCPPosition -91"<<endl;

		  sprintf(buffer,"%03d",i);
		  script_file<<"#!/bin/bash"<<endl;
		  script_file<<"#PBS -e Cluster/Output/STD"<<std_string<<"Err"<<buffer<<".txt"<<endl;
		  script_file<<"#PBS -o Cluster/Output/STD"<<std_string<<"Out"<<buffer<<".txt"<<endl;
		  //script_file<<"#PBS -l walltime=26:00:00"<<endl;
		  if (i==1) script_file<<"#PBS -M hongran@uw.edu"<<endl;
		  script_file<<"#PBS -m abe"<<endl;
		  script_file<<"cd $PBS_O_WORKDIR"<<endl;
		  script_file<<". /share/apps/root/bin/thisroot.sh"<<endl;
		  script_file<<". /share/apps/geant4/bin/geant4.sh"<<endl;
		  script_file<<". $HE6_SIMULATION_DIRECTORY/local_setup.sh"<<endl;
		  script_file<<"$HE6_SIMULATION_DIRECTORY/BetaTracking/BetaTracking $HE6_SIMULATION_DIRECTORY/Cluster/"<<filename1<<endl;
		  script_file<<"echo G4 Simulation Done!"<<endl;
		  script_file<<"$HE6_SIMULATION_DIRECTORY/IonTracking/IonTracking $HE6_SIMULATION_DIRECTORY/Cluster/"<<filename2<<endl;
		  script_file<<"echo Ion Tracking Done!"<<endl;
		  script_file<<"$HE6_SIMULATION_DIRECTORY/PostProcessor/PostProcessor "<<i<<" "<<RunID<<" " <<IonID<<" 1 1"<<endl;
		  script_file<<"echo Processing Done!"<<endl;
		  submit_file<<"qsub -q express $HE6_SIMULATION_DIRECTORY/Cluster/"<<filename3<<endl;
		  //close files
		  beta_macro_file.close();
		  ion_macro_file.close();
		  script_file.close();
	  }
  }
  submit_file.close();
  command_log.open("ScriptGeneratorCommandLog.txt",ios::out|ios::app);
  command_log<<"./ConstructSTD_ScriptGenerator "<<argv[1]<<" "<<argv[2]<<" "<<argv[3]<<" "<<argv[4]<<endl;
  command_log.close();
  return 0;
}

