//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: BetaTracPhysicsList.hh,v 1.12 2008-09-22 16:41:20 maire Exp $
// GEANT4 tag $Name: not supported by cvs2svn $
//
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#ifndef BetaTracPhysicsList_h
#define BetaTracPhysicsList_h 1

#include "G4VUserPhysicsList.hh"
//#include "G4VModularPhysicsList.hh"
#include "G4VPhysicsConstructor.hh"
#include "globals.hh"

//class G4VPhysicsConstructor;
class BetaTracPhysicsListMessenger;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

//class BetaTracPhysicsList: public G4VModularPhysicsList
class BetaTracPhysicsList: public G4VUserPhysicsList
{
  public:
    BetaTracPhysicsList();
    ~BetaTracPhysicsList();

    void ConstructParticle();
    void ConstructProcess();

    void AddPhysicsList(const G4String& name);
    void AddStepMax();
    void SetCuts();
/*    void SetCutForGamma(G4double);
    void SetCutForElectron(G4double);
    void SetCutForPositron(G4double);*/
  private:
    BetaTracPhysicsListMessenger* pMessenger; 

    G4String emName;	//name of module
    G4VPhysicsConstructor*  emPhysicsList;	//EM Physics list constructor
/*    G4double cutForGamma;
    G4double cutForElectron;
    G4double cutForPositron; */   
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

 
