#ifndef BetaTracDataUnit_h
#define BetaTracDatUnit_h 1

/*
typedef struct OutData{
  G4int    EventID;
  G4double ScintEnergy;
  G4double MWPCEnergy;
  G4double PosMWPCEntrance[3];
  G4double PosMWPCExit[3];
  G4double ExitAngle_Be;
  G4double DECPos[3];
  G4double V_Ion[3];
  uint8_t  Hits;	//Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
}OutData;
*/

typedef struct OutData{	//compressed version of data structure
  unsigned int ScintEnergy;	//in eV
  unsigned short  MWPCEnergy;	//in eV
  short PosMWPCEntrance[3];	//in um,z component 10um
  short PosMWPCExit[3];		//in um,z component 10um
  unsigned short ExitAngle_Be;	//[.01 deg] as in multiply by .01 to get deg
  int InitP[3]; //Initial momentum of the beta particle [eV]
  short DECPos[3];		//in um
  int V_Ion[3];			//in [10^-5 cm/us]
  uint16_t  Hits;	//Hits bitmap: MCP Wall Ele Col Win MWPC Sci, bit7: backscattered from Scintillator
  uint8_t ParticleID;  //Initial particle type. 0:electron 1:photon
  uint8_t MWPCFiredCell;	//Number of fired cells/wires
  uint8_t MWPCCellIndex[20];	//Fired cell index
  unsigned short MWPCCellIonNumber[20];	//Total ion number in a cell
  short MWPCCellXpos[20];	//Average y position [um]
  unsigned short MWPCCellXvar[20];	//y position variation [um]
  short ScintTime;	//Time when the beta hit the scintillator [ps]
  short MCPTime;	//Time when the beta hit the MCP [ps]
}OutData;

#endif

    
