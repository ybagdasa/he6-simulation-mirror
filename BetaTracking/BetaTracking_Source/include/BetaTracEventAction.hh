//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracEventAction.hh
/// \brief Definition of the BetaTracEventAction class

#ifndef BetaTracEventAction_h
#define BetaTracEventAction_h 1

#include "G4UserEventAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include <iostream>
#include <fstream>
#include <vector>
#include <stdint.h>
#include "BetaTracDataUnit.hh"
#include "TH1.h"
#include "TTree.h"

class BetaTracSteppingAction;


/// Event action class
///
/// It holds data member fEnergySum and fEnergy2Sum for accumulating 
/// the event energy deposit its square event by event.
/// These data are then used in the run action to compute the dose.
/// The accumulated energy and enrgy square sums are reset for each 
/// new run via the Reset() function from the run action.
using namespace std;

class BetaTracEventAction : public G4UserEventAction
{
  public:
    BetaTracEventAction();
    virtual ~BetaTracEventAction();
    
    // static access method
    static BetaTracEventAction* Instance();

    virtual void BeginOfEventAction(const G4Event* event);
    virtual void EndOfEventAction(const G4Event* event);

    void Reset();
    void OutputHistograms();
    void WriteToEventFile();

    //set methods
//    void SetScintTime(G4double time);
//    void SetMCPTime(G4double time);
    void SetKeepAll(bool Switch){KeepAll=Switch;}
    void SetRootOutputRequested(bool Switch){RootOutputRequested=Switch;}

    // get methods
    G4int GetRegScintCount() const { return RegScintCount; }
    G4int GetRegMWPCCount() const { return RegMWPCCount; }
    G4int GetHitScintCount() const { return HitScintCount; }
    G4int GetHitMWPCCount() const { return HitMWPCCount; }
    G4int GetHitMCPCount() const { return HitMCPCount; }
    G4int GetHitWallCount() const { return HitWallCount; }
    G4int GetHitElectrodeCount() const { return HitElectrodeCount; }
    G4int GetHitWindowCount() const { return HitWindowCount; }
    G4int GetHitCollimatorCount() const { return HitCollimatorCount; }
    G4int GetScatterCount() const {return ScatterCount;}
    G4int GetOutputN() const {return OutEventID;}

    // output file handling
    void Open_Output(string Data_Directory,G4int genID, G4int fileID);
    void Close_Output();
    void Open_RootOutput(string Data_Directory,G4int genID,G4int fileID);
    void InitializeRootTree();
    void SetBranchAddresses(int i);
   
    

  private:
    static BetaTracEventAction* fgInstance;  

    G4int fPrintModulo;
    G4int RegScintCount;	//Count No. of events deposit energy in Scint
    G4int RegMWPCCount;		//Count No. of events deposit energy in MWPC
    G4int HitScintCount;	//Count No. of events hit Scint,MWPC,Wall,etc.
    G4int HitMWPCCount;
    G4int HitMCPCount;
    G4int HitWallCount;
    G4int HitElectrodeCount;
    G4int HitCollimatorCount;
    G4int HitWindowCount;
    G4int ScatterCount;

    //Output files
    bool KeepAll;
    std::ofstream BetaEventFile;
    std::ofstream IonInfoFile;
    bool RootOutputRequested;
    TTree* BetaTree;
    TFile* RootOutFile;

    std::vector<OutData> OutputQueue;
    G4int OutputCount;
    G4int OutEventID;
    TH1* hist1;	//Scint Energy
    TH1* hist2;	//MWPC Energy
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
