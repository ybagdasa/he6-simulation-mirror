//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: BetaTracPGMessenger.hh,v 1.18 2007-11-13 15:48:44 asaim Exp $
// GEANT4 tag $Name: not supported by cvs2svn $
//
// 
//	GEANT 4 class header file 

// class description:
//
//      This is a messenger class for BetaTracPGAction.
//      Implemented commands are following:
//      SetRandGen,SetGeneratorID,SetRunID,SetDataDirectory
//
// 

#ifndef BetaTracPGMessenger_h
#define BetaTracPGMessenger_h 1

class BetaTracPrimaryGeneratorAction;
class G4UIdirectory;
class G4UIcmdWithoutParameter;
class G4UIcmdWithAString;
class G4UIcmdWithAnInteger;
class G4UIcmdWithADouble;
class G4UIcmdWith3Vector;
class G4UIcmdWithABool;
class G4UIcommand;

#include "G4UImessenger.hh"
#include "globals.hh"

class BetaTracPGMessenger: public G4UImessenger
{
  public:
    BetaTracPGMessenger(BetaTracPrimaryGeneratorAction* PGAct);
    ~BetaTracPGMessenger();

  public:
    void SetNewValue(G4UIcommand * command,G4String newValues);
//    G4String GetCurrentValue(G4UIcommand * command);

  private:
    BetaTracPrimaryGeneratorAction * PGAction;
    
  private: //commands
    G4UIdirectory *         GeneratorDirectory;
    G4UIcmdWithAString *    SetSourceType;
    G4UIcmdWithAString *    SetIsotope;
    G4UIcmdWithADouble *    Set_a;
    G4UIcmdWithADouble *    Set_b;
    G4UIcmdWithADouble *    SetFixed_Energy;
    G4UIcmdWithADouble *    SetHardRatio;
    G4UIcmdWith3Vector *    SetSolidAngle;
    G4UIcmdWith3Vector *    SetMOT_pos;
    G4UIcmdWith3Vector *    SetMOT_sigma;
    G4UIcmdWithADouble *    SetDiffuse_R;
    G4UIcmdWithADouble *    SetDiffuse_H;
};

#endif


