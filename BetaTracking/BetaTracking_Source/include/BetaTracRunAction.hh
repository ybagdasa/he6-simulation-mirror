//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracRunAction.hh
/// \brief Definition of the BetaTracRunAction class

#ifndef BetaTracRunAction_h
#define BetaTracRunAction_h 1

#include "G4UserRunAction.hh"
#include "globals.hh"
#include <string>

class G4Run;
class BetaTracRunMessenger;

/// Run action class
///
/// In EndOfRunAction(), it calculates the dose in the selected volume 
/// from the energy deposit accumulated via stepping and event actions.
/// The computed dose is then printed on the screen.
using namespace std;
class BetaTracRunAction : public G4UserRunAction
{
  public:
    BetaTracRunAction();
    virtual ~BetaTracRunAction();

    virtual void BeginOfRunAction(const G4Run*);
    virtual void EndOfRunAction(const G4Run*);

    void set_runID(G4int ID);
    void set_generatorID(G4int ID);
    void set_Data_Direct(G4String);
    void set_RandomGenType(int type);
    void set_KeepAll(bool Switch);

    void Logging();	//log setups to a file in setup directory
//    void set_SourceType(int type);
  private:
    G4int generatorID;
    G4int runID;
    string Data_Direct;
    int RandomGenType;	//0, internal; 1, external
//    int SourceType;	//0, MOT; 1, Diffused
    int TotalN;

    BetaTracRunMessenger * theMessenger;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

