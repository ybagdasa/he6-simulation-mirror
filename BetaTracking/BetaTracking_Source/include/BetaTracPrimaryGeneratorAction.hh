//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracPrimaryGeneratorAction.hh
/// \brief Definition of the BetaTracPrimaryGeneratorAction class

#ifndef BetaTracPrimaryGeneratorAction_h
#define BetaTracPrimaryGeneratorAction_h 1

#include "G4VUserPrimaryGeneratorAction.hh"
#include "G4ParticleGun.hh"
#include "globals.hh"
#include "EventGenerator.h"
#include <string>
#include <stdint.h>

#include <iostream>
#include <fstream>

class G4ParticleGun;
class G4Event;
class BetaTracDetectorConstruction;
class BetaTracPGMessenger;


/// The primary generator action class with particle gum.
///
/// The default kinematic is a 6 MeV gamma, randomly distribued 
/// in front of the phantom across 80% of the (X,Y) phantom size.
using namespace std;

class BetaTracPrimaryGeneratorAction : public G4VUserPrimaryGeneratorAction
{
  public:
    BetaTracPrimaryGeneratorAction();    
    virtual ~BetaTracPrimaryGeneratorAction();

    // static access method
    static BetaTracPrimaryGeneratorAction* Instance();

    // method from the base class
    virtual void GeneratePrimaries(G4Event*);         
  
    // method to access particle gun
    const G4ParticleGun* GetParticleGun() const { return fParticleGun; } 

    // method to access spectrum parameters
    G4double GetEndPointEnergy() const{ return E_endpoint; }
//    void SetEndPointEnergy(G4double inputEnergy){ E_endpoint = inputEnergy;}
  
    G4double Get_b() const{ return b; }
    void Set_b(G4double input_b);
    
    G4double Get_a() const{ return a; }
    void Set_a(G4double input_a);

    // Internal generator switches
    void Set_Fixed_Energy(double val);
    void Set_SolidAngle(G4ThreeVector Angles);
    void Set_HardRatio(double val);

    void Set_External_Rand(bool decision){ USE_EXTERNAL_RAND = decision;}

    void SetSourceType(string type);
    void SetIsotope(string iso);
    
    void Set_MOT_pos(G4ThreeVector POS);
    void Set_MOT_sigma(G4ThreeVector sigma);
    
    void Set_Diffuse_R(double radius);
    void Set_Diffuse_H(double h);

    string GetGeneratorInfo();
    // Read input event file
//    void Set_Data_Directory(char *);
    void Open_Input(string Data_Directory, G4int fileID);
    void Close_Input();


    void Get_IonInfo(int *) const;
    void Get_DecayPos(short *) const;
    void Get_Init_P(int *) const;
    uint8_t Get_ParticleID() const;
  
    void UpdateGeneratorConstants();
  private:
    static BetaTracPrimaryGeneratorAction* fgInstance;
    EventGenerator* EVGenerator;
//    G4double BetaTractrum(G4double E,G4double Pe_dot_Pv);
//    G4ThreeVector RandDirection(G4double CosTheta); //Largest Angle's Cos
    G4ParticleGun*  fParticleGun; // pointer a to G4 gun class
    string ParticleType;
    string CoinParticleType;

    //Spectrum parameters
    G4double Aperture;	//Aperture angle of the electron solid angle
    G4double Theta; //shooting direction angle theta
    G4double Phi;   //shooting direction angle phi
    G4double E_endpoint;
    G4double Fixed_Energy;
    G4double m_e;    //electron mass, in MeV
    G4double M_r;	//Recoil 6Li ion mass in MeV
//    G4double W_max; //Higher approximation of maximum of the decay curve
    G4double b;  //Fierz interference term
    G4double a;  //beta-antineutrino correlation
    G4double MOT_pos[3];	//MOT position
    G4double MOT_rad[3];	//MOT radius
    G4double D_radius;	//Diffused source radius
    G4double D_height;	//Diffused source height
    G4double V_Ion_initial[3];	//Ion Velocity vector, read from input file
    G4double DEC_pos[3];	//decay position
    int P_e_in[3];
    uint8_t ParticleID;
    //Internal event generator switches
/*    bool Fixed_Energy;
    bool Fixed_Direction;*/
    string SourceType;	//MOT; Diffused
    string Isotope;
    double ZDaughter;
    double Omega;
    double HardRatio;

    //Input event file parameters
    bool USE_EXTERNAL_RAND;
//    char Data_Directory[100];
    std::ifstream inputfile;
    
    //Messenger
    BetaTracPGMessenger * theMessenger;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif


