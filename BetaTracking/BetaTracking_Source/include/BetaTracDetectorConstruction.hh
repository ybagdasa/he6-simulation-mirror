//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracDetectorConstruction.hh
/// \brief Definition of the BetaTracDetectorConstruction class

#ifndef BetaTracDetectorConstruction_h
#define BetaTracDetectorConstruction_h 1

#include "G4VUserDetectorConstruction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"

class G4VPhysicalVolume;
class G4LogicalVolume;
class G4Material;
class G4UserLimits;

/// Detector construction class to define materials and geometry.

class BetaTracDetectorConstruction : public G4VUserDetectorConstruction
{
  public:
    BetaTracDetectorConstruction();
    ~BetaTracDetectorConstruction();

  public:
    static BetaTracDetectorConstruction* Instance();
    G4VPhysicalVolume* Construct();
    void SetGasMaterial(G4String);
    void SetSourceHolderPos(G4ThreeVector InputPos);
    void SetDegraderPos(G4ThreeVector InputPos);
    G4double GetMeanIonizationEnergy(){return MeanIonizationEnergy;}

  private:
    static BetaTracDetectorConstruction* fgInstance;
    G4LogicalVolume*	logicWorld;	//pointer to the logical world 
    G4VPhysicalVolume*	physiWorld;	//pointer to the physical world

    G4LogicalVolume*	logicScint;	//pointer to the logical scintillator
    G4VPhysicalVolume*	physiScint;	//pointer to the physical scintillator

    G4LogicalVolume*	logicLightguide;//pointer to the logical light guide
    G4VPhysicalVolume*	physiLightguide;//pointer to the physical light guide

    G4LogicalVolume*	logicMWPCGas;	//pointer to the logical MWPCgas
    G4VPhysicalVolume*	physiMWPCGas;	//pointer to the physical MWPCgas
    G4UserLimits*	fStepLimit_MWPC;
    
    //MWPC Frames
    G4LogicalVolume*	logicMWPCAnodeFrame;	//pointer to the logical MWPC anode frame
    G4VPhysicalVolume*	physiMWPCAnodeFrame;	//pointer to the physical MWPC anode frame
    G4LogicalVolume*	logicMWPCCathodeFrame1;	//pointer to the logical MWPC cathode frame1
    G4VPhysicalVolume*	physiMWPCCathodeFrame1;	//pointer to the physical MWPC cathode frame1
    G4LogicalVolume*	logicMWPCCathodeFrame2;	//pointer to the logical MWPC cathode frame2
    G4VPhysicalVolume*	physiMWPCCathodeFrame2;	//pointer to the physical MWPC cathode frame2

    G4LogicalVolume*	logicGasFill;	//pointer to the filling gas in other places
    G4VPhysicalVolume*	physiGasFill;	//pointer to the filling gas in other places

    G4LogicalVolume*	logicMOTChamber;//pointer to the logical MOT Wall
    G4VPhysicalVolume*  physiMOTChamber;//pointer to the physical MOT Wall

    G4LogicalVolume*	logicBetaChmb;	//pointer to the logial betadetector chamber
    G4VPhysicalVolume*	physiBetaChmb;	//pointer to the physical beta detector chamber
    
    G4LogicalVolume*	logicBeWindow;	//pointer to the logial Berrylium windows
    G4VPhysicalVolume*	physiBeWindow;	//pointer to the physical Berrylium windows
    G4UserLimits*	fStepLimit_Be;	//Step limiter of Be window

    G4LogicalVolume*	logicScintChmb;	//pointer to the logial Scintillator chamber
    G4VPhysicalVolume*	physiScintChmb;	//pointer to the physical Scintillator chamber

    G4LogicalVolume*	logicCol;	//pointer to the logical collimator
    G4VPhysicalVolume*	physiCol;	//pointer to the physical collimator
    G4UserLimits*	fStepLimit_Col;

    G4LogicalVolume*	logicElectrode1;	//pointer to the logical Electrodes
    G4LogicalVolume*	logicElectrode3;	//pointer to the logical Electrodes
    G4LogicalVolume*	logicElectrode4;	//pointer to the logical Electrodes
    G4LogicalVolume*	logicElectrodeCut2;	//pointer to the logical Electrode with cut
    G4LogicalVolume*	logicElectrodeCut5;	//pointer to the logical Electrode with cut
    G4VPhysicalVolume*	physiElectrode1;//pointer to the physical Electrode1
    G4VPhysicalVolume*	physiElectrode2;//pointer to the physical Electrode1
    G4VPhysicalVolume*	physiElectrode3;//pointer to the physical Electrode1
    G4VPhysicalVolume*	physiElectrode4;//pointer to the physical Electrode1
    G4VPhysicalVolume*	physiElectrode5;//pointer to the physical Electrode1
    G4UserLimits*	fStepLimit_Ele;

    G4LogicalVolume*	logicGrid;	//pointer to the logical Grid
    G4VPhysicalVolume*	physiGrid;	//pointer to the physical Grid
    G4UserLimits*	fStepLimit_Grid;

    G4LogicalVolume*	logicMCP;	//pointer to the logical MCP
    G4VPhysicalVolume*	physiMCP;	//pointer to the physical MCP
    G4UserLimits*	fStepLimit_MCP;
    
    G4LogicalVolume*	logicSourceHolderSupport;	//pointer to the logical MCP
    G4VPhysicalVolume*	physiSourceHolderSupport;	//pointer to the physical MCP
    G4UserLimits*	fStepLimit_SourceHolderSupport;
    G4LogicalVolume*	logicSourceHolderCover;	//pointer to the logical MCP
    G4VPhysicalVolume*	physiSourceHolderCover;	//pointer to the physical MCP
    G4LogicalVolume*	logicSourceHolderPt;	//pointer to the logical MCP
    G4VPhysicalVolume*	physiSourceHolderPt;	//pointer to the physical MCP
    G4UserLimits*	fStepLimit_SourceHolderPt;
    G4LogicalVolume*	logicSourceHolderSS;	//pointer to the logical MCP
    G4VPhysicalVolume*	physiSourceHolderSS;	//pointer to the physical MCP
    G4UserLimits*	fStepLimit_SourceHolderSS;
    G4LogicalVolume*	logicSourceHolderPlastic;	//pointer to the logical MCP
    G4VPhysicalVolume*	physiSourceHolderPlastic;	//pointer to the physical MCP
    G4UserLimits*	fStepLimit_SourceHolderPlastic;

    G4LogicalVolume*	logicDegrader;	//pointer to the logical MCP
    G4VPhysicalVolume*	physiDegrader;	//pointer to the physical MCP
    G4UserLimits*	fStepLimit_Degrader;
    double DegraderZ_pos;	//Z position of the Source Holder

    G4Material*		GasMaterial;
    G4double		MeanIonizationEnergy;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

