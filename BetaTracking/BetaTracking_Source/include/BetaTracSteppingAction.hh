//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracSteppingAction.hh
/// \brief Definition of the BetaTracSteppingAction class

#ifndef BetaTracSteppingAction_h
#define BetaTracSteppingAction_h 1

#include "G4UserSteppingAction.hh"
#include "globals.hh"
#include "G4ThreeVector.hh"
#include <vector>

class G4LogicalVolume;

/// Stepping action class
/// 
/// It holds data member fEnergy for accumulating the energy deposit
/// in a selected volume step by step.
/// The selected volume is set from  the detector construction via the  
/// SetVolume() function. The accumulated energy deposit is reset for each 
/// new event via the Reset() function from the event action.

typedef struct SteppingInfoUnit{
  //Energy Deposition
  G4double EnergyScint;
  G4double EnergyMWPC;
  G4double EnergyGasFill;
  G4double EnergyBeWindow;
  G4double EnergyColimmator;
  //Entrance and Exit positions on A side
  G4ThreeVector PosMWPCEntrance;
  G4ThreeVector PosMWPCExit;
  //Exit angle when leaving Be window
  G4double ExitAngle_Be;
  //Flags
  int MCP_Hit;
  int Electrode_Hit;
  int Collimator_Hit;
  int Wall_Hit;
  int Window_Hit;
  int MWPC_Hit;
  int Scint_Hit;
  int BackScattering;
} SteppingInfoUnit;

typedef struct MWPCScoreUnit{
  double xpos;
  double xpos2;
  double IonNumber;
}MWPCScoreUnit;


class BetaTracSteppingAction : public G4UserSteppingAction
{
  public:
    BetaTracSteppingAction();
    virtual ~BetaTracSteppingAction();

    // static access method
    static BetaTracSteppingAction* Instance();

    // method from the base class
    virtual void UserSteppingAction(const G4Step*);

    // reset accumulated energy
    void Reset();

    // set methods

    // get methods
    const SteppingInfoUnit& GetSteppingInfo() const {return SteppingInfo;}
    const std::vector<MWPCScoreUnit>& GetMWPCScore() const {return MWPCScoreArray;}
    G4double GetScintTime()const{return ScintTime;}
    G4double GetMCPTime()const{return MCPTime;}

  private:
    static BetaTracSteppingAction* fgInstance;  

    /*    G4LogicalVolume* fVolume_1;
	  G4LogicalVolume* fVolume_2;
	  G4double fEnergy_1;
	  G4double fEnergy_2;
     */
//    double Z_Cathode_Low;
    std::vector<MWPCScoreUnit> MWPCScoreArray;
    SteppingInfoUnit SteppingInfo;
    int Entered;
    int Exited;
    //Timing
    G4double ScintTime;
    G4double MCPTime;
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif
