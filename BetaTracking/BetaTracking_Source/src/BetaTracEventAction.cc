//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracEventAction.cc
/// \brief Implementation of the BetaTracEventAction class

#include "BetaTracEventAction.hh"

#include "BetaTracRunAction.hh"
#include "BetaTracSteppingAction.hh"
#include "BetaTracPrimaryGeneratorAction.hh"
  // use of stepping action to get and reset accumulated energy  

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "Randomize.hh"
#include "GlobalConstants.h"
#include <math.h>
#include <stdint.h>
#include <vector>
//ROOT includes
#include "TH1.h"
#include "TFile.h"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracEventAction* BetaTracEventAction::fgInstance = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracEventAction* BetaTracEventAction::Instance()
{
// Static acces function via G4RunManager 

  return fgInstance;
}      

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracEventAction::BetaTracEventAction()
: G4UserEventAction()
{ 
  fgInstance = this;

  hist1 = new TH1D("ScintE","Scint. Energy",4000,0,4000);
  hist2 = new TH1D("MWPCE","MWPC Energy",4000,0,40);
  fPrintModulo = 100000;
  OutputCount = 0;
  OutEventID = 0;
  RegScintCount = 0;
  RegMWPCCount = 0;
  HitScintCount = 0;
  HitMWPCCount = 0;
  HitMCPCount = 0;
  HitWallCount = 0;
  HitElectrodeCount = 0;
  HitWindowCount = 0;
  HitCollimatorCount = 0;

  KeepAll = false;
  RootOutputRequested = true;
  
  BetaTree=NULL;
  RootOutFile=NULL;
  InitializeRootTree();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracEventAction::~BetaTracEventAction()
{ 
  fgInstance = 0;
  delete hist1;
  delete hist2;
  Close_Output();
  delete BetaTree;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::InitializeRootTree()
{
OutData dataSet; //Declare a dummy instance of OutData struct to use to initialize Branch addresses
BetaTree = new TTree("BetaTree","The Beta and Primary Event Dataset");
//Creat Branches
BetaTree->Branch("ScintEnergy",&dataSet.ScintEnergy,"ScintEnergy/i");
BetaTree->Branch("MWPCEnergy",&dataSet.MWPCEnergy,"MWPCEnergy/s");
BetaTree->Branch("ExitAngle_Be",&dataSet.ExitAngle_Be,"ExitAngle_Be/s");
BetaTree->Branch("P0e",&dataSet.InitP[0],"x/I:y/I:z/I");
BetaTree->Branch("Pos0",&dataSet.DECPos[0],"x/S:y/S:z/S");
BetaTree->Branch("VIon0",&dataSet.V_Ion[0],"x/I:y/I:z/I");
BetaTree->Branch("Hits",&dataSet.Hits,"Hits/s");
BetaTree->Branch("ParticleID",&dataSet.ParticleID,"ParticleID/b");

BetaTree->Branch("MWPCFiredCell",&dataSet.MWPCFiredCell,"MWPCFiredCell/b");
//for (int i=0;i<20;i++) dataSet.MWPCCellIndex[i] = (uint8_t )i; //just a check
BetaTree->Branch("MWPCCellIndex",&dataSet.MWPCCellIndex[0],"MWPCCellIndex[20]/b");
BetaTree->Branch("MWPCCellIonNumber",&dataSet.MWPCCellIonNumber[0],"MWPCCellIonNumber[20]/s");
BetaTree->Branch("MWPCCellXpos",&dataSet.MWPCCellXpos[0],"MWPCCellXpos[20]/S");
BetaTree->Branch("MWPCCellXvar",&dataSet.MWPCCellXvar[0],"MWPCCellXvar[20]/s");

BetaTree->Branch("PosEn",&dataSet.PosMWPCEntrance[0],"x/S:y/S:z/S");
BetaTree->Branch("PosEx",&dataSet.PosMWPCExit[0],"x/S:y/S:z/S");
BetaTree->Branch("ScintTime",&dataSet.ScintTime,"ScintTime/S");
BetaTree->Branch("MCPTime",&dataSet.MCPTime,"MCPTime/S");
BetaTree->ResetBranchAddresses();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::SetBranchAddresses(int i)
{
BetaTree->SetBranchAddress("ScintEnergy",&(OutputQueue.at(i).ScintEnergy));
BetaTree->SetBranchAddress("MWPCEnergy",&(OutputQueue.at(i).MWPCEnergy));
BetaTree->SetBranchAddress("ExitAngle_Be",&(OutputQueue.at(i).ExitAngle_Be));
BetaTree->SetBranchAddress("P0e",&(OutputQueue.at(i).InitP[0]));
BetaTree->SetBranchAddress("Pos0",&(OutputQueue.at(i).DECPos[0]));
BetaTree->SetBranchAddress("VIon0",&(OutputQueue.at(i).V_Ion[0]));
BetaTree->SetBranchAddress("Hits",&(OutputQueue.at(i).Hits));
BetaTree->SetBranchAddress("ParticleID",&(OutputQueue.at(i).ParticleID));

BetaTree->SetBranchAddress("MWPCFiredCell",&(OutputQueue.at(i).MWPCFiredCell));
//for (int i=0;i<20;i++) dataSet.MWPCCellIndex[i] = (uint8_t )i; //just a check
BetaTree->SetBranchAddress("MWPCCellIndex",&(OutputQueue.at(i).MWPCCellIndex[0]));
BetaTree->SetBranchAddress("MWPCCellIonNumber",&(OutputQueue.at(i).MWPCCellIonNumber[0]));
BetaTree->SetBranchAddress("MWPCCellXpos",&(OutputQueue.at(i).MWPCCellXpos[0]));
BetaTree->SetBranchAddress("MWPCCellXvar",&(OutputQueue.at(i).MWPCCellXvar[0]));

BetaTree->SetBranchAddress("PosEn",&(OutputQueue.at(i).PosMWPCEntrance[0]));
BetaTree->SetBranchAddress("PosEx",&(OutputQueue.at(i).PosMWPCExit[0]));
BetaTree->SetBranchAddress("ScintTime",&(OutputQueue.at(i).ScintTime));
BetaTree->SetBranchAddress("MCPTime",&(OutputQueue.at(i).MCPTime));
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::Open_Output(string Data_Directory,G4int genID , G4int fileID)
{
  //Open Beta Event File
  char filename[100];
  sprintf(filename,"%s/BetaEventFile%03d%02d.dat",Data_Directory.c_str(),genID,fileID);

  if(BetaEventFile.is_open()){
    G4cout << "Beta output file "<<filename<<" is already opened!\n";
    return;
  }
  BetaEventFile.open(filename,std::ios::out | std::ios::binary);
  if(!BetaEventFile.is_open()){
    G4cout << "Error: Can't open Beta output file "<<filename<<"!\n" <<std::endl;
    exit(1);
  }
  G4cout << "Beta output file "<<filename<<" opened.\n";

  //Open Ion Info File
  sprintf(filename,"%s/IonInfoFile%03d%02d.dat",Data_Directory.c_str(),genID,fileID);

  if(IonInfoFile.is_open()){
    G4cout << "Ion output file "<<filename<<" is already opened!\n";
    return;
  }
  IonInfoFile.open(filename,std::ios::out | std::ios::binary);
  if(!IonInfoFile.is_open()){
    G4cout << "Error: Can't open Ion output file "<<filename<<"!\n" <<std::endl;
    exit(1);
  }
  G4cout << "Ion output file "<<filename<<" opened.\n";
  
  if(RootOutputRequested) 
  	Open_RootOutput(Data_Directory,genID,fileID);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::Open_RootOutput(string Data_Directory,G4int genID,G4int fileID)
{
  char filename[100];
  sprintf(filename,"%s/BetaEventFile%03d%02d.root",Data_Directory.c_str(),genID,fileID);
  
//  if(RootOutFile->IsOpen()){
//      G4cout << "Root output file "<<filename<<" is already opened!\n";
//    return;
//  }
  RootOutFile = new TFile(filename,"RECREATE");
	if(RootOutFile->IsZombie()){
	    G4cout << "Error: Can't open root output file "<<filename<<"!\n" <<std::endl;  
	    exit(1);
	}
	RootOutFile->cd(); //Write stuff here
	G4cout << "Root output file "<<filename<<" opened.\n";
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::Close_Output()
{
  if(BetaEventFile.is_open())
    BetaEventFile.close();
  if(IonInfoFile.is_open())
    IonInfoFile.close();
  if(RootOutFile!=NULL){
  	if (RootOutFile->IsOpen()) RootOutFile->Close();
  	delete RootOutFile;
  	RootOutFile=NULL;
  }
  G4cout << "Output files closed!\n";
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::BeginOfEventAction(const G4Event* event)
{  
  G4int eventNb = event->GetEventID();
  if (eventNb%fPrintModulo == 0) { 
    G4cout << "\n---> Begin of event: " << eventNb << G4endl;
  }
 
  // Reset accounted energy in stepping action
  BetaTracSteppingAction::Instance()->Reset();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::EndOfEventAction(const G4Event* event)
{
  SteppingInfoUnit SteppingInfo = BetaTracSteppingAction::Instance()->GetSteppingInfo();
  std::vector<MWPCScoreUnit> MWPCScore = BetaTracSteppingAction::Instance()->GetMWPCScore();

  // accumulate statistics for Scintillator
  G4double energy_Scint = SteppingInfo.EnergyScint;
  
  // accumulate statistics for MWPC
  G4double energy_MWPC = SteppingInfo.EnergyMWPC;
  
  //Push to histograms
  int index1 = 0;
  int index2 = 0;
  index1 = int(energy_Scint/keV);
  index2 = int(energy_MWPC/keV*10);
  if(index1!=0 && index2!=0){
    hist1->Fill(energy_Scint/keV);
    hist2->Fill(energy_MWPC/keV);
    RegScintCount++;
    if (SteppingInfo.MCP_Hit==1)HitMCPCount++;
    if (SteppingInfo.Wall_Hit==1)HitWallCount++;
    if (SteppingInfo.Electrode_Hit==1)HitElectrodeCount++;
    if (SteppingInfo.Collimator_Hit==1)HitCollimatorCount++;
    if (SteppingInfo.Window_Hit==1)HitWindowCount++;
    if (SteppingInfo.MWPC_Hit==1)HitMWPCCount++;
    if (SteppingInfo.Scint_Hit==1)HitScintCount++;
    if (SteppingInfo.MCP_Hit==1 || SteppingInfo.Wall_Hit==1 || SteppingInfo.Electrode_Hit==1 || SteppingInfo.Collimator_Hit==1)ScatterCount++;
  }
  if(index2!=0){
    RegMWPCCount++;
  }
  
  //Pushback to Output Queue
  
//  double broadenEnergyScint= energy_Scint/MeV+G4RandGauss::shoot()*0.09*sqrt(energy_Scint/MeV);
//  if (broadenEnergyScint<0)broadenEnergyScint=0;
  OutData dataSet;
  dataSet.Hits = 0;

  //If KeepAll==true, keep all events
  if ((energy_Scint/keV)>0.1 || (energy_MWPC/keV)>0.1 || KeepAll){	//output data with high bit density
//    dataSet.EventID = OutEventID;
    dataSet.ScintEnergy = (unsigned int)(energy_Scint/eV);
    dataSet.MWPCEnergy = (unsigned short)(energy_MWPC/eV);
//    G4cout <<"**********"<<dataSet.ScintEnergy<<G4endl;
    dataSet.PosMWPCEntrance[0] = short(floor(SteppingInfo.PosMWPCEntrance[0]/mm*1000.0));
    dataSet.PosMWPCEntrance[1] = short(floor(SteppingInfo.PosMWPCEntrance[1]/mm*1000.0));
    dataSet.PosMWPCEntrance[2] = short(floor(SteppingInfo.PosMWPCEntrance[2]/mm*100.0));
    //cout<<"X:Y:Z"<<dataSet.PosMWPCEntrance[0]<<"\t"<<dataSet.PosMWPCEntrance[1]<<"\t"<<dataSet.PosMWPCEntrance[2]<<"\n";
    dataSet.PosMWPCExit[0] = short(floor(SteppingInfo.PosMWPCExit[0]/mm*1000.0));
    dataSet.PosMWPCExit[1] = short(floor(SteppingInfo.PosMWPCExit[1]/mm*1000.0));
    dataSet.PosMWPCExit[2] = short(floor(SteppingInfo.PosMWPCExit[2]/mm*100.0));

    dataSet.ExitAngle_Be = (unsigned short)(floor(SteppingInfo.ExitAngle_Be/PI*180.0*100.0));
    dataSet.ParticleID = BetaTracPrimaryGeneratorAction::Instance()->Get_ParticleID();
    //The following three get methods are defined in the PrimaryGenerator
    //type conversions are done there
    BetaTracPrimaryGeneratorAction::Instance()->Get_Init_P(dataSet.InitP);
    BetaTracPrimaryGeneratorAction::Instance()->Get_DecayPos(dataSet.DECPos);
    BetaTracPrimaryGeneratorAction::Instance()->Get_IonInfo(dataSet.V_Ion);
    G4double ScintTime = BetaTracSteppingAction::Instance()->GetScintTime()/ns;
    G4double MCPTime = BetaTracSteppingAction::Instance()->GetMCPTime()/ns;
//    cout <<ScintTime<<" "<<MCPTime<<endl;
    if (ScintTime > 0 && MCPTime > ScintTime)dataSet.Hits |= 1<<8;
    if (SteppingInfo.BackScattering==1)dataSet.Hits |= 1<<7;
    if (SteppingInfo.MCP_Hit==1)dataSet.Hits |= 1<<6;
    if (SteppingInfo.Wall_Hit==1)dataSet.Hits |= 1<<5;
    if (SteppingInfo.Electrode_Hit==1)dataSet.Hits |= 1<<4;
    if (SteppingInfo.Collimator_Hit==1)dataSet.Hits |= 1<<3;
    if (SteppingInfo.Window_Hit==1)dataSet.Hits |= 1<<2;
    if (SteppingInfo.MWPC_Hit==1)dataSet.Hits |= 1<<1;
    if (SteppingInfo.Scint_Hit==1)dataSet.Hits |= 1;
    //Processing MWPC Scores
    dataSet.MWPCFiredCell = 0;
    int k=0;
    for (uint8_t i=0;i<20;i++){
      if (MWPCScore[i].IonNumber!=0){
				dataSet.MWPCFiredCell++;
				dataSet.MWPCCellIndex[k] = i;
				dataSet.MWPCCellIonNumber[k] = (unsigned short)(MWPCScore[i].IonNumber);
			//	cout << MWPCScore[i].IonNumber<<endl;
				dataSet.MWPCCellXpos[k] = short (MWPCScore[i].xpos/double(MWPCScore[i].IonNumber)*1000.0);	//Unit um
				dataSet.MWPCCellXvar[k] =  (unsigned short) (sqrt(MWPCScore[i].xpos2/double(MWPCScore[i].IonNumber)-pow(MWPCScore[i].xpos/double(MWPCScore[i].IonNumber),2.0))*1000.0);	//Unit um
				k++;
      }
    }
//    cout << int(dataSet.MWPCFiredCell)<<endl;
//    cout << dataSet.MWPCCellIonNumber[0]<<endl;
    dataSet.ScintTime=short(ScintTime*1000.0);
    dataSet.MCPTime=short(MCPTime*1000.0);

    OutputQueue.push_back(dataSet);
    OutEventID++;
    OutputCount++; //Class scope
  }
  //Output when the queue is larger than 1000000
  if (OutputCount > 1000){
    WriteToEventFile();
  }

}  

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracEventAction::Reset()
{
  //reset cumulative quantities
  //
  hist1->Reset();
  hist2->Reset();
  OutEventID = 0;
  RegScintCount = 0;
  RegMWPCCount = 0;
  HitScintCount = 0;
  HitMWPCCount = 0;
  HitMCPCount = 0;
  HitWallCount = 0;
  HitElectrodeCount = 0;
  HitWindowCount = 0;
  HitCollimatorCount = 0;
  ScatterCount = 0;
}

void BetaTracEventAction::OutputHistograms()
{
/*  TFile* HistOut = new TFile("BetaTestHist.root","recreate");
  hist1->Write();
  hist2->Write();
  HistOut->Close();
  delete HistOut;*/
  ;
}

void BetaTracEventAction::WriteToEventFile()
{
  if(OutputCount==0)return;
  for(int i=0;i<OutputQueue.size();i++){
//    BetaEventFile.write((char *)&(OutputQueue.at(i).EventID),sizeof(G4int));
    BetaEventFile.write((char *)&(OutputQueue.at(i).ScintEnergy),sizeof(unsigned int));
    BetaEventFile.write((char *)&(OutputQueue.at(i).MWPCEnergy),sizeof(unsigned short));
//    BetaEventFile.write((char *)OutputQueue.at(i).PosMWPCEntrance,3*sizeof(short));
//    BetaEventFile.write((char *)OutputQueue.at(i).PosMWPCExit,3*sizeof(short));
    //Detailed output of MWPC
    BetaEventFile.write((char *)&(OutputQueue.at(i).MWPCFiredCell),sizeof(uint8_t));
    int k = OutputQueue.at(i).MWPCFiredCell;
    if (k>0){
      BetaEventFile.write((char *)OutputQueue.at(i).MWPCCellIndex,k*sizeof(uint8_t));
      BetaEventFile.write((char *)OutputQueue.at(i).MWPCCellIonNumber,k*sizeof(unsigned short));
      BetaEventFile.write((char *)OutputQueue.at(i).MWPCCellXpos,k*sizeof(short));
      BetaEventFile.write((char *)OutputQueue.at(i).MWPCCellXvar,k*sizeof(unsigned short));
    }

    BetaEventFile.write((char *)&(OutputQueue.at(i).ExitAngle_Be),sizeof(unsigned short));
    BetaEventFile.write((char *)OutputQueue.at(i).InitP,3*sizeof(int));
    BetaEventFile.write((char *)OutputQueue.at(i).DECPos,3*sizeof(short));
    BetaEventFile.write((char *)&(OutputQueue.at(i).Hits),sizeof(uint16_t));
    BetaEventFile.write((char *)&(OutputQueue.at(i).ParticleID),sizeof(uint8_t));
    BetaEventFile.write((char *)(&(OutputQueue.at(i).ScintTime)),sizeof(short));
    BetaEventFile.write((char *)(&(OutputQueue.at(i).MCPTime)),sizeof(short));
    //Output for debug
/*    cout << OutputQueue.at(i).ScintEnergy << " "
      <<OutputQueue.at(i).MWPCEnergy<< " "
      <<int(OutputQueue.at(i).MWPCFiredCell)<< " "
      <<endl;*/
/*    IonInfoFile//<<OutputQueue.at(i).EventID<<" "
      <<OutputQueue.at(i).DECPos[0]<<" " <<OutputQueue.at(i).DECPos[1]<<" "
      <<OutputQueue.at(i).DECPos[2]<<" "
      <<OutputQueue.at(i).V_Ion[0]<<" " <<OutputQueue.at(i).V_Ion[1]<<" "
      <<OutputQueue.at(i).V_Ion[2]<<"\n";*/

    IonInfoFile.write((char *)OutputQueue.at(i).DECPos,3*sizeof(short));
    IonInfoFile.write((char *)(&(OutputQueue.at(i).V_Ion[0])),sizeof(int));
    IonInfoFile.write((char *)(&(OutputQueue.at(i).V_Ion[1])),sizeof(int));
    IonInfoFile.write((char *)(&(OutputQueue.at(i).V_Ion[2])),sizeof(int));
  }
  if(RootOutputRequested){
  	for(int i=0;i<OutputQueue.size();i++){
  		SetBranchAddresses(i);
  		BetaTree->Fill();
  	}
  	BetaTree->Write("", TObject::kOverwrite); // save only the new version of the tree
  }
  OutputQueue.erase(OutputQueue.begin(),OutputQueue.end()); //erases all events in OutputQueue
  OutputCount=0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
