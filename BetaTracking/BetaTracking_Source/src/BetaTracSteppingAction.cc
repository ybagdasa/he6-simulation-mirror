//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracSteppingAction.cc
/// \brief Implementation of the BetaTracSteppingAction class

#include "BetaTracSteppingAction.hh"
#include "BetaTracEventAction.hh"

#include "BetaTracDetectorConstruction.hh"

#include "G4Step.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"
#include "GlobalConstants.h"

#include <math.h>

using namespace std;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
extern double Z_Cathode_Low;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracSteppingAction* BetaTracSteppingAction::fgInstance = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracSteppingAction* BetaTracSteppingAction::Instance()
{
// Static acces function via G4RunManager 

  return fgInstance;
}      

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracSteppingAction::BetaTracSteppingAction()
: G4UserSteppingAction()
{
  SteppingInfo.EnergyScint = 0.;
  SteppingInfo.EnergyMWPC = 0.;
  SteppingInfo.EnergyGasFill = 0.;
  SteppingInfo.EnergyBeWindow = 0.;
  SteppingInfo.EnergyColimmator = 0.;
         
  SteppingInfo.PosMWPCEntrance = G4ThreeVector(0.,0.,0.);
  SteppingInfo.PosMWPCExit = G4ThreeVector(0.,0.,0.);

  SteppingInfo.ExitAngle_Be = 0.0;

  SteppingInfo.MCP_Hit = 0;
  SteppingInfo.Electrode_Hit = 0;
  SteppingInfo.Collimator_Hit = 0;
  SteppingInfo.Wall_Hit = 0;
  SteppingInfo.Window_Hit = 0;
  SteppingInfo.MWPC_Hit = 0;
  SteppingInfo.Scint_Hit = 0;
  SteppingInfo.BackScattering = 0;

//  Z_Cathode_Low = (2.769+0.25+0.062)*2.54*cm;
//  Z_Cathode_Low = (2.769+0.125)*2.54*cm;
  Entered = 0;
  Exited = 0; 
  for (int i=0;i<20;i++){
    MWPCScoreUnit theScoreUnit;
    theScoreUnit.xpos=0.0;
    theScoreUnit.xpos2=0.0;
    theScoreUnit.IonNumber=0;
    MWPCScoreArray.push_back(theScoreUnit);
  }

  ScintTime = -1.0;
  MCPTime = -1.0;

  fgInstance = this;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracSteppingAction::~BetaTracSteppingAction()
{ 
  fgInstance = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracSteppingAction::UserSteppingAction(const G4Step* step)
{
  G4double GasIonizationEnergy = BetaTracDetectorConstruction::Instance()->GetMeanIonizationEnergy();
    // Get Pre and Post step points
  G4StepPoint* PreStep_Point = step->GetPreStepPoint();
  G4StepPoint* PostStep_Point= step->GetPostStepPoint();
  // get name of the volume of the current step
  //  G4LogicalVolume* volume= PreStep_Point->GetTouchableHandle()->GetVolume()->GetLogicalVolume();
  G4String volumeName = PreStep_Point->GetPhysicalVolume()->GetName();

  // get the pointer to the current track
  G4Track* CurrentTrack = step->GetTrack();
  G4String PostVolumeName;
  G4double X,Y,Z,T;	//Aux parameters to save positions, since the return value of the get_position is reference
  X=Y=Z=T=0.0;
  //Center between pre and post step point
//  X = (PostStep_Point->GetPosition()[0] + PreStep_Point->GetPosition()[0])/2.0;
//  Y = (PostStep_Point->GetPosition()[1] + PreStep_Point->GetPosition()[1])/2.0;
//  Z = (PostStep_Point->GetPosition()[2] + PreStep_Point->GetPosition()[2])/2.0;
  //Random position in between pre and post step point
  X = PreStep_Point->GetPosition()[0] + (PostStep_Point->GetPosition()[0] - PreStep_Point->GetPosition()[0])*G4UniformRand();
  Y = PreStep_Point->GetPosition()[1] + (PostStep_Point->GetPosition()[1] - PreStep_Point->GetPosition()[1])*G4UniformRand();
  Z = PreStep_Point->GetPosition()[2] + (PostStep_Point->GetPosition()[2] - PreStep_Point->GetPosition()[2])*G4UniformRand();
  T = PreStep_Point->GetGlobalTime() + (PostStep_Point->GetGlobalTime() - PreStep_Point->GetGlobalTime())*G4UniformRand();


  if (PostStep_Point->GetTouchableHandle()->GetVolume()!=NULL){
    PostVolumeName = PostStep_Point->GetPhysicalVolume()->GetName();
    if (volumeName.compare("GasFill")==0 && PostVolumeName.compare("MWPCGas")==0 && CurrentTrack->GetCreatorProcess()==0){
      //	cout<<"Enter\t"<<PostStep_Point->GetPosition()[0]/cm<<"\t"<<PostStep_Point->GetPosition()[1]<<endl;
      if(Entered == 0){
				SteppingInfo.PosMWPCEntrance = G4ThreeVector(X,Y,Z);
				//cout<<"X:Y:Z = "<<X<<"\t"<<Y<<"\t"<<Z<<"\n";
				Entered = 1;
      }
    }
    if (volumeName.compare("MWPCGas")==0 && PostVolumeName.compare("GasFill")==0 && CurrentTrack->GetCreatorProcess()==0){
      //	cout<<"Exit\t"<<PostStep_Point->GetPosition()[0]/cm<<"\t"<<PostStep_Point->GetPosition()[1]<<endl;
      if(Exited == 0){
				SteppingInfo.PosMWPCExit = G4ThreeVector(X,Y,Z);
				Exited = 1;
      }
    }

    //check backscatterring from the scintillator
    if (volumeName.compare("Scintillator")==0 && PostVolumeName.compare("World")==0 && CurrentTrack->GetCreatorProcess()==0){
      SteppingInfo.BackScattering = 1;
    }
    if (volumeName.compare("World")==0 && PostVolumeName.compare("Scintillator")==0 && CurrentTrack->GetCreatorProcess()==0){
//      ScintTime = PostStep_Point->GetGlobalTime();
      ScintTime=T;
//      cout <<T<<endl;
    }

    //Determine the exit angle when leaving the be window
    if (volumeName.compare("BeWindow")==0 && PostVolumeName.compare("GasFill")==0 && CurrentTrack->GetCreatorProcess()==0){
      SteppingInfo.ExitAngle_Be = acos(CurrentTrack->GetMomentumDirection ()[2]);
    }
    //Label primary particles if it hits something else
    if (volumeName.compare("BeWindow")==0 && CurrentTrack->GetCreatorProcess()==0){
      SteppingInfo.Window_Hit = 1;
    }
    if (volumeName.compare("MCP")==0 && CurrentTrack->GetCreatorProcess()==0){
      SteppingInfo.MCP_Hit = 1;
    }
    if (volumeName.compare("World")==0 && PostVolumeName.compare("MCP")==0 && CurrentTrack->GetCreatorProcess()==0){
      //MCPTime = PostStep_Point->GetGlobalTime();
      MCPTime=T;
    }
    if (volumeName.compare("MCP")==0 && PostVolumeName.compare("World")==0 && CurrentTrack->GetCreatorProcess()==0){
      //MCPTime = PostStep_Point->GetGlobalTime();
      MCPTime=T;
    }
    if ((volumeName.compare("BetaChamber")==0 || volumeName.compare("MOTChamber")==0) && CurrentTrack->GetCreatorProcess()==0){
      SteppingInfo.Wall_Hit = 1;
    }
    if ((volumeName.compare("Electrode1")==0 || volumeName.compare("Electrode2")==0 || volumeName.compare("Electrode3")==0 || volumeName.compare("Electrode4")==0 || volumeName.compare("Electrode5")==0 || volumeName.compare("Grid")==0) && CurrentTrack->GetCreatorProcess()==0){
      SteppingInfo.Electrode_Hit = 1;
    }
    if (volumeName.compare("Collimator")==0 && CurrentTrack->GetCreatorProcess()==0){
      SteppingInfo.Collimator_Hit = 1;
    }
  }

  // check if we are in scoring volumes
  if (volumeName.compare("Scintillator")==0 ){
    G4double edep = step->GetTotalEnergyDeposit();
    SteppingInfo.EnergyScint += edep;
    // Lable Scint_Hit if primary
    if (CurrentTrack->GetCreatorProcess()==0)
      SteppingInfo.Scint_Hit = 1;
  }else if (volumeName.compare("MWPCGas")==0){
    //Collect only ionization energy deposition
    G4double edep = step->GetTotalEnergyDeposit() - step->GetNonIonizingEnergyDeposit();
    SteppingInfo.EnergyMWPC += edep;
    // Label MWPC_Hit if primary
    if (CurrentTrack->GetCreatorProcess()==0)
      SteppingInfo.MWPC_Hit = 1;
    // Detail recording ionizations and hit positions
    //First, rotate the X,Y to local coordinate system
    //double MWPCRot = 30.0/180.0*PI;
    double MWPCRot = -21.0/180.0*PI;
    double Xp = X*cos(MWPCRot)-Y*sin(MWPCRot);
    double Yp = X*sin(MWPCRot)+Y*cos(MWPCRot);
    short Index = short((Yp/mm+20.0)/2.0);
    //Handle the boundary round about
    if (Index<0)Index=0;
    if (Index>19)Index=19;
    double NumIon = edep/GasIonizationEnergy;
//    if (Yp<-2*cm)cout  <<Xp<<" "<<Yp<<" "<<PostVolumeName<<" "<<Index<<endl;
//    std::cout << "edep= "<<edep<<" non ion "<<step->GetNonIonizingEnergyDeposit()<<endl;
//    std::cout << "EIonization= "<<GasIonizationEnergy<<endl;
//    std::cout << "Ion number= "<<NumIon<<endl;
    MWPCScoreArray[Index].IonNumber += NumIon;
    if (Z>=Z_Cathode_Low+0.3*mm){
      MWPCScoreArray[Index].xpos += Xp/mm*NumIon;
      MWPCScoreArray[Index].xpos2 += pow(Xp/mm,2.0)*NumIon;
    }else{
      MWPCScoreArray[Index].xpos += (int((Xp/mm+21.0)/2.0)*2.0-20)*NumIon;
      MWPCScoreArray[Index].xpos2 += pow((int((Xp/mm+21.0)/2.0)*2.0-20),2.0)*NumIon;
    }

  }else{
    return;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracSteppingAction::Reset()
{
  SteppingInfo.EnergyScint = 0.;
  SteppingInfo.EnergyMWPC = 0.;
  SteppingInfo.EnergyGasFill = 0.;
  SteppingInfo.EnergyBeWindow = 0.;
  SteppingInfo.EnergyColimmator = 0.;
         
  SteppingInfo.PosMWPCEntrance = G4ThreeVector(0.,0.,0.);
  SteppingInfo.PosMWPCExit = G4ThreeVector(0.,0.,0.);
 
  SteppingInfo.ExitAngle_Be = 0.0;

  SteppingInfo.MCP_Hit = 0;
  SteppingInfo.Electrode_Hit = 0;
  SteppingInfo.Collimator_Hit = 0;
  SteppingInfo.Wall_Hit = 0;
  SteppingInfo.Window_Hit = 0;
  SteppingInfo.MWPC_Hit = 0;
  SteppingInfo.Scint_Hit = 0;
  SteppingInfo.BackScattering = 0;

  for (int i=0;i<20;i++){
    MWPCScoreArray[i].xpos=0.0;
    MWPCScoreArray[i].xpos2=0.0;
    MWPCScoreArray[i].IonNumber=0;
  }

  Entered = 0;
  Exited = 0;

  ScintTime = -1.0;
  MCPTime = -1.0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

