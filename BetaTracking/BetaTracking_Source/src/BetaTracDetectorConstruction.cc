//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracDetectorConstruction.cc
/// \brief Implementation of the BetaTracDetectorConstruction class

#include "BetaTracDetectorConstruction.hh"
#include "BetaTracSteppingAction.hh"
#include "GlobalConstants.h"
// use of stepping action to set the accounting volume

#include "G4PhysicalConstants.hh"
#include "G4RunManager.hh"
#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Tubs.hh"
#include "G4UnionSolid.hh"
#include "G4SubtractionSolid.hh"
//#include "G4Cons.hh"
//#include "G4Orb.hh"
//#include "G4Sphere.hh"
//#include "G4Trd.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4Region.hh"
#include "G4ProductionCuts.hh"

#include "G4UserLimits.hh"
#include <iostream>

using namespace std;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
//Global variable
double Z_Cathode_Low=0;
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracDetectorConstruction::BetaTracDetectorConstruction()
: GasMaterial(0), logicMWPCGas(0), physiMWPCGas(0), logicScint(0), physiScint(0),
  logicMOTChamber(0), physiMOTChamber(0), logicCol(0), physiCol(0), logicMCP(0),
  physiMCP(0), logicWorld(0), physiWorld(0),fStepLimit_Be(NULL),fStepLimit_MWPC(NULL),fStepLimit_Col(NULL),
  fStepLimit_Ele(NULL),fStepLimit_Grid(NULL),fStepLimit_MCP(NULL), G4VUserDetectorConstruction() 
{ 
  fgInstance = this;
} 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracDetectorConstruction::~BetaTracDetectorConstruction()
{
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracDetectorConstruction* BetaTracDetectorConstruction::fgInstance = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracDetectorConstruction * BetaTracDetectorConstruction::Instance()
{
  // Static acces function via G4RunManager 

  return fgInstance;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

G4VPhysicalVolume* BetaTracDetectorConstruction::Construct()
{
  G4double a,z;
  G4double density, temperature, pressure;
  G4String name;

  // Get nist material manager
  G4NistManager* nist = G4NistManager::Instance();

  // World parameters
  //
  G4double world_sizeXY = 80*cm, world_sizeZ = 80*cm;

  density = universe_mean_density;
  pressure = 1.e-19*pascal;
  temperature = 0.1*kelvin;
  G4Material* Vacuum = new G4Material(name="Vacuum", z=1., a=1.01*g/mole, density,
      kStateGas,temperature,pressure);

  // Option to switch on/off checking of volumes overlaps
  //
  G4bool checkOverlaps = true;

  //     
  // World
  //

  G4Box* solidWorld =    
    new G4Box("World",                       //its name
	0.5*world_sizeXY, 0.5*world_sizeXY, 0.5*world_sizeZ);     //its size

  logicWorld = new G4LogicalVolume(solidWorld,          //its solid
      Vacuum,              //it's vacuum
      "World");            //its name

  physiWorld = new G4PVPlacement(0,           //no rotation
      G4ThreeVector(),       //at (0,0,0)
      logicWorld,            //its logical volume
      "World",               //its name
      0,                     //its mother  volume
      false,                 //no boolean operation
      0,                     //copy number
      checkOverlaps);        //overlaps checking


  //
  // Electrode system
  //

  //Glassy Carbon material
  G4Material* GlassyCarbon= nist->FindOrBuildMaterial("G4_C");
  G4Material* StainlessSteel= nist->FindOrBuildMaterial("G4_STAINLESS-STEEL");

  //Define MCP surface position first
  G4double MCP_Surf_Pos = -91.0*mm;

  //
  // MCP
  //

  G4double MCP_R_in = 0.0;
//  G4double MCP_R_out = 87.0/2*mm;
  G4double MCP_R_out = 80.0/2*mm; //Exposed area
  G4double MCP_hz = 1.5/2*mm;

  G4ThreeVector MCPPos = G4ThreeVector(0,0,MCP_Surf_Pos-MCP_hz);

  //Material Glass  
  G4Material* Glass = nist->FindOrBuildMaterial("G4_SILICON_DIOXIDE");

  G4Tubs* solidMCP
    = new G4Tubs("MCP",		//It's name
	MCP_R_in,			//inner radius
	MCP_R_out,			//outer radius
	MCP_hz,				//hight
	0.*deg,360.*deg);			//start and spanning angle

  logicMCP
    = new G4LogicalVolume(solidMCP,Glass,"MCP");

  physiMCP
    = new G4PVPlacement(0,  	           //no rotation
	MCPPos,	           //Position
	logicMCP,       //its logical volume
	"MCP",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  //Set max step limit
//  fStepLimit_MCP = new G4UserLimits(MCP_hz*0.5);
//  logicMCP->SetUserLimits(fStepLimit_MCP); 


  //
  // Grid (MCP holder)
  //

  G4double Grid_R_in = 80.0/2*mm;
  G4double Grid_R_out = 170.0/2*mm;
  G4double Grid_hz = 1.9622/2*mm;
//  G4double Grid_Surf_Pos = MCP_Surf_Pos+Grid_hz*2.0;
//  G4double Grid_Surf_Pos = MCP_Surf_Pos+0.9122*mm;
  G4double Grid_Surf_Pos = MCP_Surf_Pos+1.0*mm;

//  G4ThreeVector GridPos = G4ThreeVector(0,0,Colli_z_abs-6.0*26.0*mm);
  G4ThreeVector GridPos = G4ThreeVector(0,0,Grid_Surf_Pos-Grid_hz);

  G4Tubs* solidGrid
    = new G4Tubs("Grid",		//It's name
	Grid_R_in,			//inner radius
	Grid_R_out,			//outer radius
	Grid_hz,				//hight
	0.*deg,360.*deg);			//start and spanning angle

  logicGrid
      = new G4LogicalVolume(solidGrid,StainlessSteel,"Grid");
    //= new G4LogicalVolume(solidGrid,GlassyCarbon,"Grid");

  physiGrid
    = new G4PVPlacement(0,  	           //no rotation
	GridPos,	           //Position
	logicGrid,           //its logical volume
	"Grid",              //its name
	logicWorld,          //its mother  volume
	false,               //no boolean operation
	0,                   //copy number
	checkOverlaps);      //overlaps checking


  //Set max step limit
//  fStepLimit_Grid = new G4UserLimits(Grid_hz*0.5);
//  logicGrid->SetUserLimits(fStepLimit_Grid); 

  //
  // Electrodes
  //

  G4double Elect_R_in = 80.0/2*mm;
  G4double Elect_R_out = 170.0/2*mm;
  G4double Elect_hz = 2.0/2*mm;

  G4double Elect1_hz = 1.8383/2*mm;
  G4double Elect2_hz = 1.8352/2*mm;
  G4double Elect3_hz = 1.8574/2*mm;
  G4double Elect4_hz = 1.8479/2*mm;
  G4double Elect5_hz = 1.8542/2*mm;
  G4double Elect6_hz = 1.8606/2*mm;

  G4double E1_Surf_Pos = Grid_Surf_Pos + 23.9776 + 2.0*Elect1_hz;
  G4double E2_Surf_Pos = E1_Surf_Pos + 23.9967 + 2.0*Elect2_hz;
  G4double E3_Surf_Pos = E2_Surf_Pos + 23.9522  + 2.0*Elect3_hz;
  G4double E4_Surf_Pos = E3_Surf_Pos + 23.9967 + 2.0*Elect4_hz;
  G4double E5_Surf_Pos = E4_Surf_Pos + 23.9522 + 2.0*Elect5_hz;
  G4double E6_Surf_Pos = E5_Surf_Pos + 23.8633 + 2.0*Elect6_hz;

  G4ThreeVector Elect_Pos1 = G4ThreeVector(0,0,E1_Surf_Pos-Elect1_hz);
  G4ThreeVector Elect_Pos2 = G4ThreeVector(0,0,E2_Surf_Pos-Elect2_hz);
  G4ThreeVector Elect_Pos3 = G4ThreeVector(0,0,E3_Surf_Pos-Elect3_hz);
  G4ThreeVector Elect_Pos4 = G4ThreeVector(0,0,E4_Surf_Pos-Elect4_hz);
  G4ThreeVector Elect_Pos5 = G4ThreeVector(0,0,E5_Surf_Pos-Elect5_hz);
  G4ThreeVector Elect_Pos6 = G4ThreeVector(0,0,E6_Surf_Pos-Elect6_hz);

  G4Tubs* solidElectrode1
    = new G4Tubs("Electrodes",		//It's name
	Elect_R_in,		//inner radius
	Elect_R_out,		//outer radius
	Elect1_hz,		//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidElectrode2
    = new G4Tubs("Electrodes",		//It's name
	Elect_R_in,		//inner radius
	Elect_R_out,		//outer radius
	Elect2_hz,		//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidElectrode3
    = new G4Tubs("Electrodes",		//It's name
	Elect_R_in,		//inner radius
	Elect_R_out,		//outer radius
	Elect3_hz,		//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidElectrode4
    = new G4Tubs("Electrodes",		//It's name
	Elect_R_in,		//inner radius
	Elect_R_out,		//outer radius
	Elect4_hz,		//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidElectrode5
    = new G4Tubs("Electrodes",		//It's name
	Elect_R_in,		//inner radius
	Elect_R_out,		//outer radius
	Elect5_hz,		//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidElectrodeRoundCut
    = new G4Tubs("ElectrodeRoundCut",		//It's name
	0.0,		//inner radius
	12.5*mm,		//outer radius
	Elect_hz,		//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Box* solidElectrodeRecCut
    = new G4Box("ElectrodeRecCut",		//It's name
	44.572*mm,			//X-dim
	12.5*mm,			//Y-dim
	Elect_hz);			//hight

  G4SubtractionSolid* solidElectrodeCut2_1 //Create subtraction of Cut
    = new G4SubtractionSolid("solidElectrodeCut2_1",solidElectrode2,solidElectrodeRoundCut,0,G4ThreeVector(-44.572*mm,0.0,0.0));

  G4SubtractionSolid* solidElectrodeCut2_2 //Create subtraction of Cut
    = new G4SubtractionSolid("solidElectrodeCut2_2",solidElectrodeCut2_1,solidElectrodeRoundCut,0,G4ThreeVector(44.572*mm,0.0,0.0));

  G4SubtractionSolid* solidElectrodeCut2 //Create subtraction of Cut
    = new G4SubtractionSolid("solidElectrodeCut2",solidElectrodeCut2_2,solidElectrodeRecCut,0,G4ThreeVector(0.0,0.0,0.0));

  G4SubtractionSolid* solidElectrodeCut5_1 //Create subtraction of Cut
    = new G4SubtractionSolid("solidElectrodeCut5_1",solidElectrode5,solidElectrodeRoundCut,0,G4ThreeVector(-44.572*mm,0.0,0.0));

  G4SubtractionSolid* solidElectrodeCut5_2 //Create subtraction of Cut
    = new G4SubtractionSolid("solidElectrodeCut5_2",solidElectrodeCut5_1,solidElectrodeRoundCut,0,G4ThreeVector(44.572*mm,0.0,0.0));

  G4SubtractionSolid* solidElectrodeCut5 //Create subtraction of Cut
    = new G4SubtractionSolid("solidElectrodeCut5",solidElectrodeCut5_2,solidElectrodeRecCut,0,G4ThreeVector(0.0,0.0,0.0));

  logicElectrode1
    = new G4LogicalVolume(solidElectrode1,StainlessSteel,"Electrodes1");
  //  = new G4LogicalVolume(solidElectrode,GlassyCarbon,"Electrodes");

  logicElectrode3
    = new G4LogicalVolume(solidElectrode3,StainlessSteel,"Electrodes3");
  //  = new G4LogicalVolume(solidElectrode,GlassyCarbon,"Electrodes");

  logicElectrode4
    = new G4LogicalVolume(solidElectrode4,StainlessSteel,"Electrodes4");
  //  = new G4LogicalVolume(solidElectrode,GlassyCarbon,"Electrodes");

  logicElectrodeCut2
    = new G4LogicalVolume(solidElectrodeCut2,StainlessSteel,"Electrodes_Cut2");
  //  = new G4LogicalVolume(solidElectrode,GlassyCarbon,"Electrodes");

  logicElectrodeCut5
    = new G4LogicalVolume(solidElectrodeCut5,StainlessSteel,"Electrodes_Cut5");
  //  = new G4LogicalVolume(solidElectrode,GlassyCarbon,"Electrodes");

  physiElectrode1
    = new G4PVPlacement(0,  	           //no rotation
	Elect_Pos1,          //Position
	logicElectrode1,       //its logical volume
	"Electrode1",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiElectrode2
    = new G4PVPlacement(0,  	           //no rotation
	Elect_Pos2,          //Position
	logicElectrodeCut2,       //its logical volume
	"Electrode2",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiElectrode3
    = new G4PVPlacement(0,  	           //no rotation
	Elect_Pos3,          //Position
	logicElectrode3,       //its logical volume
	"Electrode3",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiElectrode4
    = new G4PVPlacement(0,  	           //no rotation
	Elect_Pos4,          //Position
	logicElectrode4,       //its logical volume
	"Electrode4",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiElectrode5
    = new G4PVPlacement(0,  	           //no rotation
	Elect_Pos5,          //Position
	logicElectrodeCut5,       //its logical volume
	"Electrode5",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  //Set max step limit
//  fStepLimit_Ele = new G4UserLimits(Elect_hz*0.5);
//  logicElectrode->SetUserLimits(fStepLimit_Ele); 

  //
  // Collimator
  //

  G4double Colli_R_in = 26.0/2*mm;
  G4double Colli_R_out = 170.0/2*mm;


  G4Tubs* solidCollimator
    = new G4Tubs("Collimator",		//It's name
	Colli_R_in,			//inner radius
	Colli_R_out,			//outer radius
	Elect6_hz,				//hight
	0.*deg,360.*deg);			//start and spanning angle

  logicCol
    = new G4LogicalVolume(solidCollimator,StainlessSteel,"Collimator");
  //  = new G4LogicalVolume(solidCollimator,GlassyCarbon,"Collimator");

  physiCol
    = new G4PVPlacement(0,  	           //no rotation
	Elect_Pos6,	           //Position
	logicCol,       //its logical volume
	"Collimator",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  //Set max step limit
//  fStepLimit_Col = new G4UserLimits(Colli_hz*0.5);
//  logicCol->SetUserLimits(fStepLimit_Col); 

  //
  // Beryllium Window
  //

  G4Material* Be= nist->FindOrBuildMaterial("G4_Be");

  // Beryllium Window geometrical parameters
  G4double BeWindow_R_in = 0.*cm;
  G4double BeWindow_R_out = 1.5*2.54/2*cm;
  G4double BeWindow_hz = 0.005*2.54/2*cm;
//  G4double BeWindowBottomPos = E6_Surf_Pos+20.7945*mm-0.5*25.4*mm;
  G4double BeWindowBottomPos = E6_Surf_Pos+20.7945*mm;

  G4ThreeVector BeWindowPos = G4ThreeVector(0, 0, BeWindowBottomPos+BeWindow_hz);

  G4Tubs* solidBeWindow
    = new G4Tubs("BeWindow",		//It's name
	BeWindow_R_in,			//inner radius
	BeWindow_R_out,			//outer radius
	BeWindow_hz,				//hight
	0.*deg,360.*deg);			//start and spanning angle

  logicBeWindow = new G4LogicalVolume(solidBeWindow,Be,"BeWindow");

  physiBeWindow = new G4PVPlacement(0,		//no rotation
      BeWindowPos,					//position of the center
      logicBeWindow,				//its logical volume
      "BeWindow",				//its name
      logicWorld,				//its mother
      false,					//no boolean operations
      0,					//copy number
      checkOverlaps);				//checking overlaps

  //Set max step limit
//  fStepLimit_Be = new G4UserLimits(BeWindow_hz*0.5);
//  logicBeWindow->SetUserLimits(fStepLimit_Be); 

  //
  // Beta detector Chamber
  //

  G4double BetaWall_R_in = 3.60*2.54/2*cm;
  G4double BetaWall_R_out = 3.79*2.54/2*cm;
  G4double BetaWall_hz = 1.225*2.54/2*cm;

  G4double BetaTop_R_in = BetaWall_R_in;
  G4double BetaTop_R_out = 6*2.54/2*cm;
  G4double BetaTop_hz = 0.125*2.54/2*cm;

  G4double BetaBottom_R_in = 1.5*2.54/2*cm;
  G4double BetaBottom_R_out = BetaWall_R_out;
  G4double BetaBottom_hz = 0.125*2.54/2*cm;

//  G4double BetaBottomPos = E6_Surf_Pos+18.8895*mm-0.5*25.4*mm; 
  G4double BetaBottomPos = E6_Surf_Pos+18.8895*mm; 

  G4ThreeVector BetachmbPos = G4ThreeVector(0, 0, BetaBottomPos+2*BetaBottom_hz+BetaWall_hz);

  G4Tubs* solidBetaWall
    = new G4Tubs("BetaWall",		//It's name
	BetaWall_R_in,			//inner radius
	BetaWall_R_out,			//outer radius
	BetaWall_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4Tubs* solidBetaTop
    = new G4Tubs("BetaTop",		//It's name
	BetaTop_R_in,			//inner radius
	BetaTop_R_out,			//outer radius
	BetaTop_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4Tubs* solidBetaBottom
    = new G4Tubs("BetaBottom",		//It's name
	BetaBottom_R_in,			//inner radius
	BetaBottom_R_out,			//outer radius
	BetaBottom_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4UnionSolid* UnionBetaWallTop 		//Create union wall + top
    = new G4UnionSolid("Wall+Top",solidBetaWall,solidBetaTop,0,G4ThreeVector(0,0,BetaWall_hz+BetaTop_hz));

  G4UnionSolid* solidBetaChamber  	//Create the whole Beta Chamber
    = new G4UnionSolid("BetaChamber",UnionBetaWallTop,solidBetaBottom,0,G4ThreeVector(0,0,-BetaWall_hz-BetaBottom_hz));

  logicBetaChmb
    = new G4LogicalVolume(solidBetaChamber,StainlessSteel,"BetaChamber");

  physiBetaChmb
    = new G4PVPlacement(0,  	           //no rotation
	BetachmbPos,	           //Position
	logicBetaChmb,       //its logical volume
	"BetaChamber",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  //
  // MOT Chamber
  //

  G4double MOTWall_R_in = 8.0*2.54/2*cm;
  G4double MOTWall_R_out = 8.5*2.54/2*cm;
  G4double MOTWall_hz = (3.869)*2.54*cm+1.0*cm;

  G4double MOTTop_R_in = 3.80*2.54/2*cm;
  G4double MOTTop_R_out = MOTWall_R_out;
  G4double MOTTop_hz = 0.635/2*cm;

  G4double MOTBottom_R_in = 0.*cm;
  G4double MOTBottom_R_out = MOTWall_R_out;
  G4double MOTBottom_hz = 0.635/2*cm;

  G4double MOTChmbMiddle = BetaBottomPos+2*BetaBottom_hz+2*BetaWall_hz-2*MOTTop_hz-MOTWall_hz;

  G4ThreeVector MOTchmbPos = G4ThreeVector(0, 0, MOTChmbMiddle);

  G4Tubs* solidMOTWall
    = new G4Tubs("MOTWall",		//It's name
	MOTWall_R_in,			//inner radius
	MOTWall_R_out,			//outer radius
	MOTWall_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4Tubs* solidMOTTop
    = new G4Tubs("MOTTop",		//It's name
	MOTTop_R_in,			//inner radius
	MOTTop_R_out,			//outer radius
	MOTTop_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4Tubs* solidMOTBottom
    = new G4Tubs("MOTBottom",		//It's name
	MOTBottom_R_in,			//inner radius
	MOTBottom_R_out,			//outer radius
	MOTBottom_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4UnionSolid* UnionWallTop 		//Create union wall + top
    = new G4UnionSolid("Wall+Top",solidMOTWall,solidMOTTop,0,G4ThreeVector(0,0,MOTWall_hz+MOTTop_hz));

  G4UnionSolid* solidMOTChamber  	//Create the whole MOT Chamber
    = new G4UnionSolid("MOTChamber",UnionWallTop,solidMOTBottom,0,G4ThreeVector(0,0,-MOTWall_hz-MOTBottom_hz));

  logicMOTChamber
    = new G4LogicalVolume(solidMOTChamber,StainlessSteel,"MOTChamber");

  physiMOTChamber
    = new G4PVPlacement(0,  	           //no rotation
	MOTchmbPos,	           //Position
	logicMOTChamber,       //its logical volume
	"MOTChamber",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  //
  // Scintillator Chamber
  //

  G4double ScintChmbWall_R_in = BetaTop_R_out;
  G4double ScintChmbWall_R_out = BetaTop_R_out+0.125*2.54*cm;
  G4double ScintChmbWall_hz = 6*2.54/2*cm;

  G4double ScintChmbTop_R_in = 5*2.54/2*cm;
  G4double ScintChmbTop_R_out = ScintChmbWall_R_out;
  G4double ScintChmbTop_hz = 0.25*2.54/2*cm;

  G4double ScintChmbMiddlePos = BetaBottomPos+2*BetaBottom_hz+2*BetaWall_hz+ScintChmbWall_hz;

  G4ThreeVector ScintChmbPos = G4ThreeVector(0, 0, ScintChmbMiddlePos);

  G4Tubs* solidScintChmbWall
    = new G4Tubs("ScintChmbWall",		//It's name
	ScintChmbWall_R_in,			//inner radius
	ScintChmbWall_R_out,			//outer radius
	ScintChmbWall_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4Tubs* solidScintChmbTop
    = new G4Tubs("ScintChmbTop",		//It's name
	ScintChmbTop_R_in,			//inner radius
	ScintChmbTop_R_out,			//outer radius
	ScintChmbTop_hz,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4UnionSolid* solidScintChmbChamber 		//Create union wall + top
    = new G4UnionSolid("ScintChamber",solidScintChmbWall,solidScintChmbTop,0,G4ThreeVector(0,0,ScintChmbWall_hz+ScintChmbTop_hz));

  logicScintChmb
    = new G4LogicalVolume(solidScintChmbChamber,StainlessSteel,"ScintChamber");

  physiScintChmb
    = new G4PVPlacement(0,  	           //no rotation
	ScintChmbPos,	           //Position
	logicScintChmb,       //its logical volume
	"ScintChamber",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking


  //
  // Plastic Scintillator
  //

  G4Material* ScintMat= nist->FindOrBuildMaterial("G4_PLASTIC_SC_VINYLTOLUENE");

  // Scintillator geometrical parameters
  G4double Scint_R_in = 0.*cm;
  G4double Scint_R_out = 4.7*2.54/2*cm;
  G4double Scint_hz = 1.5*2.54/2*cm;

  G4double Scint_TopSurf = BetaBottomPos+78.105*mm+0.125*2.54*cm;
  G4double Scint_BottomSurf = Scint_TopSurf-2*Scint_hz;

  G4ThreeVector ScintPos = G4ThreeVector(0, 0, Scint_BottomSurf+Scint_hz);
  //The last 0.125 inch is the O-ring

  G4Tubs* solidScint
    = new G4Tubs("Scintillator",		//It's name
	Scint_R_in,			//inner radius
	Scint_R_out,			//outer radius
	Scint_hz,				//hight
	0.*deg,360.*deg);			//start and spanning angle

  logicScint = new G4LogicalVolume(solidScint,ScintMat,"Scintillator");
  physiScint = new G4PVPlacement(0,		//no rotation
      ScintPos,					//position of the center
      logicScint,				//its logical volume
      "Scintillator",				//its name
      logicWorld,				//its mother
      false,					//no boolean operations
      0,					//copy number
      checkOverlaps);				//checking overlaps

  //
  // Light Guide
  //

  G4Material* LightguideMat= nist->FindOrBuildMaterial("G4_PLEXIGLASS");

  // Lightguide geometrical parameters
  G4double Lightguide_R_in = 0.*cm;
  G4double Lightguide_R_out = 5.0*2.54/2*cm;
  G4double Lightguide_hz = 5.5*2.54/2*cm;

  G4ThreeVector LightguidePos = G4ThreeVector(0, 0, Scint_TopSurf+Lightguide_hz);

  G4Tubs* solidLightguide
    = new G4Tubs("Lightguide",		//It's name
	Lightguide_R_in,			//inner radius
	Lightguide_R_out,			//outer radius
	Lightguide_hz,				//hight
	0.*deg,360.*deg);			//start and spanning angle

  logicLightguide = new G4LogicalVolume(solidLightguide,LightguideMat,"Lightguide");
  physiLightguide = new G4PVPlacement(0,		//no rotation
      LightguidePos,					//position of the center
      logicLightguide,				//its logical volume
      "Lightguide",				//its name
      logicWorld,				//its mother
      false,					//no boolean operations
      0,					//copy number
      checkOverlaps);				//checking overlaps

  //
  //Gas Fill
  //
  //Argon1atm
  G4Material* Argon1atm = new G4Material("Argon1atm", z=18., a= 39.95*g/mole, 
      density=1.662*mg/cm3, kStateGas, temperature=293.15*kelvin,
      pressure=1*atmosphere);
  Argon1atm->GetIonisation()->SetMeanEnergyPerIonPair(26.34*eV);
  //ArgonCO2
  G4double zz, aa, fractionmass, Density;
  G4String Name, Symbol;
  G4int Ncomponents;
  aa = 40.00*g/mole;
  G4Element* elAr = new G4Element(Name="Argon",Symbol="Ar" , zz= 18., aa);
  aa = 16.00*g/mole;
  G4Element* elO = new G4Element(Name="Oxygen" ,Symbol="O" , zz= 8., aa);
  aa = 12.00*g/mole;
  G4Element* elC = new G4Element(Name="Carbon" ,Symbol="C" , zz= 6., aa);
  Density = 1.804*mg/cm3;
  G4Material* ArCO2 = new G4Material(name="ArCO2 ",Density,Ncomponents=3);
  ArCO2->AddElement(elAr, fractionmass=89.1*perCent);
  ArCO2->AddElement(elO, fractionmass=7.9*perCent);
  ArCO2->AddElement(elC, fractionmass=3.0*perCent);
  ArCO2->GetIonisation()->SetMeanEnergyPerIonPair(26.00*eV); //Refer to Ar-CH4 in Knoll's book

  //Default Gas Material
  GasMaterial = ArCO2;
  MeanIonizationEnergy = GasMaterial->GetIonisation()->GetMeanEnergyPerIonPair();

  G4double R1_out = BetaWall_R_in;
  G4double Hz1    = BetaWall_hz + BetaTop_hz;
  G4double R2_out = ScintChmbWall_R_in;
//  G4double Hz2    = ((1.5+0.124)*2.54*cm-Hz1*2)/2;
  G4double Hz2    = (Scint_BottomSurf-(BetaBottomPos+2*BetaBottom_hz+2*BetaWall_hz+2*BetaTop_hz)-0.02*mm)/2.0;//clearance for future wrapping
  G4double R3_in  = Scint_R_out;
  G4double R3_out = R2_out;
  G4double Hz3    = Scint_hz;
  G4double R4_in  = Lightguide_R_out;
  G4double R4_out = R2_out;
  G4double Hz4    = ScintChmbWall_hz-BetaTop_hz-Hz2-Hz3;

  G4double GasFillMiddle = BetaBottomPos+2*BetaBottom_hz+2*BetaWall_hz+2*BetaTop_hz-Hz1;
//  cout<<"XXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXXX" << GasFillMiddle/cm<<endl;

  G4ThreeVector GasFillPos = G4ThreeVector(0, 0, GasFillMiddle);

  G4Tubs* solidGasFill1
    = new G4Tubs("GasFill1",		//It's name
	0.,			    //inner radius
	R1_out,			//outer radius
	Hz1,				//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidGasFill2
    = new G4Tubs("GasFill2",		//It's name
	0.,			    //inner radius
	R2_out,			//outer radius
	Hz2,				//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidGasFill3
    = new G4Tubs("GasFill1",		//It's name
	R3_in,			    //inner radius
	R3_out,			//outer radius
	Hz3,				//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4Tubs* solidGasFill4
    = new G4Tubs("GasFill1",		//It's name
	R4_in,			    //inner radius
	R4_out,			//outer radius
	Hz4,				//hight
	0.*deg,360.*deg);	//start and spanning angle

  G4UnionSolid* solidGasFill12 		//Create union
    = new G4UnionSolid("GasFill12",solidGasFill1,solidGasFill2,0,G4ThreeVector(0,0,Hz1+Hz2));

  G4UnionSolid* solidGasFill123 		//Create union
    = new G4UnionSolid("GasFill123",solidGasFill12,solidGasFill3,0,G4ThreeVector(0,0,Hz1+Hz2*2+Hz3));

  G4UnionSolid* solidGasFill1234 		//Create union
    = new G4UnionSolid("GasFill1234",solidGasFill123,solidGasFill4,0,G4ThreeVector(0,0,Hz1+Hz2*2+Hz3*2+Hz4));

/*  G4SubtractionSolid* solidGasFill1234Sub //Create subtraction of MWPC Gas
    = new G4SubtractionSolid("GasFill1234Sub",solidGasFill1234,solidMWPCGas,0,G4ThreeVector(0,0,-(Hz1-0.437*2.54*cm)));

  logicGasFill = new G4LogicalVolume(solidGasFill1234Sub,     //its solid
      GasMaterial,             //its material
      "GasFill");          	 //its name*/
  logicGasFill = new G4LogicalVolume(solidGasFill1234,     //its solid
      GasMaterial,             //its material
      "GasFill");          	 //its name

  physiGasFill = new G4PVPlacement(0,         //no rotation
      GasFillPos,               //at position
      logicGasFill,             //its logical volume
      "GasFill",                //its name
      logicWorld,            //its mother  volume
      false,                 //no boolean operation
      0,                     //copy number
      checkOverlaps);        //overlaps checking

  //     
  // MWPCGas
  //

  // MWPCGas geometrical parameters
  G4double MWPCGas_X = 4.00*cm;
  G4double MWPCGas_Y = 4.00*cm;
//  G4double MWPCGas_Z = 3.81*cm-0.1*mm;
//  G4double MWPCGas_Z = 2.21*cm;
  G4double MWPCGas_Z = 2.05*cm;
//  G4double MWPCGas_Z = 0.9*cm;
  //G4double MWPCRot = 30.0/180.0*PI;//MWPC rotation angle, old config
  G4double MWPCRot = -21.0/180.0*PI;//MWPC rotation angle
  G4RotationMatrix* MWPCRotMatrix = new G4RotationMatrix; // Rotates X and Y axes only
  MWPCRotMatrix->rotateZ(MWPCRot*rad); 

//  G4ThreeVector MWPCGasPos = G4ThreeVector(0, 0, (2.769+0.5+0.062)*2.54*cm);
//  G4ThreeVector MWPCGasPos = G4ThreeVector(0, 0, 3.81*cm/2.0-Hz1);
  G4ThreeVector MWPCGasPos = G4ThreeVector(0, 0, (0.125+0.062+0.25)*2.54*cm-Hz1);

  G4Box* solidMWPCGas
    = new G4Box("MWPCGas",		//It's name
	MWPCGas_X*0.5,			//X-dim
	MWPCGas_Y*0.5,			//Y-dim
	MWPCGas_Z*0.5);			//hight

  logicMWPCGas = new G4LogicalVolume(solidMWPCGas,     //its solid
      GasMaterial,             //its material
      "MWPCGas");          	 //its name

  // Define a region for MWPCGas
  G4Region* MWPCRegion = new G4Region("MWPC");
  logicMWPCGas->SetRegion(MWPCRegion);
  MWPCRegion->AddRootLogicalVolume(logicMWPCGas);
  G4ProductionCuts* MWPC_cuts;
  MWPC_cuts = new G4ProductionCuts;
  MWPC_cuts->SetProductionCut(0.05*mm);
  MWPCRegion->SetProductionCuts(MWPC_cuts);

  physiMWPCGas = new G4PVPlacement(MWPCRotMatrix,         //rotation
      MWPCGasPos,               //at position
      logicMWPCGas,             //its logical volume
      "MWPCGas",                //its name
      logicGasFill,            //its mother  volume
      false,                 //no boolean operation
      0,                     //copy number
      checkOverlaps);        //overlaps checking

  //Set max step limit
  fStepLimit_MWPC = new G4UserLimits(0.25*mm);
//  fStepLimit_MWPC = new G4UserLimits(1*mm);
  logicMWPCGas->SetUserLimits(fStepLimit_MWPC); 

  //     
  // MWPC frames
  //

  // MWPCGas geometrical parameters
  G4double MWPCAnodeCut_XY = 4.00*cm;
  G4double MWPCCathodeCut_XY = 4.80*cm;
  G4double MWPCCuts_Z = 0.064*2.54*cm;
  G4double MWPCFrames_Z = 0.062*2.54*cm;

  G4ThreeVector MWPCAnodePos = G4ThreeVector(0, 0, (0.125+0.062+0.25)*2.54*cm-Hz1-0.031*2.54*cm);
  G4ThreeVector MWPCCathodePos1 = G4ThreeVector(0, 0, (0.125+0.062)*2.54*cm-Hz1-0.031*2.54*cm);
  G4ThreeVector MWPCCathodePos2 = G4ThreeVector(0, 0, (0.125+0.062+0.5)*2.54*cm-Hz1-0.031*2.54*cm);
  Z_Cathode_Low=(0.125+0.062)*2.54*cm-Hz1+GasFillMiddle;

  G4Box* solidMWPCAnodeCut
    = new G4Box("MWPCAnodeCut",		//It's name
	MWPCAnodeCut_XY*0.5,			//X-dim
	MWPCAnodeCut_XY*0.5,			//Y-dim
	MWPCCuts_Z*0.5);			//hight

  G4Box* solidMWPCCathodeCut
    = new G4Box("MWPCCathodeCut",		//It's name
	MWPCCathodeCut_XY*0.5,			//X-dim
	MWPCCathodeCut_XY*0.5,			//Y-dim
	MWPCCuts_Z*0.5);			//hight

  G4Tubs* solidMWPCBoard
    = new G4Tubs("MWPCBoard",		//It's name
	0.0,			//inner radius
	BetaWall_R_in,			//outer radius
	MWPCFrames_Z/2.0,			//hight
	0.*deg,360.*deg);			//start and spanning angle

  G4SubtractionSolid* solidMWPCAnodeFrame //Create subtraction for the anode
    = new G4SubtractionSolid("solidMWPCAnodeFrame",solidMWPCBoard,solidMWPCAnodeCut,0,G4ThreeVector(0.0,0.0,0.0));

  G4SubtractionSolid* solidMWPCCathodeFrame1 //Create subtraction for the cathode 1
    = new G4SubtractionSolid("solidMWPCCathodeFrame1",solidMWPCBoard,solidMWPCCathodeCut,0,G4ThreeVector(0.0,0.0,0.0));

  G4SubtractionSolid* solidMWPCCathodeFrame2 //Create subtraction for the cathode 2
    = new G4SubtractionSolid("solidMWPCCathodeFrame2",solidMWPCBoard,solidMWPCCathodeCut,0,G4ThreeVector(0.0,0.0,0.0));

  logicMWPCAnodeFrame = new G4LogicalVolume(solidMWPCAnodeFrame,     //its solid
      Glass,             //its material
      "MWPCAnodeFrame");          	 //its name

  logicMWPCCathodeFrame1 = new G4LogicalVolume(solidMWPCCathodeFrame1,     //its solid
      Glass,             //its material
      "MWPCCathodeFrame1");          	 //its name

  logicMWPCCathodeFrame2 = new G4LogicalVolume(solidMWPCCathodeFrame2,     //its solid
      Glass,             //its material
      "MWPCCathodeFrame2");          	 //its name

  physiMWPCAnodeFrame = new G4PVPlacement(MWPCRotMatrix,         //rotation
      MWPCAnodePos,               //at position
      logicMWPCAnodeFrame,             //its logical volume
      "MWPCAnodeFrame",                //its name
      logicGasFill,            //its mother  volume
      false,                 //no boolean operation
      0,                     //copy number
      checkOverlaps);        //overlaps checking

  physiMWPCCathodeFrame1 = new G4PVPlacement(MWPCRotMatrix,         //rotation
      MWPCCathodePos1,               //at position
      logicMWPCCathodeFrame1,             //its logical volume
      "MWPCCathodeFrame1",                //its name
      logicGasFill,            //its mother  volume
      false,                 //no boolean operation
      0,                     //copy number
      checkOverlaps);        //overlaps checking

  physiMWPCCathodeFrame2 = new G4PVPlacement(MWPCRotMatrix,         //rotation
      MWPCCathodePos2,               //at position
      logicMWPCCathodeFrame2,             //its logical volume
      "MWPCCathodeFrame2",                //its name
      logicGasFill,            //its mother  volume
      false,                 //no boolean operation
      0,                     //copy number
      checkOverlaps);        //overlaps checking

  //
  //Source Holder
  //
  
  G4Material* MylarMat= nist->FindOrBuildMaterial("G4_MYLAR");
  G4double SourceHolderAlPiece_x = 2.0/2.0*2.54*cm;
  G4double SourceHolderAlPiece_y = 1.5/2.0*2.54*cm;
  G4double SourceHolderAlPiece_z = 0.2/2.0*2.54*cm;
  G4double SourceHolderRot = 45.0/180.0*PI;//Source Holder rotation angle
  G4RotationMatrix* SourceHolderRotMatrix = new G4RotationMatrix; // Rotates X and Y axes only
  SourceHolderRotMatrix->rotateZ(SourceHolderRot*rad); 

  G4Box* solidSourceHolderAlPiece
    = new G4Box("SourceHolderAlPiece",         //It's name
	SourceHolderAlPiece_x,
	SourceHolderAlPiece_y,
	SourceHolderAlPiece_z);    

  G4double SourceHolderHole_R = 0.5*2.54*cm;

  G4Tubs* solidSourceHolderHole
    = new G4Tubs("SourceHolderHole",
	0.0,
	SourceHolderHole_R,
	SourceHolderAlPiece_z,
	0.*deg,360.*deg);       

  G4double SourceHolderCover_z = 0.00643/2.0*mm;
  //G4double SourceHolderCover_z = 0.00071/2.0*mm; //Thick Source
  G4double SourceHolderPt_z = 0.00643/2.0*mm;
  //G4double SourceHolderPt_z = 0.002/2.0*2.54*cm; //Thick Source
  G4double SourceHolderSS_z = 0.010/2.0*2.54*cm;
  G4double SourceHolderPlastic_z = 0.004/2.0*2.54*cm;

  G4Tubs* solidSourceHolderCover
    = new G4Tubs("SourceHolderCover",
	0.0,
	SourceHolderHole_R,
	SourceHolderCover_z,
	0.*deg,360.*deg);       

  G4Tubs* solidSourceHolderPt
    = new G4Tubs("SourceHolderPt",
	0.0,
	SourceHolderHole_R,
	SourceHolderPt_z,
	0.*deg,360.*deg);       

  G4Tubs* solidSourceHolderSS
    = new G4Tubs("SourceHolderSS",
	0.0,
	SourceHolderHole_R,
	SourceHolderSS_z,
	0.*deg,360.*deg);       

  G4Tubs* solidSourceHolderPlastic
    = new G4Tubs("SourceHolderPlastic",
	0.0,
	SourceHolderHole_R,
	SourceHolderPlastic_z,
	0.*deg,360.*deg);       

  G4ThreeVector SourceHolderPos = G4ThreeVector(20*cm,-20*cm,-38.0*cm);

  G4SubtractionSolid* solidSourceHolderSupport //Create subtraction of the hole
    = new G4SubtractionSolid("SourceHolderSupport",solidSourceHolderAlPiece,solidSourceHolderHole,0,G4ThreeVector(0.0,0.0,0.0));

  //Material Aluminum
  G4Material* Al = nist->FindOrBuildMaterial("G4_Al");
  G4Material* Pt = nist->FindOrBuildMaterial("G4_Pt");

  logicSourceHolderSupport
    = new G4LogicalVolume(solidSourceHolderSupport,Al,"SourceHolderAl");

  logicSourceHolderCover
    = new G4LogicalVolume(solidSourceHolderCover,MylarMat,"SourceHolderCover");

  logicSourceHolderPt
  //  = new G4LogicalVolume(solidSourceHolderPt,Pt,"SourceHolderPt");
  = new G4LogicalVolume(solidSourceHolderPt,MylarMat,"SourceHolderPt");

  logicSourceHolderSS
    //= new G4LogicalVolume(solidSourceHolderSS,StainlessSteel,"SourceHolderStainlessSteel");
		= new G4LogicalVolume(solidSourceHolderSS,Vacuum,"SourceHolderStainlessSteel");

  logicSourceHolderPlastic
    //= new G4LogicalVolume(solidSourceHolderPlastic,LightguideMat,"SourceHolderPlastic");
    = new G4LogicalVolume(solidSourceHolderPlastic,Vacuum,"SourceHolderPlastic");

  physiSourceHolderSupport
    = new G4PVPlacement(SourceHolderRotMatrix,//rotation
	SourceHolderPos,	           //Position
	logicSourceHolderSupport,       //its logical volume
	"SourceHolderSupport",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiSourceHolderCover
    = new G4PVPlacement(SourceHolderRotMatrix,//rotation
	SourceHolderPos+G4ThreeVector(0,0,0.2*mm),	           //Position
	logicSourceHolderCover,       //its logical volume
	"SourceHolderCover",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiSourceHolderPt
    = new G4PVPlacement(SourceHolderRotMatrix,//rotation
	SourceHolderPos,	           //Position
	logicSourceHolderPt,       //its logical volume
	"SourceHolderPt",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiSourceHolderSS
    = new G4PVPlacement(SourceHolderRotMatrix,//rotation
	SourceHolderPos-G4ThreeVector(0,0,SourceHolderPt_z+SourceHolderSS_z),	           //Position
	logicSourceHolderSS,       //its logical volume
	"SourceHolderSS",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  physiSourceHolderPlastic
    = new G4PVPlacement(SourceHolderRotMatrix,//rotation
	SourceHolderPos-G4ThreeVector(0,0,SourceHolderPt_z+SourceHolderSS_z*2+SourceHolderPlastic_z),	           //Position
	logicSourceHolderPlastic,       //its logical volume
	"SourceHolderPlastic",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  //Set max step limit
/*  fStepLimit_SourceHolderSupport = new G4UserLimits(SourceHolderAlPiece_z*0.5);
  logicSourceHolderSupport->SetUserLimits(fStepLimit_SourceHolderSupport); 

  fStepLimit_SourceHolderPt = new G4UserLimits(SourceHolderPt_z*0.5);
  logicSourceHolderPt->SetUserLimits(fStepLimit_SourceHolderPt); 

  fStepLimit_SourceHolderSS = new G4UserLimits(SourceHolderSS_z*0.5);
  logicSourceHolderSS->SetUserLimits(fStepLimit_SourceHolderSS); 

  fStepLimit_SourceHolderPlastic = new G4UserLimits(SourceHolderPlastic_z*0.5);
  logicSourceHolderPlastic->SetUserLimits(fStepLimit_SourceHolderPlastic); 
*/
  //
  //Degrader
  //

  G4double Degrader_x = 20.0/2*mm;
  G4double Degrader_y = 30.0/2*mm;
  G4double Degrader_z = 0.050/2*mm;

  G4ThreeVector DegraderPos = G4ThreeVector(20*cm,-20*cm,-37.0*cm);
//  G4ThreeVector DegraderPos = G4ThreeVector(0,0,0.1*mm);

  G4Box* solidDegrader
    = new G4Box("Degrader",		//It's name
	Degrader_x,			//inner radius
	Degrader_y,			//outer radius
	Degrader_z);					//hight

  logicDegrader
    = new G4LogicalVolume(solidDegrader,ScintMat,"Degrader");

  physiDegrader
    = new G4PVPlacement(0,//rotation
	DegraderPos,	           //Position
	logicDegrader,       //its logical volume
	"Degrader",          //its name
	logicWorld,            //its mother  volume
	false,                 //no boolean operation
	0,                     //copy number
	checkOverlaps);        //overlaps checking

  //Set max step limit
//  fStepLimit_Degrader = new G4UserLimits(Degrader_z*0.5);
//  logicDegrader->SetUserLimits(fStepLimit_Degrader); 
  //
  //always return the physical World
  //
  return physiWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracDetectorConstruction::SetGasMaterial(G4String materialName)
{
  //search the material by its name
  G4Material* pttoMaterial = G4Material::GetMaterial(materialName);
  if (pttoMaterial){
    GasMaterial = pttoMaterial;
    logicMWPCGas->SetMaterial(pttoMaterial);
    MeanIonizationEnergy = GasMaterial->GetIonisation()->GetMeanEnergyPerIonPair();
    G4cout << "\n----> The MWPCGas is filled with " << materialName <<G4endl;
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracDetectorConstruction::SetSourceHolderPos(G4ThreeVector InputPos)
{
  G4double SourceHolderPt_z = 0.002/2.0*2.54*cm;
  //G4double SourceHolderPt_z = 0.00643/2.0*mm;
  G4double SourceHolderSS_z = 0.010/2.0*2.54*cm;
  G4double SourceHolderPlastic_z = 0.004/2.0*2.54*cm;
  physiSourceHolderSupport->SetTranslation(InputPos);
  physiSourceHolderCover->SetTranslation(InputPos+G4ThreeVector(0,0,0.2*mm));
  physiSourceHolderPt->SetTranslation(InputPos);
  physiSourceHolderSS->SetTranslation(InputPos-G4ThreeVector(0,0,SourceHolderPt_z+SourceHolderSS_z));
  physiSourceHolderPlastic->SetTranslation(InputPos-G4ThreeVector(0,0,SourceHolderPt_z+SourceHolderSS_z*2+SourceHolderPlastic_z));
  G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracDetectorConstruction::SetDegraderPos(G4ThreeVector InputPos)
{
  physiDegrader->SetTranslation(InputPos);
  G4RunManager::GetRunManager()->GeometryHasBeenModified();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
