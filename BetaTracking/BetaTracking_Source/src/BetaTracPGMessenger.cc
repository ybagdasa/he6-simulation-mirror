//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: BetaTracPGMessenger.cc,v 1.31 2007-11-16 22:37:43 asaim Exp $
// GEANT4 tag $Name: not supported by cvs2svn $
//

#include "BetaTracPGMessenger.hh"
#include "BetaTracPrimaryGeneratorAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithADouble.hh"
#include "G4UIcmdWith3Vector.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UImanager.hh"
#include "G4ProductionCutsTable.hh"
#include "G4Tokenizer.hh"
#include "Randomize.hh"
#include <sstream>

BetaTracPGMessenger::BetaTracPGMessenger(BetaTracPrimaryGeneratorAction * PGAct)
:PGAction(PGAct)
{
  GeneratorDirectory = new G4UIdirectory("/Generator/");
  GeneratorDirectory->SetGuidance("Setup Parameters for Primary Generator.");
  
  SetSourceType = new G4UIcmdWithAString("/Generator/SetSourceType",this);
  SetSourceType->SetGuidance("MOT or Diffuse source?");
  SetSourceType->SetParameterName("V_SourceType",true);
  SetSourceType->SetDefaultValue("MOT");
  SetSourceType->SetCandidates("MOT Diffuse");
  
  SetIsotope = new G4UIcmdWithAString("/Generator/SetIsotope",this);
  SetIsotope->SetGuidance("Set Isotope.");
  SetIsotope->SetParameterName("V_Isotope",true);
  SetIsotope->SetDefaultValue("He6");
  SetIsotope->SetCandidates("He6 Bi207 Bi207E Sr90 Fe55");

  Set_a = new G4UIcmdWithADouble("/Generator/Set_a",this);
  Set_a->SetGuidance("Set correlation coefficient a.");
  Set_a->SetParameterName("V_a",true);
  Set_a->SetDefaultValue(-0.3333333333333333333);
  Set_a->SetRange("V_a>-1.0 && V_a<1.0");
  
  Set_b = new G4UIcmdWithADouble("/Generator/Set_b",this);
  Set_b->SetGuidance("Set Fierz term b.");
  Set_b->SetParameterName("V_b",true);
  Set_b->SetDefaultValue(0.0);
  Set_b->SetRange("V_b>-10 && V_b<10");

  SetFixed_Energy = new G4UIcmdWithADouble("/Generator/SetFixed_Energy",this);
  SetFixed_Energy->SetGuidance("Set Fixed_Energy.");
  SetFixed_Energy->SetParameterName("V_Fixed_Energy",true);
  SetFixed_Energy->SetDefaultValue(0.0);
  SetFixed_Energy->SetRange("V_Fixed_Energy>=0.0");
    
  SetHardRatio = new G4UIcmdWithADouble("/Generator/SetHardRatio",this);
  SetHardRatio->SetGuidance("Set HardRatio.");
  SetHardRatio->SetParameterName("V_HardRatio",true);
  SetHardRatio->SetDefaultValue(0.02955);
  SetHardRatio->SetRange("V_HardRatio>=0.0 && V_HardRatio<=1");
    
  SetSolidAngle = new G4UIcmdWith3Vector("/Generator/SetSolidAngle",this);
  SetSolidAngle->SetGuidance("Set SolidAngle.");
  SetSolidAngle->SetParameterName("V_theta","V_phi","V_Aperture",true);
  SetSolidAngle->SetDefaultValue(G4ThreeVector(0.,0.,180.));
  
  SetMOT_pos = new G4UIcmdWith3Vector("/Generator/SetMOT_pos",this);
  SetMOT_pos->SetGuidance("Set MOT_pos.");
  SetMOT_pos->SetParameterName("V_x","V_y","V_z",true);
  SetMOT_pos->SetDefaultValue(G4ThreeVector(0.,0.,0.));
  
  SetMOT_sigma = new G4UIcmdWith3Vector("/Generator/SetMOT_sigma",this);
  SetMOT_sigma->SetGuidance("Set MOT_sigma.");
  SetMOT_sigma->SetParameterName("V_sigma_x","V_sigma_y","V_sigma_z",true);
  SetMOT_sigma->SetDefaultValue(G4ThreeVector(0.,0.,0.));
  //  SetMOT_R->SetRange("V_MOT_R>0.0");
  
  SetDiffuse_R = new G4UIcmdWithADouble("/Generator/SetDiffuse_R",this);
  SetDiffuse_R->SetGuidance("Set Diffuse_R.");
  SetDiffuse_R->SetParameterName("V_Diffuse_R",true);
  SetDiffuse_R->SetDefaultValue(0.0);
  SetDiffuse_R->SetRange("V_Diffuse_R>0.0");
  
  SetDiffuse_H = new G4UIcmdWithADouble("/Generator/SetDiffuse_H",this);
  SetDiffuse_H->SetGuidance("Set Diffuse_H.");
  SetDiffuse_H->SetParameterName("V_Diffuse_H",true);
  SetDiffuse_H->SetDefaultValue(0.0);
  SetDiffuse_H->SetRange("V_Diffuse_H>0.0");

}

BetaTracPGMessenger::~BetaTracPGMessenger()
{
  delete  GeneratorDirectory;
  delete  SetSourceType;
  delete  SetIsotope;
  delete  Set_a;
  delete  Set_b;
  delete  SetFixed_Energy;
  delete  SetHardRatio;
  delete  SetSolidAngle;
  delete  SetMOT_pos;
  delete  SetMOT_sigma;
  delete  SetDiffuse_R;
  delete  SetDiffuse_H;
}

void BetaTracPGMessenger::SetNewValue(G4UIcommand * command,G4String newValue)
{
  if( command==SetSourceType )
  {
    PGAction->SetSourceType(newValue);
  }
  else if( command==SetIsotope )
  {
    PGAction->SetIsotope(newValue);
  }
  else if( command==Set_a )
  {
    PGAction->Set_a(Set_a->GetNewDoubleValue(newValue));
  }
  else if( command==Set_b )
  {
    PGAction->Set_b(Set_b->GetNewDoubleValue(newValue));
  }
  else if( command==SetFixed_Energy )
  {
    PGAction->Set_Fixed_Energy(SetFixed_Energy->GetNewDoubleValue(newValue));
  }
  else if( command==SetHardRatio )
  {
    PGAction->Set_HardRatio(SetHardRatio->GetNewDoubleValue(newValue));
  }
  else if( command==SetSolidAngle )
  {
    PGAction->Set_SolidAngle(SetSolidAngle->GetNew3VectorValue(newValue));
  }
  else if( command==SetMOT_pos )
  {
    PGAction->Set_MOT_pos(SetMOT_pos->GetNew3VectorValue(newValue));
  }
  else if( command==SetMOT_sigma )
  {
    PGAction->Set_MOT_sigma(SetMOT_sigma->GetNew3VectorValue(newValue));
  }
  else if( command==SetDiffuse_R )
  {
    PGAction->Set_Diffuse_R(SetDiffuse_R->GetNewDoubleValue(newValue));
  }
  else if( command==SetDiffuse_H)
  {
    PGAction->Set_Diffuse_H(SetDiffuse_H->GetNewDoubleValue(newValue));
  }
}

/*
G4String BetaTracPGMessenger::GetCurrentValue(G4UIcommand * command)
{
  G4String cv;
  return cv;
}*/

