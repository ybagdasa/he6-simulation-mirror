//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracPrimaryGeneratorAction.cc
/// \brief Implementation of the BetaTracPrimaryGeneratorAction class

#include "BetaTracPrimaryGeneratorAction.hh"
#include "BetaTracDetectorConstruction.hh"
#include "BetaTracPGMessenger.hh"
#include "G4LogicalVolumeStore.hh"
#include "G4LogicalVolume.hh"
#include "G4Box.hh"
#include "G4RunManager.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
#include "Randomize.hh"
#include <math.h>
#include "EventGenerator.h"
#include "GlobalConstants.h"
#include <string>
#include <cstring>
#include <stdint.h>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracPrimaryGeneratorAction* BetaTracPrimaryGeneratorAction::fgInstance = 0;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracPrimaryGeneratorAction* BetaTracPrimaryGeneratorAction::Instance()
{
// Static acces function via G4RunManager 

  return fgInstance;
}      

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracPrimaryGeneratorAction::Open_Input(string Data_Directory,G4int fileID)
{
  char filename[100];
  sprintf(filename,"%s/OriginalEvent%03d.dat",Data_Directory.c_str(),fileID);

  if(inputfile.is_open()){
    G4cout << "Input file is already opened!\n";
    return;
  }
  inputfile.open(filename,std::ios::in | std::ios::binary);
  if(!inputfile.is_open()){
    G4cout << "Can't open input file!\n" <<G4endl;
    return;
  }
  G4cout << "Input file "<<filename<<" opened.\n";
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracPrimaryGeneratorAction::Close_Input()
{
  inputfile.close();
  G4cout << "Input file closed!\n";
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
BetaTracPrimaryGeneratorAction::BetaTracPrimaryGeneratorAction()//defualt constructor
: G4VUserPrimaryGeneratorAction(), //call's parents defualt constructor
  fParticleGun(0),Theta(0),Phi(0),Aperture(180.0),Fixed_Energy(0.0),
  b(0.00),a(-1.0/3.0),D_radius(50),D_height(65),USE_EXTERNAL_RAND(false),SourceType("MOT"),
  Isotope("He6"),ZDaughter(3.0),Omega(0.000001),HardRatio(0.02955) //sets default member values
{
  G4int n_particle = 1;
  fParticleGun  = new G4ParticleGun(n_particle);
  ParticleType = "Electron";

  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  G4ParticleDefinition* particle
    = particleTable->FindParticle(particleName="e-");
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  fParticleGun->SetParticleEnergy(E_endpoint);
  
  //m_e = particle->GetPDGMass()/MeV;
  m_e = MASS_E;
  M_r = MASS_LI6 - MASS_E;
  E_endpoint = MASS_HE6 - MASS_LI6;
  

  for (int i=0;i<3;i++){
    MOT_pos[i]=0.0;
    MOT_rad[i]=0.2;
    V_Ion_initial[i]=0.0;
    DEC_pos[i]=0.0;
    P_e_in[i] = 0.0;
  }
  
  ParticleID = 0;
  
  //Construct EventGenerator
  EVGenerator = new EventGenerator(M_r,ZDaughter,E_endpoint,Fixed_Energy,Theta,Phi,Aperture,SourceType,Isotope);
  //Calculate maximum of the spectrum
  //CalculateW_max();
  fgInstance = this;
  //Messenger
  theMessenger = new BetaTracPGMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracPrimaryGeneratorAction::~BetaTracPrimaryGeneratorAction()
{
  delete fParticleGun;
  if(inputfile.is_open())
    Close_Input();
  fgInstance = 0;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
G4double BetaTracPrimaryGeneratorAction::BetaTractrum(G4double E,G4double Pe_dot_Pv)
{
  G4double Ee = E + m_e;
  G4double E0 = E_endpoint + m_e;
  G4double W;
  W = sqrt(Ee*Ee - m_e*m_e)*Ee*(E0 - Ee)*(E0 - Ee)*(1 + b*m_e/Ee+a*Pe_dot_Pv)/pow(m_e,4);
  return W;
}*/

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
/*
G4ThreeVector BetaTracPrimaryGeneratorAction::RandDirection(G4double CosTheta)
{
  G4double PI = 3.14159265358;
  G4double PHI = 2*PI*G4UniformRand();
//  G4double COS_THETA = -1.0 + 2.0*G4UniformRand(); 
  G4double COS_THETA = CosTheta + (1-CosTheta)*G4UniformRand();  //Forward angle only
  G4double SIN_THETA = sqrt(1-pow(COS_THETA,2));
  return G4ThreeVector(SIN_THETA*cos(PHI),SIN_THETA*sin(PHI),COS_THETA);
}*/
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracPrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{
  //this function is called at the begining of each event
  //
  //Define parameters
  G4double x0,y0,z0;
  G4double energy;
  G4ThreeVector Direction;
  
  //for reading
  int eventID;
  double P_e[3];
  double P_r[3];
  double P_e_sqr;
  //for Coincidence particles
  double CoinEnergy;
  double P_coin[3];
  double P_coin_sqr;
  G4ThreeVector CoinDirection;
  bool CoinParExist=false;
  
  short DEC_pos_in[3];
//  int P_e_in[3];  moved to member
  int V_r_in[3];
//  uint8_t ParticleID; moved to member

  if(USE_EXTERNAL_RAND){
    //Read the input file
    if(!inputfile.is_open()){
      G4cout << "Error: Input file is not opened!\n";
      return;
    }
    inputfile.read((char *)&eventID,sizeof(int)); //Original event ID
    inputfile.read((char *)DEC_pos_in,3*sizeof(short)); //decay position
    inputfile.read((char *)P_e_in,3*sizeof(int)); //Electron momentum
    inputfile.read((char *)V_r_in,3*sizeof(int)); //Recoil velocity
    inputfile.read((char *)&ParticleID,sizeof(uint8_t)); //Particle type
    for (int i=0;i<3;i++){
      DEC_pos[i] = double(DEC_pos_in[i]/1000.0);
      P_e[i] = double(P_e_in[i]/1000000.0);
      V_Ion_initial[i] = double(V_r_in[i]/100000.0);
    }
    if (ParticleID==0) ParticleType = "Electron";
    else if (ParticleID==1) ParticleType = "Photon";
  }else{
    EVGenerator->Generate(DEC_pos,P_e,P_r,ParticleType,CoinParExist);
    //Ion information
    //unit: cm/us
    for (int i=0;i<3;i++){
      V_Ion_initial[i] = P_r[i]/M_r*30000.0;
    }
    //Save initial momentum
    for (int i=0;i<3;i++){
      P_e_in[i] = int(P_e[i]*1000000.0);
    }
    //Generate Coincidence particles from following decay
    //If CoinParticleType
    if (CoinParExist){
      EVGenerator->GenerateCoin(P_coin,CoinParticleType);
    }
  }
  //Decay position
  x0 = DEC_pos[0]*mm;
  y0 = DEC_pos[1]*mm;
  z0 = DEC_pos[2]*mm;
  //electron or photon momentum and energy
  P_e_sqr = pow(P_e[0],2.0)+pow(P_e[1],2.0)+pow(P_e[2],2.0);
  for (int i=0;i<3;i++){
    Direction[i] = P_e[i]/sqrt(P_e_sqr);
  }
  
  if (ParticleType.compare("Electron")==0){
    energy = (sqrt(P_e_sqr+m_e*m_e)-m_e)*MeV;	//Kinetic Energy!!!
    ParticleID = 0;
  }else if (ParticleType.compare("Photon")==0){
    energy = sqrt(P_e_sqr)*MeV;	//Kinetic Energy!!!
    ParticleID = 1;
  }

//  double E_Field = 0.2;//MV/m;            //Need to make priate member....
//  double Distance = 9.2;//cm
//  double a_recoil = 0.2/6.015/931.494*9000000.0; //unit cm/us^2
 // TOF = (V_Ion_z+sqrt(pow(V_Ion_z,2.0)+Distance*2*a_recoil))/a_recoil*1000; //unit ns
 
  //Construct particle
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4String particleName;
  if (ParticleType.compare("Electron")==0){
    particleName = "e-";
  }else if (ParticleType.compare("Photon")==0){
    particleName = "gamma";
  }
  
  G4ParticleDefinition* particle = particleTable->FindParticle(particleName);
  fParticleGun->SetParticleDefinition(particle);
  fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));
  fParticleGun->SetParticleMomentumDirection(Direction); 
  fParticleGun->SetParticleEnergy(energy);

  //if(z0<-90)cout <<z0<<endl;
  //Generate event
  fParticleGun->GeneratePrimaryVertex(anEvent);
//  cout << "Main Particle: "<<particleName<<" "<<energy<<" "<<Direction[0]<<" "<<Direction[1]<<" "<<Direction[2]<<endl;

  //Generate another primary particle,happening in coincidence with the one from the decay
  /*********************************************************************/
  //electron or photon momentum and energy
  if (CoinParExist){
    P_coin_sqr = pow(P_coin[0],2.0)+pow(P_coin[1],2.0)+pow(P_coin[2],2.0);
    for (int i=0;i<3;i++){
      CoinDirection[i] = P_coin[i]/sqrt(P_coin_sqr);
    }

    if (CoinParticleType.compare("Electron")==0){
      CoinEnergy = (sqrt(P_coin_sqr+m_e*m_e)-m_e)*MeV;	//Kinetic Energy!!!
    }else if (CoinParticleType.compare("Photon")==0){
      CoinEnergy = sqrt(P_coin_sqr)*MeV;	//Kinetic Energy!!!
    }

    //Construct particle
    G4String CoinParticleName;
    if (CoinParticleType.compare("Electron")==0){
      CoinParticleName = "e-";
    }else if (CoinParticleType.compare("Photon")==0){
      CoinParticleName = "gamma";
    }

    G4ParticleDefinition* CoinParticle = particleTable->FindParticle(CoinParticleName);
    fParticleGun->SetParticleDefinition(CoinParticle);
    fParticleGun->SetParticlePosition(G4ThreeVector(x0,y0,z0));
    fParticleGun->SetParticleMomentumDirection(CoinDirection); 
    fParticleGun->SetParticleEnergy(CoinEnergy);

    //Generate event
    fParticleGun->GeneratePrimaryVertex(anEvent);
//    cout << "Coincidence Particle: "<<CoinParticleName<<" "<<CoinEnergy<<" "<<CoinDirection[0]<<" "<<CoinDirection[1]<<" "<<CoinDirection[2]<<endl;
  }
  /*********************************************************************/
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

/*void BetaTracPrimaryGeneratorAction::CalculateW_max()
{
  W_max = SpectrumMax(a,b,E_endpoint,m_e,M_r);
}*/

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracPrimaryGeneratorAction::Get_IonInfo(int *V_Ion) const
{
  for (int i=0;i<3;i++){
    V_Ion[i] = int(floor(V_Ion_initial[i]*100000.0));
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracPrimaryGeneratorAction::Get_DecayPos(short *Ion_pos) const
{
  for (int i=0;i<3;i++){
    Ion_pos[i] = short(floor(DEC_pos[i]*200.0));
  }
//  if (Ion_pos[0]/200.0<-60)cout << Ion_pos[0]/200.0<<" "<<Ion_pos[1]/200.0<<" "<<Ion_pos[2]/200.0<<endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracPrimaryGeneratorAction::Get_Init_P(int *Init_P) const
{
  for (int i=0;i<3;i++){
    Init_P[i] = P_e_in[i];
  }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
uint8_t BetaTracPrimaryGeneratorAction::Get_ParticleID() const
{
  return BetaTracPrimaryGeneratorAction::ParticleID;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_SolidAngle(G4ThreeVector Angles)
{
  Theta = Angles[0];
  Phi = Angles[1];
  Aperture = Angles[2]/180.0*PI;
//  EVGenerator->SetDirection(Theta,Phi,Aperture);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_MOT_pos(G4ThreeVector POS)
{
  for (int i=0;i<3;i++){
    MOT_pos[i] = POS[i];
  }
//  EVGenerator->SetMOTSourceParameter(MOT_rad,MOT_pos);
  //diffuse source and MOT source shared the same position
//  EVGenerator->SetDiffuseSourceParameter(D_radius,D_height,MOT_pos);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_MOT_sigma(G4ThreeVector sigma)
{
  for (int i=0;i<3;i++){
    MOT_rad[i] = sigma[i];
  }
//  EVGenerator->SetMOTSourceParameter(MOT_rad,MOT_pos);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_Diffuse_R(double radius)
{
  D_radius = radius;
//  EVGenerator->SetDiffuseSourceParameter(D_radius,D_height,MOT_pos);
}
//...oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_Diffuse_H(double h)
{
  D_height = h;
//  EVGenerator->SetDiffuseSourceParameter(D_radius,D_height,MOT_pos);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_b(G4double input_b)
{ 
  b = input_b;
//  EVGenerator->SetCorrelation(a,b);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_a(G4double input_a)
{ 
  a = input_a;
//  EVGenerator->SetCorrelation(a,b);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_Fixed_Energy(double val)
{ 
  Fixed_Energy = val; 
  //These parameters are updated at the beginning of each run
//  EVGenerator->SetConstants(m_e,M_r,ZDaughter, E_endpoint,Fixed_Energy);
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::Set_HardRatio(double val)
{ 
  HardRatio = val; 
  //These parameters are updated at the beginning of each run
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::SetSourceType(string type)
{
  SourceType = type;
//  EVGenerator->SetSourceType(SourceType);
  cout << "SourceType is set to "<<SourceType<<endl;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::SetIsotope(string iso)
{
  Isotope = iso;
  EVGenerator->SetIsotope(Isotope);
  
  if (Isotope.compare("Sr90")==0)
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],MOT_pos[2]-0.1*mm));
  else if (Isotope.compare("Bi207")==0)
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],MOT_pos[2]-0.1*mm));
  else if (Isotope.compare("Fe55")==0)
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],MOT_pos[2]-0.1*mm));
  else if (Isotope.compare("He6")==0)
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],-38.0*cm));
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracPrimaryGeneratorAction::UpdateGeneratorConstants()
{
  EVGenerator->SetDirection(Theta,Phi,Aperture);
  EVGenerator->SetMOTSourceParameter(MOT_rad,MOT_pos);
  //diffuse source and MOT source shared the same position
  EVGenerator->SetDiffuseSourceParameter(D_radius,D_height,MOT_pos);
  EVGenerator->SetCorrelation(a,b);
  EVGenerator->SetConstants(M_r,ZDaughter,E_endpoint,Fixed_Energy,Omega,HardRatio);

  EVGenerator->SetSourceType(SourceType);
  EVGenerator->SetIsotope(Isotope);
  if (Isotope.compare("Sr90")==0){
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],MOT_pos[2]-0.2*mm));
    BetaTracDetectorConstruction::Instance()->SetDegraderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],MOT_pos[2]+0.2*mm));
  }
  else if (Isotope.compare("Bi207")==0){
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],MOT_pos[2]-0.2*mm));
    BetaTracDetectorConstruction::Instance()->SetDegraderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],-37.0*cm));
  }
  else if (Isotope.compare("Fe55")==0){
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],MOT_pos[2]-0.2*mm));
    BetaTracDetectorConstruction::Instance()->SetDegraderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],-37.0*cm));
  }
  else if (Isotope.compare("He6")==0){
    BetaTracDetectorConstruction::Instance()->SetSourceHolderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],-38.0*cm));
    BetaTracDetectorConstruction::Instance()->SetDegraderPos(G4ThreeVector(MOT_pos[0],MOT_pos[1],-37.0*cm));
  }
    
  EVGenerator->SetSpectrumMax();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
string BetaTracPrimaryGeneratorAction::GetGeneratorInfo()
{
  string Output;
  char temp[1000];
  Output = Isotope + " ";
  Output += SourceType;
  Output += " ";
  sprintf(temp,"%e %e %e %e %e %e %e %e %e %e %e ",MOT_pos[0],MOT_pos[1],MOT_pos[2],MOT_rad[0],MOT_rad[1],MOT_rad[2],b,a,Theta,Phi,Aperture/PI*180.0);
  Output += temp;
  return Output;
}







