//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
// $Id$
//
/// \file BetaTracRunAction.cc
/// \brief Implementation of the BetaTracRunAction class

#include "BetaTracRunAction.hh"
#include "BetaTracRunMessenger.hh"
#include "BetaTracPrimaryGeneratorAction.hh"
#include "BetaTracEventAction.hh"
#include "BetaTracSteppingAction.hh"
#include "stdio.h"
#include "stdlib.h"
// use of other actions 
  // - primary generator: to get info for printing about the primary
  // - event action: to get and reset accumulated energy sums
  // - stepping action: to get info about accounting volume 

#include "G4Run.hh"
#include "G4RunManager.hh"
#include "G4UnitsTable.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracRunAction::BetaTracRunAction()
: generatorID(0),runID(1),RandomGenType(0),TotalN(0),G4UserRunAction()
{
  char *Directory;
  Directory=getenv("HE6_SIMULATION_DATA_DIRECTORY");
  if (Directory==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first! Default directory is used now.\n";
    Data_Direct="../Data";
  }else{
    Data_Direct=Directory;
  }
  cout <<"Data Directory is set to "<<Data_Direct.c_str()<<endl;
  theMessenger = new BetaTracRunMessenger(this);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

BetaTracRunAction::~BetaTracRunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracRunAction::set_runID(G4int ID)
{
  runID = ID;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracRunAction::set_generatorID(G4int ID)
{
  generatorID = ID;
}
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracRunAction::set_Data_Direct(G4String direct_name)
{
  Data_Direct=direct_name;
  cout <<"Data Directory is set to "<<Data_Direct.c_str()<<endl;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracRunAction::set_RandomGenType(int type)
{
  RandomGenType = type;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracRunAction::set_KeepAll(bool Switch)
{
  BetaTracEventAction::Instance()->SetKeepAll(Switch);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void BetaTracRunAction::BeginOfRunAction(const G4Run* aRun)
{ 
  G4cout << "### Run " << aRun->GetRunID() << " start." << G4endl;

  //inform the runManager to save random number seed
  G4RunManager::GetRunManager()->SetRandomNumberStore(false);
  
  //initialize primary event generator
  if(RandomGenType==0){
    BetaTracPrimaryGeneratorAction::Instance()->Set_External_Rand(false);
  }else if(RandomGenType==1){
    BetaTracPrimaryGeneratorAction::Instance()->Set_External_Rand(true);
    BetaTracPrimaryGeneratorAction::Instance()->Open_Input(Data_Direct,generatorID);
  }
  //BetaTracPrimaryGeneratorAction::Instance()->ProcessParInput(Data_Direct);
  //Update Generator's constants
  BetaTracPrimaryGeneratorAction::Instance()->UpdateGeneratorConstants();

  //initialize event cumulative quantities
  BetaTracEventAction::Instance()->Reset();
  BetaTracEventAction::Instance()->Open_Output(Data_Direct,generatorID,runID);
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void BetaTracRunAction::EndOfRunAction(const G4Run* aRun)
{
  G4int nofEvents = aRun->GetNumberOfEvent();
  if (nofEvents == 0) return;
  TotalN = nofEvents;
  
  // Compute dose
  // Not applicable for this experiment
  
  // Run conditions
  //
  const G4ParticleGun* particleGun 
    = BetaTracPrimaryGeneratorAction::Instance()->GetParticleGun();
  G4String particleName 
    = particleGun->GetParticleDefinition()->GetParticleName();                       
  G4double particleEnergy = BetaTracPrimaryGeneratorAction::Instance()
    ->GetEndPointEnergy(); 
    
  // Print
  // 
  G4int Hit1Count = BetaTracEventAction::Instance()->GetRegScintCount();
  G4int Hit2Count = BetaTracEventAction::Instance()->GetRegMWPCCount(); 
  G4int HitScint  = BetaTracEventAction::Instance()->GetHitScintCount();
  G4int HitMWPC   = BetaTracEventAction::Instance()->GetHitMWPCCount(); 
  G4int HitMCP    = BetaTracEventAction::Instance()->GetHitMCPCount();
  G4int HitWall   = BetaTracEventAction::Instance()->GetHitWallCount();
  G4int HitElect  = BetaTracEventAction::Instance()->GetHitElectrodeCount();
  G4int HitColli  = BetaTracEventAction::Instance()->GetHitCollimatorCount();
  G4int HitWindow = BetaTracEventAction::Instance()->GetHitWindowCount(); 
  G4int Scatterred = BetaTracEventAction::Instance()->GetScatterCount(); 

  G4cout
     << "\n--------------------End of Run------------------------------\n"
     << " The run consists of " << nofEvents << " "<< particleName //<<-----------------------------------------Need to include photons!!@@@!!!!!
     << " with endpoint energy of "
     <<   G4BestUnit(particleEnergy,"Energy")      
     << "\n------------------------------------------------------------\n"
     << "Number of MWPC Events "<< Hit2Count <<", " << 100*double(Hit2Count)/double(nofEvents)<<"% " <<"of total number of events"<<G4endl
     << "Number of Coincidence "<< Hit1Count <<", " << 100*double(Hit1Count)/double(nofEvents)<<"% " <<"of total number of events"<<G4endl
     << "Number of Scint hits "<< HitScint <<", " << 100*double(HitScint)/double(Hit1Count)<<"% "<<"of detected particles"<<G4endl
     << "Number of MWPC hits "<< HitMWPC <<", " << 100*double(HitMWPC)/double(Hit1Count)<<"% "<<"of detected particles" <<G4endl
     << "Number of MCP hits "<< HitMCP <<", " << 100*double(HitMCP)/double(Hit1Count)<<"% "<<"of detected particles" <<G4endl
     << "Number of Wall hits "<< HitWall <<", " << 100*double(HitWall)/double(Hit1Count)<<"% "<<"of detected particles" <<G4endl
     << "Number of Electrode hits "<< HitElect <<", " << 100*double(HitElect)/double(Hit1Count)<<"% "<<"of detected particles" <<G4endl
     << "Number of Collimator hits "<< HitColli <<", " << 100*double(HitColli)/double(Hit1Count)<<"% "<<"of detected particles" <<G4endl
     << "Number of Window hits "<< HitWindow <<", " << 100*double(HitWindow)/double(Hit1Count)<<"% "<<"of detected particles" <<G4endl
     << "Number of total scattering events " << Scatterred <<", "<<100*double(Scatterred)/double(Hit1Count)<<"% "<<"of detected particles"<<G4endl
     << "\n------------------------------------------------------------\n"
     << G4endl;

  //Output Histograms
  BetaTracEventAction::Instance()->OutputHistograms();
  BetaTracEventAction::Instance()->WriteToEventFile();

  //Close input file and output file
  if(RandomGenType==1){
    BetaTracPrimaryGeneratorAction::Instance()->Close_Input();
  }
  BetaTracEventAction::Instance()->Close_Output();
  Logging();
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


void BetaTracRunAction::Logging()
{
  G4int OutputCount = BetaTracEventAction::Instance()->GetOutputN();
  string LogFileDirect = Data_Direct + "/Setup";
  char FileName[100];
  sprintf(FileName,"%s/BetaSetup%03d%02d.log",LogFileDirect.c_str(),generatorID,runID);
  ofstream LogOut;
  LogOut.open(FileName,ios::out);
  if (RandomGenType==0){
    LogOut << "Internal "<<BetaTracPrimaryGeneratorAction::Instance()->GetGeneratorInfo().c_str() <<TotalN<<" " <<OutputCount <<endl;
  } else if(RandomGenType==1){
    LogOut << "External " <<TotalN<<" " <<OutputCount <<endl;
  } else{
    ;
  }
  cout << "Logged to " << FileName <<endl;
}

