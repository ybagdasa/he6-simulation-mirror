//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
// $Id: BetaTracRunMessenger.cc,v 1.31 2007-11-16 22:37:43 asaim Exp $
// GEANT4 tag $Name: not supported by cvs2svn $
//

#include "BetaTracRunMessenger.hh"
#include "BetaTracRunAction.hh"
#include "G4UIdirectory.hh"
#include "G4UIcmdWithoutParameter.hh"
#include "G4UIcmdWithAString.hh"
#include "G4UIcmdWithAnInteger.hh"
#include "G4UIcmdWithABool.hh"
#include "G4UIcommand.hh"
#include "G4UIparameter.hh"
#include "G4UImanager.hh"
#include "G4ProductionCutsTable.hh"
#include "G4Tokenizer.hh"
#include "Randomize.hh"
#include <sstream>

BetaTracRunMessenger::BetaTracRunMessenger(BetaTracRunAction * runAct)
:runAction(runAct)
{
  SetupDirectory = new G4UIdirectory("/Setup/");
  SetupDirectory->SetGuidance("Setup Random Generator, file number, etc.");

  SetRandomGen = new G4UIcmdWithAString("/Setup/SetRandomGen",this);
  SetRandomGen->SetGuidance("External or Internal Random Generator?");
  SetRandomGen->SetParameterName("RandomGen",true);
  SetRandomGen->SetDefaultValue("External");
  SetRandomGen->SetCandidates("External Internal");

  SetKeepAll = new G4UIcmdWithAString("/Setup/SetKeepAll",this);
  SetKeepAll->SetGuidance("Keep all data?");
  SetKeepAll->SetParameterName("KeepAll",true);
  SetKeepAll->SetDefaultValue("False");
  SetKeepAll->SetCandidates("True False");

  SetGeneratorID = new G4UIcmdWithAnInteger("/Setup/SetGeneratorID",this);
  SetGeneratorID->SetGuidance("The number to lable generator ID (for input).");
  SetGeneratorID->SetParameterName("GeneratorID",true);
  SetGeneratorID->SetDefaultValue(0);
  SetGeneratorID->SetRange("GeneratorID>=0");

  SetRunID = new G4UIcmdWithAnInteger("/Setup/SetRunID",this);
  SetRunID->SetGuidance("The number to lable run ID (for output).");
  SetRunID->SetParameterName("RunID",true);
  SetRunID->SetDefaultValue(0);
  SetRunID->SetRange("RunID>=0");

  SetDataDirect = new G4UIcmdWithAString("/Setup/SetDataDirect",this);
  SetDataDirect->SetGuidance("Set the directory where data are read and written.");
  SetDataDirect->SetParameterName("DataDirect",true);
  SetDataDirect->SetDefaultValue("../Data");

}

BetaTracRunMessenger::~BetaTracRunMessenger()
{
  delete SetRandomGen;
  delete SetKeepAll;
  delete SetGeneratorID;
  delete SetRunID;
  delete SetDataDirect;
  delete SetupDirectory;
}

void BetaTracRunMessenger::SetNewValue(G4UIcommand * command,G4String newValue)
{
  if( command==SetRandomGen )
  {
    if(newValue == "External"){
      runAction->set_RandomGenType(1);
    }else if(newValue == "Internal"){
      runAction->set_RandomGenType(0);
    }
  }
  if( command==SetKeepAll)
  {
    if(newValue == "True"){
      runAction->set_KeepAll(true);
    }else if(newValue == "False"){
      runAction->set_KeepAll(false);
    }
  }
  else if( command==SetGeneratorID )
  {
    runAction->set_generatorID(SetGeneratorID->GetNewIntValue(newValue));
  }
  else if( command==SetRunID )
  {
    runAction->set_runID(SetRunID->GetNewIntValue(newValue));
  }
  else if( command==SetDataDirect )
  {
    runAction->set_Data_Direct(newValue);
  }

}

/*
G4String BetaTracRunMessenger::GetCurrentValue(G4UIcommand * command)
{
  G4String cv;
  return cv;
}*/

