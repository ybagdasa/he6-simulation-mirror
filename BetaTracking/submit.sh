#!/bin/bash
#PBS -e STDErr.txt
#PBS -o STDOut.txt
#PBS -l walltime=18:00:00
cd $PBS_O_WORKDIR
. setup.sh
./BetaTracking run.mac
echo Done!
