/**************************************************
Simulate Shake-off electrons and recoil ions
**************************************************/

//Std includes
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

//Root includes
#include "TROOT.h"
#include "TStyle.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TH1.h"
#include "TH2.h"
#include "TCanvas.h"

//Project includes
#include "UFTracking.h"
#include "Processor.h"
#include "EventDataStruct.h"

//Define macros
#define PI 3.14159265358

//main program

using namespace std;

int main(int argc,char **argv)
{
    
    
  //Data directory
  char* DATA_DIRECTORY;
  //File names
  char inputfilename[100];
  char outputfilename[100];
  char buffer[100];		//Input buffer
  //file streams
  ifstream InParFile;
  ifstream InDataFile;
  //Input parameters
  double E_endpoint;
  double M_r;
  double m_e;
  double E_field = -0.002;
  double KE_recoil = 0;
  double c = 30000.00;// 29979.2458;	//Speed of light, in cm/us^2
  int GeneratorID;
  string ModuleName;
  int ModuleType = 0;
  double IonMCPPosZ = -9.2;
  int N_of_File = 1;
  double TOF_width = 0.5;  //ns
  double MCP_width = 0.1; //mm
  int RecoilShakeOffsID; // 1 -> turns ON recoil dependent shakeoffs : 0 -> OFF

  //Input Data
  int eventID;
  uint8_t ParticleID;
  int P_e_in[3];
  int V_Ion_in[3];
  short DEC_pos_in[3];
  //Double versions of parameters
  double P_e[3];
  double V_Ion[3];
  double DEC_pos[3];
  //Data for processor
  double TOF;			//Time of flight
  double X_hit;
  double Y_hit;			//Hit positions on MCP,in mm
  double Ek_Ion;		//Kinetic energy of ion
  double HitAngle;		//Hit angle on MCP
  uint8_t Status;
  
  InBetaDataStruct_D DummyBeta_Data;
  InIonDataStruct_D InIon_Data;
  OutDataStruct_D Out_Data[N_OF_OUT];
  double E_Init;
  double total = 0;
  
  int OutputID;
  //Counters
  int counts;
  
  //Get data directory
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (DATA_DIRECTORY==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  
  //Check input argument
  if (argc<5 || argc>5){
    cout << "Usage: ShakeOffSimulation GeneratorID OutputID TrackingModule RecoilShakeOffsID\n";
    return -1;
  }
  GeneratorID = atoi(argv[1]);
  OutputID = atoi(argv[2]);
  ModuleName = argv[3];
  RecoilShakeOffsID = atoi(argv[4]);
  if (ModuleName.compare("UF")==0) ModuleType = 0;
    
  
  //Read parameter
  sprintf(inputfilename,"%s/Input",DATA_DIRECTORY);
  InParFile.open(inputfilename,ios::in);
  
  if(!InParFile.is_open()){
    cout << "Can't open input file!\n" <<endl;
    return -1;
  }
  cout <<"Input parameters:\n";
  InParFile.getline(buffer,100);
  while(buffer[0]!='\0'){
    if (buffer[0] == '#'){
      InParFile.getline(buffer,100);
      continue;
    }
    std::string StrComp(buffer);
    if (StrComp.compare("E_endpoint")==0){
      InParFile >> E_endpoint;
      cout << "E_endpoint= "<<E_endpoint<<endl;
      InParFile.get();
    }
    else if (StrComp.compare("M_r")==0){
      InParFile >> M_r;
      cout << "M_r= "<<M_r<<endl;
      InParFile.get();
    }
    else if (StrComp.compare("m_e")==0){
      InParFile >> m_e;
      cout << "m_e= "<<m_e<<endl;
      InParFile.get();
    }
    else{
      ;
    }
    InParFile.getline(buffer,100);
  }
  InParFile.close();
  
  //Construct Ion tracking class and Processor Class
  UFTracking* UFTracker = new UFTracking(E_field,IonMCPPosZ,M_r);  //Efield,MCP position,M_r
  Processor* TheProcessor = new Processor(N_of_File);  //Process the ion information and apply detector response
  
  //Initialize Processor
  for (int i=0;i<N_of_File;i++){
    TheProcessor->SetParameter("TOFWidth",i,TOF_width);
    TheProcessor->SetParameter("MCPWidth",i,MCP_width);
  }
  
  //Define Output Histograms
  TH1 *hTOF = new TH1D("hTOF","TOF histogram [ns]",200,120,220);
  TH2 *hImage = new TH2D("hImage","MCP Image",500,-100,100,500,-100,100);
  TH2 *hTOF_vs_RHit = new TH2D("hTOF_vs_RHit","TOF vs R_Hit",200,120,220,200,0,40);
  TH1 *hEIonInitial = new TH1D("hEIonInitial","EIonInitial histogram;eV",1600,0,1600);
  
  //Read input data
  sprintf(inputfilename,"%s/OriginalEvent%03d.dat",DATA_DIRECTORY,GeneratorID);
  InDataFile.open(inputfilename,ios::in | ios::binary);
  
  counts = 0;
  while(1){ 
    InDataFile.read((char *)&eventID,sizeof(int)); //Original event ID
    if (InDataFile.eof())break;
    InDataFile.read((char *)DEC_pos_in,3*sizeof(short)); //decay position
    InDataFile.read((char *)P_e_in,3*sizeof(int)); //Electron momentum
    InDataFile.read((char *)V_Ion_in,3*sizeof(int)); //Recoil velocity
    InDataFile.read((char *)&ParticleID,sizeof(uint8_t)); //Particle type
    for (int i=0;i<3;i++){
      DEC_pos[i] = double(DEC_pos_in[i]/1000.0);//mm
      P_e[i] = double(P_e_in[i]/1000.0);//keV
      V_Ion[i] = double(V_Ion_in[i]/100000.0);//cm/us
        KE_recoil = 0.5*M_r*(V_Ion[0]*V_Ion[0]+V_Ion[1]*V_Ion[1]+V_Ion[2]*V_Ion[2])/c/c*1000000.0;
    }
  
    /**************************************Module Dependent***********************************************************/
    if (ModuleType==0){	//UFTracking
      UFTracker->SetIonInfo(2.0,DEC_pos,V_Ion);	//Set ion initial information
      UFTracker->Track();					//Track ion
      UFTracker->GetOutput(TOF,X_hit,Y_hit,Ek_Ion,HitAngle,Status);
    }
    /**************************************Module Dependent***********************************************************/
    
    //Processor>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>
    InIon_Data.TOF = TOF;
    InIon_Data.HitPos[0] = X_hit;
    InIon_Data.HitPos[1] = Y_hit;
    InIon_Data.HitAngle = HitAngle;
    InIon_Data.IonEnergy = Ek_Ion;
    
    TheProcessor->SetInput(&DummyBeta_Data,&InIon_Data);
    TheProcessor->SetOutput(Out_Data);
      
    
    TheProcessor->ApplyTOFWidth();
    TheProcessor->ApplyMCPResponse();
    
    /**************************************Module Dependent***********************************************************/
    UFTracker->TrackBack(Out_Data[0].TOF,Out_Data[0].HitPos[0],Out_Data[0].HitPos[1],E_Init);
    /**************************************Module Dependent***********************************************************/
    //<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<<
      
    //Fill histogram
    double R = sqrt(pow(Out_Data[0].HitPos[0],2.0)+pow(Out_Data[0].HitPos[1],2.0));
    hTOF->Fill(Out_Data[0].TOF,1.0+0.06*(KE_recoil/1400)*RecoilShakeOffsID);
    hImage->Fill(Out_Data[0].HitPos[0],Out_Data[0].HitPos[1],1.0+0.06*(KE_recoil/1400)*RecoilShakeOffsID);
    hTOF_vs_RHit->Fill(Out_Data[0].TOF,R,1.0+0.06*(KE_recoil/1400)*RecoilShakeOffsID);
    hEIonInitial->Fill(E_Init,1.0+0.06*(KE_recoil/1400)*RecoilShakeOffsID);
      total +=((1+0.06*(KE_recoil/1400))/10000000)*RecoilShakeOffsID;
    counts++;
  }
    if(RecoilShakeOffsID > 0){
        cout << "Probability of shakeoff from recoil = " << 1.0 - (1.0/total) << endl;
    }
    
  InDataFile.close();
  //Output Histograms
  sprintf(outputfilename,"%s/../Spectra/ShakeOffSimulationTOFSpec%03d%02d.pdf(",DATA_DIRECTORY,GeneratorID,OutputID);
  TCanvas *canv = new TCanvas("canv","Time of Flight",0,0,800,600);
  hTOF->Draw();
  canv->SaveAs(outputfilename);
  hImage->Draw("COLZ");
  sprintf(outputfilename,"%s/../Spectra/ShakeOffSimulationTOFSpec%03d%02d.pdf",DATA_DIRECTORY,GeneratorID,OutputID);
  canv->SaveAs(outputfilename);
  hTOF_vs_RHit->Draw("COLZ");
  canv->SaveAs(outputfilename);
  hEIonInitial->Draw();
  sprintf(outputfilename,"%s/../Spectra/ShakeOffSimulationTOFSpec%03d%02d.pdf)",DATA_DIRECTORY,GeneratorID,OutputID);
  canv->SaveAs(outputfilename);
  
  sprintf(outputfilename,"%s/../Spectra/ShakeOffSimulationHist%03d%02d.root",DATA_DIRECTORY,GeneratorID,OutputID);
  TFile *OutRootFile = new TFile(outputfilename,"RECREATE");
  hTOF->Write();
  hImage->Write();
  hTOF_vs_RHit->Write();
  hEIonInitial->Write();
  OutRootFile->Close();
  
  delete UFTracker;
  delete canv;
  delete OutRootFile;
  delete hTOF;
  
  return 0;
}












