//Ran Hong, Dec 2012
//Analyze the event file from the beta detector simulation
//Draw histograms, apply cuts

//C++ includes
#include <iostream>
#include <math.h>
#include <fstream>
#include <string>
#include <vector>

//ROOT includes
#include "TROOT.h"
#include "TStyle.h"
#include "TH1.h"
#include "TH2.h"
#include "TMath.h"
#include "TFile.h"
#include "TCanvas.h"

using namespace std;

int main(int argc, char **argv)
{
  string filename;	//input/output file name
  ifstream filein;
  ifstream fileinb;
  ifstream fileinr;
  
  int    EventID;
  double DEC_pos[3];
  double P_e[3];
  double P_r[3];
  double E_e;
  double E_r;
  double E_eComp;
  double E_rComp;
  int counts;
  
  double mE = 0.510998910;  //MeV
  double mR = 5603.051;     //MeV

  if(argc<4 || argc>4){
    cout<< "Usage: ./compare EventFileName BetaCompare IonCompare\n";
    return -1;
  }

  filename = argv[1];
  filein.open(filename.c_str(),ios::in | ios::binary);
  if(filein.fail()){
    cout << "No such file!\n";
    return -1;
  }
  
  filename = argv[2];
  fileinb.open(filename.c_str(),ios::in);
  if(fileinb.fail()){
    cout << "No such file!\n";
    return -1;
  }
  
  filename = argv[3];
  fileinr.open(filename.c_str(),ios::in);
  if(fileinr.fail()){
    cout << "No such file!\n";
    return -1;
  }

  //Define histograms
  TH1 *BetaEnergy = new TH1D("BetaEnergy","Beta energy",500,0,4000);
  TH1 *IonEnergy  = new TH1D("IonEnergy","Ion energy",500,0,1500);
  TH1 *BetaEnergyComp= new TH1D("BetaEnergyComp","Beta Energy Comp",500,0,4000);
  TH1 *IonEnergyComp= new TH1D("IonEnergyComp","Ion Energy Comp",500,0,1500);
  
  TH1 *DifferBeta= new TH1D("DifferBeta","Difference Beta",500,0,4000);
  TH1 *DifferIon= new TH1D("DifferIon","Difference Ion",500,0,1500);

  while(1)
  {
    filein.read((char *)&EventID,sizeof(int));
    if(filein.eof())break;
    
    filein.read((char *)DEC_pos,3*sizeof(double));
    filein.read((char *)P_e,3*sizeof(double));
    filein.read((char *)P_r,3*sizeof(double));
    
    E_e = (sqrt(pow(P_e[0],2.0)+pow(P_e[1],2)+pow(P_e[2],2.0)+pow(mE,2.0))-mE)*1000.0;
    E_r = (P_r[0]*P_r[0] + P_r[1]*P_r[1] + P_r[2]*P_r[2])/2.0/mR*1000000.0;
    
    BetaEnergy->Fill(E_e);
    IonEnergy->Fill(E_r);
  }
  
  while(1)
  {
    fileinb>>E_eComp;
    if(fileinb.eof())break;
    fileinb>>counts;
//    cout <<E_eComp/1000.0<<" "<<counts<<endl;
    
    BetaEnergyComp->Fill(E_eComp/1000.0,counts);
  }

  while(1)
  {
    fileinr>>E_rComp;
    if(fileinr.eof())break;
    fileinr>>counts;
//    cout <<E_rComp<<" "<<counts<<endl;
    
    IonEnergyComp->Fill(E_rComp,counts);
  }

  for(int i=1;i<=500;i++){
    double difference = BetaEnergy->GetBinContent(i)-BetaEnergyComp->GetBinContent(i);
    double norm = sqrt(pow(BetaEnergy->GetBinError(i),2.0)+pow(BetaEnergyComp->GetBinError(i),2.0));
    if (norm !=0 )DifferBeta->SetBinContent(i,difference/norm);
  }
  
  for(int i=1;i<=500;i++){
    double difference = IonEnergy->GetBinContent(i)-IonEnergyComp->GetBinContent(i);
    double norm = sqrt(pow(IonEnergy->GetBinError(i),2.0)+pow(IonEnergyComp->GetBinError(i),2.0));
    if (norm !=0 )DifferIon->SetBinContent(i,difference/norm);
  }

  gStyle->SetOptStat("");

  TCanvas *canv = new TCanvas("canv","Generator Compare",0,0,800,600);
  canv->Divide(2,2);
  canv->cd(1);
  BetaEnergy->GetXaxis()->SetTitle("Energy [keV]");
  BetaEnergy->GetXaxis()->SetTitleFont(60);
  BetaEnergy->GetXaxis()->SetLabelFont(60);
  BetaEnergy->GetYaxis()->SetTitle("Counts");
  BetaEnergy->GetYaxis()->SetTitleFont(60);
  BetaEnergy->GetYaxis()->SetLabelFont(60);
  BetaEnergy->Draw();
  BetaEnergyComp->SetLineColor(kRed);
  BetaEnergyComp->Draw("same");
  canv->cd(2);
  IonEnergy->GetXaxis()->SetTitle("Energy [eV]");
  IonEnergy->GetXaxis()->SetTitleFont(60);
  IonEnergy->GetXaxis()->SetLabelFont(60);
//  IonEnergy->GetXaxis()->SetRangeUser(1300,1420);
  IonEnergy->GetYaxis()->SetTitle("Counts");
  IonEnergy->GetYaxis()->SetTitleFont(60);
  IonEnergy->GetYaxis()->SetLabelFont(60);
  IonEnergy->Draw();
  IonEnergyComp->SetLineColor(kRed);
  IonEnergyComp->Draw("same");
  canv->cd(3);
  DifferBeta->GetXaxis()->SetTitle("Energy [keV]");
  DifferBeta->GetXaxis()->SetTitleFont(60);
  DifferBeta->GetXaxis()->SetLabelFont(60);
  DifferBeta->GetYaxis()->SetTitle("Diff. Counts");
  DifferBeta->GetYaxis()->SetTitleOffset(1.4);
  DifferBeta->GetYaxis()->SetTitleFont(60);
  DifferBeta->GetYaxis()->SetLabelFont(60);
  DifferBeta->Draw();
  canv->cd(4);
  DifferIon->GetXaxis()->SetTitle("Energy [eV]");
  DifferIon->GetXaxis()->SetTitleFont(60);
  DifferIon->GetXaxis()->SetLabelFont(60);
  DifferIon->GetYaxis()->SetTitle("Diff. Counts");
  DifferIon->GetYaxis()->SetTitleOffset(1.4);
  DifferIon->GetYaxis()->SetTitleFont(60);
  DifferIon->GetYaxis()->SetLabelFont(60);
  DifferIon->Draw();

  canv->SaveAs("Spectra.pdf");
  

  //Save coin. histograms to root file
  TFile *OutFile = new TFile("Histograms.root","RECREATE");
  BetaEnergy->Write();
  IonEnergy->Write();
  OutFile->Close();
  filein.close();
  fileinb.close();
  fileinr.close();
  cout<<"Done!\n";
  return 0;
}
