#!/bin/bash
echo Building EventGenerator
cd EventGenerator
make
cd ..
echo Building BetaTracking
cd BetaTracking
make
cd ..
echo Building IonTracking
cd IonTracking
make
cd ..
echo Building PostProcessor
cd PostProcessor
make
cd ..
echo Done!
