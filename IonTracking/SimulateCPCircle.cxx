#include "Potential.h"
#include "VGridUni.h"
#include "Tracking.h"
#include "TrackingCircle.h"
#include "UFTracking.h"
#include "Toolbox.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>

//#define CSPEED 299.792458

using namespace std;
double K2V(double K, double mass);
void IonGenerator(double Emax, double Emin, double Thetamax, double Thetamin, double mass, double pos[3], double vel[3]);


int main() {
	//Data directory
  char* DATA_DIRECTORY;
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (DATA_DIRECTORY==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  srand(23);
	
  string Efile, Bfile;
  double M_Li6 = 5603.051; //Mass of Lithium-6 [MeV]
  double M_He4 = 3727.379; //Mass of Helium-4 [MeV]
	//double pos[3] = {2.123,2.95,-0.97};
	double T, T2,X, Y, Z, Vx, Vy, Vz, E, THETA,Q;
	uint8_t STATUS;
	
	double k = .1*50000*300; // [G] = [.1 V ns/mm^2]
	double timestep[7] = {0.2272, 0.1541, 0.1167, 0.0940, 0.0788, 0.0678, 0.0595};
	double v[7] = {.1,.15,.2,.25,.3,.35,.4};


	TrackingCircle letstrack(M_Li6,99,-100,100,0,1,k,30); //mass,R0,Zi, Zf, nE, nB, k, numoforbits;
	
	Efile = DATA_DIRECTORY;
	Efile += "/../EFieldFiles/EGridUF1mm39000.dat";
	Bfile = DATA_DIRECTORY;
	Bfile += "/../EFieldFiles/BGridUF1mm1500T.dat";
	//Efile += "/EfieldTextFiles/EGridCentral1mm.txt";
	//letstrack.InitializeFields(Efile.c_str(),"E");
	letstrack.InitializeFields(Bfile.c_str(),"B");
	
	ofstream OutIonInfo;
	OutIonInfo.open("IonInfoOutput.txt",ios::out);
	for(int i = 0;i<7;i++){
		printf("\n\n\n\n\n\n\n\n\nNEW ION!!!!!!!!!!!!!!!!!!!!!!!!!!\n");
		//letstrack.SetIonInfo(v[i],1);
		letstrack.SetIonInfoB(CSPEED*2*v[i],1);
		//letstrack.TrackFixStp(timestep[i]);
		letstrack.TrackVarStp(1);
		letstrack.GetOutputFull(T, X, Y, Z, Vx, Vy, Vz, E, THETA, STATUS, Q);
		if(STATUS ==0){
			//printf("vx = %f mm/ns\t vx = %f mm/ns\t vx = %f mm/ns\t Zhit = %f mm\n",Vx,Vy,Vz,Z);
			//printf("N = %i\t xhit = %f mm\t yhit = %f mm\t t = %f ns\t theta = %f deg\t E = %f keV\n",i,X, Y, T,THETA,E, Q);
			OutIonInfo<<i<<" "<<Q<<" "<<Vx<<" "<<Vy<<" "<<Vz<<" "<<X<<" "<<Y<<" "<<Z<<" "<<T<<endl;
		}

		
	}
	OutIonInfo.close();
	return 0;
}



void IonGenerator(double Emax, double Emin, double Thetamax, double Thetamin, double mass, double pos[3], double vel[3]){
	double E,v,tang,theta,phi,vr,vz;
	E = Emin + (Emax-Emin)*rand01();
	v = K2V(E,mass);
	do{
		theta = sqrt(rand01())* PI/2;
		tang = tan(theta);
		vz = sqrt(v*v/(1+tang*tang));
		if (rand01()<.5){vz*=-1;}
	}while (acos(v/vz)<Thetamin ||acos(v/vz)>Thetamax);
	phi = rand01()*2*PI;
	vr = vz*tang;
	vel[2] = vz;
	vel[0] = vr*cos(phi);
	vel[1] = vr*sin(phi);
}
