#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <stdio.h>
#include <stdlib.h>

#define IN2MM 25.4

using namespace std;

int main(int argc, char **argv){

  //Data directory
  char *DATA_DIRECTORY, *Efile, *Hfile, *units;
  char filename[1000];
  
  double x0, y0, z0, x1, y1, z1, x, y, z, Ex, Ey, Ez, V;
  int nx, ny, nz, convert, elements;
  
  ofstream OutFile;
  
    //Check input argument
  if (argc != 4){
		cout << "Usage: Efile Hfile units\n";
		return -1;
	}
	else{
		Efile = argv[1];
		Hfile = argv[2];
		units = argv[3];
	}
	
    //Get data directory
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (DATA_DIRECTORY==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  
  if(strcmp(units, "mm")==0){convert = 0;} 
	else if (strcmp(units, "in")==0){convert = 1;}
	else{ 
		printf("Unrecognizable units %s. Input must be 'mm' or 'in'.\n",units);
		return -1;
	}
	
	sprintf(filename,"%s/../EFieldFiles/EFieldTextFiles/%s.txt",DATA_DIRECTORY, Hfile);	
	//Read header file
	elements = 0;
	FILE* fid = fopen(filename,"r");
	if (fid==NULL){
		printf("%s does not exist.\n", filename);
		return -1;
	}
	else{
		char buffer[100];
		fgets(buffer,100,fid);
		elements = fscanf(fid,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%i\t%i\t%i\t",&x0,&y0,&z0,&x1,&y1,&z1,&nx,&ny,&nz);	
		fclose(fid);
		
		if (elements != 9){
			printf("%i out of %i elements read from %s \n",elements,9,filename);
			return -1;
		}
	}
	
	//Open binary file for writing
	sprintf(filename,"%s/../EFieldFiles/%s.dat",DATA_DIRECTORY, Efile);
	OutFile.open(filename, ios::binary);
	if(OutFile.fail()){
		cout<< "Output file " <<filename<<" could not be opened for writing.\n";
		return -1;
	}
	OutFile.write((char*) &x0, sizeof(double));
	OutFile.write((char*) &y0, sizeof(double));
	OutFile.write((char*) &z0, sizeof(double));
	OutFile.write((char*) &x1, sizeof(double));
	OutFile.write((char*) &y1, sizeof(double));
	OutFile.write((char*) &z1, sizeof(double));
	OutFile.write((char*) &nx, sizeof(int));
	OutFile.write((char*) &ny, sizeof(int));
	OutFile.write((char*) &nz, sizeof(int));
	
	//Read source file
	sprintf(filename,"%s/../EFieldFiles/EFieldTextFiles/%s.txt",DATA_DIRECTORY, Efile);	
	//sprintf(filename,"%s%s.txt","/mnt/",Efile);
	elements = 0;
	fid = fopen(filename,"r");
	if (fid==NULL){
		printf("ERROR: %s does not exist.\n", filename);
		return false;
	}
	else{
		for(int i=0; i<nx; i++){
			for(int j=0; j<ny; j++){
				for(int k=0; k<nz; k++){
					elements += fscanf(fid,"%lf\t %lf\t %lf\t %lf\t %lf\t %lf\t %lf\n",&x,&y,&z,&V,&Ex,&Ey,&Ez);
					if(convert){
						x = IN2MM*x;
						y = IN2MM*y;
						z = IN2MM*z;
					}
					//write to binary
					OutFile.write((char*) &x, sizeof(double));
					OutFile.write((char*) &y, sizeof(double));
					OutFile.write((char*) &z, sizeof(double));
					OutFile.write((char*) &V, sizeof(double));
					OutFile.write((char*) &Ex, sizeof(double));
					OutFile.write((char*) &Ey, sizeof(double));
					OutFile.write((char*) &Ez, sizeof(double));

						
				}
			}
		}	
		
		fclose(fid);
		if (elements != nx*ny*nz*7){
			printf("%i out of %i elements read from %s\n",elements,nx*ny*nz*7,filename);
			return -1;
		}
		else{
			printf("%i out of %i elements read from %s\n",elements,nx*ny*nz*7,filename);
		}
		OutFile.close();
	}
  
  return 0;
		
}
