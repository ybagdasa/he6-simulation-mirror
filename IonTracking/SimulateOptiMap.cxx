#include "Optimizer.h"
#include <fstream>
#include <iostream>
#include <cmath>
//Root includes
#include "TROOT.h"
#include "TFitter.h"
//#include "TStyle.h"
//#include "TRandom3.h"
//#include "TFile.h"



using namespace std;

Optimizer *myfields;
void minuitFunctionWrapper(int& nDim, double *gout, double &result, double par[], int flg){
	myfields->minuitFunction(nDim,gout,result,par,flg); //execute member function
}
int main(int argc,char **argv){

  time_t startTime, endTime, iTime0, iTime1;
  double timeSec,timeMin;
  time(&startTime); //time at start of simulation
  
  
	const int nfiles = 9;
	double radius, zlow, zhi;
	//double potentials[10] = {-320,-500,-3000,-2000,2000,6000,10000,14000,18000,22000};
	//double potentials[nfiles] = {-320,-2000,2000,6000,10000,14000,18000,22000};
	double potentials[nfiles] = {-320,-2000,2000,6000,10000,14000,18000,22000,-2535};
	double k0[nfiles];
	double dk[nfiles];
	double atemp[nfiles];
	double ChiMin, ChiNew, Emean, Emin;
	double E0 = -155;
	double newPotens[nfiles];
	char ScaledName[300]; //Field filename to export
	double maxDiff = 5000; //max diff between sequential potentials (MCP thru E6)
	string response;

	char* DATA_DIRECTORY;  
	char* FIELD_DIRECTORY;
	string pFile, FitType;
	int cnt;
	
	//Get data directory
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");
  FIELD_DIRECTORY=getenv("HE6_FIELD_MAPS_DIRECTORY");

  if (DATA_DIRECTORY==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  if (FIELD_DIRECTORY==NULL) {
    cout << "The environment variable HE6_FIELD_MAPS_DIRECTORY is not defined! Set it up to the field data directory first!\n";
    return -1;
  }
	
	if (argc<6 || argc>7){
		cout << "Usage:\n";
		cout << "FitType E0[V/mm] zlo[mm] zhi[mm] radius[mm] (ParameterFile.txt)\n";
		return -1;
	}	


	
  E0 = atof(argv[2]);
  FitType = argv[1];
  zlow = atof(argv[3]);
  zhi = atof(argv[4]);
  radius = atof(argv[5]);
	
	if(argc==7) pFile = argv[6];
	else pFile = "Linear"; //this will be used for the FitLog filename
	
	if (FitType.compare("MINUIT")!=0 && FitType.compare("GRADLS")!=0 && FitType.compare("LINEAR")!=0 && FitType.compare("SCALE")!=0){
		cout << "Unrecognizable fit type "<<FitType<<". Choose either MINUIT or GRADLS or LINEAR or SCALE."<<endl;
		return -1;
	}
	string EFieldFullPath = FIELD_DIRECTORY;
	EFieldFullPath +="/JulyGeoNew_BaseMaps.list";
//	EFieldFullPath +="/BeWindowNew_BaseMaps.list";
	//EFieldFullPath +="/../EFieldFiles/BeWindowOld_BaseMaps.list";
	//EFieldFullPath +="/../EFieldFiles/CurrentSpacingsBaseVoltageMapOptimizewREG.txt";
	string ParamFullPath = FIELD_DIRECTORY;
	ParamFullPath += "/";
	ParamFullPath += pFile;
	
	//*****Initialize parameters for all fit routines**********
	for (int i=0;i<nfiles;i++){
		k0[i] = 1;
		dk[i] = .1;
		atemp[i] = 1;
		newPotens[i] = 0;
	}	
	myfields = new Optimizer(nfiles, EFieldFullPath.c_str(), potentials, radius, zlow, zhi, E0);
	myfields->SetParams(k0);
	myfields->SetParamIncr(dk);
//	myfields->FixPotensDiff(2,1, 4000); //Fix MCP Back wrt MCP Front
//	myfields->FixPotensDiff(2,0, 4000+180); //Fix Holder Block wrt MCP Front
//	myfields->FixPotensDiff(2,3, 1000); //Fix Grid wrt MCP Front
//	myfields->FixPotensDiff(2,2, 0); //Fix MCP Front
	
//	myfields->FixPotensDiff(2,1, 2500); //Fix MCP Back wrt MCP Front
//	atemp[8] = 0;//vReg
//	atemp[0] = 0;//vBlock
//	myfields->SetParamsExternal(atemp);//skips over fixed params
	
	//myfields->FixPotensDiff(1,1, 0); //Fix MCP Front/VGrid
	myfields->FixPotential(1,-2000);
	//myfields->FixPotensDiff(1,0, 1000);//Fix Holder Block wrt MCP Front
	myfields->FixPotensDiff(0,0, 0);//Fix Holder Block
	//myfields->FixPotensDiff(8,8, 0); //Fix vReg
	//Fix E1 thru E6
//	myfields->FixPotensDiff(2,2,0);
//	myfields->FixPotensDiff(3,3,0);
//	myfields->FixPotensDiff(4,4,0);
//	myfields->FixPotensDiff(5,5,0);
//	myfields->FixPotensDiff(6,6,0);
//	myfields->FixPotensDiff(7,7,0);
	
	const int nfits = 6;
	//************Initialize root fitter***********
	TFitter* minuit = new TFitter(nfits);
	string paramName[nfits];
	char buffer[40];
	double par[nfits]; //fitter paramters
	double parlo[nfits],parhi[nfits]; //lower and upper bounds of parameters
	if (FitType.compare("MINUIT")==0){	
		double p1 = -1;
		minuit->ExecuteCommand("SET PRINTOUT",&p1,1); //make it quiet!
		minuit->SetFCN(minuitFunctionWrapper); //select chi2 function to minimize
		for(int i=0;i<nfits;i++) {
			sprintf(buffer,"a%02d",i+2);
			paramName[i] = string(buffer);
		}
		double PotensHi[nfiles] = {0,0,5000,10000,15000,20000,25000,30000};//,0}; //max voltage bounds
		myfields->CalcPotens2Params(PotensHi, atemp);
		for(int i=0;i<nfits;i++) {
			parhi[i] = atemp[i+2];
			cout<<parhi[i]<<endl;
		}
		double PotensLo[nfiles] = {0,-3000,0,0,0,0,0,0};//,-5000}; //min voltage bounds
		myfields->CalcPotens2Params(PotensLo, atemp);
		for(int i=0;i<nfits;i++){
			parlo[i] = atemp[i+2];
			cout<<parlo[i]<<endl;
		}			
	}
	
//**********Create and open log file ******************		
	ofstream LogFile;
	string logpath = FIELD_DIRECTORY;
	logpath += "/OptimizerFitLog_";
	logpath += pFile;
	LogFile.open(logpath.c_str(),ios::app);
	
//************* NONLINEAR FITTING ROUTINES WITH ITERATIVE START VALUES **************		
	if(FitType.compare("MINUIT")==0 || FitType.compare("GRADLS")==0){
		double chi2 = 1e-12; //chi2 cut-off	
		ChiMin = myfields->CalcChiSq(); //initial value for minimum chi2

	 
	 	ifstream param_file;
	 	param_file.open(ParamFullPath.c_str());
	 	if (param_file.fail()){
	 		cout << "Error opening file " <<ParamFullPath<<"!"<<endl;
	 		return -1;
	 	}
	 	
	 	cnt = 0;
	 	while(!param_file.eof()){
	 		cnt++;
	 		for(int i=0;i<nfiles;i++){
	 			param_file >> k0[i];
	 		}
	 		
			cout<< "Run "<<cnt<<"."<<endl;
			if (FitType.compare("GRADLS")==0){
				myfields->SetParams(k0);
				myfields->RunFit(chi2,.1,ChiNew,Emean);
				myfields->GetParams(atemp);
			}
			else if (FitType.compare("MINUIT")==0){
				for(int i=0;i<nfits;i++) minuit->SetParameter(i,paramName[i].c_str(),k0[i+2],.5,parlo[i],parhi[i]);
				minuit->FixParameter(1); //fix mcp front
				minuit->ExecuteCommand("SIMPLEX",0,0);
				minuit->ExecuteCommand("MIGRAD",0,0);
				for(int i=0;i<nfits;i++) par[i]=minuit->GetParameter(i);
				myfields->SetParamsExternal(par);
				myfields->GetParams(atemp);
				ChiNew = myfields->CalcChiSq();
				Emean = myfields->CalcMean();
			}
			else{
				cout<< "No option for FitType "<<FitType<<endl;
				return -1;
			}
			myfields->ComputePotens(newPotens);
	 		for (int s=0; s<nfiles; s++) LogFile << newPotens[s] << "\t";
			LogFile << ChiNew << "\t"<<Emean<<endl;
		}
		param_file.close();
	}
	//**********LINEAR REGRESSION *************************************************************
	else if (FitType.compare("LINEAR")==0){ 
		if(myfields->LinearSolve(ChiNew, Emean)){
			cout<< "Failed to solve with Linear Least Squares Method!"<<endl;
			exit(1);
		}
		myfields->GetParams(atemp);
		myfields->ComputePotens(newPotens);
	  for (int s=0; s<nfiles; s++) LogFile << newPotens[s] << "\t";
		LogFile << ChiNew << "\t"<<Emean<<"\t"<<zlow<<"\t"<<zhi<<"\t"<<radius<<endl;
		for (int i=1; i<7; i++){
			if ((newPotens[i+1] - newPotens[i])>maxDiff){
				cout<<"WARNING: Difference between potentials "<<i<<" and "<<i+1<<" exceeds "<<maxDiff<<"!"<<endl;
			}
		}
	  sprintf(ScaledName,"OptimizedField_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f",newPotens[0],newPotens[1],newPotens[2],newPotens[3],newPotens[4],newPotens[5],newPotens[6],newPotens[7],newPotens[8]);
		cout<< "Output scaled field to data file? (y/n)"<<endl;
		cin >> response;
		if (response.compare("y")==0) myfields->OutputEScaled(ScaledName, "dat"); //Output scaled version of field to file
	}
	/*******************************VDROP LEAKAGE HYPOTHESIS*************************************/
//	double VDrop = 1000; //V
//	
//	
//	for (int i=2;i<6;i++){
//		newPotens[i] -= VDrop;	
//		myfields->CalcPotens2Params(newPotens, atemp);
//	 	myfields->SetParams(atemp);
//		myfields->ComputeEScaled();
//		myfields->ComputePotens(newPotens); //Check
//		cout << VDrop<<" V Drop on E"<<i-1<<":\t";
//	  for (int s=0; s<nfiles; s++) cout << newPotens[s] << "\t";
//	  cout<<"\n";
//		sprintf(ScaledName,"CurrentSpacingsOptimized_El%dVDrop%.0f",i-1,VDrop);
//		//myfields->OutputEScaled(ScaledName, "txt"); //Output scaled version of field to text file
//		newPotens[i] += VDrop;	
//	}
	/*****************************SCALE VOLTAGE SETTINGS*******************************/
	else if (FitType.compare("SCALE")==0){
		//Open file to read potentials used for each run
		ifstream fin;
		ofstream fout;
		string outlist;
		unsigned found = pFile.find_last_of(".");
		string pFileroot = pFile.substr(0,found);
		string fieldname;
		string PotensListFullPath = FIELD_DIRECTORY;
		PotensListFullPath +="/";
		outlist = PotensListFullPath;
		PotensListFullPath += pFile; //argument
		outlist = outlist + "OptimizerOutputFiles_" + pFileroot + ".list";
		
		
		fin.open(PotensListFullPath.c_str()); //open potential list file
		fout.open(outlist.c_str());
		if (!fin.is_open()){
			cout<< "ERROR: Could not open "<< PotensListFullPath <<endl;
			exit(1);
		}
		else{
			myfields->UnfixAllPotens(); //Unfix all
			for(int i=0;i<10;i++){
			  fin >> fieldname;
				for (int j=0; j<nfiles; j++){ //populate potentials 
					fin >>newPotens[j];
                                        //cout<<newPotens[j]<<endl;
				}
				if (fin.eof()) break;
				myfields->CalcPotens2Params(newPotens, atemp);
				myfields->SetParamsExternal(atemp);//skips over fixed params
				myfields->ComputeEScaled();
				myfields->ComputePotens(newPotens); //Check //computes fixed params
				sprintf(ScaledName,"ScaledField_%s_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f_%.0f",fieldname.c_str(),newPotens[0],newPotens[1],newPotens[2],newPotens[3],newPotens[4],newPotens[5],newPotens[6],newPotens[7],newPotens[8]);
				myfields->OutputEScaled(ScaledName, "dat"); //Output scaled version of field to file with fixed params
				fout<<ScaledName<<".dat"<<endl;
			}
		}
		fin.close();
		fout.close();
	}

	

/******************************WRAP UP**********************************************************/	
	LogFile.close();
	delete minuit;
	delete myfields;
	
	time(&endTime);
	timeSec = difftime(endTime, startTime);  
	printf("Runtime: %.3f s \n",timeSec);
	timeMin = floor(timeSec/60);
	printf("Runtime: %02.0f:%02.0f \n",timeMin,fmod(timeSec,60));


}

