#ifndef POTENTIAL_H
#define POTENTIAL_H

class Potential{

protected:
double DX[3]; 	//extents
int nx[3]; 		//number of elements in grid
double x0[3], x1[3]; 	//start and stop of grid in coordinant system
double ***V; 			//potential array
double ****E; //electric field array
double **coord; //grid arrays xyz
int skpcnt;

public:
Potential(const char* Vname, const char* Hname, const char* units);
Potential(const char* Vname);
void InitArrays();
Potential();
~Potential();
bool ReadHFile(const char* filename);
bool ReadVFile(const char* filename, int convert);
bool ReadHeader(const char* filename);
bool ReadSource(const char* filename);
double GetV(int k[3]);
double GetE(int k[3], int dir);
void GetPos(int k[3],double pos[3]);
void GetNx(int n[3]);
void GetRegion(double pos0[3], double pos1[3]);
bool Within(double pos[3]);
bool CalcV(double pos[3], double &Vout); //interpolation function
bool CalcE(double pos[3], double Eout[3]);//interpolation function
virtual void Findklo (double pos[3], int klo[3]){}
};

#endif



