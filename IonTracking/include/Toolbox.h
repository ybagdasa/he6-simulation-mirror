#ifndef TOOLBOX_H
#define TOOLBOX_H
#include "GlobalConstants.h"
#include <math.h>

/* Useful macros ============================================================ */
#ifndef PI
#define PI 3.14159265358979323846
#endif

#ifndef CSPEED
#define CSPEED 299.792458
#endif

#ifndef CSPEED2
#define CSPEED2 89875.5179
#endif

#define DEG2RAD  (PI/180)
#define RAD2DEG  (180/PI)
#define HBAR     6.582e-16   /* [eV s] h bar Planck constant */
#define GRAVITY  9.81   /* [m/s^2] gravitational acceleration */
#define rand01() ( ((double)rand())/((double)RAND_MAX+1) )
#define DBL_MAX 1.7976931348623158e+308 /* max value */
#define TINY 1.0e-12
#define LESSTINY 1.0e-10




int SolveQuadratic(double A, double B, double C, double* root1, double* root2);
int SolveCubic(double A, double B, double C, double D, double* root1, double* root2, double* root3);
int SolveQuartic(double A, double B, double C, double D, double E, double* root1,
double* root2, double* root3, double* root4);

double Bessi0(double x);

void BuildRotInMatrix(double a, double b, double c, double rot[3][3]);
void InvertMatrix(double A[3][3], double invA[3][3]);
void MatrixProd(double A[3][3], double B[3][3], double C[3][3]);
void MatrixVecProd(double A[3][3], double v[3], double vnew[3]);
double DotProd(double v1[3], double v2[3]);
void CrossProd(double v1[3], double v2[3], double xprod[3]);
void VectorSum(double v1[3], double v2[3], double sum[3]);
void VectorDiff(double v1[3], double v2[3], double diff[3]);
void PrintMatrix(double A[3][3]);
void FloorVec(double v[3]);
void PrintVec(double v[3]);
void Normalize(double v[3]);

double K2V(double K, double mass);
double V2K(double v2, double mass);

#endif
