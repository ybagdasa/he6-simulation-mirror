#ifndef ARM_H
#define ARM_H

//The rotation matrices are based on Euler angles (phi,theta,psi)
class Arm {
	public:
	double armPos[3];
	double RotIn[3][3];
	double RotOut[3][3];

	Arm(double x, double y, double z, double alpha, double beta, double gamma);
	Arm(double x, double y, double z, double alpha, double beta, double gamma, double pos0toRefABS[3], double RotIn0toRef[3][3]);
	void rotateINTO(double vold[3], double vnew[3]);
	void rotateOUTOF(double vold[3], double vnew[3]);
	void transformINTO(double pos[3], double vel[3], double newpos[3], double newvel[3]);
	void transformOUTOF(double pos[3], double vel[3], double newpos[3], double newvel[3]);
};

#endif
