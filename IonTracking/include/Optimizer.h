#ifndef OPTIMIZER_H
#define OPTIMIZER_H

#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cstring>
#include <string>
#include "TMatrixD.h"
#include "Potential.h"
//#include "VGridUni.h"
//#include "Toolbox.h"

#define N_PARAM 10    // max number of parameters
#define MAXSTEP 50
#define FRESTORE .80 //fraction of original step to restore for perturbation
#define MAXTRIALS 100
class Optimizer {


protected:
std::string EfilePath;			//Path to Efield files
Potential **Efield;				//array of original field maps (instances of class Potential)
double **E; 							//electric field array
double *EScaled;					//scaled field
int nPts;							//number of elements 
double R0,Z0,Z1;					//cylindrical region of interest
double E0; 						//ideal uniform field
int g_m;		 					//number of parameters
double  Volt[N_PARAM];			//potential at plate surface
double	a[N_PARAM], siga[N_PARAM], deltaa[N_PARAM], da[N_PARAM], grad[N_PARAM];				//parameters  
double	chiSq1, chiSq2, chiSq3, stepDown, fract;
double maxDiff;
bool ParamsSet, ParamIncrSet;

int afix[N_PARAM-1], wrta[N_PARAM-1]; //array entries are param numbers
double dV[N_PARAM-1];
int isafixed[N_PARAM]; //boolean
int nfix;




public:
Optimizer(int nPlates, const char* filename, double vals[], double r, double z0, double z1, double Egoal);
~Optimizer();

void InitValues(int nPlates, double r, double z0, double z1, double vals[], double Egoal);
int Getm(){return g_m;}
int GetNPts(){return nPts;}
void Putm(int m){g_m = m;}
double GetParam(int i){return a[i];}
void PutParam(int i, double aNew){a[i] = aNew;}
void PutParamIncr(int i, double deltaaNew){deltaa[i] = deltaaNew;}
void SetParams(double vals[]);
void GetParams(double vals[]);
void SetParamIncr(double vals[]);
void FixPotensDiffInternal(int i, int j, double delV);
void FixPotensDiff(int i, int j, double delV);
void FixPotential(int i, double V);
void UnfixPotential(int i, double incr = .1);
void UnfixAllPotens();
void OutputEScaled(const char* OutputName, const char* FileType = "bin");
void SetE0(double Egoal) {E0 = Egoal;}
double GetE0() {return E0;}

void SigParab();
void ComputeFixedParams();
void ComputeEScaled();
double CalcMean();
double	CalcChiSq();	
void	Gradls(double &chiSqr);
void	CalcGrad();
void RunFit(double ChiMax, double stepSize, double &chiNew, double &Emean);

void minuitFunction(int& nDim, double *gout, double &result, double par[], int flg); //function wrapper for TMinuit chi2 function
void SetParamsExternal(double par[]); //sets internal non-fixed entries of a[] to par[] 
void ComputePotens(double newPotens[]); //fills array newPotens[] with scaled potentials based on current values a[]
void CalcPotens2Params(double potens[], double par[]); //calculates scaled parameters par[] for desired potential potens[] where dimenstion of each is assumed to be g_m
double Poten2Param(int i, double V); //returns scaled parameter aNew for the desired potential V

//int LinearSolve(double &chiNew, double &Emean) {
//	return LinearSolve(chiNew,Emean,"normal");
//}
int LinearSolve(double &chiNew, double &Emean, const char* mode = "normal");
};

#endif
