#ifndef UFTRACKING_H
#define UFTRACKING_H

#include <stdint.h>

class UFTracking{
  private:
    double E_Field;		//Electric field, in MV/cm;
    double End_Pos;
    double M_r;
    double c;			//speed of light,  in cm/us^2
    double Charge;
    double Init_Pos[3];
    double V_Ion[3];
    double TOF;                   //Time of flight
    double X_hit;
    double Y_hit;                 //Hit positions on MCP,in mm
    double Ek_Ion;                //Kinetic energy of ion
    double HitAngle;              //Hit angle on MCP
//    double Vxy;
//    double V;
    uint8_t Status;
  public:
    UFTracking(double Field,double Pos,double IonMass);
    ~UFTracking();
    void SetIonInfo(double C,double* DEC_POS,double* Init_V);
    void Track();
    void GetOutput(double& Tof,double& xhit,double& yhit,double &IonEnergy,double& hitangle,uint8_t& Sta);
		double GetVz();
    void TrackBack(double TOF,double xhit,double yhit,double& E_init);
};

#endif
