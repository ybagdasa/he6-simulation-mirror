#ifndef IonDataStruct_h
#define IonDataStruct_h

#include <stdint.h>

typedef struct InIonDataStruct{
  short DECPosition[3];
  int	V_Ion[3];
  unsigned short EIon0;
}InIonDataStruct;

typedef struct OutIonDataStruct{
  unsigned int TOF;
  int HitPos[2];
  unsigned short HitAngle;
  unsigned short IonEnergy;
  uint8_t Status;
  uint8_t ChargeState;
}OutIonDataStruct;

#endif
