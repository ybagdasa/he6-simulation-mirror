#ifndef VGRIDUNI_H
#define VGRIDUNI_H
#include "Potential.h"

//This is a potential whose grid is uniformly mapped to the field values.

class VGridUni:public Potential{

double dx[3]; 	//bin sizes


public:
VGridUni (const char* Vname, const char* Hname, const char* units);
VGridUni (const char* Vname);
void Findklo (double pos[3], int klo[3]);
~VGridUni();
};

#endif
