#ifndef COMPONENT_H
#define COMPONENT_H
#include "Arm.h"

class Component{

public:

char name[20];
int Nabsorbed, PositionErrorCount;
int Nnumber;

Arm* coord;
Component(char* nombre, double x, double y, double z, double alpha, double beta, double gamma);
Component(char* nombre, Arm* ref, double x, double y, double z, double alpha, double beta, double gamma);
~Component();
virtual bool Within(double pos[3]){}
const char* GetName();



};

#endif
