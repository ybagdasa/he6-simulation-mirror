#ifndef TRACKING_H
#define TRACKING_H
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include "Potential.h"
#include "VGridUni.h"
#include "Geometry.h"
#include "Toolbox.h"

#define MAXSTEP 10000000
#define EPS 1e-8
#define H1 1.0e-2
#define HMIN .0001
#define HMAX 1e9; //1 s
#define DMAX .25 //mm
#define SMIN 10.0 //mm minimum stopping length
#define SAFETY .9
#define PGROW -.2
#define PSHRINK -.25
#define ERRCON 1.89e-4	//ERRCON = (5/SAFETY)^(1/PGROW)
#define ACCURACY .1   // Cv>ACCURACY ->binary boundary approach


class Tracking{
//**************Module Parameters (initialized once)*********************
protected:
	const double mass;					//mass [MeV] of ion
	const double R0; 
	const double Z0; 
	const double Z1; 						//boundaries of flight region [mm]
	const int nregE;						//number of E field regions
	const int nregB;						//number of B field regions
	int EtoInit, BtoInit;
	Potential **regionsE;				//array of E field instances
	Potential **regionsB;				//array of B field instances
	Geometry *geo;
//************Ion dependent parems**************************************	
	double Q; 								//a = Q(E+vxB),Q = q/m, [(mm/ns)^2/V]
	double pos0[3],vel0[3]; 			//initial position [mm] and velocity [mm/ns]
	
	double pos[3], vel[3];//, tof;		//current position [mm],velocity [mm/ns], and tof [ns]
	double gamma; //relativistic parameter, = 1/sqrt(1-(v/c)^2)
	double tof;
	uint8_t status;			//0 means keep, 1 means toss
	bool flying; 				//hits MCP or not, ion is still in flight
	bool boundary;			//true if crosses boundary
	bool highGrad;			//true if Ca>ACCURACY
	bool reversal;			//true if Cv>ACCURACY
	int nok, nbad;							//number of steps predicted correctly/corrected by stepper 
	double Cv[3],Ca[3];       //coefficient of variation sig/mu
	bool MuForce;				//flag for using alternate force equation for adiabatic invariant gamma*mu for magnetic field
	
	//double Emax[3];			//maximum occuring field

public:
Tracking(double m, double r, double z0, double z1, double nE = 1, double nB = 0);
~Tracking();
void SetIonInfo(double x0[3], double v0[3], double charge_state);
void GetOutput(double &TOF, double &xhit, double &yhit, double &energy, double &hitangle,  uint8_t &stat);
void GetOutputFull(double &TOF, double &xhit, double &yhit, double &zhit, double &vx, double &vy, double &vz, double &energy, double &hitangle, uint8_t &stat, double &Qconst);
void GetInitPos(double x0[3]);
void GetInitVel(double v0[3]);
void TrackFixStp(double tstp = H1);
void TrackFixStp(double tstp, double tend, double nstpwrite = 1); 
void TrackVarStp(int option = 1, double tend = 0);
void InitializeFields(const char * filename, const char * FieldType = "E");
void WriteToFile(std::ofstream& fid, double tstp);
void WriteToFile(std::ofstream& fid, int nstp, double tstp, double accel[3]);
void SetMuForce(bool flag);
double CalcGamma(double someV[3]);


protected:
void GetAccel(double someX[3], double someV[3], double Aout[3]);
//void Stepper(double a[3], double htry, double vscal[3], double &hdid, double &hnext);
void rk4(double h, double a[3], double vout[3], double xout[3]);
void rk5(double h, double a[3], double vout[3], double xout[3], double verr[3]);
bool Done();
bool CrossZ0(double ztemp);
bool CrossZ1(double ztemp);
void Propogate(double t, double a[3],double vtemp[3], double xtemp[3]);

};
#endif
