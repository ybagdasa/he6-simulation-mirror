#ifndef TRACKINGCIRCLE_H
#define TRACKINGCIRCLE_H
#include "Tracking.h"

//This is a potential whose grid is uniformly mapped to the field values.

class TrackingCircle:public Tracking{

//**************Module Parameters (initialized once)*********************

const double k;	//field strength [V*mm] for E, [V*ns/mm^2] for B (10 Gauss)
const int N_T;		//number of periods to fly (arbitrary)
//************Ion dependent parems**************************************

//set externally
double v0;	//velocity of orbit [mm/ns]

//calculated and set internally 
double K;	// =kQ = kq/m = [mm^3/ns^2] (field and charge state dependent)
double R0;  //radius of stable orbit in Circle field = K/v^2 [mm], v/QB [mm]
double T;	//period of orbit 2*PI*K/v^3 [ns], 2*PI/QB [ns]


public:
TrackingCircle(double m, double r, double z0, double z1, double nE, double nB, double fieldStrength, int numOfOrbits);
~TrackingCircle();
void SetIonInfo(double v_orbit, double charge_state); //for E Field
void SetIonInfoB(double v_orbit, double charge_state); //for B Field
double GetT();
void TrackFixStp(double tstp);
void TrackVarStp(int option);


};

#endif
