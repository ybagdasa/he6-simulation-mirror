#ifndef CYLINDER_H
#define CYLINDER_H
#include "Component.h"
#include "Arm.h"

class Cylinder:public Component{
protected:
//double normal[3];
double rin, rout, length, rin2, rout2, zup, zdown;

public:
Cylinder(char* nombre, double x, double y, double z, double alpha, double beta, double gamma, double r0, double r1, double l);
Cylinder(char* nombre, Arm* ref, double x, double y, double z, double alpha, double beta, double gamma, double r0, double r1, double l);
~Cylinder();
void Init(double r0,double r1,double l);
bool Within(double pos[3]);
};
#endif
