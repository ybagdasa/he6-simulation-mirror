#ifndef GEOMETRY_H
#define GEOMETRY_H
#include "Component.h"


class Geometry{
	static const int ntrodes = 6;
	Component **Electrodes; //Electrode Array
	Component *Grid;

public:
	Geometry();
	~Geometry();
	bool	Within(double pos[3]);

};
#endif
