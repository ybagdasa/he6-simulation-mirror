#include "Tracking.h"
#include "Toolbox.h"
#include "Potential.h"
#include "VGridUni.h"
#include "Geometry.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <string>
#include <cstring>


using namespace std;

Tracking::Tracking(double m, double r, double z0, double z1, double nE, double nB):mass(m), R0(r), Z0(z0), Z1(z1), nregE(nE), nregB(nB){
	if(nregE==0 && nregB==0){
		cout << "ERROR: Must have at least one electric or magnetic field region.\n";
		exit(1);
	} 
  regionsE = new Potential* [nregE];
  regionsB = new Potential* [nregB];
  EtoInit = 0;
  BtoInit = 0;
  geo = new Geometry(); //declare and construct geometery with Geometry class
  MuForce = false;
}
Tracking::~Tracking(){
	delete [] regionsE;
	delete [] regionsB;
	delete geo;
}

void Tracking::InitializeFields(const char * EFileName, const char * FieldType){
	if(strcmp(FieldType,"E")!=0 && strcmp(FieldType,"B")!=0){
		cout << "Unrecognizable FieldType "<<FieldType<<". Use values 'E' or 'B'."<<endl;
		exit(1);
	}
  string fin = EFileName;
  unsigned found = fin.find_last_of(".");
  string root = fin.substr(0,found);
  string ext = fin.substr(found+1);
  
  if(strcmp(ext.c_str(), "txt")==0){
  	string finH = root;
  	finH+="H.txt";
  	cout << finH<<endl;
  	if(strcmp(FieldType,"E")==0 && nregE){
  		if(EtoInit<nregE){
  			regionsE[EtoInit] = new VGridUni(fin.c_str(),finH.c_str(),"mm");
  			EtoInit++;
  		}
  		else{
  			cout<<"All allocated electric field regions already initialized."<<endl;
  			exit(1);
  		}
  	}else if(strcmp(FieldType,"B")==0 && nregB){
   		if(BtoInit<nregB){
  			regionsB[BtoInit] = new VGridUni(fin.c_str(),finH.c_str(),"mm");
  			BtoInit++;
  		}
  		else{
  			cout<<"All allocated magnetic field regions already initialized."<<endl;
  			exit(1);
  		}
  	}else {
  		cout << "No regions of type "<<FieldType<<" to initialize.\n";
  		exit(1);
  	}
  } 
	
	else if (strcmp(ext.c_str(), "dat")==0){
		if(strcmp(FieldType,"E")==0 && nregE){
  		if(EtoInit<nregE){
  			regionsE[EtoInit] = new VGridUni(fin.c_str());
  			EtoInit++;
  		}
  		else{
  			cout<<"All allocated electric field regions already initialized."<<endl;
  			exit(1);
  		}
  	}else if(strcmp(FieldType,"B")==0 && nregB){
   		if(BtoInit<nregB){
  			regionsB[BtoInit] = new VGridUni(fin.c_str());
  			BtoInit++;
  		}
  		else{
  			cout<<"All allocated magnetic field regions already initialized."<<endl;
  			exit(1);
  		}
		}else {
  		cout << "No regions of type "<<FieldType<<" to initialize.\n";
  		exit(1);
  	}
	}
	
	else{ 
		cout << "Unrecognizable file extension "<<ext<<" for file "<<EFileName<<". Must be either .txt or .dat format."<<endl;
		exit(1);
	}


	
}
void Tracking::SetMuForce(bool flag){
	MuForce = flag;
	if (flag) cout << "WARNING: using alternate force equation for adiabatic invariant gamma*mu for magnetic field gradient. Assuming EFile is map of the magnetic field gradient in [G/mm]." ;
}
double Tracking::CalcGamma(double someV[3]){
	double v2 = DotProd(someV,someV);
	double gamma = 1/sqrt(1-v2/CSPEED2);
	return gamma;
}
void Tracking::SetIonInfo(double x0[3], double v0[3], double charge_state){
	for (int i=0;i<3;i++){
		pos0[i] = x0[i];
		vel0[i] = v0[i]; 			
		pos[i] = x0[i];
		vel[i] = v0[i];
	}
	tof =0.0;
	if (MuForce){ //Construct Q = gamma0*mu0/m = (gamma0*v_t)^2/(2 B0) [mm^2/ns^2/G]
		double gamma, B0, Bout[3], VxB[3], vt2;
		int hereE = -1;
		int hereB = -1;
		int i;
		//Get B0
		if(nregB!=0){
			for (i=0;i<nregB;i++){
				if(regionsB[i]->Within(x0)){
					hereB = i;
					break;
				}
			}
	
			if(hereB<0){
				printf("Error: Magnetic field not defined for position (%.2f\t%.2f\t%.2f)\n", x0[0],x0[1],x0[2]);
				exit(1);
			}
			else{
				if(!regionsB[hereB]->CalcE(x0,Bout)){
					//printf("CalcE returned 'false'\n");
					flying = false;
					status = 1; //don't keep
				}
			}
		} 
		CrossProd(v0,Bout,VxB);
		B0 = sqrt(DotProd(Bout,Bout)); //magnetic field norm
		vt2 = DotProd(VxB,VxB)/B0/B0; //velocity transverse to magnetic field squared
		gamma = CalcGamma(v0);	
		Q = gamma*gamma*vt2/2/B0;			// [mm^2/ns^2/G]
	}
	else{
		Q = (1e-6)*charge_state*CSPEED*CSPEED/mass; 			//[mm/ns]^2/V
	}
	flying = true;
	boundary = false;
	highGrad = false;
	status = 0; //keep ion initially
	nok = 0;
	nbad = 0;
}
void Tracking::GetOutput(double &t, double &xhit, double &yhit, double &energy, double &hitangle, uint8_t &stat){
	t = tof; //ns
	xhit = pos[0]; //mm
	yhit = pos[1]; //mm
	double Vmag2 = DotProd(vel, vel);
	energy = V2K(Vmag2,mass); //keV 
	hitangle = acos(fabs(vel[2])/sqrt(Vmag2))* 180.0 / PI; //deg 
	stat = status;
}
void Tracking::GetOutputFull(double &t, double &xhit, double &yhit, double &zhit, double &vx, double &vy, double &vz, double &energy, double &hitangle, uint8_t &stat, double &Qconst){
 	t = tof; //ns
	xhit = pos[0]; //mm
	yhit = pos[1]; //mm
	zhit = pos[2]; //mm
	vx = vel[0]; //mm/ns
	vy = vel[1];
	vz = vel[2];

		

	double Vmag2 = DotProd(vel, vel);
	double gamma = 1/sqrt(1-Vmag2/CSPEED/CSPEED);
	energy = mass*(gamma-1)*1000; //keV 
	hitangle = acos(fabs(vel[2])/sqrt(Vmag2))* 180.0 / PI; //deg 
	stat = status;
	Qconst = Q;
}

void Tracking::GetInitPos(double xout[3]){
	for (int i=0; i<3; i++) xout[i] = pos0[i];
}

void Tracking::GetInitVel(double vout[3]){
	for (int i=0; i<3; i++) vout[i] = vel0[i];
}

void Tracking::GetAccel(double someX[3], double someV[3], double Aout[3]){
	int hereE = -1;
	int hereB = -1;
	int i;
	double Eout[3],Bout[3],BForce[3];
	double v2,vdotE; //magnitude of the velocity squared [mm/ns]^2
	for (i=0;i<3;i++){
		Eout[i]=Bout[i]=BForce[i]=0;
	}
	
	if(nregE!=0){
		for (i=0;i<nregE;i++){
			if(regionsE[i]->Within(someX)){
				hereE = i;
				break;
			}
		}
		if(hereE<0){
			printf("Error: Electric field not defined for position (%.2f\t%.2f\t%.2f)\n", someX[0],someX[1],someX[2]);
    	printf("x0 = %f\t y0 = %f\t z0 = %f\t vx0 = %f\t vy0 = %f\t vz0 = %f\n", pos0[0], pos0[1], pos0[2], vel0[0], vel0[1], vel0[2]);
			exit(1);
		}
		else{
			if(!regionsE[hereE]->CalcE(someX,Eout)){
				//printf("CalcE returned 'false'\n");
				flying = false;
				status = 1; //don't keep
			}
		}
	}
	if(nregB!=0){
		//Do the same for B field.
		for (i=0;i<nregB;i++){
			if(regionsB[i]->Within(someX)){
				hereB = i;
				break;
			}
		}
	
		if(hereB<0){
			printf("Error: Magnetic field not defined for position (%.2f\t%.2f\t%.2f)\n", someX[0],someX[1],someX[2]);
			exit(1);
		}
		else{
			if(!regionsB[hereB]->CalcE(someX,Bout)){
				//printf("CalcE returned 'false'\n");
				flying = false;
				status = 1; //don't keep
			}
		}
		CrossProd(someV,Bout,BForce);

//		PrintVec(someV);
//		PrintVec(Bout);
//		PrintVec(BForce);
//		printf("\n\n\n");
		
	}
	gamma = CalcGamma(someV);
	if (MuForce) {
		for (i=0;i<3;i++){
			Aout[i] = -Q/gamma/gamma*Eout[i]; // Q = gamma0*mu0/m, Eout = Grad(B) [G/mm] (F = mu * Grad(B))
		}
	}
	else{
		vdotE = DotProd(someV,Eout);
		//cout<< gamma<<endl;
	
		for (i=0;i<3;i++){
			Aout[i] = Q/gamma*(Eout[i] - vdotE*someV[i]/CSPEED2 + .1*BForce[i]); //1 Gauss = .1 V*ns/mm^2
	//		if (Eout[i]>Emax[i]){
	//			Emax[i] = Eout[i];
	//			cout<<"Emax["<<i<<"] = "<<Emax[i]<<"\t Position = ";
	//			PrintVec(pos);
	//		}
		}
	}
}
void Tracking::TrackFixStp(double tstp){ //tstp has a default value of H1
	int nstp, i;
	double h,tnew;
	double a[3],vtemp[3], xtemp[3];
	
	h = tstp;
	
	if(h<=HMIN){
		printf("Error: h (%lf ns) is smaller than HMIN (%lf ns).\n",h, HMIN);
		exit(1);
	}
	
	if (Done()) return; //check that initial position is in region of flight
	
	for (nstp=0;nstp<MAXSTEP;nstp++){
		GetAccel(pos,vel,a);
		rk4(h, a, vtemp, xtemp);
		if(!flying){
			status = 1;
			break;
		} //this would happen if looked for Efield in an undefined region. vtemp and xtemp are meaningless.
		if(CrossZ0(xtemp[2])){//for now, fly ion to Z0 analytically. This assumes that ion's trajectory is insensitive to field near boundary.
			double A,B,C, t1,t2;
			
			A = a[2]/2;
			B = vel[2];
			C = pos[2]-Z0;
			//printf("A=%f\tB=%f\tc=%f\tt1=%f\tt2=%f\n",A,B,C,t1,t2);
			if(SolveQuadratic(A,B,C,&t1,&t2)>0){
				//printf("t1 = %f\t t2=%f\n",t1,t2);
				h = t1; //new timestep to Z0
				Propogate(h,a,vtemp,xtemp);
			}
			else{
				printf("Cannot find positive solution to quadratic equation in TrackingFixStp().\n");
				exit(1);
			}
		}	 
		//check for crossed boundaries here,halve h and repeat rk4 until boundary no longer crossed unless h<.0001
		
//		boundary = geo->Within(xtemp); //set flag if boundary crossed
//		if(boundary){
//			printf("Boundary is crossed at");
//			PrintVec(xtemp);
//			printf("\n\n");
//		}
		tnew = tof + h;
		if (tnew==tof){
			printf("Stepsize underflow in Stepper.\n");
			exit(1);
		}
		//Update tof, pos, and vel
		tof += h;
		for (i=0;i<3;i++){
			vel[i] = vtemp[i];
			pos[i] = xtemp[i];
		}
		//Check if done	
		if(Done()){break;}
	}
	if(nstp==MAXSTEP){printf("max step reached.\n");}	
	//else{printf("Nstep = %i\n",nstp);}
	
}
void Tracking::TrackFixStp(double tstp, double tend, double nstpwrite){ //all arguments must be specified: timestep, total time of allowed flight, when to write state vars to output file (every nth timestep)
	int nstp, i;
	double h,tnew;
	double a[3],vtemp[3], xtemp[3];
	
	h = tstp;
	
	if(h<=HMIN){
		printf("Error: h (%lf ns) is smaller than HMIN (%lf ns).\n",h, HMIN);
		exit(1);
	}
	
	nstp = 0;
	
	ofstream IonTracktstp;
	WriteToFile(IonTracktstp, tstp);
	
	if (Done()) return; //check that initial position is in region of flight
	
	while ((tend-tof)!=0){
		if((tend-tof)<tstp){
			h = tend-tof;
		}
		GetAccel(pos,vel,a);
		rk4(h, a, vtemp, xtemp);
		if(!flying){
			status = 1;
			break;
		} //this would happen if looked for Efield in an undefined region. vtemp and xtemp are meaningless.
		if(CrossZ0(xtemp[2])){//for now, fly ion to Z0 analytically. This assumes that ion's trajectory is insensitive to field near boundary.
			double A,B,C, t1,t2;
			
			A = a[2]/2;
			B = vel[2];
			C = pos[2]-Z0;
			//printf("A=%f\tB=%f\tc=%f\tt1=%f\tt2=%f\n",A,B,C,t1,t2);
			if(SolveQuadratic(A,B,C,&t1,&t2)>0){
				//printf("t1 = %f\t t2=%f\n",t1,t2);
				h = t1; //new timestep to Z0
				Propogate(h,a,vtemp,xtemp);
			}
			else{
				printf("Cannot find positive solution to quadratic equation in TrackingFixStp().\n");
				exit(1);
			}
		}	 
		//check for crossed boundaries here,halve h and repeat rk4 until boundary no longer crossed unless h<.0001
		
//		boundary = geo->Within(xtemp); //set flag if boundary crossed
//		if(boundary){
//			printf("Boundary is crossed at");
//			PrintVec(xtemp);
//			printf("\n\n");
//		}
		tnew = tof + h;
		if (tnew==tof){
			printf("Stepsize underflow in Stepper.\n");
			exit(1);
		}
		//Update tof, pos, and vel
		tof += h;
		for (i=0;i<3;i++){
			vel[i] = vtemp[i];
			pos[i] = xtemp[i];
		}
		nstp++;
		if(nstp==nstpwrite){
			WriteToFile(IonTracktstp,tstp);
			nstp=0; //reset counter
		}
		//Check if done	
		if(Done()){break;}
	}

}
void Tracking::TrackVarStp(int option, double tend){
	int nstp, i,n1,n2,n3;
	double h,ha,hv,htot,S,tnew;
	double a[3],vtemp[3], xtemp[3], verr[3],vscal[3];
	double a2[3],vtemp2[3],xtemp2[3];
	n1=0;
	n2=0;
	n3=0;
	
	double nstpwrite = 1; //write every n tstps
	int wstp=0;
	ofstream IonTracktstp;
	//WriteToFile(IonTracktstp, 0,0,a);	//mark new ion with nstp=0
	
	if (tend==0) tend = DBL_MAX;
	
	if (Done()) return; //check that initial position is in region of flight
	for (nstp=0;nstp<MAXSTEP;nstp++){
		//Reinitialize temporary arrays
		for (i=0;i<3;i++){
			vtemp[i] = vtemp2[i] = vel[i];
			xtemp[i] = xtemp2[i] = pos[i];
		}
		GetAccel(pos,vel,a);
		
		//Decide HERE whether to go with previous time step cut by half (if boundary was crossed) or to compute new timestep
		if(highGrad || boundary){
			h = h/2.0;
			boundary = false;
			highGrad = false;
		}
		else{
			h = HMAX;
	/********************************************SIMION TIMESTEP ALGORITHM*******************************************/
			for (i=0;i<3;i++){
				hv = fabs(DMAX/vel[i]);
				ha = sqrt(fabs(2*DMAX/a[i]));
				if (fabs(a[i])>TINY && fabs(vel[i])>TINY){
					htot = hv*ha/(hv+ha);
				}
				else if(fabs(a[i])>TINY){htot = ha;}
				else if(fabs(vel[i])>TINY){htot = hv;}
				else{
					htot = hv*ha/(hv+ha);
					printf("Warning: acceleartion and velocity both zero for direction %d. Ion's not going anywhere.",i);
					//exit(1);
				}
				//adjust time step according to stopping length
				S = fabs(.5*vel[i]*vel[i]/a[i]);
				//printf("hv = %f\t ha = %f\t htot = %f\t S = %f\n",hv,ha,htot,S);
				if (S>=SMIN){htot = htot;n1++;}
				else if (S<SMIN/10.0){htot = htot/10.0;n2++;} //reduce no more than factor of 10
				else{htot = htot*S/SMIN;n3++;}
				h = fmin(h,htot);
				//printf("h = %f\t htot = %f\n",h,htot);
			}
	/******************************************************************************************************************/
		}	
		if(h<HMIN){
			printf("WARNING: h (%f ns) is smaller than HMIN (%f ns).Restoring to HMIN.\n",h, HMIN);
			h = HMIN;
			//exit(1);
		}
		
//		//Genral error parameters for RK alogrithms
//		for (i=0;i<3;i++) vscal[i] =fabs(vel[i])+fabs(h*a[i]);
//		//for (i=0;i<3;i++) vscal[i] =fabs(h*a[i]);
//		errmax = 0.0;
		
		if(option==1){
		/***********************Fourth Order Runge-Kutta with fifth order Step-doubling error estimate.******************/				
			rk4(h, a, vtemp, xtemp);
		
			//Error computation
//		  rk4(h/2.0, a, vtemp2, xtemp2); //half step
//			GetAccel(xtemp2,vtemp2,a2);
//			rk4(h/2.0, a2, vtemp2, xtemp2); //half step
//			for (i=0;i<3;i++){
//				Del1 = vtemp[i]-vtemp2[i];
//				errmax = fmax(errmax,fabs(Del1/vscal[i]));
//			}		
		}
		/****************************************************************************************************************/
		else{
			/***********************Fifth Order Runge-Kutta with fifth order error computation for the fourth order solution.***/
			rk5(h,a,vtemp,xtemp,verr); //full step
			//Error computation
			//for (i=0;i<3;i++)	errmax = fmax(errmax,fabs(verr[i]/vscal[i]));		
			/****************************************************************************************************************/
		}
				
		if(!flying){
			status = 1;
			break;
		} //this would happen if looked for Efield in an undefined region. vtemp and xtemp are meaningless.
		
//	
//		hnext = HMAX;
//		/********************General Timestep Reduction for RK alogrithms*****************************************/
//		errmax/=EPS;
//		//cout<<errmax<<endl;
//		if(errmax> 1.0 && h>HMIN) { //Shrink step and try again.		
//			htemp = SAFETY*h*pow(errmax,PSHRINK); //Shrink step.
//			hnext = fmax(htemp, .1*h);
//			continue;
//		}
//		else if(errmax > ERRCON){hnext = SAFETY*h*pow(errmax,PGROW);} //Grow step.
//		else{hnext = 5*h;} //But no more than a factor larger.
//		/****************************************************************************************************************/

		
		
		
		if(CrossZ0(xtemp[2])){//for now, fly ion to Z0 analytically. This assumes that ion's trajectory is insensitive to field near boundary.
			double A,B,C, t1,t2;
		
			A = a[2]/2;
			B = vel[2];
			C = pos[2]-Z0;
			//printf("A=%f\tB=%f\tc=%f\tt1=%f\tt2=%f\n",A,B,C,t1,t2);
			if(SolveQuadratic(A,B,C,&t1,&t2)>0){
				//printf("t1 = %f\t t2=%f\n",t1,t2);
				h = t1; //new timestep to Z0
				Propogate(h,a,vtemp,xtemp);
			}
			else{
				printf("Cannot find positive solution to quadratic equation in TrackingVarStp().\n");
				exit(1);
			}
		}	
		 
		//check for crossed boundaries here,halve h and repeat rk4 until boundary no longer crossed unless h<.0001
		//boundary = CrossZ1(xtemp[2]);	
		//for (i=0;i<3;i++) if(Ca[i]>ACCURACY) highGrad = true;	
		if((highGrad || boundary) && h!=HMIN) continue; //don't take step
		//else if (h==HMIN) cout << "This was done."<<endl;
		
		
		
		
//		boundary = geo->Within(xtemp); //set flag if boundary crossed
//		if(boundary){
//			printf("Boundary is crossed at");
//			PrintVec(xtemp);
//			printf("\n\n");
//		}
		tnew = tof + h;
		if (tnew==tof){
			printf("Stepsize underflow in Stepper.\n");
			exit(1);
		}
		//Update tof, pos, and vel
		tof += h;
		for (i=0;i<3;i++){
			vel[i] = vtemp[i];
			pos[i] = xtemp[i];
		}
		wstp++;
		/*if(wstp==nstpwrite){
			WriteToFile(IonTracktstp,nstp+1,h,a);
			wstp=0; //reset counter
		}*/
		//Check if done	
		if(Done() || (tend-tof) < 0){break;}
		//printf("%f,%f,%f,%f\n",tof,a[2],vtemp[2],xtemp[2]);
	}
	//printf("%f\t%f\t%f\n",(double) n1/3/nstp,(double)n2/3/nstp,(double)n3/3/nstp);
	if(nstp==MAXSTEP){printf("max step reached.\n");}	
	else{
		//printf("Nstep = %i\n",nstp)
	}

}

void Tracking::rk4(double h, double a[3], double vout[3], double xout[3]){
	int i;
	double hh, h6;
	double x2[3], x3[3], x4[3], v2[3], v3[3], v4[3], a2[3], a3[3], a4[3];
	double muA[3], A2[3], muV[3], V2[3];
	hh = h/2.0;
	h6 = h/6.0;
	for (i=0;i<3;i++){
		v2[i] = vout[i] + hh*a[i];
		x2[i] = xout[i] + hh*(vout[i] + v2[i])/2;
	}
	GetAccel(x2,v2,a2);
	for (i=0;i<3;i++){
		v3[i] = vout[i] + hh*a2[i];
		x3[i] = xout[i] + hh*(vout[i] + v3[i])/2;
	}
	GetAccel(x3,v3,a3);
	for (i=0;i<3;i++){
		v4[i] = vout[i] + h*a3[i];
		x4[i] = xout[i] + h*(vout[i] + v4[i])/2;
	}
	GetAccel(x4,v4,a4);
	for (i=0;i<3;i++){
	//High Gradient and Velcity Reversal Coefficients of Variation
	
		muA[i] = (a[i]+a2[i]+a3[i]+a4[i])/4.0;
		A2[i] = (a[i]*a[i]+a2[i]*a2[i]+a3[i]*a3[i]+a4[i]*a4[i])/4.0;
		Ca[i] = sqrt(fabs(A2[i]-muA[i]*muA[i]))/fabs(muA[i]);
		
	/*	muV[i] = (vout[i]+v2[i]+v3[i]+v4[i])/4.0;
		V2[i] = (vout[i]*vout[i]+v2[i]*v2[i]+v3[i]*v3[i]+v4[i]*v4[i])/4.0;
		Cv[i] = sqrt(fabs(V2[i]-muV[i]*muV[i]))/fabs(muV[i]);
		*/
		
		//assignment!
		xout[i] = xout[i] + h6*(vout[i]+v4[i]+2*(v2[i]+v3[i]));
		vout[i] = vout[i] + h6*(a[i]+a4[i]+2*(a2[i]+a3[i]));
		

		//cout<<"muA^2 = "<<muA[i]*muA[i]<<" A2[i] = "<<A2[i]<<endl;
	}
	
	
}
void Tracking::rk5(double h, double a[3], double vout[3], double xout[3], double verr[3]){
	static double a2=0.2,a3=0.3,a4=0.6,a5=1.0,a6=0.875,b21=0.2,
		b31=3.0/40.0,b32=9.0/40.0,b41=0.3,b42 = -0.9,b43=1.2,
		b51 = -11.0/54.0, b52=2.5,b53 = -70.0/27.0,b54=35.0/27.0,
		b61=1631.0/55296.0,b62=175.0/512.0,b63=575.0/13824.0,
		b64=44275.0/110592.0,b65=253.0/4096.0,c1=37.0/378.0,
		c3=250.0/621.0,c4=125.0/594.0,c6=512.0/1771.0,
		dc5 = -277.00/14336.0;
	double dc1=c1-2825.0/27648.0,dc3=c3-18575.0/48384.0,
		dc4=c4-13525.0/55296.0,dc6=c6-0.25;
	double ak2[3],ak3[3],ak4[3],ak5[3],ak6[3],vtemp[3],xtemp[3];
	double x2[3], x3[3], x4[3], x5[3], x6[3], v2[3], v3[3], v4[3], v5[3], v6[3];
	int i;

	for (i=0;i<3;i++){
		v2[i]=vout[i]+b21*h*a[i];
		x2[i]=xout[i]+a2*h*(vout[i]+v2[i])/2.0;
	}
	GetAccel(x2,v2,ak2);
	//Second step.
	for (i=0;i<3;i++){
		v3[i]=vout[i]+h*(b31*a[i]+b32*ak2[i]);
		x3[i]=xout[i]+a3*h*(vout[i]+v3[i])/2.0;
	}
	GetAccel(x3,v3,ak3);
	//Third step.
	for (i=0;i<3;i++){
		v4[i]=vout[i]+h*(b41*a[i]+b42*ak2[i]+b43*ak3[i]);
		x4[i]=xout[i]+a4*h*(vout[i]+v4[i])/2.0;
	}
	GetAccel(x4,v4,ak4);
	//Fourth step.
	for (i=0;i<3;i++){
		v5[i]=vout[i]+h*(b51*a[i]+b52*ak2[i]+b53*ak3[i]+b54*ak4[i]);
		x5[i]=xout[i]+a5*h*(vout[i]+v5[i])/2.0;
	}
	GetAccel(x5,v5,ak5);
	//Fifth step.
	for (i=0;i<3;i++){
		v6[i]=vout[i]+h*(b61*a[i]+b62*ak2[i]+b63*ak3[i]+b64*ak4[i]+b65*ak5[i]);
		x6[i]=xout[i]+a6*h*(vout[i]+v6[i])/2.0;
	}
	GetAccel(x6,v6,ak6);
	//Sixth step.
	for (i=0;i<3;i++){
	//Accumulate increments with proper weights.
		xout[i]=xout[i]+h*(c1*vout[i]+c3*v3[i]+c4*v4[i]+c6*v6[i]);
		vout[i]=vout[i]+h*(c1*a[i]+c3*ak3[i]+c4*ak4[i]+c6*ak6[i]);
		verr[i]=h*(dc1*a[i]+dc3*ak3[i]+dc4*ak4[i]+dc5*ak5[i]+dc6*ak6[i]);
	//Estimate error as difference between fourth and fifth order methods.
	}
}
bool Tracking::Done(){
	if((pos[0]*pos[0] + pos[1]*pos[1])>=R0*R0 || pos[2]>=Z1){
		status = 1;	//don't keep
		flying = false;
		//printf("Zfinal=%f\n",pos[2]);
		return true;
	}
	else if(pos[2]<=Z0 || fabs(pos[2]-Z0)<1e-12){
		flying = false;
		//printf("Zfinal=%f\n",pos[2]);
		return true;
	}
	else{return false;}
}
bool Tracking::CrossZ0(double ztemp){
	if(ztemp<=Z0){return true;}
	else{return false;}
}
bool Tracking::CrossZ1(double ztemp){
	if(ztemp>=Z1){return true;}
	else{return false;}
}

void Tracking::Propogate(double t, double a[3], double vtemp[3], double xtemp[3]){
	int i;
	for (i=0;i<3;i++){
		xtemp[i] = .5*a[i]*t*t + vel[i]*t + pos[i];
		vtemp[i] = a[i]*t + vel[i];
	}
}
void Tracking::WriteToFile(ofstream& fid, double tstp){
	fid.open("IonTrajectory.txt",ios::app);
	fid<<tstp<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\n";
	fid.close();
}
void Tracking::WriteToFile(ofstream& fid, int nstp, double tstp, double accel[3]){
	fid.open("IonTrajectory.txt",ios::app);
	fid<<nstp<<"\t"<<tstp<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<accel[0]<<"\t"<<accel[1]<<"\t"<<accel[2]<<"\n";
	fid.close();
}
