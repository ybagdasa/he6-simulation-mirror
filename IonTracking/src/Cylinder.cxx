#include "Cylinder.h"
#include "Component.h"

Cylinder::Cylinder(char* nombre, double x, double y, double z, double alpha, double beta, double gamma, double r0, double r1, double l):
	Component(nombre, x, y, z, alpha, beta, gamma){
	Init(r0,r1,l);

}


Cylinder::Cylinder(char* nombre, Arm* ref, double x, double y, double z, double alpha, double beta, double gamma, double r0, double r1, double l):
	Component(nombre, ref, x, y, z, alpha, beta, gamma){
	Init(r0,r1,l);
}

Cylinder::~Cylinder(){}

void Cylinder::Init(double r0, double r1,double l){
	rin = r0;
	rout = r1;
	length = l;
	rin2 = r0*r0;
	rout2 = r1*r1;
	zup = l/2;
	zdown = -l/2;
}

bool Cylinder::Within(double pos[3]){
	double fakevel[3] = {0,0,0};
	double Npos[3], Nvel[3];
	coord->transformINTO(pos, fakevel , Npos, Nvel);
	double r2 = Npos[0]*Npos[0] + Npos[1]*Npos[1];
	if (Npos[2]<zup && Npos[2]>zdown && r2>rin2 && r2<rout2){ //ion is inside electrode
		return true;
	}
	else{return false;}		 
}
