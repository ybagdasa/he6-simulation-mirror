#include "Potential.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>

#define IN2MM 25.4

using namespace std;

Potential::Potential(const char* Vname, const char* Hname, const char* units){
	int convert; // value 0 if VFile grid is in mm, value 1 if in inches
	
	if(!ReadHFile(Hname)){
		printf("File %s could not be read.\n",Hname);
		exit(1);
	}

	InitArrays();
	
	if(strcmp(units, "mm")==0){convert = 0;} 
	else if (strcmp(units, "in")==0){convert = 1;}
	else{ 
		printf("Unrecognizable units %s. Input must be 'mm' or 'in'.\n",units);
		exit(1);
	}
		
	if(!ReadVFile(Vname,convert)){
		printf("File %s could not be read.\n",Vname);
		exit(1);
	}
	
	printf("Coord is REALLY initialized, coord[0][0] = %f\n", coord[0][0]);
}

Potential::Potential(const char* filename){

	if(!ReadHeader(filename)){
		exit(1);
	}
	InitArrays();	
	if(!ReadSource(filename)){
		exit(1);
	}
	//printf("Coord is REALLY initialized, coord[0][0] = %f\n", coord[0][0]);
	//printf("Coord is REALLY initialized, coord[0][1] = %f\n", coord[0][1]);
	for (int i = 0; i<3;i++) cout<<"coord["<<i<<"][nx-1] = "<<coord[i][(nx[i]-1)]<<endl;
}
Potential::~Potential(){
	for (int i=0; i<nx[0]; i++){
		for (int j=0; j<nx[1]; j++){
			delete [] V[i][j];
			delete [] E[0][i][j];
			delete [] E[1][i][j];
			delete [] E[2][i][j];
		}
		delete [] V[i];
		delete [] E[0][i];
		delete [] E[1][i];
		delete [] E[2][i];
	}
	delete [] V;
	delete [] E[0];
	delete [] E[1];
	delete [] E[2];
	
	delete [] E;
	
	for(int i=0;i<3;i++){
		delete [] coord[i];
	}
	delete [] coord;

}

void Potential::InitArrays(){
	E = new double*** [3];

	E[0] = new double ** [nx[0]];
	E[1] = new double ** [nx[0]];
	E[2] = new double ** [nx[0]];
	V = new double** [nx[0]];
	
	for(int i=0;i<nx[0];i++){
		V[i] = new double* [nx[1]];
		E[0][i] = new double* [nx[1]];
		E[1][i] = new double* [nx[1]];
		E[2][i] = new double* [nx[1]];
		for(int j=0;j<nx[1];j++){
			V[i][j] = new double [nx[2]];
			E[0][i][j] = new double [nx[2]];
			E[1][i][j] = new double [nx[2]];
			E[2][i][j] = new double [nx[2]];
			for(int k=0;k<nx[2];k++){
				V[i][j][k] = 0.0;
				E[0][i][j][k] = 0.0;
				E[1][i][j][k] = 0.0;
				E[2][i][j][k] = 0.0;
			}
		}
	}
	coord = new double * [3];
	for(int i=0;i<3;i++){
		coord[i] = new double [nx[i]];
		for(int j=0;j<nx[i];j++){
			coord[i][j] = 0.0;
		}
	}
	printf("Coord is initialized, coord[0][0] = %f\n", coord[0][0]);
	for(int i=0;i<3;i++){
		DX[i] = fabs(x1[i]-x0[i]);
		printf("xi = %f\t xf = %f\t Dx = %f\t nx = %i\n", x0[i],x1[i], DX[i], nx[i]);
	}
	skpcnt = 0;
}
bool Potential::ReadHFile(const char* filename){
	int elements = 0;
	FILE* fid = fopen(filename,"r");
	if (fid==NULL){
		printf("%s does not exist.\n", filename);
		return false;
	}
	else{
		char buffer[100];
		fgets(buffer,100,fid);
		elements = fscanf(fid,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%i\t%i\t%i\t",&x0[0],&x0[1],&x0[2],&x1[0],&x1[1],&x1[2],&nx[0],&nx[1],&nx[2]);	
		fclose(fid);
		
		if (elements != 9){
			printf("%i out of %i elements read from %s \n",elements,9,filename);
			return false;
		}
		else{return true;}
	}
}
bool Potential::ReadHeader(const char* filename){
	ifstream InputFile;
	InputFile.open(filename, ios::binary);
	if(InputFile.fail()){
		cout<< "Input file " <<filename<<" could not be opened for reading.\n";
		return false;
	}
	else{
		//read in header portion
		InputFile.read((char*) &x0, 3*sizeof(double));
		InputFile.read((char*) &x1, 3*sizeof(double));
		InputFile.read((char*) &nx, 3*sizeof(int));
		
		if(InputFile.fail()){
			cout<<"Error reading file "<<filename<<".\n";
			return false;
		}
		else{return true;}	
	}	
}

bool Potential::ReadSource(const char* filename){
	ifstream InputFile;
	InputFile.open(filename, ios::binary);
	if(InputFile.fail()){
		cout<< "Input file " <<filename<<" could not be opened for reading.\n";
		return false;
	}
	else{
		//ignore header portion
		InputFile.seekg(6*sizeof(double) + 3*sizeof(int), ios::beg);
		//read in source portion
		double x, y, z;
		for(int i=0; i<nx[0]; i++){
			for(int j=0; j<nx[1]; j++){
				for(int k=0; k<nx[2]; k++){
					InputFile.read((char*) &x, sizeof(double));
					InputFile.read((char*) &y, sizeof(double));
					InputFile.read((char*) &z, sizeof(double));
					InputFile.read((char*) &V[i][j][k], sizeof(double));
					InputFile.read((char*) &E[0][i][j][k], sizeof(double));
					InputFile.read((char*) &E[1][i][j][k], sizeof(double));
					InputFile.read((char*) &E[2][i][j][k], sizeof(double));
					
					if(i==0 && j==0) coord[2][k] = z;
				}
				if(i==0) coord[1][j] = y;
			}
			coord[0][i] = x;
		}
		if(InputFile.fail()){
			cout<<"Error reading file "<<filename<<".\n";
			return false;
		}
		else{return true;}	
	}
	
	
}
bool Potential::ReadVFile(const char* filename,int convert){
	int elements = 0;
	//char Vline [100];
	FILE* fid = fopen(filename,"r");
	if (fid==NULL){
		printf("%s does not exist.\n", filename);
		return false;
	}
	else{
	double x,y,z; //[in]
		for(int i=0; i<nx[0]; i++){
			for(int j=0; j<nx[1]; j++){
				for(int k=0; k<nx[2]; k++){
					elements += fscanf(fid,"%lf\t %lf\t %lf\t %lf\t %lf\t %lf\t %lf\n",&x,&y,&z,&V[i][j][k],&E[0][i][j][k],&E[1][i][j][k],&E[2][i][j][k]);
					if(i==0 && j==0) coord[2][k] = (convert ? round(100*IN2MM*z)/100.0 :z);
				}
				if(i==0) coord[1][j] = (convert ? round(100*IN2MM*y)/100.0 :y);
			}		
			coord[0][i] = (convert ? round(100*IN2MM*x)/100.0 :x);
		}
		
		fclose(fid);
		if (elements != nx[0]*nx[1]*nx[2]*7){
			printf("%i out of %i elements read from %s\n",elements,nx[0]*nx[1]*nx[2]*7,filename);
			return false;
		}
		else{
			printf("%i out of %i elements read from %s\n",elements,nx[0]*nx[1]*nx[2]*7,filename);
			return true;
		}
	}
}
double Potential::GetV(int k[3]){
	return V[k[0]][k[1]][k[2]];
}
double Potential::GetE(int k[3], int dir){
	return E[dir][k[0]][k[1]][k[2]];
}
void Potential::GetPos(int k[3],double pos[3]){
	for(int i=0;i<3;i++){
		pos[i] = coord[i][k[i]];
	}
}
void Potential::GetNx(int n[3]){
	for(int i=0;i<3;i++){
		n[i] = nx[i];
	}
}
void Potential::GetRegion(double pos0[3], double pos1[3]){
	for(int i=0;i<3;i++){
		pos0[i] = x0[i];
		pos1[i] = x1[i];		
	}
}
bool Potential::Within(double pos[3]){
	bool state = true;
	for(int i=0;i<3;i++){
		if(pos[i]>x1[i] || pos[i]<x0[i]){
			state = false;
			//printf("x1[%i] = %f\t x0[%i] = %f\n", i,x1[i],i,x0[i]);
			break;
		}
	}
	return state;
}
bool Potential::CalcV(double pos[3], double &Vout){
	double Vtemp[2][2][2], A[2][3], xlo, xhi, delX[3], sum;
	int klo[3];
	int i,l,m,n;//iterators l,m,m reserved for 0,1; all others 0,1,2
	
	bool nansExist = true;
	int nskp[3] = {1,1,1};
	
	Findklo(pos, klo);
	
	//initialize Vtemp
	while(nansExist){
		nansExist = false; 
		for(l=0;l<2;l++){
			for(m=0;m<2;m++){
				for(n=0;n<2;n++){
					Vtemp[l][m][n] = V[klo[0]+l*nskp[0]][klo[1]+m*nskp[1]][klo[2]+n*nskp[2]];
					if(isnan(Vtemp[l][m][n]) && !nansExist){ //only do once per assignment attempt
						//printf("Skipping z index at z = %f\t", coord[2][klo[2]+n*nskp[2]]);
						if(n==0){klo[2]--;} //if lower point is in electrode, look at lower row down
						nansExist = true; //repeat assignment
						nskp[2]++; //raise z index gap by 1
						if (nskp[2]>1){return false;}
						//printf("zskp = %i\n", nskp[2]);
						break;
					}				
				}
			}
		}
	}
	//calculate A,B, and delX
	for(i=0;i<3;i++){ 
		//printf("here and \n");
		xlo = coord[i][klo[i]];
		//printf("here\n");
		xhi = coord[i][klo[i]+nskp[i]];
		delX[i] = xhi-xlo;
		A[1][i] = (pos[i] - xlo)/delX[i]; //B
		A[0][i] = 1-A[1][i]; //A	
		//printf("delX = %f\t A = %f\t B = %f\n",delX[i],A[0][i],A[1][i]);
		//printf("klo[%i] = %i\t xlo[klo] = %f\t xhi[klo+1] = %f\n",i,klo[i],xlo, xhi);
		
	}
	//calculate V
	sum=0;
	for(l=0;l<2;l++){
		for(m=0;m<2;m++){
			for(n=0;n<2;n++){
				sum += A[m][1]*A[n][2]*A[l][0]*Vtemp[l][m][n];
			}
		}
	}
	if(isnan(sum)){
		printf("delX = %f\t A = %f\t B = %f\n",delX[i],A[0][i],A[1][i]);
		printf("position: %f\t%f\t%f\n",pos[0],pos[1],pos[2]);
		for(i=0;i<3;i++){ 	
			xlo = coord[i][klo[i]];
			xhi = coord[i][klo[i]+nskp[i]];
			printf("klo[%i] = %i\t xlo[klo] = %f\t xhi[klo+nskp] = %f\n",i,klo[i],xlo, xhi);
		}
		printf("Error: Using nan entry for potential!\n");
		//exit(1);
		return false;
	}
	else{
		Vout = sum;
		return true;
	}

}
bool Potential::CalcE(double pos[3], double Eout[3]){
	double Etemp[2][2][2], A[2][3], xlo, xhi, delX[3], sum;
	int klo[3];
	int i,l,m,n;//iterators l,m,m reserved for 0,1; all others 0,1,2

	Findklo(pos, klo);
//	printf("Okay?");
//	xlo = coord[0][0];
//	printf("Okay..");
	//calculate A,B, and delX
	for(i=0;i<3;i++){ 
		//printf("pos[%i] = %f\t klo[%i] = %i\n",i,pos[i],i,klo[i]);
		xlo = coord[i][klo[i]];
		xhi = coord[i][klo[i]+1];
		//printf("klo[%i] = %i\t xlo[klo] = %f\t xhi[klo+1] = %f\n",i,klo[i],xlo, xhi);

		delX[i] = xhi-xlo;
		A[1][i] = (pos[i] - xlo)/delX[i]; //B
		A[0][i] = 1-A[1][i]; //A	
		//printf("delX = %f\t A = %f\t B = %f\n",delX[i],A[0][i],A[1][i]);
		//printf("klo[%i] = %i\t xlo[klo] = %f\t xhi[klo+1] = %f\n",i,klo[i],xlo, xhi);
		
	}
	//calculate E
	for(i=0;i<3;i++){ //for each direction in E
		//Initialize E
		for(l=0;l<2;l++){
			for(m=0;m<2;m++){
				for(n=0;n<2;n++){
					Etemp[l][m][n] = E[i][klo[0]+l][klo[1]+m][klo[2]+n];
					if(isnan(Etemp[l][m][n])){return false;}//here check for boundary crossing
					//else{printf("Etemp = %f\n",Etemp[l][m][n]);}
				}
			}
		}
		//Interpolate Linearly
		sum=0;
		for(l=0;l<2;l++){
			for(m=0;m<2;m++){
				for(n=0;n<2;n++){
					sum += A[m][1]*A[n][2]*A[l][0]*Etemp[l][m][n];	
				}
			}
		}	
		if(isnan(sum)){
			printf("delX = %f\t A = %f\t B = %f\n",delX[i],A[0][i],A[1][i]);
			printf("position: %f\t%f\t%f\n",pos[0],pos[1],pos[2]);
			//for(i=0;i<3;i++){ 	
				
				xlo = coord[i][klo[i]];
				xhi = coord[i][klo[i]+1];
				printf("klo[%i] = %i\t xlo[klo] = %f\t xhi[klo+1] = %f\n",i,klo[i],xlo, xhi);
			//}
			printf("Error: Using nan entry for field!\n");
			exit(1);
			return false;
		}
		else{Eout[i] = sum;}
	}
	return true;
}
		
	
