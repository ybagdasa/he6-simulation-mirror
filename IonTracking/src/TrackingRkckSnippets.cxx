
void Tracking::TrackRkck(){
	int nstp, i;
	double h, hdid, hnext;
	double vscal[3], a[3];
	
	h = H1;
	for (nstp=0;nstp<MAXSTEP;nstp++){
		GetAccel(pos,vel,a);
		for (i=0;i<3;i++){
			vscal[i] = fabs(vel[i]) +fabs(a[i]*h) +TINY;
		}
		StepperRkck(a, h, vscal, hdid, hnext);
		if(hdid==h){++nok;}
		else{++nbad;}
		if(Done()){break;}
		if(hnext<=HMIN){
			printf("Error: hnext (%lf ns) is smaller than HMIN (%lf ns).\n",hnext, HMIN);
			exit(1);
		}
		h=hnext;		
	}
	if(nstp==MAXSTEP){printf("max step reached.\n");}	
	else{printf("Nstep = %i\n",nstp);}
	
}

void Tracking::StepperRkck(double a[3], double htry, double vscal[3], double &hdid, double &hnext){
	double errmax, h, htemp, tnew;
	double verr[3], vtemp[3], xtemp[3];
	int i;
	
	h = htry;
	do{
		rkck(h, a, vtemp, xtemp, verr);
		errmax = 0.0;
		for (i=0;i<3;i++){
			errmax = fmax(errmax,fabs(verr[i]/vscal[i]));
		}
		errmax /= EPS;
		if(errmax<= 1.0) {
			//printf("z=%f\n",pos[2]);
			break;
		} //Step successful. Grow step size.
		else{ //Shrink step and try again.
			htemp = SAFETY*h*pow(errmax,PSHRINK); //Shrink step.
			h = fmax(htemp, .1*h);
			tnew = tof + h;
			if (tnew==tof){
				printf("Stepsize underflow in Stepper.\n");
				exit(1);
			}
		}
	}while(true);
	if(errmax > ERRCON){hnext = SAFETY*h*pow(errmax,PGROW);} //Grow step.
	else{hnext = 5*htemp;} //But no more than a factor larger.
	hdid = h;
	//Update tof,pos, and vel of ion.
	tof += h;
	for (i=0;i<3;i++){
		vel[i] = vtemp[i];
		pos[i] = xtemp[i];
	}		  
}





void Tracking::rk5(double h, double a[3], double vout[3], double xout[3], double verr[3]){
	static double a2=0.2,a3=0.3,a4=0.6,a5=1.0,a6=0.875,b21=0.2,
		b31=3.0/40.0,b32=9.0/40.0,b41=0.3,b42 = -0.9,b43=1.2,
		b51 = -11.0/54.0, b52=2.5,b53 = -70.0/27.0,b54=35.0/27.0,
		b61=1631.0/55296.0,b62=175.0/512.0,b63=575.0/13824.0,
		b64=44275.0/110592.0,b65=253.0/4096.0,c1=37.0/378.0,
		c3=250.0/621.0,c4=125.0/594.0,c6=512.0/1771.0,
		dc5 = -277.00/14336.0;
	double dc1=c1-2825.0/27648.0,dc3=c3-18575.0/48384.0,
		dc4=c4-13525.0/55296.0,dc6=c6-0.25;
	double ak2[3],ak3[3],ak4[3],ak5[3],ak6[3],vtemp[3],xtemp[3];
	double x2[3], x3[3], x4[3], v2[3], v3[3], v4[3];

	for (i=0;i<3;i++){
		v2[i]=vout[i]+b21*h*a[i];
		x2[i]=xout[i]+a2*h*(vout[i]+v2[i])/2.0;
	}
	GetAccel(x2,v2,ak2);
	//Second step.
	for (i=0;i<3;i++){
		v3[i]=vout[i]+h*(b31*a[i]+b32*ak2[i]);
		x3[i]=xout[i]+a3*h*(vout[i]+v3[i])/2.0;
	}
	GetAccel(x3,v3,ak3);
	//Third step.
	for (i=0;i<3;i++){
		v4[i]=vout[i]+h*(b41*a[i]+b42*ak2[i]+b43*ak3[i]);
		x4[i]=xout[i]+a4*h*(vout[i]+v4[i])/2.0;
	}
	GetAccel(x4,v4,ak4);
	//Fourth step.
	for (i=0;i<3;i++){
		v5[i]=vout[i]+h*(b51*a[i]+b52*ak2[i]+b53*ak3[i]+b54*ak4[i]);
		x5[i]=xout[i]+a5*h*(vout[i]+v5[i])/2.0;
	}
	GetAccel(x5,v5,ak5);
	//Fifth step.
	for (i=0;i<3;i++){
		v6[i]=vout[i]+h*(b61*a[i]+b62*ak2[i]+b63*ak3[i]+b64*ak4[i]+b65*ak5[i]);
		x6[i]=xout[i]+a6*h*(vout[i]+v6[i])/2.0;
	}
	GetAccel(x6,v6,ak6);
	//Sixth step.
	for (i=0;i<3;i++){
	//Accumulate increments with proper weights.
		xout[i]=xout[i]+h*(c1*vout[i]+c3*v3[i]+c4*v4[i]+c6*v6[i]);
		vout[i]=vout[i]+h*(c1*a[i]+c3*ak3[i]+c4*ak4[i]+c6*ak6[i]);
		verr[i]=h*(dc1*a[i]+dc3*ak3[i]+dc4*ak4[i]+dc5*ak5[i]+dc6*ak6[i]);
	//Estimate error as difference between fourth and fifth order methods.
}
