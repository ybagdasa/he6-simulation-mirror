#include "TrackingCircle.h"
#include "Tracking.h"
#include "Toolbox.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>

using namespace std;

TrackingCircle::TrackingCircle(double m, double r, double z0, double z1, double nE, double nB, double fieldStrength, int numOfOrbits):Tracking(m,r,z0,z1,nE,nB), k(fieldStrength),N_T(numOfOrbits){}
TrackingCircle::~TrackingCircle(){}
void TrackingCircle::SetIonInfo(double v_orbit, double charge_state){
	v0 = v_orbit;
	K = k*(1e-6)*charge_state*CSPEED*CSPEED/mass; 			//K=kQ [mm/ns]^2/V
	R0 = K/v0/v0;
	T = 2*PI*K/v0/v0/v0;
	double v[3] = {0, v0, 0};
	double x[3] = {R0, 0, 0};
	Tracking::SetIonInfo(x,v,charge_state);
	//cout<< v0<<"\t"<<R0<<endl;
}
void TrackingCircle::SetIonInfoB(double v_orbit, double charge_state){
	v0 = v_orbit;
	gamma = 1/sqrt(1-v0*v0/CSPEED/CSPEED);
	K = k*(1e-6)*charge_state*CSPEED*CSPEED/mass/gamma; 			//K = BQ [ns^-1]
	R0 = v0/K; 
	T = 2*PI/K;
	double v[3] = {0, v0, 0};
	double x[3] = {R0, 0, 0};
	cout<<Q<<"\t"<< v0<<"\t"<<R0<<endl;
	Tracking::SetIonInfo(x,v,charge_state);
}
double TrackingCircle::GetT(){return T;}

void TrackingCircle::TrackVarStp(int option){
	int nstp, i,norbit,n1,n2,n3;
	double h,ha,hv,htot,S,tnew,htemp,hnext,errmax,Del1,Del0;
	double a[3],vtemp[3], xtemp[3], verr[3],vscal[3];
	double a2[3],vtemp2[3],xtemp2[3];
	double t;
	n1=0;
	n2=0;
	n3=0;
	//Initialize
	for (i=0;i<3;i++){
		Cv[i]=ACCURACY;
		xtemp[i]=xtemp2[i]=pos[i];
		vtemp[i]=vtemp2[i]=vel[i];
		verr[i] = 0.0;
	}
	
	h = HMAX;
	hnext = 1;
//	

	
	norbit = 0.0;
	t=0;
	ofstream IonTrack;
	ofstream IonTracktstp;
	
	
	IonTrack.open("IonOrbits.txt",ios::app);
	//IonTrack<<h<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";
	IonTracktstp.open("IonOrbitsTstp.txt",ios::app);
	//IonTracktstp<<h<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";

	while (abs(N_T*T-tof)>tof*LESSTINY){
		while((T-t)!=0){
			//printf("arg2 = %.15f\n",T-t);
			//Reinitialize temporary arrays
			for (i=0;i<3;i++){
				vtemp[i] = vtemp2[i] = vel[i];
				xtemp[i] = xtemp2[i] = pos[i];
			}

			if(highGrad || boundary){
				h = h/2.0;
				boundary = false;
				highGrad = false;
			}
			else{
				h = hnext;
			}
			GetAccel(pos,vel,a);
			
			/********************************************SIMION TIMESTEP ALGORITHM*******************************************/
			for (i=0;i<3;i++){
				hv = fabs(DMAX/vel[i]);
				ha = sqrt(fabs(2*DMAX/a[i]));
				if (fabs(a[i])>TINY && fabs(vel[i])>TINY){
					htot = hv*ha/(hv+ha);
				}
				else if(fabs(a[i])>TINY){htot = ha;}
				else if(fabs(vel[i])>TINY){htot = hv;}
				else{
					htot = HMAX;
					//printf("Error: acceleartion and velocity both zero. Ion's not going anywhere.");
					//exit(1);
				}
				//adjust time step according to stopping length
				S = fabs(.5*vel[i]*vel[i]/a[i]);
				//printf("Coord %i: hv = %f\t ha = %f\t htot = %f\t S = %f\n",i,hv,ha,htot,S);
				if (S>=SMIN){htot = htot;n1++;}
				else if (S<SMIN/10.0){htot = htot/10.0;n2++;} //reduce no more than factor of 10
				else{htot = htot*S/SMIN;n3++;}
				h = fmin(h,htot);
				//printf("h = %f\t htot = %f\n",h,htot);
			}
			/******************************************************************************************************************/
			
			//printf("Chosen step h = %f\n",h);
			if(h<=HMIN){
				//printf("WARNING: h (%f ns) is smaller than HMIN (%f ns).\n",h, HMIN);
				//exit(1);
			}
			//h=1;
			if((T-t)<h) h = T-t; //reduce even further if close to end of orbit
			
			//Genral error parameters for RK alogrithms
			for (i=0;i<3;i++) vscal[i] =fabs(vel[i])+fabs(h*a[i]);
			//for (i=0;i<3;i++) vscal[i] =fabs(h*a[i]);
			errmax = 0.0;
			if(option==1){
				/***********************Fourth Order Runge-Kutta with fifth order Step-doubling error estimate.******************/
				rk4(h, a, vtemp, xtemp); //full step
				//Error computation
				rk4(h/2.0, a, vtemp2, xtemp2); //half step
				GetAccel(xtemp2,vtemp2,a2);
				rk4(h/2.0, a2, vtemp2, xtemp2); //half step
				for (i=0;i<3;i++){
					Del1 = vtemp[i]-vtemp2[i];
					errmax = fmax(errmax,fabs(Del1/vscal[i]));		
				}
				/****************************************************************************************************************/
			}
			else{
				/***********************Fifth Order Runge-Kutta with fifth order error computation for the fourth order solution.***/
				rk5(h,a,vtemp,xtemp,verr); //full step
				//Error computation
				for (i=0;i<3;i++)	errmax = fmax(errmax,fabs(verr[i]/vscal[i]));		
				/****************************************************************************************************************/
			}
			hnext = HMAX;
			/********************General Timestep Reduction for RK alogrithms*****************************************/
			errmax/=EPS;
			//cout<<errmax<<endl;
			if(errmax> 1.0 && h>HMIN) { //Shrink step and try again.		
				htemp = SAFETY*h*pow(errmax,PSHRINK); //Shrink step.
				hnext = fmax(htemp, .1*h);
				continue;
			}
			else if(errmax > ERRCON){hnext = SAFETY*h*pow(errmax,PGROW);} //Grow step.
			else{hnext = 5*h;} //But no more than a factor larger.
			/****************************************************************************************************************/
				
			for (i=0;i<3;i++) if(Ca[i]>ACCURACY && h>HMIN) {highGrad = true; /*printf("Cv = %.5f\n", Cv[i]);*/}
			if(highGrad) continue; //don't take step
				//printf("Cv[0] = %.5f\t Cv[1] = %.5f\n",Cv[0],Cv[1]);
			tnew = tof + h;
			//printf("Chosen step h = %f\n",h);
			if (h<TINY)exit(1);
			//cout<<errmax<<endl;
			if (tnew==tof){
				printf("Stepsize underflow in Stepper.\n");
				exit(1);
			}
			//Update tof, pos, and vel
			tof += h;
			for (i=0;i<3;i++){
				vel[i] = vtemp[i];
				pos[i] = xtemp[i];
			}
			t += h;	//update time for current orbit
			IonTracktstp<<h<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";
		}//end of orbit
		norbit++;

		IonTrack<<h<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";
		//printf("torbit =%f\t period = %f\n tof =%f\t n*T=%f\n",t,T,tof,norbit*T);
		//reset timestep and orbit time
		//h=tstp;
		t=0;

		
	}
	printf("Tdif = %.15f\n", N_T*T-tof);
	IonTrack<<endl;
	IonTrack.close();
	IonTracktstp<<endl;
	IonTracktstp.close();
	//if(nstp==maxstep){printf("max step reached.\n");}	
	//else{printf("Nstep = %i\t maxstep = %i\n",nstp,maxstep);}
	
}



void TrackingCircle::TrackFixStp(double tstp){
	int nstp, i,norbit;
	double h,tnew;
	double a[3],vtemp[3], xtemp[3];
	double t;
	
	//h = tstp;
	
	if(h<=HMIN){
		printf("Error: h (%lf ns) is smaller than HMIN (%lf ns).\n",h, HMIN);
		exit(1);
	}
	
	norbit = 0.0;
	t=0;
	ofstream IonTrack;
	ofstream IonTracktstp;
	
	
	IonTrack.open("IonOrbits.txt",ios::app);
	IonTrack<<tstp<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";
	IonTracktstp.open("IonOrbitsTstp.txt",ios::app);
	IonTracktstp<<tstp<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";

	while (abs(N_T*T-tof)>tof*LESSTINY){
		while((T-t)!=0){
			//printf("arg2 = %.15f\n",T-t);
			if((T-t)<tstp){
				h = T-t;
			}
			GetAccel(pos,vel,a);
			rk4(h, a, vtemp, xtemp);
			tnew = tof + h;
			if (tnew==tof){
				printf("Stepsize underflow in Stepper.\n");
				exit(1);
			}
			//Update tof, pos, and vel
			tof += h;
			for (i=0;i<3;i++){
				vel[i] = vtemp[i];
				pos[i] = xtemp[i];
			}
			t += h;	//update time for current orbit
			IonTracktstp<<tstp<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";
		}//end of orbit
		norbit++;

		IonTrack<<tstp<<"\t"<<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<vel[0]<<"\t"<<vel[1]<<"\t"<<vel[2]<<"\t"<<tof<<"\t"<<norbit<<"\n";
		//printf("torbit =%f\t period = %f\n tof =%f\t n*T=%f\n",t,T,tof,norbit*T);
		//reset timestep and orbit time
		h=tstp;
		t=0;

		
	}
	printf("Tdif = %.15f\n", N_T*T-tof);
	IonTrack<<endl;
	IonTrack.close();
	IonTracktstp<<endl;
	IonTracktstp.close();
	//if(nstp==maxstep){printf("max step reached.\n");}	
	//else{printf("Nstep = %i\t maxstep = %i\n",nstp,maxstep);}
	
}

