#include "Toolbox.h"
#include <stdio.h>
#include <math.h>
#include <cstdlib>
//#include <limits>



double NaN = sqrt(-1.0);
void BuildRotInMatrix(double a, double b, double c, double rot[3][3]){
double ca = cos(a);
double cb = cos(b);
double cc = cos(c);
double sa = sin(a);
double sb = sin(b);
double sc = sin(c);

rot[0][0] = ca*cc - cb*sa*sc;
rot[0][1] = cc*sa + ca*cb*sc;
rot[0][2] = sb*sc;
rot[1][0] = -cb*cc*sa - ca*sc;
rot[1][1] = ca*cb*cc - sa*sc;
rot[1][2] = cc*sb;
rot[2][0] = sa*sb;
rot[2][1] = -ca*sb;
rot[2][2] = cb;
}
void InvertMatrix(double A[3][3], double invA[3][3]){
int i, j;
for (i=0;i<3;i++){
for (j=0;j<3;j++){
invA[i][j]=A[j][i];
}
}
}
void MatrixProd(double A[3][3], double B[3][3], double C[3][3]){
int i, j;
for (i=0;i<3;i++){
for (j=0;j<3;j++){
C[i][j] = A[i][0]*(B[0][j]) + A[i][1]*(B[1][j]) + A[i][2]*(B[2][j]);
}
}
}
void MatrixVecProd(double A[3][3], double v[3], double prod[3]){
int i;
for (i=0;i<3;i++){
prod[i]=A[i][0]*v[0] + A[i][1]*v[1] + A[i][2]*v[2];
}
}
double DotProd(double v1[3], double v2[3]){
return v1[0]*v2[0] + v1[1]*v2[1] + v1[2]*v2[2];
}
void CrossProd(double v1[3], double v2[3], double xprod[3]){
xprod[0] = v1[1]*v2[2] - v1[2]*v2[1];
xprod[1] = v1[2]*v2[0] - v1[0]*v2[2];
xprod[2] = v1[0]*v2[1] - v1[1]*v2[0];
}
void VectorSum(double v1[3], double v2[3], double sum[3]){
int i;
for (i=0;i<3;i++){
sum[i] = v1[i] + v2[i];
}
}
void VectorDiff(double v1[3], double v2[3], double diff[3]){
int i;
for (i=0;i<3;i++){
diff[i] = v1[i] - v2[i];
}
}
void PrintMatrix(double A[3][3]){
int i;
for (i=0;i<3;i++){
printf("%e \t %e \t %e \n",A[i][0], A[i][1],A[i][2]);
}
}
void FloorVec(double v[3]){
int i;
for (i=0;i<3;i++){
if (fabs(v[i])<2e-13){v[i] = 0;}
}
}
void PrintVec(double v[3]){
printf("%e \t %e \t %e \n", v[0],v[1],v[2]);
}
void Normalize(double v[3]){
double vmag = sqrt(DotProd(v,v));
int i;
for (i=0;i<3;i++){
v[i] = v[i]/vmag;
}
}


double K2V(double K, double mass){ 
	//converts energy (keV) to velocity (mm/ns) for an ion mass (MeV)
	return sqrt(K/mass/1000)*CSPEED;
}

double V2K(double v2, double mass){
	//converts velocity^2 (mm/ns)^2 to energy (keV) for an ion mass (MeV)
	double gamma = 1/sqrt(1-v2/CSPEED2);
	return 	mass*(gamma-1)*1000.0; //keV 
}

int SolveQuadratic(double A, double B, double C, double* root1, double* root2){
//returns 2 in the case of 2 positive solutions and 1 in the case of 1 positive and 1 negative solution.
//zero is considered to be a positive solution. The smallest positive solution is assigned to root1.
//returns -2 in the case of 2 negative solutions. No order in roots assignment.
//returns 0 in the case of 2 imaginary solutions. Doesn't assign roots.

int sign_B = 1;
if (B < 0) {sign_B = -1;}
double D;
D = B*B - 4*A*C;
if (D >= 0) {     /* Delta > 0: solutions are real */
double q, r1, r2;
q = -0.5*(B + sign_B * sqrt(D));
r1 = q/A;
r2 = C/q;
/* we identify very small values with zero */
if (fabs(r1) < 1e-12) {r1=0.0;}
if (fabs(r2) < 1e-12) {r2=0.0;}

//if (A==0.0){
// *root1 = r2;
// //*root2 = r1;
// return 3 // no good;
//}
/* now we choose the smallest positive solution */
if(r1<0.0 && r2>=0.0){
*root1=r2;
*root2=r1;
return 1;
//printf("One positive and one negative solution was found. \n");
    } /* r2 positive */

else if (r2<0.0 && r1>=0.0) {
*root1 = r1;
*root2 = r2;
return 1;
//printf("One positive and one negative solution was found. \n");
} /* r1 positive */

else if (r1>=0.0 && r2>=0.0){
//printf("Both solutions are positive. \n");
if ((r1 != 0 && r1 <= r2) || r2 == 0) {
*root1 = r1;
*root2 = r2;
}
else { //if r2 < r1 or r1 is zero
*root1 = r2;
*root2 = r1;
}
return 2;
}
else{
*root1 = r1;
*root2 = r2;
return -2;
} /* else two solutions are negative. */
}

   else {
*root1 = NaN;
*root2 = NaN;
//printf("D is negative => Imaginary solutions.\n");
//printf("D = %.8f \n",D);
return 0;
   }
}
int SolveCubic(double A, double B, double C, double D, double* root1, double* root2, double* root3){
//returns 1 or -1 in the case of only one real positive (>=0) or negative solution, assigned to root1.
//returns 3,2,-2,-3 in the cases of 3, 2, 1, or 0 of the three roots being positive (the others are negative).
//in the cases of returning 3 and 2 the smallest nonzero positive (>0) root is assigned to root1 if possible.
//in the case of -2, the positive (>=0) root is assigned to root1.

double r1, r2, r3;
// if A == 0, reduce to quadratic?
double p = B/A;
double q = C/A;
double r = D/A;
//printf("p = %.8f\t q = %.8f\t r = %.8f\n",p,q,r);

double u = q - (p*p)/3;
double v = r - p*q/3 + 2*(p*p*p)/27;

double j = 4*(u*u*u/27) + v*v; //discriminant
//if (fabs(j)<1e-15){j=0;}//associate small values with zero
//printf("u = %.8f\t v = %.8f\t j = %.8f\n", u,v,j);
if (j>0){ //one real solution
//double M = numerical_limits <double>::max();
//printf("max: %.10f",M);
/*if (fabs(p) > 27*pow(M, 1.0/3)){r1 = -p;}
else if (fabs(v) > sqrt(M)){r1 = pow(v,1.0/3);}
else if (fabs(u) > (3.0/4)*pow(M, 1.0/3)){r1 = (u/3)*pow(4, 1.0/3);}
else{*/
double w = sqrt(j);
//printf("w = %.8f\n",w);
if (v < 0){r1 = pow((w-v)/2, 1.0/3) - (u/3)*pow(2/(w-v), 1.0/3) - p/3;}
else {r1 = (u/3)*pow(2/(w+v), 1.0/3) - pow((w+v)/2, 1.0/3) - p/3;}
//}
*root1 = r1;
*root2 = NaN;
*root3 = NaN;
//printf("Cubic has only one solution: %.3f\n",r1);
if (r1 >= 0){return 1;}//one positive solution, including zero
else {return -1;} //one negative solution
}

else { // j<=0; three roots
//printf("j<=0\n");
double s = sqrt(-u/3);
double t;
if (s==0){t=0;}
else {t = - v/(2*pow(s,3.0));}
double k = acos(t)/3;
r1 = 2*s*cos(k) - p/3;
r2 = s*(-cos(k) + sqrt(3.0)*sin(k)) - p/3;
r3 = s*(-cos(k) - sqrt(3.0)*sin(k)) - p/3;
//printf("y1 = %.8f\t y2 = %.8f\t y3 = %.8f\n",r1,r2,r3);
//finding least positive root, non-zero if possible
double npos=0;
if (r1>=0){++npos;}
if (r2>=0){++npos;}
if (r3>=0){++npos;}

if (npos == 0){
*root1 = r1;
*root2 = r2;
*root3 = r3;
return -3;
}
else if(npos == 1){
if (r1>=0){
*root1 = r1;
*root2 = r2;
*root3 = r3;
}
else if (r2>=0){
*root1 = r2;
*root2 = r1;
*root3 = r3;
}
else {
*root1 = r3;
*root2 = r2;
*root3 = r1;
}
return -2;
}
else if (npos == 2){
if (r1>=0){
if(r2>=0 && r2<=r1 && r2!=0){
*root1 = r2;
*root2 = r1;
*root3 = r3;
}
else if(r3>=0 && r3<=r1 && r3!=0){
*root1 = r3;
*root2 = r1;
*root3 = r2;
}
else {
*root1 = r1;
*root2 = r2;
*root3 = r3;
}
}
else if (r2<=r3 && r2!=0){
*root1 = r2;
*root2 = r3;
*root3 = r1;
}
else{
*root1 = r3;
*root2 = r2;
*root3 = r1;
}
return 2;
}
else { //all three solutions positive
if (r1!=0){
if ((r2==0 || r1<r2) && (r3==0 || r1<r3)){
*root1 = r1;
*root2 = r2;
*root3 = r3;
}
else if (r2!=0 && r2<r1 && (r3==0 || r2<r3)){
*root1 = r2;
*root2 = r1;
*root3 = r3;
}
else {
*root1 = r3;
*root2 = r2;
*root3 = r1;
}
}
else if (r2!=0 && (r3==0 || r2<r3)){
*root1 = r2;
*root2 = r1;
*root3 = r3;
}
else {
*root1 = r3;
*root2 = r2;
*root3 = r1;
}
return 3;
}
}
}
int SolveQuartic(double A, double B, double C, double D, double E, double* root1, double* root2, double* root3, double* root4){
double r1,r2,r3,r4;
if (B==0){
if (A==0){
*root3 = NaN;
*root4 = NaN;
return SolveQuadratic(C,D,E,root1,root2);
}
if (D==0){
*root3 = NaN;
*root4 = NaN;
int sol_type = SolveQuadratic(A,C,E,root1,root2);
*root1 = sqrt(*root1);
*root2 = sqrt(*root2);
return sol_type;
}
}


   double a = B/A;
   double b = C/A;
   double c = D/A;
   double d = E/A;
//printf("A = %.8f\t B = %.8f\t C = %.8f\t D = %.8f\t E = %.8f\n",A,B,C,D,E);


double G,g,g1,g2,H,h,h1,h2,y;

if (b == a*a/4 + 2*c/a){
if (d < c*c/(a*a)){
           G = a/2;
           g = a/2;
           h1 = c/a;
           h2 = sqrt(c*c/(a*a) -d);
           H = h1 + h2;
           h = h1 - h2;
}
else if (d > c*c/(a*a)){ //all roots complex
           *root1 = NaN;
*root2 = NaN;
*root3 = NaN;
*root4 = NaN;
return 0;

}
else { //d = c^2/a^2, perfect square case
           G = a/2;
           H = c/a;
           int solutionType = SolveQuadratic(1, G, H, &r1, &r2);
if (solutionType == 0){return 0;}
else {
*root1 = r1;
*root2 = r1;
*root3 = r2;
*root4 = r2;
if (solutionType == 1){return 8;}
else {return solutionType*2;}
}
}
}
else {

double p = -2*b;
double q = a*c + b*b - 4*d;
double r = d*a*a -a*b*c + c*c;

double y1,y2,y3;

int solutionType = SolveCubic(1,p,q,r,&y1,&y2,&y3);

if (solutionType == 1 || solutionType == -1){y = y1;}
else { //find least root
y = y1;
if (y2 <= y){y = y2;}
if (y3 <= y){y = y3;}
}
//printf("yroot = %.8f\n",y);


//compute coefficients of quadratic equations
       g1 = a/2;
       h1 = (b-y)/2;
if (y < 0){ //negative root case
double n = sqrt(a*a - 4*y);
g2 = n/2;
           h2 = (a*h1 - c)/n;
}
else { //positive root case
           double m = sqrt((b-y)*(b-y) -4*d);
           g2 = (a*h1 - c)/m;
           h2 = m/2;
}

if ((g1<0 && g2<0) || (g1>0 && g2>0)){ //if same sign
           G = g1 + g2;
           g = y/G;
}
else { //if different sign
           g = g1 - g2;
           G = y/g;
}

if ((h1<0 && h2<0) || (h1>0 && h2>0)) { //if same sign
           H = h1 + h2;
           h = d/H;
}
else { //if different sign
           h = h1 - h2;
           H = d/h;
}
}
//printf("G = %.4f\t H = %.4f\t g = %.4f\t h = %.4f\n",G,H,g,h);
   int SolType1 = SolveQuadratic(1,G,H,&r1,&r2); //0,1,2,-2
   int SolType2 = SolveQuadratic(1,g,h,&r3,&r4);
//printf("SolType1: %i \t r1 = %.3f, r2 = %.3f \nSolType2: %i \t r3 = %.3f, r4 = %.3f \n",SolType1,r1,r2,SolType2,r3,r4);


if (SolType1 == 0 && SolType2 == 0){
//printf("ERROR: all 4 roots complex! \n");
   //printf("G = %.4f\t H = %.4f\t g = %.4f\t h = %.4f\n",G,H,g,h);
//printf("y = %.4e\n",y);
*root1 = NaN;
*root2 = NaN;
*root3 = NaN;
*root4 = NaN;
return 0;}
else if (SolType1 == 0 && SolType2 != 0){
*root1 = r3;
*root2 = r4;
*root3 = NaN;
*root4 = NaN;
return SolType2;
}
else if (SolType2 == 0 && SolType1 != 0){
*root1 = r1;
*root2 = r2;
*root3 = NaN;
*root4 = NaN;
return SolType1;
}
else if (SolType1 == -2){
*root1 = r3;
*root2 = r4;
*root3 = r1;
*root4 = r2;
if (SolType2 == -2){return -4;} //4 negative
else if (SolType2 == 1){return -3;}//3 neg, 1 pos
else {return 8;} // 2 neg, 2 pos
}
else if (SolType2 == -2){
*root1 = r1;
*root2 = r2;
*root3 = r3;
*root4 = r4;
if (SolType1 == 1){return -3;}//3 neg, 1 pos
else {return 8;}// 2 neg, 2 pos
}
else {//(SolType1 == 2 || SolType2 == 2)
//only combinations left (++ +- || +- ++ || ++ ++ || +- +-), will sift through others before getting here

if ((r1!=0 && r1 <= r3)||r3 == 0){
*root1 = r1;
*root2 = r3;
}
else {
*root1 = r3;
*root2 = r1;
}
*root3 = r2;
*root4 = r4;
int sol = SolType1 + SolType2;
if (sol == 2){return 8;}
else {return sol;}
}
}


double Bessi0(double x)
//Returns the modified Bessel fucntion I0(x) for any real x.
{
double ax,ans;
double y;
if ((ax=fabs(x)) < 3.75) {
y=x/3.75;
y*=y;
ans=1.0+y*(3.5156229+y*(3.0899424+y*(1.2067492+y*(0.2659732+y*(0.360768e-1+y*0.45813e-2)))));
}
else {
y=3.75/ax;
ans=(exp(ax)/sqrt(ax))*(0.39894228+y*(0.1328592e-1
+y*(0.225319e-2+y*(-0.157565e-2+y*(0.916281e-2
+y*(-0.2057706e-1+y*(0.2635537e-1+y*(-0.1647633e-1
+y*0.392377e-2))))))));
}
return ans;
}
