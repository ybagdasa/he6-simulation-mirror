#include "Optimizer.h"
#include "Toolbox.h"
#include "Potential.h"
#include "VGridUni.h"

#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <fstream>
#include <cmath>
#include <cstring>

using namespace std;

Optimizer::Optimizer(int nPlates, const char* filename, double vals[], double r, double z0, double z1, double Egoal){
	InitValues(nPlates,r,z0,z1,vals,Egoal);
	
	//InitializeFields
	//Potential *Efield[nPlates]; 			//temporary array of Efield instance
	Efield = new Potential* [nPlates]; //permanent array of original fields
	E = new double* [nPlates];				//permanent array of fields
	double pos[3];
	int kdex[3], nx[3], nxtemp[3];
	int ncounts;
	
	string Efile, EfileToOpen;   					//bare name and full name
	string fin = filename;
	unsigned found = fin.find_last_of("/");
  EfilePath = fin.substr(0,found+1);
	
	ifstream NameFile;				//name of the file that lists field map file names
	NameFile.open(filename);
	//Read in fields into temporary array
	for (int i =0;i<nPlates;i++){
		NameFile>>Efile;
		EfileToOpen = EfilePath + Efile;  
		Efield[i] = new VGridUni(EfileToOpen.c_str()); //initialize field instance
		if(i==0){//first instance
			//Check if ROI lies inside field region
			pos[0] = -r;
			pos[1] = -r;
			pos[2] = z0;
			if (!Efield[i]->Within(pos)){
				cout<<"Region of interest outside field region corresponding to file "<<Efile<<endl;
				exit(1);
			}
			pos[0] = r;
			pos[1] = r;
			pos[2] = z1;
			if (!Efield[i]->Within(pos)){
				cout<<"Region of interest outside field region corresponding to file "<<Efile<<endl;
				exit(1);
			}
			Efield[i]->GetNx(nx);
		}
		else{ //Check that all fields have same dimension
			Efield[i]->GetNx(nxtemp);
			if (nx[0] != nxtemp[0] || nx[1] != nxtemp[1] || nx[2] != nxtemp[2]){
				cout << "Fields do not have the same number of elements in all dimensions!" <<endl;
				exit(1);
			}
		}	
	}
	
	//Get the total number of points which lie within the ROI
	for(int i=0; i<nx[0]; i++){
		for(int j=0; j<nx[1]; j++){
			for(int k=0; k<nx[2]; k++){
				kdex[0] = i;
				kdex[1] = j;
				kdex[2] = k;
				Efield[0]->GetPos(kdex,pos);
				if((pos[0]*pos[0] + pos[1]*pos[1])<r*r && pos[2]>z0 && pos[2]<z1){ //if in bounds
					if (!isnan(Efield[0]->GetE(kdex,2))) nPts++; //if field value is not Nan
				}
			}
		}
	}
	//Initialize permanent field arrays
	for (int l =0;l<nPlates;l++){
		ncounts = 0; //permanent field array element
		E[l] = new double [nPts];
		for(int i=0; i<nx[0]; i++){
			for(int j=0; j<nx[1]; j++){
				for(int k=0; k<nx[2]; k++){
					kdex[0] = i;
					kdex[1] = j;
					kdex[2] = k;
					Efield[l]->GetPos(kdex,pos);
					if((pos[0]*pos[0] + pos[1]*pos[1])<r*r && pos[2]>z0 && pos[2]<z1){ //if in bounds
						if (!isnan(Efield[l]->GetE(kdex,2))){ //if field value is not Nan
							E[l][ncounts] = Efield[l]->GetE(kdex,2); //put into permanent array
							ncounts++;
						}
					}
				}
			}
		}	
	}
	
	//Initialized Scaled field array
	EScaled = new double [nPts];
		for (int i=0; i<nPts; i++){
			EScaled[i] = 0.0;
		}
//	for (int i=0; i<nPlates; i++){
//		delete Efield[i];
//	}
	
}

Optimizer::~Optimizer(){
	for (int i=0;i<g_m;i++){
		delete [] E[i];
	}
	delete [] E;
	delete EScaled;
	for (int i=0; i<g_m; i++){
		delete Efield[i];
	}
	delete Efield;
}

void Optimizer::InitValues(int nPlates, double r, double z0, double z1, double vals[], double Egoal){
	g_m = nPlates;
	R0 = r;
	Z0 = z0;
	Z1 = z1;
	E0 = Egoal;
	for (int i =0;i<N_PARAM;i++){
		Volt[i]=0;
		a[i]=0;
		deltaa[i]=0;
		da[i]=0;
		grad[i]=0;
	}
	nPts = 0; 
  chiSq1 = chiSq2 = chiSq3 = stepDown = 0;
  for (int i =0;i<nPlates;i++){
		Volt[i] = vals[i];
  }
  ParamsSet = false;
  ParamIncrSet = false;
  fract=.1;
  
  for (int i =0;i<N_PARAM-1;i++){
		afix[i] = 0;
		wrta[i] = 0;
		dV[i] = 0.0;
	}
	for (int i =0;i<N_PARAM;i++){
		isafixed[i] = 0;
	}
  nfix = 0;
  maxDiff = 5000; //V
}
void Optimizer::ComputeEScaled(){
	double sum;
	for (int i=0; i<nPts; i++){
		sum = 0;
		for (int j=0; j<g_m; j++){
			sum += a[j]*E[j][i];
		}
		EScaled[i] = sum;
	}
}
double Optimizer::CalcMean(){
	double mu,sum;
	sum = 0;
	for (int i=0; i<nPts; i++){
		sum += EScaled[i];
	}
	mu = sum/nPts;
	return mu;
}
void Optimizer::SetParams(double vals[]){
	for (int i =0;i<g_m;i++){
		a[i] = vals[i];
	}
	ParamsSet = true;
}

void Optimizer::GetParams(double vals[]){
	for (int i =0;i<g_m;i++){
		vals[i] = a[i];
	}
}

void Optimizer::SetParamIncr(double vals[]){
	for (int i =0;i<g_m;i++){
		deltaa[i] = vals[i];
	}
	ParamIncrSet = true;
}
void Optimizer::FixPotensDiff(int i, int j, double delV){
	if(!ParamsSet){
		cout << "Initial values of scaling parameters are not set! Cannot fix potential difference." <<endl;
		exit(1);
	}
	if (isafixed[j]) {
		//cout<< "WARNING: Parameter "<<j<<" is already fixed wrt to a potential. Final value will take precidence but any calculation based on value up to this point will still occur."<<endl;
		cout << "Error: Parameter "<<j<<" is already fixed wrt to a potential. Paramater can only be fixed once."<<endl;
		exit(1); 
	}
	afix[nfix] = j; //a to fix
	wrta[nfix] = i;	//fix wrt this a
	dV[nfix] = delV; 
	nfix+=1;
	FixPotensDiffInternal(i,j,delV);
	deltaa[j] = 0;
	isafixed[j] = 1; //mark as true
}
void Optimizer::FixPotential(int i, double V){
	double aNew = Poten2Param(i, V);
	PutParam(i,aNew);
	FixPotensDiff(i,i,0);
	ComputeFixedParams();
}
void Optimizer::UnfixPotential(int i, double incr){
	if (isafixed[i]){
		nfix-=1;
		deltaa[i] = incr;
		isafixed[i] = 0;
	}
	else {
		cout<< "WARNING: Paramter "<<i<<" was not fixed. Current increment = "<<deltaa[i]<<endl;
	}
}
void Optimizer::UnfixAllPotens(){
	for (int i =0;i<N_PARAM;i++){
		isafixed[i] = 0;
	}
  nfix = 0;
  ParamIncrSet = false;
}
void Optimizer::FixPotensDiffInternal(int i, int j, double delV){
	a[j] = (a[i]*Volt[i] + delV)/Volt[j];
}
void Optimizer::ComputeFixedParams(){
	for (int i=0;i<nfix;i++){
		FixPotensDiffInternal(wrta[i],afix[i],dV[i]);
	}
}
double Optimizer::CalcChiSq(){     
	double Xi2, var, mu;
	Xi2=var=0;
	ComputeFixedParams();
	ComputeEScaled();
	//mu = CalcMean(); //mean of EScaled
	for (int i=0; i<nPts; i++){
		var += pow((EScaled[i]-E0),2);
	}
	Xi2 = var/(nPts-1);
	return Xi2;
}

void Optimizer::RunFit(double ChiMax, double stepSize, double &chiNew, double &Emean){	
	if(!ParamsSet){
		cout << "Initial values of scaling parameters are not set! Cannot run fit." <<endl;
		exit(1);
	}
	if(!ParamIncrSet){
		cout << "Initial values of scaling parameter increments are not set! Cannot run fit." <<endl;
		exit(1);
	}
	double chiCut, chiOld;
	int trial;
	
	chiCut = ChiMax;
	chiNew = CalcChiSq();
	stepDown = stepSize;
	trial = 0;
	
	do{
	 printf("chiOld = %.8e\t chiNew = %.8e\n",chiOld,chiNew);
	 printf ("|chiOld - chiNew| = %.8e\n", fabs(chiOld-chiNew));
	 cout<<"\n\n";
	 if (fmod((float)trial,(float)MAXTRIALS)==0){
	 	chiCut = 2*chiCut;
	 }
		chiOld = chiNew;
		
		cout <<"Trial #  "<<trial <<"\t chiSq= " << chiNew << "\t a = " <<endl;
		for (int j=0; j<g_m; j++) printf("%2.4f\t", GetParam(j));
		cout << endl;

		Gradls(chiNew);
		trial ++;
	}while(fabs(chiOld-chiNew)>=chiCut);
	
	chiNew = CalcChiSq();
	Emean = CalcMean();
	SigParab();
	
	cout << "*****************************************************************************************************"<<endl;
	cout << "Results:\n";
	cout << "Voltages = \t";
	for (int j=0; j<g_m; j++) printf("%6.0f\t", GetParam(j)*Volt[j]);
	cout<<endl;
	cout<<"Uncertainties= \t";
	for (int j=0; j<g_m; j++) printf("%6.0f\t", siga[j]*Volt[j]);
	cout<< endl;
	cout<< "Differences =      \t";
	for (int j=1; j<g_m; j++) printf("%6.0f\t", GetParam(j)*Volt[j]-GetParam(j-1)*Volt[j-1]);
	cout << "\n Chi2 = "<<chiNew<<"\t E0 = "<<Emean<<" V/mm \t"<< "StdDev = " << sqrt(chiNew) <<endl; 
	
}

void Optimizer::Gradls(double &chiSqr){
	double stepSum,step1,delV,diff, diffSum;
	int   j, sign;
	int nbad, ngood;
	nbad = 0;
	CalcGrad();   //calculate the gradient
//-Evaluate chiSqr at new point and make sure chiSqr decreases-
	do{
		diffSum = 0;
		if (nbad==MAXSTEP){
			stepDown = stepDown*pow(2,FRESTORE*MAXSTEP); //restore fraction of origional stepsize and take step anyway
			cout << "Max number of bad steps reached. Restored stepsize = " << stepDown <<". \n";
			break;
		}
		
		for (j=0; j<g_m; j++){
			a[j] = a[j] + stepDown * grad[j]; //slide down
			//cout << a[j]*Volt[j] <<"\t";
		}
		//cout<<endl;
	
		
		
		
		
		
		chiSq3 = CalcChiSq();
		if (chiSq3 >= chiSq2){                         //must have overshot minimum
			//cout <<"Overshot: Chi2 = "<<chiSq3<<endl;
			//cout <<"Grad:\n"; 
			for (j=0; j<g_m; j++){
				//cout << grad[j]<<"/"<<a[j]<<"\t";
				a[j] = a[j] - stepDown * grad[j]; //restore
			}
			stepDown = stepDown/2;              //decrease stepSize
			nbad++;
			//cout <<"StepDown = "<<stepDown<<endl;
		}
	} while (chiSq3 >= chiSq2);
	stepSum = 0;
	ngood = 0;
// -- Increment parameters until chiSqr starts to increase -- 
	do{
		ngood++; //counts last step as good step 
		stepSum = stepSum + stepDown;   //counts total increment
		chiSq1 = chiSq2;
		chiSq2 = chiSq3;
		
		if (ngood==10*MAXSTEP){
			cout << "Max number of good steps reached. \n";
			break;
		}
		
		for (j=0; j<g_m; j++){
			a[j] = a[j] + stepDown * grad[j];
			//cout << grad[j]<<"/"<<a[j]<<"\t";
		}
		chiSq3 = CalcChiSq();
		//cout <<"Chi2 = "<<chiSq3<<endl;
		//cout <<"StepDown = "<<stepDown<<endl;
		
	} while (chiSq3 <= chiSq2);
// -- Find minimum of parabola defined by last three points -- 
	step1=stepDown*((chiSq3-chiSq2)/(chiSq1-2*chiSq2+chiSq3)+0.5);
	if (!isnan(step1)){
		for (j=0; j<g_m; j++) a[j] = a[j] - step1 * grad[j];    //move to minimum
	}
	chiSqr = CalcChiSq();
	stepDown = stepSum;                 //start with this next time
	
//	//**************************************************************************************88
//	cout << "Number of good steps = "<<ngood<<"\t Number of bad steps = "<<nbad<<"\n\n";
//	cout << "Adjust parameters for next step: \n";
//	//check maxDiff
//	for (j=0; j<g_m; j++){
//		cout << a[j]*Volt[j] <<"\t";
//	}
//	cout<<endl;
//	for (j=1; j<g_m; j++){
//		delV = a[j]*Volt[j] - a[j-1]*Volt[j-1];
//		sign = (delV > 0 ? 1 :-1);
//		if (fabs(delV) > maxDiff){
//			diff = fabs(delV) - maxDiff;
//			diffSum += sign*diff;
//			a[j] = a[j] - sign*diff/Volt[j]; //make difference = maxDiff
//		}
//	}
//	//cout << diffSum/g_m<<endl;
//	for (j=0; j<g_m; j++){			
//		a[j] = a[j] + .1*diffSum/g_m/g_m/Volt[j]; //add average offset to Params
//		cout << a[j]*Volt[j] <<"\t";
//	} 
//	cout<<endl;
//	cout<<"\n \n \n";
//	chiSqr = CalcChiSq();
	//********************************************************************************************88
}
void Optimizer::SigParab(){
	int j;

	for(j=0; j<g_m; j++){
		chiSq2 = CalcChiSq();
		a[j]   = a[j] + deltaa[j];
		chiSq3 = CalcChiSq();
		a[j]   = a[j] - 2*deltaa[j];
		chiSq1 = CalcChiSq();
		a[j]   = a[j] + deltaa[j];
		siga[j] = deltaa[j]*sqrt(2/(chiSq1-2*chiSq2+chiSq3));
	}
}
void Optimizer::CalcGrad(){
	int   j;
	double  dA, sum;

	sum = 0.0;
	chiSq2 = CalcChiSq();

	for (j=0; j<g_m; j++){
		dA  = fract * deltaa[j];     //differential element for gradent
		a[j]    = a[j] + dA;
		chiSq1  = CalcChiSq();
		a[j]    = a[j] - dA;
		grad[j] = chiSq2 - chiSq1;   //2*da*grad
		sum     = sum + grad[j]*grad[j];
  }

	for (j=0; j<g_m; j++)
		grad[j] =  deltaa[j]*grad[j]/sqrt(sum); //step * grad
}

void Optimizer::minuitFunction(int& nDim, double *gout, double &result, double par[], int flg){
	if(nDim>g_m){
		cout <<"Error: Parameter dimension mismatch between Optimizer and Minuit! Exiting."<<endl;
		exit(1);
	}
	SetParamsExternal(par);
	result = CalcChiSq();	
}
void Optimizer::SetParamsExternal(double par[]){
	int pindx=0;
	for (int i=0; i<g_m;i++){
		if(isafixed[i]) continue;
		a[i] = par[pindx];
		pindx++;
	}
	ComputeFixedParams();
}
void Optimizer::ComputePotens(double newPotens[]){
	int j;
	ComputeFixedParams();
	for (j=0; j<g_m; j++){
		newPotens[j] = a[j]*Volt[j];
	}
}
void Optimizer::CalcPotens2Params(double potens[], double par[]){
	int j;
	for (j=0; j<g_m; j++){
		par[j] = potens[j]/Volt[j];
	}
}

double Optimizer::Poten2Param(int i, double V){ //returns scaled parameter for the desired potential V for the ith electrode	
	return V/Volt[i];
}

int Optimizer::LinearSolve(double &chiNew, double &Emean, const char* mode){
	int i,l,k,ll,kk; //i runs to N=nPts; l,k run to g_m; ll,kk run to M = g_m - nfix 
	int M,N;
	N = nPts; 
	M = g_m - nfix; //number of free parameters to solve for
	string display = mode;
	
	TMatrixD alpha (M,M);
	TMatrixD beta (M,1);
	TMatrixD xsol (M,1); //solved parameter values
	double *Efixed; 
	Efixed = new double [N]; //E0 adjusted for fixed parameters
	
	double det = 0; //determanent for matrix inverse
	
	if(nfix>0){ //if some params fixed
		ComputeFixedParams();
		for(i=0;i<N;i++){
			Efixed[i] = E0; //initialize to mean field
			for (l=0;l<g_m;l++){
				Efixed[i] -= isafixed[l]*a[l]*E[l][i]; //subtract off fixed solutions from fit function
			}
		}
	}
	
	//construct matrix to invert alpha, and solution column vector beta
	ll=0;

	for (l=0;l<g_m;l++){
		if(isafixed[l]) continue;
		beta(ll,0) = 0;
		for(i=0;i<N;i++){
			beta(ll,0) += Efixed[i]*E[l][i];
		}
		kk=0;
		for (k=0;k<g_m;k++){
			if(isafixed[k]) continue;
			alpha(ll,kk) = 0;
			for(i=0;i<N;i++){
				alpha(ll,kk) += E[l][i]*E[k][i];
			}
			kk++;
		}
		ll++;
	}
	
	//Solve
	alpha.Invert(&det); 
	if(det==0){
		cout<< "Determanent of matrix is zero. Matrix is sigular or nearly singular. Cannot compute."<<endl;
		delete Efixed;
		return -1;
	}
	xsol = alpha*beta; 
	
	//put solution back into a
	int pindx=0;
	for (l=0; l<g_m;l++){
		if(isafixed[l]) continue;
		a[l] = xsol(pindx,0);
		siga[l] = alpha(pindx,pindx);
		pindx++;
	}
	chiNew = CalcChiSq();
	Emean = CalcMean();
	
	if (!display.compare("quiet")==0){
		cout << "*****************************************************************************************************"<<endl;
		cout << "Results:\n";
		cout << "Voltages = \t";
		for (int j=0; j<g_m; j++) printf("%6.2f\t", GetParam(j)*Volt[j]);
		cout<<endl;
		cout<<"Uncertainties= \t";
		for (int j=0; j<g_m; j++) printf("%6.2f\t", siga[j]*Volt[j]);
		cout<< endl;
		cout<< "Differences =      \t";
		for (int j=1; j<g_m; j++) printf("%6.2f\t", GetParam(j)*Volt[j]-GetParam(j-1)*Volt[j-1]);
		cout << "\n Chi2 = "<<chiNew<<"\t E0 = "<<Emean<<" V/mm \t"<< "Std/|E0| = "<<sqrt(chiNew)/fabs(Emean) <<endl; 
	}
	delete Efixed;
	return 0;
	
}

void Optimizer::OutputEScaled(const char* OutputName, const char* FileType){
	double pos[3], x0[3], x1[3], Etemp[3], Vtemp;
	int kdex[3], nx[3];
	int ncounts;
	ofstream OutputFile;
	
	Efield[0]->GetNx(nx);
	Efield[0]->GetRegion(x0,x1);
	
	ComputeFixedParams();
	
	
	string EfileToWrite;
	string ext = FileType;
	EfileToWrite = EfilePath + OutputName;
	if (ext.compare("txt")==0){
		string EfileH, EfileS;
		EfileS = EfileToWrite + ".txt";
		EfileH = EfileToWrite + "H.txt";
		cout << "Writing field to "<<EfileS<<" and "<<EfileH<<"."<<endl;
		ofstream OutputFileH;
		OutputFileH.open(EfileH.c_str(),ios::out);
		OutputFileH <<"x0\ty0\tz0\tx1\ty1\tz1\tnx\tny\tnz"<<endl;
		OutputFileH <<x0[0]<<"\t"<<x0[1]<<"\t"<<x0[2]<<"\t"<<x1[0]<<"\t"<<x1[1]<<"\t"<<x1[2]<<"\t"<<nx[0]<<"\t"<<nx[1]<<"\t"<<nx[2]<<endl;
		OutputFileH.close();
		OutputFile.open(EfileS.c_str(),ios::out);
		for(int i=0; i<nx[0]; i++){
			for(int j=0; j<nx[1]; j++){
				for(int k=0; k<nx[2]; k++){
					kdex[0] = i;
					kdex[1] = j;
					kdex[2] = k;
					Efield[0]->GetPos(kdex,pos);
					Etemp[0] = Etemp[1] = Etemp[2] = 0;
					Vtemp = 0;
					
					for (int l=0; l<g_m; l++){
						for (int m=0; m<3; m++){
							Etemp[m] = Etemp[m] + a[l]*Efield[l]->GetE(kdex,m);
						}
					Vtemp = Vtemp + a[l]*Efield[l]->GetV(kdex);	
					}
					OutputFile <<pos[0]<<"\t"<<pos[1]<<"\t"<<pos[2]<<"\t"<<Vtemp<<"\t"<<Etemp[0]<<"\t"<<Etemp[1]<<"\t"<<Etemp[2]<<endl;
				}
			}
		}
		OutputFile.close();	
		
	}
	else{
		EfileToWrite += ".dat";
		cout << "Writing field to "<<EfileToWrite <<"."<<endl;
		OutputFile.open(EfileToWrite.c_str(),ios::binary);
		OutputFile.write((char*) &x0, 3*sizeof(double));
		OutputFile.write((char*) &x1, 3*sizeof(double));
		OutputFile.write((char*) &nx, 3*sizeof(int));
		
		for(int i=0; i<nx[0]; i++){
			for(int j=0; j<nx[1]; j++){
				for(int k=0; k<nx[2]; k++){
					kdex[0] = i;
					kdex[1] = j;
					kdex[2] = k;
					Efield[0]->GetPos(kdex,pos);
					Etemp[0] = Etemp[1] = Etemp[2] = 0;
					Vtemp = 0;
					
					for (int l=0; l<g_m; l++){
						for (int m=0; m<3; m++){
							Etemp[m] = Etemp[m] + a[l]*Efield[l]->GetE(kdex,m);
						}
					Vtemp = Vtemp + a[l]*Efield[l]->GetV(kdex);	
					}
					OutputFile.write((char*) &pos, 3*sizeof(double));
					OutputFile.write((char*) &Vtemp, sizeof(double));
					OutputFile.write((char*) &Etemp, 3*sizeof(double));
				}
			}
		}
		OutputFile.close();		
	}

}
