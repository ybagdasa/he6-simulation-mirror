/*********************************************************************
  6He simulation UniformField Ion Tracking
  Author:      Ran Hong
  Date:        Jul. 29th 2013
  Version:     v1.04

  Track recoil ions in a uniform Electric field.
  Charge state selection availible.

*********************************************************************/

//Std includes
#include <iostream>
#include <string>
#include <cstring>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>


//Project includes
#include "IonDataStruct.h"
#include "UFTracking.h"
#include "Toolbox.h"

//Define macros
#ifndef PI
#define PI 3.14159265358
#endif

//main program

using namespace std;

/****************************************************************************/
UFTracking::UFTracking(double Field,double Pos,double IonMass)
{
  E_Field = Field;	//Electric field, in MV/cm;            
  M_r = IonMass;	//Ion mass, in MeV
  c = CSPEED*100;// 29979.2458;	//Speed of light, in cm/us
  End_Pos = Pos;

  Charge = 1.0;		//Charge state of the recoil ion
  for (int i=0;i<3;i++)
  {
    Init_Pos[i] = 0.0;
    V_Ion[i] = 0.0;
  }
  TOF = 0;			//Time of flight
  X_hit = 0;
  Y_hit = 0;			//Hit positions on MCP,in mm
  Ek_Ion = 0;		//Kinetic energy of ion
  HitAngle = 0;		//Hit angle on MCP
  Status = 0;
}

/****************************************************************************/
UFTracking::~UFTracking()
{
  ;
}

/****************************************************************************/
void UFTracking::SetIonInfo(double C,double* DEC_POS,double* Init_V)
{
  Charge = C;
  for (int i=0;i<3;i++)
  {
    Init_Pos[i] = DEC_POS[i];
    V_Ion[i] = Init_V[i];
  }
}

/****************************************************************************/
void UFTracking::GetOutput(double& Tof,double& xhit,double& yhit,double &IonEnergy,double& hitangle,uint8_t& Sta)
{
  Tof = TOF;
  xhit = X_hit;
  yhit = Y_hit;
  IonEnergy = Ek_Ion;
  hitangle = HitAngle;
  Sta = Status;
  //PrintVec(V_Ion);
}

/****************************************************************************/
double UFTracking::GetVz()
{
  return V_Ion[2];
  //PrintVec(V_Ion);
}

/****************************************************************************/
void UFTracking::Track()
{
  double distance = Init_Pos[2]/10.0 - End_Pos;	//Distance between MOT and MCP, in cm
  double a_recoil = -E_Field * Charge / M_r * pow(c,2.0); //positive acceleration, in cm/us
    
  TOF = (V_Ion[2]+sqrt(pow(V_Ion[2],2.0)+distance*2.0*a_recoil))/a_recoil*1000.0; //unit ns
//  cout<<distance<<"\t"<<a_recoil<<"\t"<<TOF<<endl;
//  cout<<pow(V_Ion[2],2.0)<<endl;
  X_hit = Init_Pos[0] + V_Ion[0] * TOF /1000.0 * 10.0;
  Y_hit = Init_Pos[1] + V_Ion[1] * TOF /1000.0 * 10.0;	//in mm
  V_Ion[2] -= a_recoil * TOF / 1000.0; //cm/us

  double Vxy = sqrt(pow(V_Ion[0],2.0)+pow(V_Ion[1],2.0));
  double V = sqrt(pow(Vxy,2.0)+pow(V_Ion[2],2.0));

  HitAngle = acos(-V_Ion[2]/V)/PI*180.0;	//deg
  Ek_Ion = 0.5*M_r*V*V/c/c*1000.0;	//keV
  
  double R_hit = sqrt(X_hit*X_hit+Y_hit*Y_hit);//in mm
  if (R_hit>38)Status=1;
  else Status = 0;
}

/****************************************************************************/
void UFTracking::TrackBack(double TOF,double xhit,double yhit,double& E_init)
{
  double distance = -End_Pos;	//Distance between MOT and MCP, in cm
  double a_recoil = E_Field * Charge / M_r * pow(c,2.0); //acceleration, in cm/us
  double V_Init[3];
  
  V_Init[0] = xhit/10.0*1000.0/TOF;
  V_Init[1] = yhit/10.0*1000.0/TOF;
  V_Init[2] = -(distance + 0.5*a_recoil*TOF*TOF/1000000.0)/(TOF/1000.0);
  
  E_init = 0.5*M_r*(V_Init[0]*V_Init[0]+V_Init[1]*V_Init[1]+V_Init[2]*V_Init[2])/c/c*1000000.0;
}
















