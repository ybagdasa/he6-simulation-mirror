bool Potential::CalcE(double pos[3], double E[3]){
	double Vold[2][2][2], Vnew[2][2][2], A[2][3], xlo, xhi, delX[3], sum;
	int klo[3];
	int i,j,k,l,m,n;//iterators l,m,m reserved for 0,1; all others 0,1,2
	bool nansExist = true;
	int nskp[3] = {1,1,1};
	Findklo(pos, klo);
	
	//initialize V
	while(nansExist){
		nansExist = false;
		for(l=0;l<2;l++){
			for(m=0;m<2;m++){
				for(n=0;n<2;n++){
					Vold[l][m][n] = V[klo[0]+l*nskp[0]][klo[1]+m*nskp[1]][klo[2]+n*nskp[2]];
					if(isnan(Vold[l][m][n]) && !nansExist){ //only do once per assignment attempt
						//printf("Skipping z index at z = %f\t", coord[2][klo[2]+n*nskp[2]]);
						if(n==0){klo[2]--;} //if lower point is in electrode, look at lower row down
						nansExist = true; //repeat assignment
						nskp[2]++; //raise z index gap by 1
						if (nskp[2]>1){return false;}
						//printf("zskp = %i\n", nskp[2]);
						break;
					}				
				}
			}
		}
	}
	//calculate A,B, and delX
	for(i=0;i<3;i++){ 
		xlo = coord[i][klo[i]];
		xhi = coord[i][klo[i]+nskp[i]];
		delX[i] = xhi-xlo;
		A[1][i] = (pos[i] - xlo)/delX[i]; //B
		A[0][i] = 1-A[1][i]; //A	
		//printf("delX = %f\t A = %f\t B = %f\n",delX[i],A[0][i],A[1][i]);
		//printf("klo[%i] = %i\t xlo[klo] = %f\t xhi[klo+1] = %f\n",i,klo[i],xlo, xhi);
		
	}

	//calculate E
	j=1;
	k=2;
	for(i=0;i<3;i++){
		sum=0;
		for(m=0;m<2;m++){
			for(n=0;n<2;n++){
				//sum += A[m][j]*A[n][k]*(A[1][i]*Vold[1][m][n]+A[0][i]*Vold[0][m][n]);
				sum += A[m][j]*A[n][k]*(Vold[1][m][n]-Vold[0][m][n]);
			}
		}
		E[i] = -sum/delX[i];
		//E[i] = sum;
		if(isnan(E[i])){
			printf("delX = %f\t A = %f\t B = %f\n",delX[i],A[0][i],A[1][i]);
			printf("position: %f\t%f\t%f\n",pos[0],pos[1],pos[2]);
			for(i=0;i<3;i++){ 	
				xlo = coord[i][klo[i]];
				xhi = coord[i][klo[i]+nskp[i]];
				printf("klo[%i] = %i\t xlo[klo] = %f\t xhi[klo+1] = %f\n",i,klo[i],xlo, xhi);
			}
			printf("Error: Using nan entry for field!\n");
			//exit(1);
			return false;
		}
		if(i==2){break;}
		//permute i->j, j->k, k->i
		else{	
			j=k;
			k=i;
			//i = j (in next iter)
			for(l=0;l<2;l++){
				for(m=0;m<2;m++){
					for(n=0;n<2;n++){
						Vnew[l][m][n] = Vold[n][l][m];
						//printf("Vnew[%i][%i][%i] = %f\tVold[%i][%i][%i] = %f\n",l,m,n,Vnew[l][m][n],l,m,n,Vold[l][m][n]);
					}
				}
			}
			//swap arrays
			for(l=0;l<2;l++){
				for(m=0;m<2;m++){
					for(n=0;n<2;n++){
						Vold[l][m][n] = Vnew[l][m][n];
						
					}
				}
			}
		}
	}
	return true;
}
