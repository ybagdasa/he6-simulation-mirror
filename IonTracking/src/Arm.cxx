#include "Arm.h"
#include "Toolbox.h"
#include <math.h>

Arm::Arm(double x, double y, double z, double alpha, double beta, double gamma){
	armPos[0] = x;
	armPos[1] = y;
	armPos[2] = z;
	BuildRotInMatrix(alpha, beta, gamma, RotIn);
	//PrintMatrix(RotIn);
	InvertMatrix(RotIn, RotOut);
}
Arm::Arm(double x, double y, double z, double alpha, double beta, double gamma, double pos0toRefABS[3], double RotIn0toRef[3][3]){
	double posReftoArmREF[] = {x,y,z};
	double RotInReftoArm[3][3];
	double RotOutRefto0[3][3];
	double posReftoArmABS[3];

	BuildRotInMatrix(alpha, beta, gamma, RotInReftoArm);
	MatrixProd(RotInReftoArm, RotIn0toRef, RotIn);
	InvertMatrix(RotIn0toRef, RotOutRefto0);
	MatrixVecProd(RotOutRefto0, posReftoArmREF, posReftoArmABS);
	VectorSum(pos0toRefABS, posReftoArmABS, armPos);
	InvertMatrix(RotIn, RotOut);
}
void Arm::rotateINTO(double vold[3], double vnew[3]){
	MatrixVecProd(RotIn, vold, vnew);
	FloorVec(vnew);
}
void Arm::rotateOUTOF(double vold[3], double vnew[3]){
	MatrixVecProd(RotOut, vold, vnew);
	FloorVec(vnew);
}
void Arm::transformINTO(double pos[3],double vel[3], double newpos[3], double newvel[3]){
	double diff[3];
	VectorDiff(pos,armPos,diff);
	rotateINTO(diff,newpos);
	rotateINTO(vel,newvel);
}

void Arm::transformOUTOF(double pos[3], double vel[3], double newpos[3], double newvel[3]){
	double rotpos[3];
	rotateOUTOF(pos,rotpos);
	VectorSum(rotpos,armPos,newpos);
	rotateOUTOF(vel,newvel);
}

//this is a test
