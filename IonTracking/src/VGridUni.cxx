#include "VGridUni.h"
#include "Potential.h"
#include <stdio.h>
#include <stdlib.h>
#include <cmath>

using namespace std;

VGridUni::VGridUni(const char* Vname, const char* Hname, const char* units):Potential(Vname, Hname, units){
	for(int i=0;i<3;i++){
		dx[i] = DX[i]/(nx[i]-1);
		//printf("%i\n",nx[i]);
	}
}
VGridUni::VGridUni(const char* Vname):Potential(Vname){
	for(int i=0;i<3;i++){
		dx[i] = DX[i]/(nx[i]-1);
		//printf("%i\n",nx[i]);
	}
}
VGridUni::~VGridUni(){}
void VGridUni::Findklo (double pos[3], int klo[3]){
	for(int i=0;i<3;i++){
		klo[i] = floor((pos[i]-x0[i])/dx[i]);
	}
}
