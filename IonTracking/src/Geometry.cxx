#include "Geometry.h"
#include <cstdio>
#include "Component.h"
#include "Cylinder.h"

using namespace std;

Geometry::Geometry(){
	char name[20]; 
	double zpos = -13-26*2;
	Electrodes = new Component* [ntrodes];
	for (int i=0; i<ntrodes;i++){
		sprintf(name, "Electrode%d", i);
		Electrodes[i] = new Cylinder(name, 0, 0, zpos, 0, 0, 0, 40,85,2);
		printf("Initialized %s\n",Electrodes[i]->GetName());
//		if(i!=0){
//			printf("%s\n",Electrodes[i-1]->GetName());
//		}
		zpos+=26; //next electrod 26 mm away
	}
	zpos = -13-26*3;
	Grid = new Cylinder("Grid", 0,0,zpos,0,0,0,40,85,2);
}

Geometry::~Geometry(){
//	for (int i = 0; i<ntrodes; i++){
//		printf("Deleting %s\n",Electrodes[i]->GetName());
//		delete Electrodes[i];
//	}
	delete [] Electrodes;
	delete Grid;
}
bool Geometry::Within(double pos[3]){
	char* name;
	for (int i=0;i<6;i++){
		if(Electrodes[i]->Within(pos)){
			printf("Boundary crossed in %s\n",Electrodes[i]->GetName());
			return true;
		}
	}
	
	if(Grid->Within(pos)){return true;}
	else{return false;}
}

		
