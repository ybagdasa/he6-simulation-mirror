#include "Component.h"
#include "Arm.h"
#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <typeinfo>
#include <string>


using namespace std;

Component::Component(char* nombre, double x, double y, double z, double alpha, double beta, double gamma){
	string str = nombre;
	size_t len = str.length(); 
	str.copy(name,len);
	Nabsorbed = 0;
	PositionErrorCount = 0;
	coord = new Arm(x,y,z,alpha,beta,gamma);
}

Component::Component(char* nombre, Arm* ref, double x, double y, double z, double alpha, double beta, double gamma){
	string str = nombre;
	size_t len = str.length(); 
	str.copy(name,len);
	Nabsorbed = 0;
	PositionErrorCount = 0;
	coord = new Arm(x,y,z,alpha,beta,gamma, ref->armPos, ref->RotIn);

}

Component::~Component(){
	delete coord;
}

const char* Component::GetName(){return name;}

