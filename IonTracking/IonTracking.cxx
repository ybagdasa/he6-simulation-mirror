/*********************************************************************
  6He simulation UniformField Ion Tracking
  Author:      Ran Hong (with modifications by Michael Kossin)
  Date:        Aug. 30th  2013
  Version:     v1.06

  Track recoil ions with selected module.
  Charge state selection availible.

  v1.04	Match the new IO format. Input format: see the README in
  BetaTracking program.
  v1.05 Separate the Uniform field tracking from the main program
  v1.06 (Michael Kossin) add support for Root file output.
  Output format:
  Name		Type		length(byte)
  TOF		unsigned int	3
  HitPos	int[2]		2*3
  HitAngle	unsigned short	2
  IonEnergy	unsigned short 	2
  Status	byte		1

  Total: 14 bytes per event
*********************************************************************/

//Std includes
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>
#include <ctime>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

//Root includes
#include "TROOT.h"
#include "TStyle.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TTree.h"

//Project includes
#include "IonDataStruct.h"
#include "UFTracking.h"
#include "Tracking.h"
#include "GlobalConstants.h"

//Define macros
#define NMAX 1000000000

//main program

using namespace std;

void PrintUsage();

int main(int argc, char **argv)
{
  TFile* IonOutputRoot;
  time_t startTime, endTime, iTime0, iTime1;
  double timeSec,timeMin;
  time(&startTime); //time at start of simulation

  int Nsim = NMAX;

  //Define parameters  
  double E_endpoint = 3.505;	//Endpoint energy
  double m_e = MASS_E;	//Electron mass in MeV
  double M_He6 = MASS_HE6; //6He atomic mass (from TRIUMF 2012)
  double M_Li6 = MASS_LI6;	//6Li atomic mass in MeV
	double M_He4 = MASS_HE4; //4He atomic mass in MeV
  double M_He3 = MASS_HE3; //3He atomic mass in MeV
	//double M_r = M_Li6 - m_e; //By default approximate ion mass
  double mass;
  double ChargeState = 1.0;	//Charge state of the recoil ion
  double A_ShakeOff = 0.091; //A and B parameters of shakeoff prob 1- (A + B*EIon)
  double B_ShakeOff = 0.0084;//A and B parameters of shakeoff prob 1- (A + B*EIon)
  double DEC_pos[3] = {0.0,0.0,0.0};	//Decay postion
  double V_Ion[3] = {0.0,0.0,0.0};
  double vel[3] = {0.0,0.0,0.0};
  double TOF;			//Time of flight
  double X_hit;
  double Y_hit;			//Hit positions on MCP,in mm
  double Ek_Ion;		//Kinetic energy of ion
  double HitAngle;		//Hit angle on MCP
  uint8_t Status;

  double EField;	//V/mm
  double MCPPosition;	//mm
//  double ZShift = 0; //mm
  double PosShift[3] = {0.0,0.0,0.0}; //Pos shift [mm]
  double tstp = H1; //ns//use default fixed timestep within function TrackFixTstp()
  double z0,zf,r0; //dimensions of flight region
  //zf = 32; //top plane
  //r0 = 37; //radius
  zf = 70.5; //top plane default position value [mm]
  r0 = 81; //radius default value [mm]
  
  string ModuleName, EFieldFile, BFieldFile, StepperName, Ion;
  ModuleName = "InterpF";
  StepperName = "Var";
  BFieldFile = "None";
  EFieldFile = "None";  
  int ModuleType = 1;		//0:Uniform Field; 1:Interpolated Field from Comsol
  int StepperType = 0; //0:Variable timestep stepper; 1: Fixed timestep stepper
  int ESwitch = 0; //0:off; 1:on
  int BSwitch = 0;  //0:off; 1:on

  unsigned found;
  string root, ext, dummy;
  string EFieldFullPath, BFieldFullPath, EFileToOpen, BFileToOpen;
  ifstream NameFile; //name of the file that lists field map file names

  double x0[3], v0[3]; //inital parameters of ion

  //Data directory
  char* DATA_DIRECTORY;
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");
  char* FIELD_DIRECTORY;
  FIELD_DIRECTORY=getenv("HE6_FIELD_MAPS_DIRECTORY");

  if (DATA_DIRECTORY==NULL) {
    cout << "Error:The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  if (FIELD_DIRECTORY==NULL) {
    cout << "Error:The environment variable HE6_FIELD_MAPS_DIRECTORY is not defined! Set it up to the field data directory first!\n";
    return -1;
  }


  //IO structs
  InIonDataStruct InData;
  OutIonDataStruct OutData;
  char filename[1000],rootFilename[1000];
  int counts;
  int nKept;

  int GeneratorID = 0;
  int BetafileID = 0;
  int OutputfileID = 0;

  //Random number generator
  TRandom3 rand_gen(0);
  double aux;   //Auxiliary number
	
  string command;
	string InFileName;
  //Input files
  ifstream inputfile;		
  ifstream IonInput;
  ifstream InfoInput;

  //Output files
  ofstream IonOutput;
  ofstream TOFOut;

  //TTrees
  TTree IonTree("IonTree","The Ion Dataset");
  IonTree.Branch("TOF",&(OutData.TOF),"TOF/i");
  IonTree.Branch("HitPos",&(OutData.HitPos[0]),"HitPosX/I:HitPosY/I");
  IonTree.Branch("HitAngle",&(OutData.HitAngle),"HitAngle/s");
  IonTree.Branch("IonEnergy",&(OutData.IonEnergy),"IonEnergy/s");
  IonTree.Branch("Status",&(OutData.Status),"Status/b");
  IonTree.Branch("ChargeState",&(OutData.ChargeState),"ChargeState/b");
  IonTree.Branch("Pos0",&(InData.DECPosition[0]),"x/S:y/S:z/S");
  IonTree.Branch("VIon0",&(InData.V_Ion[0]),"x/I:y/I:z/I");
  IonTree.Branch("EIon0",&(InData.EIon0),"EIon0/s");
  

  bool RootOutputRequested=false;
  bool DatOutputRequested=false;
  string OutputFormat ="b";
	char   CharBuffer[500];
	

	InFileName = argv[1];
	inputfile.open(InFileName.c_str(),ios::in);
	if(!inputfile.is_open()) {
		cout << "Error: Can't open input file "<<filename<<" !" <<endl;
		return -1;
	}

		
/**********************Input Commands from InputFile**********************/
	while(inputfile>>command) {

		if(command.compare("SetGeneratorID")==0) {
			inputfile>>GeneratorID;
		} else if(command.compare("SetRunID")==0) {
			inputfile>>BetafileID;
		} else if(command.compare("SetIonID")==0) {
			inputfile>>OutputfileID;
		} else if(command.compare("SetModule")==0) {
			inputfile>>ModuleName;
			if (ModuleName.compare("UF")==0) ModuleType = 0;
			else if (ModuleName.compare("InterpF")==0) ModuleType = 1;
			else {
				cout << "Error: Invalid specifier "<<ModuleName<<" for Module. Use 'UF' for uniform field tracker or 'InterpF' for interpolated field tracker.\n";
				return -1;
			}
		} else if(command.compare("SetFieldValue")==0) {
			inputfile>>EField;
		} else if(command.compare("SetMCPPosition")==0) {
			inputfile>>MCPPosition;		
		} else if(command.compare("SetIon")==0){
			inputfile>>Ion;
			if(Ion.compare("Li6")==0) mass = M_Li6 - m_e;
			else if (Ion.compare("He4")==0) mass = M_He4 - m_e;
			else if (Ion.compare("He6")==0) mass = M_He6 - m_e;
                        else if (Ion.compare("He3")==0) mass = M_He3 - m_e;
			else {
				cout<< "Invalid specifier "<<Ion<< "for Ion. Ex: Li6 for Lithium-6"<<endl;
				return -1;
			}
		} else if(command.compare("SetShakeoffProb")==0){//A and B parameters of shakeoff prob 1- (A + B*EIon)
			inputfile>>A_ShakeOff>>B_ShakeOff; 	
//		} else if(command.compare("SetZShift")==0){
//			inputfile>>ZShift;
		} else if(command.compare("SetPosShift")==0){
			inputfile>>PosShift[0]>>PosShift[1]>>PosShift[2];	
		} else if(command.compare("SetEFieldFile")==0){
			inputfile>>EFieldFile;
			ESwitch = 1;	
		} else if(command.compare("SetBFieldFile")==0){
			inputfile>>BFieldFile;
			BSwitch = 1;
		} else if(command.compare("SetStepperType")==0){
			inputfile>>StepperName;
			if (StepperName.compare("Var")==0) StepperType = 0;   //variable time step
     			else if (StepperName.compare("Fix")==0) StepperType = 1; //fixed time step
			else {
				cout << "Invalid specifier "<<StepperName<<" for Stepper. Use 'Var' for variable timestep or 'Fix' for fixed timestep.\n";
				return -1;
      }
		} else if(command.compare("SetTstp")==0){
			inputfile>>tstp;
		} else if(command.compare("SetRegionOfFlight")==0){
			inputfile>>r0>>z0>>zf;
			MCPPosition = z0;
		} else if(command.compare("SetOutputFormat")==0){
			inputfile>>OutputFormat;
		} else if(command.compare(0,1,"#")==0) {		//Comment line, ignore up to 500 characters
			inputfile.getline(CharBuffer,500);  
		} else {
			cout<<"Error: Unrecognizable command "<<command<<". Exiting IonTracker!"<<endl;
			return -1;
		} 
	}//end of while statement
	
/**********************Check Required Field Values**********************/
  if (MCPPosition == NULL){
    	cout<<"Error: MCPPosition value not set! Exiting!"<<endl;
  		return -1;
  }
  else z0 = MCPPosition;
    
  if (OutputfileID == 0){
    	cout<<"Error: IonID value not set! Exiting!"<<endl;
  		return -1;
  }
  if (Ion.empty()){
    	cout<<"Error: Ion value not set! Ex: Li6 for Lithium-6. Exiting!"<<endl;
  		return -1;
  }
  
	if((!OutputFormat.compare("r"))) RootOutputRequested=true;	
	else if((!OutputFormat.compare("d"))) DatOutputRequested=true;
	else if((!OutputFormat.compare("b"))){
		RootOutputRequested=true;	
		DatOutputRequested=true;
	}
	else{
		cout<<"Error: Invalid specifier "<<OutputFormat<<" for SetOutputFormat. Use 'r' for ROOT file, 'd' for DAT file, or 'b' for both."<<endl;
		return -1;
	}
	 
  if (ModuleType == 0){//UF
  	if (EField==NULL) {
  		cout<<"Error: EField value for UF Tracker not set! Exiting!"<<endl;
  		return -1;
  	}	
  }
  else if (ModuleType == 1){//InterpF
   
   if (EFieldFile.compare("None")==0) ESwitch = 0; //no field
   else {
      EFieldFullPath = FIELD_DIRECTORY;
      EFieldFullPath +="/../EFieldFiles/";
      found = EFieldFile.find_last_of(".");
      root = EFieldFile.substr(0,found);
      ext = EFieldFile.substr(found+1);
      if (strcmp(ext.c_str(), "list")==0) { //if file contains list of field files
				EFileToOpen = EFieldFullPath + EFieldFile;
				NameFile.open(EFileToOpen.c_str()); //open it
				if (!NameFile.is_open()) {
					cout<< "ERROR: Could not open "<< EFileToOpen <<endl;
					exit(1);
				}
				ESwitch = 0;
				while(1) { //count up number of filenames listed = field maps
					NameFile>>dummy;
					if (NameFile.eof()) break;
					else ESwitch ++;
				}
				NameFile.close();
      }
    }

    if (BFieldFile.compare("None")==0) BSwitch = 0; //no field
    else {
      BFieldFullPath = FIELD_DIRECTORY;
      BFieldFullPath +="/../EFieldFiles/";
      found = BFieldFile.find_last_of(".");
      root = BFieldFile.substr(0,found);
      ext = BFieldFile.substr(found+1);
      if (strcmp(ext.c_str(), "list")==0) { //if file contains list of field files
				BFileToOpen = BFieldFullPath + BFieldFile;
				NameFile.open(BFileToOpen.c_str());//open it
				if (!NameFile.is_open()) {
					cout<< "ERROR: Could not open "<< BFileToOpen <<endl;
					exit(1);
				}
				BSwitch = 0;
				while(1) { //count up number of filenames listed = field maps
					NameFile>>dummy;
					if (NameFile.eof()) break;
					else BSwitch ++;
				}
				NameFile.close();
      }
    }   
	}//end if Module 1 

  //temp hists
  int TOFHist[1000];
  for (int i=0; i<1000; i++) TOFHist[i] = 0;
  int HitAngleHist[300];
  for (int i=0; i<300; i++) HitAngleHist[i] = 0;

  /**************************************Module Dependent***********************************************************/
  //Construct Ion Tracking Module
  UFTracking* UFTracker = new UFTracking(EField*0.00001,MCPPosition/10.0,mass);  //Efield [MV/cm],MCP position [cm],M_r
  Tracking* letstrack = new Tracking(mass,r0,z0,zf,ESwitch,BSwitch); //Rest mass (MeV), Cylindrical Confines: radius,zlow,zhi [mm], NregionsofE, NregionsofB
  /**************************************Module Dependent***********************************************************/

  /**************************************Module Dependent***********************************************************/
  //Determine module type
  if(ModuleType ==1) {
    cout << "Running interpolated field tracking with electric field map "<<EFieldFile<<", magnetic field map "<<BFieldFile<<", and ";
    if(StepperType==0) cout << "variable timestep stepper.\n";
    else cout << "fixed timestep stepper with timestep "<<tstp<<" ns.\n";

    if (ESwitch==1) { //If only one field map, then filename is the filename of field map
      cout<<EFieldFullPath<<endl;
      EFieldFullPath+=EFieldFile;
      letstrack->InitializeFields(EFieldFullPath.c_str(),"E");
    }
    else if (ESwitch!=0) {
      NameFile.open(EFileToOpen.c_str()); //open previously defined file
      for (int i=0; i<ESwitch; i++) {
				NameFile>>dummy;
				EFileToOpen = EFieldFullPath + dummy;
				letstrack->InitializeFields(EFileToOpen.c_str(),"E");
      }
      NameFile.close();
    }

    if (BSwitch==1) { //If only one field map, then filename is the filename of field map
      BFieldFullPath+=BFieldFile;
      letstrack->InitializeFields(BFieldFullPath.c_str(),"B");
    }
    else if (BSwitch!=0) {
      NameFile.open(BFileToOpen.c_str()); //open previously defined file
      for (int i=0; i<BSwitch; i++) {
				NameFile>>dummy;
				BFileToOpen = BFieldFullPath + dummy;
				letstrack->InitializeFields(BFileToOpen.c_str(),"B");
      }
      NameFile.close();
    }

  }
  else {
    cout << "Running uniform field tracking with field value "<<EField<<" V/mm.\n";
  }
  /**************************************Module Dependent***********************************************************/
  //Open input/output files
  sprintf(filename,"%s/IonInfoFile%03d%02d.dat",DATA_DIRECTORY,GeneratorID,BetafileID);
  IonInput.open(filename,ios::in | ios::binary);
  if(IonInput.fail()) {
    cout << "Input file " << filename << " cannot be opened!\n";
    return -1;
  }
  sprintf(filename,"%s/IonEventFile%03d%02d%02d.dat",DATA_DIRECTORY,GeneratorID,BetafileID,OutputfileID);
  if(DatOutputRequested)
    IonOutput.open(filename,ios::out | ios::binary);
  sprintf(rootFilename,"%s/IonEventFile%03d%02d%02d.root",DATA_DIRECTORY,GeneratorID,BetafileID,OutputfileID);
  if(RootOutputRequested)
    IonOutputRoot=new TFile(rootFilename,"recreate");
  sprintf(filename,"%s/IonTofFile%03d%02d%02d.txt",DATA_DIRECTORY,GeneratorID,BetafileID,OutputfileID);
  TOFOut.open(filename);

  //Track ions
  counts = 0;
  nKept = 0;
  time(&iTime0); //start clock
  /* while(1){*/
  while(counts <Nsim) {
    //    IonInput>>EventID;
    IonInput.read((char*)InData.DECPosition,3*sizeof(short));
    if(IonInput.eof())break;
    for(int i=0; i<3; i++) {
      InData.V_Ion[i]=0;
      IonInput.read((char*)(&(InData.V_Ion[i])),sizeof(int));
      //The integer in the file occupies 3 byte, need to take care of the sign when converting it to int
      //Not anymore
      //      if(((InData.V_Ion[i]>>23) & 0x1) != 0) InData.V_Ion[i] |= 0xff000000;
      V_Ion[i] = double(InData.V_Ion[i])/100000.0;  //cm/us
      vel[i] = double(InData.V_Ion[i])/10000000.0;  //mm/ns
    }
    for(int i=0; i<3; i++) {
      DEC_pos[i] = double(InData.DECPosition[i])/200.0; //mm
      DEC_pos[i] = DEC_pos[i]+PosShift[i];
    }
//    DEC_pos[2] = DEC_pos[2] + ZShift; //mm
    // cout << DEC_pos[0]<<" " << DEC_pos[1]<<" " << DEC_pos[2]<<" "<<V_Ion[0]<<" "<<V_Ion[1]<<" "<<V_Ion[2]<<" "<<endl;

    //Tracking Ions using IonTracking Module
    //Select Charge state
    aux = rand_gen.Rndm();
    //Modify the shake-off probability, make it depend on E_Ion
    double EIon = 0.5*(M_Li6-m_e)*1000.0*(vel[0]*vel[0]+vel[1]*vel[1]+vel[2]*vel[2])/CSPEED/CSPEED;//keV
    InData.EIon0 = EIon*1000.0; //eV
    //    cout <<EIon<<endl;
    //    cout << A_ShakeOff+6.702e-3*EIon<<endl;
//    if(aux > 1.0000 - (A_ShakeOff+6.702e-3*EIon)) ChargeState=2.0;
    if(aux > 1.0000 - (A_ShakeOff+B_ShakeOff*EIon)) ChargeState=2.0;
    else ChargeState=1.0;

    /**************************************Module Dependent***********************************************************/
    if (ModuleType==0) {	//UFTracking
      UFTracker->SetIonInfo(ChargeState,DEC_pos,V_Ion);	//Set ion initial information
      UFTracker->Track();					//Track ion
      UFTracker->GetOutput(TOF,X_hit,Y_hit,Ek_Ion,HitAngle,Status);
    }
    else if (ModuleType==1) {	//(Interp) Tracking
      letstrack->SetIonInfo(DEC_pos,vel,ChargeState); //Set initial position [mm], velocity [mm/ns], and charge state (1 or 2)
      if (StepperType==0) {
	letstrack->TrackVarStp();
      }
      else {
	letstrack->TrackFixStp(tstp);
      }
      letstrack->GetOutput(TOF,X_hit,Y_hit,Ek_Ion,HitAngle,Status); //returns tof [ns],x_hit,y_hit [mm], kinetic energy of ion [keV], hit angle [deg], status (bool)
    }
    if (Status==0) TOFOut<<TOF<<"\n";
    //    cout << TOF <<endl;
    //    cin.get();
    /**************************************Module Dependent***********************************************************/
    //Fill temp hist
    if (Status==0) {
      letstrack->GetInitPos(x0);
      if (x0[2]>66) {
	letstrack->GetInitVel(v0);
	printf("x0 = %f\t y0 = %f\t z0 = %f\t vx0 = %f\t vy0 = %f\t vz0 = %f\n", x0[0], x0[1], x0[2], v0[0], v0[1], v0[2]);
      }
      //cout<<counts<<" "<<TOF<<endl;
      //TOFHist[int(TOF)]++;
      //HitAngleHist[int(HitAngle*10.0)]++;
    }

    //Save to structure, format data
    OutData.TOF = (unsigned int)(TOF*1000.0);	//in ps
    OutData.HitPos[0] = int(floor(X_hit*1000.0));	//in um
    OutData.HitPos[1] = int(floor(Y_hit*1000.0));
    OutData.HitAngle = (unsigned short)(HitAngle*100.0);	//0.01deg
    OutData.IonEnergy = (unsigned short)(Ek_Ion*1000.0);	//eV
    OutData.Status = Status;	//Keep event (0) or not(1)
    OutData.ChargeState = (uint8_t)ChargeState; 

    //Output TOF and other information
    if(DatOutputRequested)
    {
      IonOutput.write((char*)&(OutData.TOF),sizeof(unsigned int));
      IonOutput.write((char*)&(OutData.HitPos[0]),sizeof(int));
      IonOutput.write((char*)&(OutData.HitPos[1]),sizeof(int));	//Signs should be paid attention to
      IonOutput.write((char*)&(OutData.HitAngle),sizeof(unsigned short));
      IonOutput.write((char*)&(OutData.IonEnergy),sizeof(unsigned short));
      IonOutput.write((char*)&(OutData.Status),sizeof(uint8_t));
    }
    if(RootOutputRequested)
      IonTree.Fill();

    //Test output for debug
    //   cout <<"Output: "<<OutData.TOF<<" "<<OutData.HitPos[0]<<" "<<OutData.HitPos[1]<<" "<<HitAngle<<" "<<Ek_Ion<<endl;

    /**********************************Simulation Timing Info**********************************************************/
    time(&iTime1);//mark time
    timeSec = difftime(iTime1, iTime0);
    if (timeSec > 120) { //print out the state of things every 120 seconds
      timeSec = difftime(iTime1, startTime);
      timeMin = floor(timeSec/60);
      printf("%02.0f:%02.0f \t",timeMin,fmod(timeSec,60));
      timeSec = ((float)Nsim/(counts) - 1)*timeSec;
      timeMin = floor(timeSec/60);
      //printf("N%i\t Remaining: %02.0f:%02.0f \n",counts,timeMin,fmod(timeSec,60));
      time(&iTime0); //restart clock
    }
    /**********************************Simulation Timing Info**********************************************************/
    counts++;
    //    cout<<counts<<endl;
    if(OutData.Status == 0)nKept++;
  }
  if(RootOutputRequested)
  {
    IonOutputRoot->cd();
    IonTree.Write();
    IonOutputRoot->Close();
    delete IonOutputRoot;
  }
  //output MCP collection efficiency
  cout <<"MCP collection efficiency: "<< double(nKept)/double(counts)<<endl;

  IonInput.close();
  if(DatOutputRequested)
  {
    IonOutput.close();
  }
  TOFOut.close();

  if (ModuleType==0) {
    delete UFTracker;
  } else if (ModuleType==1) {
    delete letstrack;
  }
  /**********************************Simulation Timing Info**********************************************************/
  time(&endTime);
  timeSec = difftime(endTime, startTime);
  printf("Runtime: %.3f s \n",timeSec);
  timeMin = floor(timeSec/60);
  printf("Runtime: %02.0f:%02.0f \n",timeMin,fmod(timeSec,60));
  /**********************************Simulation Timing Info**********************************************************/
  ofstream tempOutput;
  tempOutput.open("TOFHist.txt",ios::out);
  for (int i=0; i<1000; i++) {
    tempOutput<<i<<" "<<TOFHist[i]<<endl;
  }
  tempOutput.close();
  tempOutput.open("AngleHist.txt",ios::out);
  for (int i=0; i<300; i++) {
    tempOutput<<i<<" "<<HitAngleHist[i]<<endl;
  }
  sprintf(filename,"%s/Setup/IonSetup%03d%02d%02d.log",DATA_DIRECTORY,GeneratorID,BetafileID,OutputfileID);
  ofstream logOutput;
  logOutput.open(filename,ios::out);
  if (ModuleType==0) {
    logOutput<<ModuleName<<" "<<EField<<" "<<MCPPosition<< " "<<Ion<<" "<<PosShift[0]<<" "<<PosShift[1]<<" "<<PosShift[2]<<" "<<counts <<endl;
  }
  else if(ModuleType==1) {
    logOutput<<ModuleName<<" "<<EFieldFile<<" "<<BFieldFile<<" "<<MCPPosition<<" "<<Ion<<" "<<PosShift[0]<<" "<<PosShift[1]<<" "<<PosShift[2]<<" "<<StepperName<<" "<<tstp<<endl;
  }
  else {
    cout<<endl<<endl<<"Unidentified ModuleType. Unable to write logfile."<<endl;
  }
  logOutput.close();
  return 0;
}

