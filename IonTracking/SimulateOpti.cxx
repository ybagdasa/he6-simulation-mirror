#include "Optimizer.h"
#include <fstream>
#include <iostream>

//Root includes
#include "TROOT.h"
#include "TFitter.h"
//#include "TStyle.h"
//#include "TRandom3.h"
//#include "TFile.h"



using namespace std;

Optimizer *myfields;
void minuitFunctionWrapper(int& nDim, double *gout, double &result, double par[], int flg){
	myfields->minuitFunction(nDim,gout,result,par,flg); //execute member function
}
int main(int argc,char **argv){

  time_t startTime, endTime, iTime0, iTime1;
  double timeSec,timeMin;
  time(&startTime); //time at start of simulation
  
  
	int nfiles = 10;
	double radius, zlow, zhi;
	double potentials[10] = {-320,-500,-3000,-2000,2000,6000,10000,14000,18000,22000};
	double k0[10] ={1,1,1,1,1,1,1,1,1,1};
	double dk[10] = {.1,.1,.1,.1,.1,.1,.1,.1,.1,.1};
	double atemp[10];
	double ChiMin, ChiNew, Emean, Emin;
	double E0 = -155;
	double newPotens[10];
	radius = 2;
	//Grid1:
	zlow = -92;
	zhi = 70;
	//Grid2:
//	zlow = -97;
//	zhi = -96;
	char* DATA_DIRECTORY;
	string pFile, FitType;
	int cnt;
	
	//Get data directory
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (DATA_DIRECTORY==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
	
	if (argc<3 || argc>4){
		cout << "Usage:\n";
		cout << "FitType E0 (ParameterFile.txt)\n";
		return -1;
	}	

	
  E0 = atoi(argv[2]);
  FitType = argv[1];
	
	if(argc==4) pFile = argv[3];
	else pFile = "Linear"; //this will be used for the FitLog filename
	
	if (FitType.compare("MINUIT")!=0 && FitType.compare("GRADLS")!=0 && FitType.compare("LINEAR")!=0){
		cout << "Unrecognizable fit type "<<FitType<<". Choose either MINUIT or GRADLS or LINEAR."<<endl;
		return -1;
	}
	string EFieldFullPath = DATA_DIRECTORY;
	EFieldFullPath +="/../EFieldFiles/FieldListOptimize.txt";
	string ParamFullPath = DATA_DIRECTORY;
	ParamFullPath += "/";
	ParamFullPath += pFile;
	
	//*****Initialize parameters for all fit routines**********
	myfields = new Optimizer(nfiles, EFieldFullPath.c_str(), potentials, radius, zlow, zhi, E0);
	myfields->SetParams(k0);
	myfields->SetParamIncr(dk);
//	myfields->FixPotensDiff(2,1, 4000); //Fix MCP Back wrt MCP Front
//	myfields->FixPotensDiff(2,0, 4000+180); //Fix Holder Block wrt MCP Front
//	myfields->FixPotensDiff(2,3, 1000); //Fix Grid wrt MCP Front
//	myfields->FixPotensDiff(2,2, 0); //Fix MCP Front
	
	myfields->FixPotensDiff(2,1, 2500); //Fix MCP Back wrt MCP Front
	myfields->FixPotensDiff(2,0, 2500+180);//Fix Holder Block wrt MCP Front
	myfields->FixPotensDiff(2,2, 0); //Fix MCP Front
	
	const int nfits = 7;
	//************Initialize root fitter***********
	TFitter* minuit = new TFitter(nfits);
	string paramName[nfits];
	char buffer[40];
	double par[nfits]; //fitter paramters
	double parlo[nfits],parhi[nfits]; //lower and upper bounds of parameters
	if (FitType.compare("MINUIT")==0){	
		double p1 = -1;
		minuit->ExecuteCommand("SET PRINTOUT",&p1,1); //make it quiet!
		minuit->SetFCN(minuitFunctionWrapper); //select chi2 function to minimize
		for(int i=0;i<nfits;i++) {
			sprintf(buffer,"a%02d",i+2);
			paramName[i] = string(buffer);
		}
		double PotensHi[10] = {0,0,-2000,0,5000,10000,15000,20000,25000,30000}; //max voltage bounds
		myfields->CalcPotens2Param(PotensHi, atemp);
		for(int i=0;i<nfits;i++) {
			parhi[i] = atemp[i+2];
			cout<<parhi[i]<<endl;
		}
		double PotensLo[10] = {0,0,-4000,-3000,0,0,0,0,0,0}; //min voltage bounds
		myfields->CalcPotens2Param(PotensLo, atemp);
		for(int i=0;i<nfits;i++){
			parlo[i] = atemp[i+2];
			cout<<parlo[i]<<endl;
		}			
	}
	
//**********Create and open log file ******************		
	ofstream LogFile;
	string logpath = DATA_DIRECTORY;
	logpath += "/OptimizerFitLog_";
	logpath += pFile;
	LogFile.open(logpath.c_str());
	
//************* NONLINEAR FITTING ROUTINES WITH ITERATIVE START VALUES **************		
	if(FitType.compare("LINEAR") != 0){
		double chi2 = 1e-12; //chi2 cut-off	
		ChiMin = myfields->CalcChiSq(); //initial value for minimum chi2

	 
	 	ifstream param_file;
	 	param_file.open(ParamFullPath.c_str());
	 	if (param_file.fail()){
	 		cout << "Error opening file " <<ParamFullPath<<"!"<<endl;
	 		return -1;
	 	}
	 	
	 	cnt = 0;
	 	while(!param_file.eof()){
	 		cnt++;
	 		for(int i=0;i<nfiles;i++){
	 			param_file >> k0[i];
	 		}
	 		
			cout<< "Run "<<cnt<<"."<<endl;
			if (FitType.compare("GRADLS")==0){
				myfields->SetParams(k0);
				myfields->RunFit(chi2,.1,ChiNew,Emean);
				myfields->GetParams(atemp);
			}
			else if (FitType.compare("MINUIT")==0){
				for(int i=0;i<nfits;i++) minuit->SetParameter(i,paramName[i].c_str(),k0[i+2],.5,parlo[i],parhi[i]);
				//minuit->FixParameter(0); //fix mcp front
				minuit->ExecuteCommand("SIMPLEX",0,0);
				minuit->ExecuteCommand("MIGRAD",0,0);
				for(int i=0;i<nfits;i++) par[i]=minuit->GetParameter(i);
				myfields->SetParamsExternal(par);
				myfields->GetParams(atemp);
				ChiNew = myfields->CalcChiSq();
				Emean = myfields->CalcMean();
			}
			else{
				cout<< "No option for FitType "<<FitType<<endl;
				return -1;
			}
			myfields->ComputePotens(newPotens);
	 		for (int s=0; s<nfiles; s++) LogFile << newPotens[s] << "\t";
			LogFile << ChiNew << "\t"<<Emean<<endl;
		}
		param_file.close();
	}
	//**********LINEAR REGRESSION *************************************************************
	else{ 
//		if(myfields->LinearSolve(ChiNew, Emean)){
//			cout<< "Failed to solve with Linear Least Squares Method!"<<endl;
//			exit(1);
//		}
//		myfields->GetParams(atemp);
//		myfields->ComputePotens(newPotens);
//	  for (int s=0; s<nfiles; s++) LogFile << newPotens[s] << "\t";
//		LogFile << ChiNew << "\t"<<Emean<<endl;
	}
	
	
	//myfields->OutputEScaled("He4CalibRun2_k1", "txt"); //Output scaled version of field to text file
	
	/*****************************COMPUTE THE HE4CALIBRUN2 FIELDS*******************************/
	myfields->UnfixPotential(2); //Unfix MCP Front
	myfields->FixPotensDiff(3,3,0); //Fix Grid
//	myfields->UnfixAllPotens();
	double k = 1.2;
	
	double Egoal = E0;
	//Open file to read potentials used for each run
	ifstream fin;
	string PotensListFullPath = DATA_DIRECTORY;
	PotensListFullPath +="/../EFieldFiles/He4PotensListRun3.txt";
	fin.open(PotensListFullPath.c_str());
	
	//Open file to write reoptimized fields for comparsion
	ofstream fout;
	string ResultsLog = DATA_DIRECTORY;
	ResultsLog += "/../EFieldFiles/AppxFebIISCRATCH.log";
	fout.open(ResultsLog.c_str(),ios::app);
	

	char ScaledName[200]; //Field txt file to export
	for (int i=0; i<1; i++){ //for each scaling run
		for (int j=2; j<10; j++){ //populate used potentials starting with MCP Front (accurate to 10 V as measured by temna voltmeter, neglecting voltage settings calibration on supplies)
			fin >>newPotens[j];
		}
		myfields->CalcPotens2Param(newPotens, atemp);
		myfields->SetParams(atemp);
		myfields->ComputeEScaled();
		sprintf(ScaledName,"FullRegionAppxFebII_k%d",i+1);
		myfields->OutputEScaled(ScaledName, "dat"); //Output scaled version of field to text file
/*		
		k-=.2;
		Egoal = k*E0;
*/		Emean = myfields->CalcMean();
		
		
/*		myfields->SetE0(Egoal);
		ChiNew = myfields->CalcChiSq();
		cout<<"\n\n\nk = "<<k<<" \t E0Goal = "<<Egoal<<"\t Std/|E0| = "<<sqrt(ChiNew)/fabs(Egoal)<<endl; //What is the deviation from the goal for the potentials used.
*/		myfields->SetE0(Emean);
		ChiNew = myfields->CalcChiSq();
		cout<<"k = "<<k<<" \t E0True = "<<Emean<<"\t Std/|E0| = "<<sqrt(ChiNew)/fabs(Emean)<<endl; //What is the deviation from the true mean for the potenatials used.
/*		
		fout<<k<<"\t"<<Emean<<"\t"<<sqrt(ChiNew)/fabs(Emean)<<"\t";
		myfields->ComputePotens(newPotens); //Output to double check what values were used
		cout << "Voltages:\n";
		for (int j=0; j<10; j++){ 
			cout << newPotens[j] <<"\t";
		}
		
		myfields->SetE0(Egoal);
		if(myfields->LinearSolve(ChiNew, Emean,"quiet")){
			cout<< "Failed to solve with Linear Least Squares Method!"<<endl;
			exit(1);
		}
		cout<<"\n";
		fout<<sqrt(ChiNew)/fabs(Emean)<<"\t";
		for (int j=0; j<10; j++){ 
			fout << newPotens[j] <<"\t";
		}
		
		myfields->ComputePotens(newPotens);
		cout<<"k = "<<k<<" \t E0True = "<<Emean<<"\t Std/|E0| = "<<sqrt(ChiNew)/fabs(Emean)<<endl;
		cout << "Voltages:\n";
		for (int j=0; j<10; j++){ 
			cout << newPotens[j] <<"\t";
			fout << newPotens[j] <<"\t";
		}
		fout<<zhi<<"\t"<<zlow<<"\t"<<radius<<"\t"<<endl;	
*/			
	}
	fin.close();
	fout.close();
/******************************WRAP UP**********************************************************/	
	LogFile.close();
	delete minuit;
	delete myfields;
	
	time(&endTime);
	timeSec = difftime(endTime, startTime);  
	printf("Runtime: %.3f s \n",timeSec);
	timeMin = floor(timeSec/60);
	printf("Runtime: %02.0f:%02.0f \n",timeMin,fmod(timeSec,60));


}

