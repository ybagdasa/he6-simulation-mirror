#include "Potential.h"
#include "VGridUni.h"
#include "Tracking.h"
#include "UFTracking.h"
#include "Toolbox.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>

//#define CSPEED 299.792458

using namespace std;
double K2V(double K, double mass);
void IonGenerator(double Emax, double Emin, double Thetamax, double Thetamin, double mass, double pos[3], double vel[3]);
int main() {
	//Data directory
  char* DATA_DIRECTORY;
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (DATA_DIRECTORY==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  srand(23);

  
  double M_Li6 = 5603.051; //Mass of Lithium-6 [MeV]
  double M_He4 = 3727.379; //Mass of Helium-4 [MeV]
	//double pos[3] = {2.123,2.95,-0.97};
	double T, T2,X, Y, Z, Vx, Vy, Vz, E, THETA,Q;
	uint8_t STATUS;
	double Emax = 1.4;
	double Emin = 0;
	double Tmax = PI; //from +z axis
 	double Tmin = 0; //from +z axis
 	double Zf = -99; //mm
 	double R0 = 38; //mm
	double vel[3] = {0,0,0};
	double vel2[3] = {0,0,0};
	double pos[3] = {0,0,0};
	int TimeDiffHist[200];
	for (int i=0;i<200;i++){
		TimeDiffHist[i] =0;
	}
	double TDif;//[100*ns]
	
	double Nsim = 1000;

	Tracking letstrack(M_Li6,R0,Zf,20);
	UFTracking utrack(-0.00155,Zf/10.0,M_Li6);  //Efield,MCP position,M_r
	letstrack.InitializeFields(DATA_DIRECTORY);
	
	ofstream OutIonInfo;
	OutIonInfo.open("IonInfoOutput.txt",ios::out);
	for(int i = 0;i<Nsim;i++){
		IonGenerator(Emax,Emin,Tmax,Tmin,M_Li6,pos,vel);
		letstrack.SetIonInfo(pos,vel,1);
		letstrack.TrackVarStp();
		letstrack.GetOutputFull(T, X, Y, Z, Vx, Vy, Vz, E, THETA, STATUS, Q);
		if(STATUS ==0){
			//printf("vx = %f mm/ns\t vx = %f mm/ns\t vx = %f mm/ns\t Zhit = %f mm\n",Vx,Vy,Vz,Z);
			//printf("N = %i\t xhit = %f mm\t yhit = %f mm\t t = %f ns\t theta = %f deg\t E = %f keV\n",i,X, Y, T,THETA,E, Q);
			OutIonInfo<<i<<" "<<Q<<" "<<vel[0]<<" "<<vel[1]<<" "<<vel[2]<<" "<<pos[0]<<" "<<pos[1]<<" "<<pos[2]<<" "<<Vx<<" "<<Vy<<" "<<Vz<<" "<<X<<" "<<Y<<" "<<Z<<" "<<T<<endl;
		}
		for(int j=0;j<3;j++){
			vel2[j] = vel[j]*100;
		}
		utrack.SetIonInfo(1,pos,vel2);	//Set ion initial information
      utrack.Track();					//Track ion
      utrack.GetOutput(T2, X, Y, E, THETA, STATUS);
      if(STATUS ==0){
	      //printf("N = %i\t xhit = %f mm\t yhit = %f mm\t t = %f ns\t theta = %f deg\t E = %f keV\n",i,X, Y, T2,THETA*RAD2DEG,E);
   	   //printf("\n\n");
   	   TDif = 100*(T-T2);
   	   //TimeDiffHist[int(TDif+100)]++;
		}
	}
	OutIonInfo.close();
	ofstream tempOutput;
  tempOutput.open("TDifHist.txt",ios::out);
  for (int i=0;i<200;i++){
    tempOutput<<i<<" "<<TimeDiffHist[i]<<endl;
  }
  tempOutput.close();

	return 0;
}



void IonGenerator(double Emax, double Emin, double Thetamax, double Thetamin, double mass, double pos[3], double vel[3]){
	double E,v,tang,theta,phi,vr,vz;
	E = Emin + (Emax-Emin)*rand01();
	v = K2V(E,mass);
	do{
		theta = sqrt(rand01())* PI/2;
		tang = tan(theta);
		vz = sqrt(v*v/(1+tang*tang));
		if (rand01()<.5){vz*=-1;}
	}while (acos(v/vz)<Thetamin ||acos(v/vz)>Thetamax);
	phi = rand01()*2*PI;
	vr = vz*tang;
	vel[2] = vz;
	vel[0] = vr*cos(phi);
	vel[1] = vr*sin(phi);
}
