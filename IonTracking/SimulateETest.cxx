#include "Potential.h"
#include "VGridUni.h"
#include "Tracking.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <cmath>

#define CSPEED 299.792458

using namespace std;
double K2V(double K, double mass);
int main() {
	int nregE = 1;
	int kLo[3]={0,0,0};
	double pos[3];
	Potential **regionsE;				//array of potential instances
	regionsE = new Potential* [nregE];
	const char* fin1 = "EGridOut1pt6mm.txt";
	const char* finH1 = "EGridOut1pt6mmH.txt";	
	
//	const char* fin1 = "GridOutpt25mmP1.txt";
//	const char* finH1 = "GridOutpt25mmP1H.txt";

//	const char* fin2 = "GridOutpt25mmP2.txt";
//	const char* finH2 = "GridOutpt25mmP2H.txt";

//	const char* fin3 = "GridOutpt25mmP3.txt";
//	const char* finH3 = "GridOutpt25mmP3H.txt";
//	
//	const char* fin4 = "GridOutpt25mmP4.txt";
//	const char* finH4 = "GridOutpt25mmP4H.txt";
	printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
	regionsE[0] = new VGridUni(fin1,finH1,"in");
	printf("&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&&\n");
	double something = regionsE[0]->GetE(kLo,1);

	printf("something = %f\n", something);
	regionsE[0]->GetPos(kLo,pos);
	printf("x = %f\t y = %f\t z = %f\n\n",pos[0],pos[1],pos[2]);
//	regionsE[1] = new VGridUni(fin2,finH2);
//	regionsE[2] = new VGridUni(fin3,finH3);
//	regionsE[3] = new VGridUni(fin4,finH4);
	
	int hereE;
	int i,j;
	//int klo[3];
//double pos[3] = {2.123,2.95,-0.97};
	//pot.Findklo(pos,klo);
	//printf("klo = %i\t%i\t%i\n",klo[0],klo[1],klo[2]);
	double Eout[3],EcomNorm[3],EcomFine[3], V, Vout;
	//printf("pos[0] = %f\tpos[1] = %f\tpos[2] = %f\n",pos[0],pos[1],pos[2]);
	//double V;
	//V = pot.GetV(klo);
	//double posout[3],Eout[3];
	//pot.GetPos(klo,posout);
	
	const char* filename = "InterpolatedSampleChamberCyl.txt";
	//const char* fname2 = "InterpolatedSampleFine.txt";
	const char* fout = "EcompareChamberGrid1pt6mm.txt";
	const char* ferr = "EerrSphere.txt";
	int val;
	bool good1,good2;
	FILE *fid = fopen(filename,"r");
	//FILE *fid2 = fopen(fname2,"r");
	FILE *fid3 = fopen(fout,"w");
	FILE *fid4 = fopen(ferr,"w");
	if (fid==NULL){
		fclose(fid);
		printf("%s does not exist.\n", filename);
	}
	//else if(fid2==NULL){
		//fclose(fid2);
		//printf("%s does not exist.\n", fname2);
	//}
	
	else{
		double x,y,z;	
		for(i=0; i<1000000; i++){
			val =fscanf(fid,"%lf\t%lf\t%lf\t%lf\t%lf\t%lf\t%lf\n",&x,&y,&z,&V,&EcomNorm[0],&EcomNorm[1],&EcomNorm[2]);
			pos[0] = 25.4*x;
			pos[1] = 25.4*y;
			pos[2] = 25.4*z;
			if(isnan(V)){
				fprintf(fid4,"%f\t%f\t%f\t%i\n",pos[0],pos[1],pos[2],0);
				continue;
			}
			if(pos[2]>20||pos[2]<-89||(pos[0]*pos[0] + pos[1]*pos[1] >39*39)){ //ROI cuts
				continue;
			}
			hereE = -1;	
			for (j=0;j<nregE;j++){
				if(regionsE[j]->Within(pos)){
					hereE = j;
					break;
				}
			}
	
			if(hereE<0){
				//printf("Point not in any field regions %i\n",i);
				//printf("x = %f\t y = %f\t z = %f\n\n",pos[0],pos[1],pos[2]);
				fprintf(fid4,"%f\t%f\t%f\t%i\n",pos[0],pos[1],pos[2],1);
				continue;
			}
			else{
			
				regionsE[hereE]->Findklo(pos,kLo);
				//printf("klo = %i\t%i\t%i\n", kLo[0], kLo[1], kLo[2]);
				//printf("hereE = %i\n",hereE);
				if(!regionsE[hereE]->CalcE(pos,Eout)){
					fprintf(fid4,"%f\t%f\t%f\t%i\n",pos[0],pos[1],pos[2],2);
					continue;
				}//no calculation available
				if(!regionsE[hereE]->CalcV(pos,Vout)){
					fprintf(fid4,"%f\t%f\t%f\t%i\n",pos[0],pos[1],pos[2],3);
					continue;
				}//no calculation available
			}
			//printf("x = %f\t y = %f\t z = %f\n",pos[0],pos[1],pos[2]);
			//printf("Ex/Excom = %f\t%f\t Ey/Eycom = %f\t%f\t Ez/Ezcom = %f\t%f\n",Eout[0],EcomNorm[0],Eout[1],EcomNorm[1],Eout[2],EcomNorm[2]);
			//fscanf(fid2,"%*f\t%*f\t%*f\t%*f\t%*f\t%*f\t%*f\t%lf\t%lf\t%lf\n",&EcomFine[0],&EcomFine[1],&EcomFine[2]);
			//fprintf(fid3,"%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n",Eout[0],EcomNorm[0],Eout[1],EcomNorm[1],Eout[2],EcomNorm[2],pos[0],pos[1],pos[2]);
			fprintf(fid3,"%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\t%f\n",Eout[0],EcomNorm[0],Eout[1],EcomNorm[1],Eout[2],EcomNorm[2],pos[0],pos[1],pos[2], Vout,V);
			
		}
	fclose(fid);
	//fclose(fid2);
	fclose(fid3);
	fclose(fid4);
	}
//	double pos[3] = {2.123,2.95,-0.97};
//	double pos[3] = {12.121545,12.946620,-2.099310};
//	double T, X, Y, E, THETA;
//	bool STATUS;
//	double M = 5603.051; //MeV
//	double v0 = K2V(1.0,M);
//	double vel[3] = {v0/4,v0/4,0};

//	Tracking letstrack(M,40,-99,20);
//	letstrack.SetIonInfo(pos,vel,1);
//	letstrack.TrackVarStp();
//	letstrack.GetOutput(T, X, Y, E, THETA, STATUS);
//	//double a[3];
//	//letstrack.GetAccel(pos,pos, a);
//	printf("xhit = %f mm\t yhit = %f mm\t t = %f ns\t theta = %f deg\t E = %f keV\n",X, Y, T,THETA,E);
	delete [] regionsE;
	return 0;
}

double K2V(double K, double mass){ 
	//converts energy (keV) to velocity (mm/ns) for an ion mass (MeV)
	return sqrt(K/mass/1000)*CSPEED;
}
