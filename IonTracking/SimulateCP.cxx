#include "Potential.h"
#include "VGridUni.h"
#include "Tracking.h"
#include "TrackingCircle.h"
#include "UFTracking.h"
#include "Toolbox.h"
#include <stdio.h>
#include <stdlib.h>
#include <cstring>
#include <iostream>
#include <fstream>
#include <cmath>

//#define CSPEED 299.792458

using namespace std;
double K2V(double K, double mass);
void IonGenerator(double Emax, double Emin, double Thetamax, double Thetamin, double mass, double pos[3], double vel[3]);


int main() {
	//Data directory
  char* DATA_DIRECTORY;
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (DATA_DIRECTORY==NULL){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  srand(23);

  
  double M_Li6 = 5603.051; //Mass of Lithium-6 [MeV]
  double M_He4 = 3727.379; //Mass of Helium-4 [MeV]
	//double pos[3] = {2.123,2.95,-0.97};
	double T, T2,X, Y, Z, Vx, Vy, Vz, E, THETA,Q;
	uint8_t STATUS;
	
	double k = 50000;
	double timestep = .2;
	double v[7] = {.1,.15,.2,.25,.3,.35,.4};
	double r = 35.6948;//80.3134; //matches circular motion v=.1
	double gamma[12];// ={225,180,170,135, 110, 90, 70, 45,10,-45};  
	
	for (int i = 0;i<12;i++){
		gamma[i]=0 +i*30;
	}
	double vel[3];
	vel[2] = 0.0;
	double pos[3] = {r, 0, 0};

	Tracking letstrack(M_Li6,98.0,-2,2); //mass,R0,Zi, Zf
	letstrack.InitializeFields(DATA_DIRECTORY);
	
	ofstream OutIonInfo;
	OutIonInfo.open("IonInfoOutput.txt",ios::out);
	for(int i = 1;i<2;i++){
		for(int j=0;j<12;j++){
			vel[0] = v[i]*cos(gamma[j]*PI/180);
			vel[1] = v[i]*sin(gamma[j]*PI/180);
			
			letstrack.SetIonInfo(pos,vel,1);
			letstrack.TrackFixStp(timestep,10000,50); //timestep, total tof, write every nth timestep
			letstrack.GetOutputFull(T, X, Y, Z, Vx, Vy, Vz, E, THETA, STATUS, Q);
			if(STATUS ==0){
				//printf("vx = %f mm/ns\t vx = %f mm/ns\t vx = %f mm/ns\t Zhit = %f mm\n",Vx,Vy,Vz,Z);
				//printf("N = %i\t xhit = %f mm\t yhit = %f mm\t t = %f ns\t theta = %f deg\t E = %f keV\n",i,X, Y, T,THETA,E, Q);
				OutIonInfo<<i<<" "<<Q<<" "<<Vx<<" "<<Vy<<" "<<Vz<<" "<<X<<" "<<Y<<" "<<Z<<" "<<T<<endl;
			}
		}

		
	}
	OutIonInfo.close();
	return 0;
}



void IonGenerator(double Emax, double Emin, double Thetamax, double Thetamin, double mass, double pos[3], double vel[3]){
	double E,v,tang,theta,phi,vr,vz;
	E = Emin + (Emax-Emin)*rand01();
	v = K2V(E,mass);
	do{
		theta = sqrt(rand01())* PI/2;
		tang = tan(theta);
		vz = sqrt(v*v/(1+tang*tang));
		if (rand01()<.5){vz*=-1;}
	}while (acos(v/vz)<Thetamin ||acos(v/vz)>Thetamax);
	phi = rand01()*2*PI;
	vr = vz*tang;
	vel[2] = vz;
	vel[0] = vr*cos(phi);
	vel[1] = vr*sin(phi);
}
