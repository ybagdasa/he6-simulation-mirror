/***************************************************************************
                          BetaSpectrum.cpp  -  description
                             -------------------
    begin                : June 2003
    copyright            : (C) 2003 by Stefan Versyck
    email                : stefan.versyck@fys.kuleuven.ac.be

    adapted by Frederik Wauters and Ran Hong for the 6He experiment. July2013

    July 31: basic version based on the formulas of Ran's EventGenerator
   
    Usage:
    Set a, b, E_end, m_e, Mass Li6 and ZLi6 in constructor
    BetaSpectrumGenerator::BetaSpectrum(double E, double Correlation) returns W(energy,correlation) as a double    
 ***************************************************************************/

#include <math.h>
#include <stdlib.h>
//Std includes
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>

//#include "globals.hh"
//#include "Randomize.hh" 
#include "BetaSpectrumGenerator.h"
#include "EventGenerator.h"
//#include "nrutil.hh"

//Root includes
#include "TROOT.h"
#include "TMath.h"
#include "TRandom3.h"
#include "GlobalConstants.h"

#define NRANSI
#define ITMAX 100
#define CGOLD 0.3819660
#define ZEPS 1.0e-10
#define SHFT(a,b,c,d) (a)=(b);(b)=(c);(c)=(d);
#define TOL 1.0e-6

using namespace std;

double SpenceL(double x);

BetaSpectrumGenerator::BetaSpectrumGenerator(double fa, double fb,double fE_endpoint, double fM_r, double fZ, double fOmega)
{
	//He6EventGen = eventGen;
  a = fa;
  b = fb;
  E_endpoint = fE_endpoint;
  m_e = MASS_E;
  M_r = fM_r;
  ZDaughter = fZ;

  Omega = fOmega; //cut off at 1 eV

//   E_nu = 0.0;
  alpha = 0.0072973525698;
   
  bb[0][0] = 0.115;	bb[0][1] = -1.8123;	bb[0][2] = 8.2498;	bb[0][3] = -11.223;	bb[0][4] = -14.854;	bb[0][5] = 32.086; 
  bb[1][0] = -0.00062;	bb[1][1] = 0.007165;	bb[1][2] = 0.01841;	bb[1][3] = -0.53736;	bb[1][4] = 1.2691;	bb[1][5] = -1.5467;
  bb[2][0] = 0.02482;	bb[2][1] = -0.5975;	bb[2][2] = 4.84199; 	bb[2][3] = -15.3374;	bb[2][4] = 23.9774;	bb[2][5] = -12.6534;
  bb[3][0] = -0.14038;	bb[3][1] = 3.64953;	bb[3][2] = -38.8143; 	bb[3][3] = 172.1368;	bb[3][4] = -346.708;	bb[3][5] = 288.7873;
  bb[4][0] = 0.008152;	bb[4][1] = -1.15664;	bb[4][2] = 49.9663; 	bb[4][3] = -273.711;	bb[4][4] = 657.6292;	bb[4][5] = -603.7033;
  bb[5][0] = 1.2145;	bb[5][1] = -23.9931;	bb[5][2] =  149.9718;	bb[5][3] = -471.2985;	bb[5][4] = 662.1909;	bb[5][5] = -305.6804;
  bb[6][0] = -1.5632;	bb[6][1] = 33.4192;	bb[6][2] = -255.1333;	bb[6][3] = 938.5297;	bb[6][4] = -1641.2845;	bb[6][5] = 1095.358;
 
  for(int i=0; i<7; i++)
  {
    aa[i] = 0;
    for(int j=0; j<6; j++){  aa[i] += bb[i][j]*pow(alpha*ZDaughter,j);} 
  }
   
  True_endpoint = sqrt(M_r*M_r+2*M_r*(E_endpoint+m_e)+m_e*m_e)-M_r-m_e;
//   CalculateSpectrumMax();
  cout << "Init beta spectrum generator ... " << a << endl; 
}

BetaSpectrumGenerator::~BetaSpectrumGenerator(){}

//Calculate E_nu
double BetaSpectrumGenerator::CalculateE_nu(double E,double Corr)
{
/* E should be Ee  
  double Ee = E + m_e; //total energy
  double E0 = E_endpoint + m_e; //total energy
  double uu = Corr*E + M_r; //aux parameter
  double value = -uu + sqrt(uu*uu+2*(E0-E)*M_r-(E*E - m_e*m_e)); */
  
  double Ee = E + m_e;
  double E0 = E_endpoint + m_e;
  double uu = Corr*Ee + M_r; //aux parameter
  double E_nu  = -uu + sqrt(uu*uu+2*(E0-Ee)*M_r-(Ee*Ee - m_e*m_e));
  return E_nu;
}

//Calculate True endpoint
double BetaSpectrumGenerator::CalculateTrueEndpoint()
{
  True_endpoint = sqrt(M_r*M_r+2*M_r*(E_endpoint+m_e)+m_e*m_e)-M_r-m_e;
  return True_endpoint;
}

//Calclulate spectrum function value
double BetaSpectrumGenerator::BetaSpectrum(double E, double Correlation)
{
  double W;
  double Ee = E + m_e; //total energy of electron
  double E0 = E_endpoint + m_e; //total energy released in the decay
  double m_p = MASS_P;//MeV
 
  double E_nu = CalculateE_nu(E,Correlation);
  double PhaseSpace = sqrt(Ee*Ee - m_e*m_e)*Ee*E_nu*E_nu/(1.0+(E_nu+Correlation*Ee)/M_r);
  //It's part of the phase-space of 3-body decay, not the recoil order correction. It can be calculated by hand
  //double NuclearRecoilCorrection = 1/(1+(E_nu+Correlation*Ee)/M_r); // Nuclear Recoil - Correction , needs documentation

  //Radiative corrections
  double beta = sqrt(pow(Ee,2.0)-pow(m_e,2.0))/Ee;
  double N = 0.5*log((1.0+beta)/(1.0-beta));
//Omega = Cs*E_nu;
  double Zvs = alpha/PI*(1.5*log(m_p/m_e)+2.0*(N/beta-1.0)*log(2.0*Omega/m_e)+2.0*N/beta*(1.0-N)+2.0/beta*SpenceL(2.0*beta/(1.0+beta))-0.375);
  double M_tild = -alpha/PI*(1.0-beta*beta)/beta*N;

  //Recoil Order Corrections
  //Following Frank's paper, take the zeroth order contribution out
  double b_term = 69.0/2.75;
  double d_term = 2.4/2.75;
  double f1 = -2.0/3.0*True_endpoint/M_r*(1.0+b_term+d_term)+2.0/3.0*Ee/M_r*(5.0+2.0*b_term)-m_e*m_e/(3.0*M_r*Ee)*(2+d_term+2.0*b_term);
  double f2 = 2.0/3.0*True_endpoint/M_r*(1.0+b_term+d_term)-4.0/3.0*Ee/M_r*(3.0+b_term);
  double f3 = Ee/M_r;
//  W = Corrections(E)*Fermi_Function(Ee)*PhaseSpace*(1 + b*m_e/Ee + a*Correlation)/pow(m_e,4);
//  W = Fermi_Function(Ee)*PhaseSpace*(1 + b*m_e/Ee + a*Correlation)/pow(m_e,4);
//  W = Corrections(E)*PhaseSpace*(1 + b*m_e/Ee + a*Correlation)/pow(m_e,4);
//  W = Corrections(E)*FermiF_Simple(Ee)*PhaseSpace*(1 + b*m_e/Ee + a*Correlation)/pow(m_e,4);
//  W = FermiF_Simple(Ee)*PhaseSpace*(1 + b*m_e/Ee + a*Correlation)/pow(m_e,4); //This is what we use for v1.0x

    //Use the radiative correction calculated on our own
  W = FermiF_Simple(Ee)*PhaseSpace*((1+f1 + b*m_e/Ee + (a+f2)*Correlation + f3*(Correlation*Correlation-1.0/3.0*beta*beta))*(1.0+Zvs)+M_tild)/pow(m_e,4);
  return W;
}

//Spence Function, evaluating using Taylor expansion to 100th order
double SpenceL(double x)
{
  double y=0;
  for (int i=1;i<=10;i++){
    y-=pow(x,i)/(double(i*i));
  }
  return y;
}

//Hard Bremsstrahlung spectrum
double BetaSpectrumGenerator::BremSpectrum(double Ee,double E_nu,double K,double pe_dot_k,double pnu_dot_k,double pnu_dot_pe,double N)
{
  //Following Glueck's paper P226 and P228
  double pek4 = Ee*K-pe_dot_k;//4-vector dot product of Pe and K
  double P2 = 1.0/(K*K)+m_e*m_e/(pek4*pek4)-2.0*Ee/(K*pek4);
  double H0 = E_nu*(-(Ee+K)*P2+K/(pek4));
  double H1 = pnu_dot_pe*(-P2+1.0/pek4)+pnu_dot_k*((Ee+K)/K/pek4-m_e*m_e/(pek4*pek4));

  double W = 2.0*N*E_nu*K*pek4*(H0+a*H1);

  return W;
}

//Find spectrum Max
double BetaSpectrumGenerator::CalculateSpectrumMax()
{
  double W_max=0.0;
  int N=100000;
  double E;
  
  for (int i=1;i<=N;i++){
    E=double(i)/double(N)*True_endpoint;
    if (a<=0) {
      if (W_max<BetaSpectrum(E,-1.0)) {
        W_max=BetaSpectrum(E,-1.0);
      }else{
        break;
      }
    }else{
      if (W_max<BetaSpectrum(E,1.0)) {
        W_max=BetaSpectrum(E,1.0);
      }else{
        break;
      }
    }
  }
  W_max*=1.01;
  cout << "W max "<<W_max<<endl;
//  W_max = 450.0;
  return W_max;
}

//Find Brem spectrum max
double BetaSpectrumGenerator::CalculateBremSpectrumMax()
{
  static TRandom3 rand_gen(0);
  double W_max=0.0;
  int Ntrial=1000000;
  double Ee = E_endpoint/2.0 + m_e;//Total energy of electron
  double K = E_endpoint/4.0;
  double E_nu = E_endpoint/4.0;
  double beta = sqrt(pow(Ee,2.0)-pow(m_e,2.0))/Ee;
  double N = 0.5*log((1.0+beta)/(1.0-beta));
  double pe_dot_k = 0;
  double pnu_dot_k = 0;
  double pnu_dot_pe = 0;
  double P_nu[3] = {0,0,1};
  double P_gamma[3] = {0,0,1};

  for (int j=0;j<Ntrial;j++){
    //Avoid hitting the endpoint, make sure the neutrino has more energy than the soft photon cut-off
    //Not taking into account the recoil effects here
    double E_e = (E_endpoint-100*Omega) * rand_gen.Rndm();      //Random electron kinetic energy
    Ee = E_e + m_e;//Total energy of electron
    beta = sqrt(pow(Ee,2.0)-pow(m_e,2.0))/Ee;
    N = 0.5*log((1.0+beta)/(1.0-beta));
    //Calculate Max neutrino energy
    double E_nu_0 = E_endpoint-E_e;
    //Generate Photon energy
    double Cs = Omega/E_nu_0;
    K = Omega*exp(-1.0*rand_gen.Rndm()*log(Cs));
    //Generate Photon direction
    double c_gamma = (1.0-(1.0+beta)*exp(-2.0*N*rand_gen.Rndm()))/beta;
    double phi_gamma = 2.0*PI*rand_gen.Rndm();
    double s_gamma = sqrt(1.0-c_gamma*c_gamma);
    P_gamma[0] = s_gamma*cos(phi_gamma);
    P_gamma[1] = s_gamma*sin(phi_gamma);
    P_gamma[2] = c_gamma;
//    double c_e = P_e[2];
//    double s_e = sqrt(1.0-c_e*c_e);
//    double c_phi_e = P_e[0]/sqrt(P_e[0]*P_e[0]+P_e[1]*P_e[1]);
//    double s_phi_e = P_e[1]/sqrt(P_e[0]*P_e[0]+P_e[1]*P_e[1]);
//    double n_prime[3] = {-s_phi_e,c_phi_e,0.0};
//    double n_prime2[3] = {-c_e*c_phi_e,-c_e*s_phi_e,s_e};
//    double n_perp[3];
//    for (int i=0;i<3;i++){
//      n_perp[i] = cos(phi_gamma)*n_prime[i]+sin(phi_gamma)*n_prime2[i];
//    }
//    for (int i=0;i<3;i++){
//      P_gamma[i] = c_gamma*P_e[i]+s_gamma*n_perp[i];
//    }
    //Generate neutrino direction
    double c_nu = 2.0*rand_gen.Rndm()-1.0;
    double phi_nu = 2.0*PI*rand_gen.Rndm();
    double s_nu = sqrt(1.0-c_nu*c_nu);
    P_nu[0] = s_nu*cos(phi_nu);
    P_nu[1] = s_nu*sin(phi_nu);
    P_nu[2] = c_nu;
//    RandomDirection(PI,P_nu);           //Random neutrino direction
    //Calculate E_nu
    E_nu = E_nu_0 - K;
    //Calculate correlation
    pe_dot_k = beta*Ee*K*c_gamma;
    pnu_dot_k = E_nu*K*(P_nu[0]*P_gamma[0]+P_nu[1]*P_gamma[1]+P_nu[2]*P_gamma[2]);
    pnu_dot_pe = beta*E_nu*Ee*c_nu;

    double Val = BremSpectrum(Ee,E_nu,K,pe_dot_k,pnu_dot_k,pnu_dot_pe,N);
    if (Val>W_max)W_max=Val;
  }
  W_max*=1.3;//For safety
  cout << "W Brem max "<<W_max<<endl;
//  W_max = 450.0;
  return W_max;
}

//Corrections
double BetaSpectrumGenerator::Corrections(double Energy)//Energy = Electron kinetic energy
{
  double result = 1.0;
  double MassNumber = 6.0;
  double W = Energy / m_e + 1.;
  double W_0 = E_endpoint/m_e + 1.; //Need True endpoint?
  double gamma1 = sqrt(1. - pow(alpha*ZDaughter, 2));
  double r_0 = (1.15 + 1.80*pow(MassNumber, -2./3.) - 1.20 * pow(MassNumber, -4./3.)) / 386.1592642;
  double R = r_0 * pow(MassNumber, 1./3.);
  double sum = 0;
    
  for(int j=1; j < 7; j++)
	  sum += aa[j]*pow(W*R, j);
    
  double L_0 = 1 + 13./60.*pow(alpha*ZDaughter,2) - W*R*alpha*ZDaughter*(41.-26.*gamma1)/15./(2.*gamma1-1) - 
		          alpha*ZDaughter*R*gamma1*(17.-2.*gamma1)/30./W/(2.*gamma1-1)+aa[0]*R/W + 
		          0.41*(R-0.0164)*pow(alpha*ZDaughter,4.5)+ sum;
		    
  double C, C0, C1, C2;
    
  C0 = -233.*pow(alpha*ZDaughter,2)/630. - pow(W_0*R,2)/5.+2*W_0*R*alpha*ZDaughter/35.0;
  C1 = -21.*R*alpha*ZDaughter/35.+4./9.*W_0*R*R;
  C2 = -4./9.*R*R;
    
  C = 1 + C0 + C1*W + C2*W*W;
   
  result =  C * L_0;
  return result;
}

//Simplified Fermi Function
double BetaSpectrumGenerator::FermiF_Simple(double E)
{
  double p = sqrt(E*E-m_e*m_e);
  double aa = 2.0*PI*alpha*ZDaughter;
  double bb = aa/(1-exp(-aa));
  double CC = bb-aa;
  double dd = 0.5*(bb-1);
  double F = aa*E/p + CC/(1+dd/(p*p/(m_e*m_e)));
  return F;
}

//Full Fermi Function
double BetaSpectrumGenerator::Fermi_Function(double Energy) //Energy in MeV 
{
	double MassNumber = 6.0;
	double W = Energy / m_e; //full energy in unit of electron mass
	double p_e = sqrt(pow(W, 2.0) - 1.);
	double beta = sqrt(1. - pow(1. / W, 2));
  double nu = alpha * ZDaughter / beta;
  double r_0 = (1.15 + 1.80*pow(MassNumber, -2./3.) - 1.20 * pow(MassNumber, -4./3.)) / 386.1592642;
  double R = r_0 * pow(MassNumber, 1./3.);
  double gamma1 = sqrt(1. - pow(alpha*ZDaughter, 2));
  
  complex<double> carg;
  complex<double> carg2;  
  carg = complex<double>(gamma1,nu);
  carg2 = complex<double>(1+2.0*gamma1,0.0);
	
//	double F_0 = FermiFunction_0(gamma1, p_e, R, nu);
  double F = 2.0*(gamma1+1)*pow(2*p_e*R, 2.*(gamma1-1.0))*exp(M_PI*nu)*pow(complex_abs(cgamma(carg,0)),2)/pow(complex_abs(cgamma(carg2,0)),2.0);
	return F;
}

double BetaSpectrumGenerator::complex_abs(complex<double> number)
{ 
  double y,re,im;

  re = real(number);
  im = imag(number);

  y = sqrt(re*re + im*im);
  
  return y;
}

//  copied from cgamma.cpp -- Complex gamma function.
//      Algorithms and coefficient values from "Computation of Special
//      Functions", Zhang and Jin, John Wiley and Sons, 1996.
//
//  (C) 2003, C. Bond. All rights reserved.
//
//  Returns gamma function or log(gamma) for complex argument 'z'.
//
//  OPT value       function
//  ---------       --------
//      0           complex gamma
//      1           complex log(gamma)
//
//  Returns (1e308,0) if the real part of the argument is a negative integer
//  or 0 or exceeds 171.
//
complex<double> BetaSpectrumGenerator::cgamma(complex<double> z,int OPT)
{
    complex<double> g,z0,z1;
    double x0,q1,q2,x,y,th,th1,th2,g0,gr,gi,gr1,gi1;
    double na,t,x1,y1,sr,si;
    int i,j,k;
    
    x1 = 0;
    na = 0;

    static double a[] = 
	{
        8.333333333333333e-02,
       -2.777777777777778e-03,
        7.936507936507937e-04,
       -5.952380952380952e-04,
        8.417508417508418e-04,
       -1.917526917526918e-03,
        6.410256410256410e-03,
       -2.955065359477124e-02,
        1.796443723688307e-01,
       -1.39243221690590
       };

    x = real(z);
    y = imag(z);
    if (x > 171) return complex<double>(1e308,0);
    if ((y == 0.0) && (x == (int)x) && (x <= 0.0))
        return complex<double>(1e308,0);
    else if (x < 0.0) 
    {
        x1 = x;
        y1 = y;
        x = -x;
        y = -y;
    }
    x0 = x;
    if (x <= 7.0) 
    {
        na = (int)(7.0-x);
        x0 = x+na;
    }
    q1 = sqrt(x0*x0+y*y);
    th = atan(y/x0);
    gr = (x0-0.5)*log(q1)-th*y-x0+0.5*log(2.0*M_PI);
    gi = th*(x0-0.5)+y*log(q1)-y;
    for (k=0;k<10;k++)
    {
        t = pow(q1,-1.0-2.0*k);
        gr += (a[k]*t*cos((2.0*k+1.0)*th));
        gi -= (a[k]*t*sin((2.0*k+1.0)*th));
    }
    if (x <= 7.0) 
    {
        gr1 = 0.0;
        gi1 = 0.0;
        for (j=0;j<na;j++) 
	{
            gr1 += (0.5*log((x+j)*(x+j)+y*y));
            gi1 += atan(y/(x+j));
        }
        gr -= gr1;
        gi -= gi1;
    }
    if (x1 < 0.0) 
    {
        q1 = sqrt(x*x+y*y);
        th1 = atan(y/x);
        sr = -sin(M_PI*x)*cosh(M_PI*y);
        si = -cos(M_PI*x)*sinh(M_PI*y);
        q2 = sqrt(sr*sr+si*si);
	th2 = atan(si/sr);
        if (sr < 0.0) th2 += M_PI;
        gr = log(M_PI/(q1*q2))-gr;
        gi = -th1-th2-gi;
        x = x1;
        y = y1;
    }
    if (OPT == 0) 
    {
        g0 = exp(gr);
        gr = g0*cos(gi);
        gi = g0*sin(gi);
    }
    g = complex<double>(gr,gi);
    return g;
}


#undef NRANSI
#undef ITMAX
#undef CGOLD
#undef ZEPS
#undef SHFT
