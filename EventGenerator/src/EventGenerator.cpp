/********************************************************************
   6He simulation Event Generator
   Author: 	Ran Hong
   Date:	Sep. 23th. 2013
   Version: 	v2.00
   This event generator generates 6He beta decay events, with initial
   decay position, beta momentum, recoil ion momentum. The input para-
   meters are stored in the file "simulation/Data/Input". It contains
   parameters like a, b, MOT position and size.
   This event generator reads it and the output file will end with th-
   is ID. The number of events is the only argument of the main progr-
   am. The output data will be saved in "simulation/Data". The file
   names are like "OriginalEvent##.dat", where the last 2 digits are
   numbers that identify each run. Generally, the event generator 
   reads the input file and use the parameter "fileID" to lable the 
   output file. The output file is a binary file with the format
     Event ID 			(4 byte int)
     Decay Position[x,y,z]	(8 byte double * 3)
     Electron Momentum[x,y,z]	(8 byte double * 3)
     Recoil Momentum[x,y,z]     (8 byte double * 3)

   v1.01 modifications: 1: the fileID is not read from Input, but an
   argument provided by user when run the program. Like:
   ./EventGenerator fileID number_of_events
   2: Use 2 digits for the file ID
   3: Neglect irrelevant inputs in the Input file, since it is shared
   by the whole simulation program, i.e. Iontracking, post processor

   v1.02 modification: Recoil effects taken into account

   v1.03 modification: Separate generator and steering main function.
   Allow fixed energy and normal incident. Not using global variables.

   v1.04 modification: removed several bugs in v1.03. Build library.

   v2.00 Upgrade to C++ style, includes fermi function and other
   radiative corrections

   NOTE: In current version, the MOT is with Gaussian distribution
*********************************************************************/
//Std includes
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>

//Root includes
#include "TROOT.h"
#include "TStyle.h"
#include "TRandom3.h"
#include "TMath.h"
#include "TFile.h"

#include "EventGenerator.h"
#include "BetaSpectrumGenerator.h"
#include "Arm.h"
#include "GlobalConstants.h"

using namespace std;


//Constructor
EventGenerator::EventGenerator(double m_ion,double Z_daughter, double endpoint, double fixenergy, double theta, double phi, double alpha, string source_input,string name)
{
  m_e = MASS_E;	//Electron mass
  M_r = m_ion;	//Recoil Ion mass
  //Z = Z_daughter;	//Recoil Z number
  E_endpoint = endpoint;		//Q value
  Fixed_Energy = fixenergy;	//Fixed beta energy, 0 for random energy
  
  SetDirection(theta,phi,alpha);
  SetSourceType(source_input);
  SetIsotope(name);
  
  double a = -0.33333333333333333;	// default "a" correlation coefficient
  double b = 0.0;	//default Fierz interference
  HardRatio = 0.02955; //Glueck's paper, page 228,230
  Cs = 0.001;
  Omega = Cs*0.001; //cut off at 1 eV
  
  for (int i=0;i<3;i++){
    MOT_r[i] = 0.2; //mm
    MOT_c[i] = 0.0;
  }
  
  Diffuse_radius = 50.0;
  Diffuse_height = 65.0;
  for (int i=0;i<3;i++){
    Diffuse_center[i] = 0.0;
  }
  

  //Set calibration data directory
  CAL_DIRECTORY = getenv("HE6_CALIBRATION_DATA_DIRECTORY");
  if ( CAL_DIRECTORY==NULL ){
    cout << "The environment variable HE6_CALIBRATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    cout << "Set to current folder.\n";
  }
  LoadSr90Spec();
  He6BetaSpectrumGenerator = new BetaSpectrumGenerator(a,b,E_endpoint,M_r,Z_daughter,Omega);
  True_endpoint = He6BetaSpectrumGenerator->CalculateTrueEndpoint();
  W_max = He6BetaSpectrumGenerator->CalculateSpectrumMax();
  W_Brem_max = He6BetaSpectrumGenerator->CalculateBremSpectrumMax();
}

//Destructor
EventGenerator::~EventGenerator()
{
  if (He6BetaSpectrumGenerator!=NULL)delete He6BetaSpectrumGenerator;
}

//SetFunctions
void EventGenerator::SetConstants(double m_ion,double Z_daughter, double endpoint, double fixenergy,double fOmega,double fHardRatio)
{
  M_r = m_ion;	//Recoil Ion mass
  E_endpoint = endpoint;		//Q value
  Fixed_Energy = fixenergy;	//Fixed beta energy, 0 for random energy
  Omega = fOmega;
  HardRatio = fHardRatio;
  //Update the Spectrum Generator and other variables
  if (He6BetaSpectrumGenerator!=NULL){
    He6BetaSpectrumGenerator->SetMassRecoil(M_r);
    He6BetaSpectrumGenerator->SetZDaughter(Z_daughter);
    He6BetaSpectrumGenerator->SetQValue(E_endpoint);
    True_endpoint = He6BetaSpectrumGenerator->CalculateTrueEndpoint();
//    W_max = He6BetaSpectrumGenerator->CalculateSpectrumMax();
//    W_Brem_max = He6BetaSpectrumGenerator->CalculateBremSpectrumMax();
  }
}

void EventGenerator::SetCorrelation(double a_input, double b_input)
{
  if (He6BetaSpectrumGenerator!=NULL){
    He6BetaSpectrumGenerator->Seta(a_input);
    He6BetaSpectrumGenerator->Setb(b_input);
    True_endpoint = He6BetaSpectrumGenerator->CalculateTrueEndpoint();
//    W_max = He6BetaSpectrumGenerator->CalculateSpectrumMax();
//    W_Brem_max = He6BetaSpectrumGenerator->CalculateBremSpectrumMax();
  }
}

void EventGenerator::SetSourceType(string source_input)
{
  SourceType = source_input;
  if (SourceType.compare("MOT")!=0 && SourceType.compare("Diffuse")!=0){
    cout << "Unsupported source type "<<SourceType.c_str()<<endl;
    cout << "Set to default: MOT\n";
    SourceType = "MOT";
  }
}

void EventGenerator::SetIsotope(string name)
{
  Isotope = name;
  cout << "Isotope is set to "<<name<<endl;
  if (Isotope.compare("He6")!=0 && Isotope.compare("Bi207")!=0 && Isotope.compare("Bi207E")!=0 && Isotope.compare("Sr90")!=0 && Isotope.compare("He4")!=0 && Isotope.compare("Fe55")!=0){
    cout <<"Unsupported isotope "<<Isotope.c_str()<<endl;
    cout <<"Set to default: He6"<<endl;
    Isotope = "He6";
  }
}

void EventGenerator::SetMOTSourceParameter(double sigma[3],double center[3])
{
  
  for (int i=0;i<3;i++){
    MOT_r[i] = sigma[i];
    MOT_c[i] = center[i];
  }
}

void EventGenerator::SetDiffuseSourceParameter(double radius,double height,double center[3])
{
  Diffuse_radius = radius;
  Diffuse_height = height;
  for (int i=0;i<3;i++){
    Diffuse_center[i] = center[i];
  }
}

void EventGenerator::SetDirection(double theta,double phi,double alpha)
{
  Theta = theta;
  Phi = phi;
  Alpha = alpha;
}

//Random Directions and positions

//Generate random direction, within a solid angle (with CosTheta)
int EventGenerator::RandomDirection(double Aperture,double *Direction)
{
  //Static random number generator for this function
  static TRandom3 R_Direction(0);
  
  //phi cos_theta and sin_theta
  double PHI;
  double COS_THETA;
  double SIN_THETA;
  //Costheta of aperture
  double CosTheta = cos(Aperture);
  if (CosTheta > 0.9999999){
    Direction[0] = 0.0;
    Direction[1] = 0.0;
    Direction[2] = 1.0;
  }else{
    PHI = 2*PI*R_Direction.Rndm();
    COS_THETA = CosTheta + (1-CosTheta)*R_Direction.Rndm();
    SIN_THETA = sqrt(1-pow(COS_THETA,2));
    //Assign values to the direction vector
    Direction[0] = SIN_THETA * cos(PHI);
    Direction[1] = SIN_THETA * sin(PHI);
    Direction[2] = COS_THETA;
  }
  return 0;
}

//Generate Rand MOT Position
int EventGenerator::RandMOTPos(double *Position)
{
  //Static random number generator for this function
  static TRandom3 R_MOTPos(0);
  //Random position in the MOT, Gaussian distribution
  for (int j=0;j<3;j++){
//    Position[j] = MOT_c[j] + MOT_r * 2 * (R_MOTPos.Rndm()-0.5);
      Position[j] = R_MOTPos.Gaus(MOT_c[j],MOT_r[j]);
  }
  return 0;
}

//Generate Rand positions in the chamber
int EventGenerator::RandDiffusePos(double *Position){
  //Static random number generator for this function
  static TRandom3 R_DiffusePos(0);
  //Random position in the MOT, Uniform distribution
  double R_square = Diffuse_radius*Diffuse_radius*R_DiffusePos.Rndm();
  double Theta = 2*PI*R_DiffusePos.Rndm();
  double z = 2*Diffuse_height*R_DiffusePos.Rndm();
  
  Position[0] = Diffuse_center[0] + sqrt(R_square)*cos(Theta);
  Position[1] = Diffuse_center[1] + sqrt(R_square)*sin(Theta);
  Position[2] = Diffuse_center[2] - Diffuse_height + z;
  
  return 0;
}

//Event Generating
int EventGenerator::Generate(double *DEC_pos,double* P_e,double * P_r,string& ParticleType,bool& CoinParExist)
{
  double aux; //aux variable for random generator
  double IfBrem;//aux variable for bremsstrahlung generator
  //Static random number generator for this function
  static TRandom3 rand_gen(0);
  //kinematic variables
  double E_e = 3.5052;		//Electron kinetic energy in MeV
  double E_nu = 0.0;		//Antineutrino energy in MeV
  double P_nu[3] = {0.0,0.0,-1.0};	//Antineutrino momentum direction
  double Correlation = 0.0;	//P_e dot P_nu

  //Random position in the MOT or diffused in the chamber
  if (SourceType.compare("MOT")==0){
    RandMOTPos(DEC_pos);
  }else if (SourceType.compare("Diffuse")==0){
    RandDiffusePos(DEC_pos);
  }
  //Random direction for the electrons
  RandomDirection(Alpha,P_e);
  
  //According to the isotope, generate particles
  if (Isotope.compare("Bi207")==0){//Bi207 calibration source
    aux  = 99.64296 * rand_gen.Rndm();//<<<<----------------------------------------------------Maximum of the Bi207 Branch 99.64296---------------------->>>>>>>>>>>>>>>>>>
 //   aux  = 190.5972 * rand_gen.Rndm();//<<<<----------------------------------------------------Maximum of the Bi207 Branch 190.5972---------------------->>>>>>>>>>>>>>>>>>
    E_e = Bi207Branch(aux,ParticleType);
    if (ParticleType.compare("Electron")==0){
      for (int j=0;j<3;j++){
        P_e[j] *= sqrt(pow(E_e+m_e,2.0)-pow(m_e,2.0));
        P_r[j] = 0.0;
      }
    } else if (ParticleType.compare("Photon")==0){
      for (int j=0;j<3;j++){
        P_e[j] *= E_e;  //For photons, we still use the same variable P_e and E_e to represent their momentum and energy, for convenience
        P_r[j] = 0.0;
      }
    }
//    CoinParExist=false;
    if (E_e>0.6){
      CoinParExist=true;
    }else{
      CoinParExist=false;
    }
    return 0; // End of Bi207 Generator
  } else if (Isotope.compare("Bi207E")==0){//Bi207 Electron Only calibration source
    aux  = 11.4772 * rand_gen.Rndm();//<<<<----------------------------------------------------Maximum of the Bi207 Branch 11.4772---------------------->>>>>>>>>>>>>>>>>>
    E_e = Bi207EBranch(aux,ParticleType);
    if (ParticleType.compare("Electron")==0){
      for (int j=0;j<3;j++){
        P_e[j] *= sqrt(pow(E_e+m_e,2.0)-pow(m_e,2.0));
        P_r[j] = 0.0;
      }
    } 
    CoinParExist=false;
    return 0; // End of Bi207E Generator
  } else if (Isotope.compare("He6")==0){//He6 decay generator begins
    ParticleType = "Electron";
    //Decide if it is hard bremsstrahlung
    if (Fixed_Energy<0.001){
      IfBrem = rand_gen.Rndm();
    }else{
      IfBrem = 2.0;//Chose something greater than 1
    }
    if (IfBrem<HardRatio){
      double P_gamma[3] = {0.0,0.0,1.0}; ;//Photon momentum
      GenerateHardBrem(P_e,P_nu,P_gamma);//This function returns the momentum of P_e, meaning times |p| to the input P_e
      for (int j=0;j<3;j++){
	P_r[j] = -P_e[j] - P_nu[j] -P_gamma[j];
      }
      CoinParExist=false;
    }else{
      //Select antineutrino direction and beta energy according to spectrum
      //Using rejection method
      //Initial condition: aux = W_max, must loop in
      aux = W_max;
      //Rejection loop
      if (Fixed_Energy<0.001){
	while(aux>He6BetaSpectrumGenerator->BetaSpectrum(E_e,Correlation)){
	  E_e = True_endpoint * rand_gen.Rndm();	//Random electron kinetic energy
	  RandomDirection(PI,P_nu);		//Random neutrino direction
	  //Calculate correlation
	  Correlation = sqrt(pow(E_e+m_e,2.0)-pow(m_e,2.0))/(E_e+m_e)*(P_e[0]*P_nu[0]+P_e[1]*P_nu[1]+P_e[2]*P_nu[2]);
	  //Calculate antineutrino energy, take recoil into account
	  E_nu = He6BetaSpectrumGenerator->CalculateE_nu(E_e,Correlation); //E is the electron kinetic energy

	  //Random number for rejection
	  aux = W_max * rand_gen.Rndm();
	}
      }else{
	E_e = Fixed_Energy;
      }
      for (int j=0;j<3;j++){
	P_e[j] *= sqrt(pow(E_e+m_e,2.0)-pow(m_e,2.0));
	P_nu[j] *= E_nu;
	P_r[j] = -P_e[j] - P_nu[j];
      }
      CoinParExist=false;
    }
    return 0;
  }else if (Isotope.compare("Sr90")==0){//Sr90 source decay generator begins
    ParticleType = "Electron";
    aux = 0.095;
    //Rejection loop
    E_e = 2.30;
    while(aux>GetSr90Spec(E_e*1000)){
      E_e = 2.30 * rand_gen.Rndm();	//Random electron kinetic energy
      //Random number for rejection
      aux = 0.095 * rand_gen.Rndm();
    }
    for (int j=0;j<3;j++){
      P_e[j] *= sqrt(pow(E_e+m_e,2.0)-pow(m_e,2.0));
      P_nu[j] *= E_nu;
      P_r[j] = -P_e[j] - P_nu[j];
    }
    CoinParExist=false;
    return 0;
  }else if (Isotope.compare("He4")==0){
    ParticleType = "None";
    for (int j=0;j<3;j++){
      P_r[j] = 0.0;
      P_e[j] = 0.0;
    }  
    CoinParExist=false;
    return 0;
  }else if (Isotope.compare("Fe55")==0){//Fe55 calibration source
    ParticleType = "Photon";
    aux  = 27.25 * rand_gen.Rndm();
    if (aux<24.4)E_e = 5.895/1000.0;
    else E_e = 6.49/1000.0;

    for (int j=0;j<3;j++){
      P_e[j] *= E_e;  //For photons, we still use the same variable P_e and E_e to represent their momentum and energy, for convenience
      P_r[j] = 0.0;
    }
    CoinParExist=false;
    return 0; // End of Bi207 Generator
  }else{
    cout <<"Unsupported isotope "<<Isotope.c_str()<<endl;
    return -1;
  }
  return -1;
}

//Generate Coincidence Particles
int EventGenerator::GenerateCoin(double* P_coin,string& CoinParticleType){
/*  double aux; //aux variable for random generator
  //Static random number generator for this function
  static TRandom3 rand_gen(0);
  double E_coin = 0.0;

  //Random direction for the electrons
  RandomDirection(Alpha,P_coin);

  if (Isotope.compare("Bi207")==0){//Bi207 calibration source
    aux  = rand_gen.Rndm();
    if (aux<0.332){
      E_coin = 0.0106;
      for (int j=0;j<3;j++){
	P_coin[j] *= E_coin;  //For photons, we still use the same variable P_e and E_e to represent their momentum and energy, for convenience
      }
      CoinParticleType = "Photon";
      return 1; // End of Bi207 Generator
    }
  }
  return 0;*/

  static double Branch[4] = {97.75, 99.287, 99.729, 99.84 };
  //Static random number generator for this function
  static TRandom3 rand_gen(0);
  double Rand = 99.84*rand_gen.Rndm();
  //Random direction for the electrons
  RandomDirection(Alpha,P_coin);

  double Ek = 0.0;
  //For Bi207
  if (Isotope.compare("Bi207")==0){//Bi207 calibration source
    for (int i=0;i<4;i++){
      if (Rand <= Branch[i]){
	if (i==0)
	  CoinParticleType = "Photon";
	else
	  CoinParticleType = "Electron";
	switch (i)
	{
	  case 0:
	    Ek = 0.569698;
	    break;
	  case 1:
	    Ek = 0.4816935;
	    break;
	  case 2:
	    Ek = 0.5538372;
	    break;
	  case 3:
	    Ek = 0.5658473;
	    break;
	  default:
	    Ek = 0.0;
	    break;
	}
	break;
      }
    }
    if (CoinParticleType.compare("Photon")==0){
      for (int j=0;j<3;j++){
	P_coin[j] *= Ek;  //For photons, we still use the same variable P_e and E_e to represent their momentum and energy, for convenience
      }
    }else if (CoinParticleType.compare("Electron")==0){
      double Pe = sqrt(pow(Ek,2.0)+2*Ek*m_e);
      for (int j=0;j<3;j++){
	P_coin[j] *= Pe;  //For photons, we still use the same variable P_e and E_e to represent their momentum and energy, for convenience
      }
    }
    return 0;
  }else{
    CoinParticleType = "Electron";
    return 0;
  }
}

//Find spectrum Max
void EventGenerator::SetSpectrumMax(){
  W_max = He6BetaSpectrumGenerator->CalculateSpectrumMax();
  W_Brem_max = He6BetaSpectrumGenerator->CalculateBremSpectrumMax();
}

//Bi207 branches
double EventGenerator::Bi207Branch(double Rand,string& Particle)
{
  static double Branch[11] = {8.69975, 83.19975, 90.06975, 90.20654, 90.24588, 90.25576, 97.33576, 99.17576, 99.61576, 99.63956, 99.64296 };
//  static double Branch[11] = {97.75,172.25,179.12,180.657,181.099,181.21,188.29,190.13,190.57,190.5938,190.5972};
  for (int i=0;i<11;i++){
    if (Rand <= Branch[i]){
      if (i<=2)
        Particle = "Photon";
      else
        Particle = "Electron";
      switch (i)
      {
        case 0:
          return 0.569698;
          break;
        case 1:
          return 1.063656;
          break;
        case 2:
          return 1.770228;
          break;
        case 3:
          return 0.4816935;
          break;
        case 4:
          return 0.5538372;
          break;
        case 5:
          return 0.5658473;
          break;
        case 6:
          return 0.975651;
          break;
        case 7:
          return 1.047795;
          break;
        case 8:
          return 1.059805;
          break;
        case 9:
          return 1.682224;
          break;
        case 10:
          return 1.754367;
          break;
        default:
          return 0.0;
          break;
      }
    }
  }
  return 0.0;
}

double EventGenerator::Bi207EBranch(double Rand,string& Particle)
{
  static double Branch[8] = {1.537,1.979,2.09,9.17,11.01,11.45,11.4738,11.4772};
  for (int i=0;i<8;i++){
    if (Rand <= Branch[i]){
      Particle = "Electron";
      switch (i)
      {
        case 0:
          return 0.4816935;
          break;
        case 1:
          return 0.5538372;
          break;
        case 2:
          return 0.5658473;
          break;
        case 3:
          return 0.975651;
          break;
        case 4:
          return 1.047795;
          break;
        case 5:
          return 1.059805;
          break;
        case 6:
          return 1.682224;
          break;
        case 7:
          return 1.754367;
          break;
        default:
          return 0.0;
          break;
      }
    }
  }
  return 0.0;
}

/***********************************************************/
//Load Calibration Spectrum
int EventGenerator::LoadSr90Spec()
{
  ifstream CalFileIn;
  string FileName(CAL_DIRECTORY);
  FileName += "/Sr90Spec.txt";
  CalFileIn.open(FileName.c_str(),ios::in);

  double Energy;
  double SpecFun;
  double SrSpec;
  double YtSpec;

  for (int i=0;i<46;i++){
    CalFileIn >> Energy>>SrSpec>>YtSpec>>SpecFun;
    Sr90Spec[i] = SpecFun;
  }
  cout << "Sr90 spectrum is loaded."<<endl;
  CalFileIn.close();
  return 0;
}

//Get Calibration Spectrum
double EventGenerator::GetSr90Spec(double Energy)
{
  int i = int((Energy+25.0)/50.0);
  double returnVal;
  if (i!=0 && i!=46){
    returnVal = Sr90Spec[i-1]+(Sr90Spec[i]-Sr90Spec[i-1])*(Energy-i*50.0+25.0)/50.0;
  }
  if (i==0){
    returnVal = Sr90Spec[i]*(Energy-i*50.0+25.0)/50.0;
  }
  if (i==46){
    returnVal = Sr90Spec[i-1]+(0.0-Sr90Spec[i-1])*(Energy-i*50.0+25.0)/50.0;
  }
  return returnVal;
}


/***********************************************************/
//Generate hard bremsstrahlung
int EventGenerator::GenerateHardBrem(double* P_e,double* P_nu,double* P_gamma)
{
  static TRandom3 rand_gen(0);
  double Ee = E_endpoint/2.0 + m_e;//Total energy of electron
  double K = E_endpoint/4.0;
  double E_nu = E_endpoint/4.0;
  double beta = sqrt(pow(Ee,2.0)-pow(m_e,2.0))/Ee;
  double N = 0.5*log((1.0+beta)/(1.0-beta));
  double pe_dot_k = 0;
  double pnu_dot_k = 0;
  double pnu_dot_pe = 0;

  //Random number for rejection
  double aux = W_Brem_max;
  //Rejection loop
  while(aux>He6BetaSpectrumGenerator->BremSpectrum(Ee,E_nu,K,pe_dot_k,pnu_dot_k,pnu_dot_pe,N)){
    //Avoid hitting the endpoint, make sure the neutrino has more energy than the soft photon cut-off
    //Not taking into account the recoil effects here
    double E_e = (E_endpoint-100*Omega) * rand_gen.Rndm();	//Random electron kinetic energy
    Ee = E_e + m_e;//Total energy of electron
    beta = sqrt(pow(Ee,2.0)-pow(m_e,2.0))/Ee;
    N = 0.5*log((1.0+beta)/(1.0-beta));
    //Calculate Max neutrino energy
    double E_nu_0 = E_endpoint-E_e;
    //Generate Photon energy
    Cs = Omega/E_nu_0;
    K = Omega*exp(-1.0*rand_gen.Rndm()*log(Cs));
    //Generate Photon direction
    double c_gamma = (1.0-(1.0+beta)*exp(-2.0*N*rand_gen.Rndm()))/beta;
    double phi_gamma = 2.0*PI*rand_gen.Rndm();
    //Express gamma direction in the global coordinate system
    double s_gamma = sqrt(1.0-c_gamma*c_gamma);
    double c_e = P_e[2];
    double s_e = sqrt(1.0-c_e*c_e);
    double c_phi_e = P_e[0]/sqrt(P_e[0]*P_e[0]+P_e[1]*P_e[1]);
    double s_phi_e = P_e[1]/sqrt(P_e[0]*P_e[0]+P_e[1]*P_e[1]);
    double n_prime[3] = {-s_phi_e,c_phi_e,0.0};
    double n_prime2[3] = {-c_e*c_phi_e,-c_e*s_phi_e,s_e};
    double n_perp[3];
    for (int i=0;i<3;i++){
      n_perp[i] = cos(phi_gamma)*n_prime[i]+sin(phi_gamma)*n_prime2[i];
    }
    for (int i=0;i<3;i++){
      P_gamma[i] = c_gamma*P_e[i]+s_gamma*n_perp[i];
    }
    //Generate neutrino direction
    RandomDirection(PI,P_nu);		//Random neutrino direction
    //Calculate E_nu
    E_nu = E_nu_0 - K;
    //Calculate correlation
    pe_dot_k = beta*Ee*K*c_gamma;
    pnu_dot_k = E_nu*K*(P_nu[0]*P_gamma[0]+P_nu[1]*P_gamma[1]+P_nu[2]*P_gamma[2]);
    pnu_dot_pe = beta*E_nu*Ee*(P_nu[0]*P_e[0]+P_nu[1]*P_e[1]+P_nu[2]*P_e[2]);

    aux = W_Brem_max * rand_gen.Rndm();
  }

  //Return momenta
  for (int i=0;i<3;i++){
    P_e[i] *= beta*Ee;
    P_nu[i] *= E_nu;
    P_gamma[i] *= K;
  }
  return 0;
}
