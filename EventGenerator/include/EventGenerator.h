/********************************************************************
   6He simulation Event Generator
   Author: 	Ran Hong
   Date:	Sep. 23th. 2013
   Version: 	v2.00
   This event generator generates 6He beta decay events, with initial
   decay position, beta momentum, recoil ion momentum. The input para-
   meters are stored in the file "simulation/Data/Input". It contains
   parameters like a, b, MOT position and size.
   This event generator reads it and the output file will end with th-
   is ID. The number of events is the only argument of the main progr-
   am. The output data will be saved in "simulation/Data". The file
   names are like "OriginalEvent##.dat", where the last 2 digits are
   numbers that identify each run. Generally, the event generator 
   reads the input file and use the parameter "fileID" to lable the 
   output file. The output file is a binary file with the format
     Event ID 			(4 byte int)
     Decay Position[x,y,z]	(8 byte double * 3)
     Electron Momentum[x,y,z]	(8 byte double * 3)
     Recoil Momentum[x,y,z]     (8 byte double * 3)

   v1.01 modifications: 1: the fileID is not read from Input, but an
   argument provided by user when run the program. Like:
   ./EventGenerator fileID number_of_events
   2: Use 2 digits for the file ID
   3: Neglect irrelevant inputs in the Input file, since it is shared
   by the whole simulation program, i.e. Iontracking, post processor

   v1.02 modification: Recoil effects taken into account

   v1.03 modification: Separate generator and steering main function.
   Allow fixed energy and normal incident. Not using global variables.

   v1.04 modification: removed several bugs in v1.03. Build library.

   v2.00 Upgrade to C++ style, includes fermi function and other
   radiative corrections

   NOTE: In current version, the MOT is with Gaussian distribution
*********************************************************************/

#ifndef EVENTGENERATOR_H
#define EVENTGENERATOR_H

#include <string>

using namespace std;

class BetaSpectrumGenerator;

class EventGenerator{
  private:
    //Calibration directory
    char * CAL_DIRECTORY;
    //Calibration Spectrum
    double Sr90Spec[46];
    //Constants
    double m_e;	//Electron mass
    double M_r;	//Recoil Ion mass
    //double Z;	//Recoil Z number
    double E_endpoint;		//E_endpoint value
    double True_endpoint; //True endpoint taking into account recoil
    double Fixed_Energy;	//Fixed beta energy, 0 for random energy
    
    //Correlations
    //double a;	//"a" correlation coefficient
    //double b;	//Fierz interference

    //Source parameters
    string SourceType;
    double MOT_r[3];	//MOT radius (3 sigmas)
    double MOT_c[3];	//MOT center coordinate
    double Diffuse_radius;	//Diffuse source radius
    double Diffuse_height;	//Diffuse source height
    double Diffuse_center[3];	//Diffuse source center
    double Theta;	//Directional angles theta and phi, not yet implimented
    double Phi;
    double Alpha;	//Opening angle: alpha

    //For radiative correction
    double HardRatio;
    double Cs;
    double Omega;

    //Spectrum variables
    string Isotope;
    double W_max;	//Max value of the spectrum function
    double W_Brem_max;	//Max value of the Bremsstrahlung spectrum function
    BetaSpectrumGenerator* He6BetaSpectrumGenerator;	//Spectrum function calculation
    //friend class BetaSpectrumGenerator;

  public:
    EventGenerator(double m_ion,double Z_daughter, double Q, double fixenergy, double theta, double phi, double alpha, string source_input,string name);	//Constructor and initiator
    ~EventGenerator();	//Destructor
    //SetFunctions
    void SetConstants(double m_ion,double Z_daughter, double Q, double fixenergy,double fOmega=0.000001,double fHardRatio=0.02955);
    void SetCorrelation(double a_input, double b_input);
    void SetSourceType(string source_input);
    void SetMOTSourceParameter(double sigma[3],double center[3]);
    void SetDiffuseSourceParameter(double radius,double height,double center[3]);
    void SetDirection(double theta,double phi,double alpha);
    void SetIsotope(string name);
    void SetSpectrumMax();//Recalculate with the BetaSpectrumGenerator
    
    //Random direction generator;
    //Given direction (theta, phi) and opening angle alpha
    int RandomDirection(double Aperture,double *Direction);	
    //Random decay position in the MOT
    int RandMOTPos(double *Position);
    //Random decay position in the chamber, diffused
    int RandDiffusePos(double *Position);
    //Load calibration spectrum
    int LoadSr90Spec();
    double GetSr90Spec(double Energy);
    
    //Generate an event
    int Generate(double *DEC_pos,double* P_e,double * P_r,string& ParticleType,bool& CoinParExist);
    int GenerateHardBrem(double* P_e,double* P_nu,double* P_gamma);
    double Bi207Branch(double Rand,string& Particle); //Given a random number, decide which branch to go. return energy, Particle is set to photon or electron
    double Bi207EBranch(double Rand,string& Particle); //Given a random number, decide which branch to go. return energy, electron only
    //Generate Coincidence event
    int GenerateCoin(double* P_coin,string& CoinParticleType);
};

#endif
