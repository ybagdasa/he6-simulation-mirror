#ifndef BetaSpectrumGenerator_h
#define BetaspectrumGenerator_h 1

#include <complex>


using namespace std;
//class EventGenerator;

class BetaSpectrumGenerator 
{

  public:
    //beta-nu, Fierz, Q value, electron mass and Mass Li6 in MeV
    BetaSpectrumGenerator(double fa, double fb,double fE_endpoint,double fM_r, double fZ, double fOmega);
    //BetaSpectrumGenerator(double fa, double fb, double fZ, EventGenerator* eventGen); 
    ~BetaSpectrumGenerator();

    double CalculateE_nu(double value, double Corr);  //Calculate E_nu with total beta energy as input.
    double CalculateTrueEndpoint();//Calculate True endpoint taking into account the recoil
    double BetaSpectrum(double E, double Correlation);
    double CalculateSpectrumMax();
    //For hard bremsstrahlung
    double BremSpectrum(double Ee,double E_nu,double K,double pe_dot_k,double pnu_dot_k,double pnu_dot_pe,double N);
    double CalculateBremSpectrumMax();

//    double GetE_nu(double value){return E_nu;} //E_e is the input
    double FermiF_Simple(double E);//Simplified Fermi Function
    double Fermi_Function(double Energy);//Full Fermi Function
    double Corrections(double Energy);

    void Seta(double value){a = value;}
    void Setb(double value){b = value;}
    void SetQValue(double value){E_endpoint = value;}
    void SetMassRecoil(double value){M_r = value;}
    void SetZDaughter(double value){ZDaughter = value;}

//    void SetE_Beta(double value){E_Beta = value;}

  private:
		//EventGenerator* He6EventGen;
    complex<double> cgamma(complex<double>, int);
    double complex_abs(complex<double>);

    double a;
    double b;
    double E_endpoint;
    double True_endpoint;
    double m_e ;
    double M_r;

    //For radiative correction
    //Set By Class EventGenerator
    double Omega;

 //   double E_nu;
 //   double E_Beta;
    double ZDaughter;
    
 //   double W_max;

    double alpha;
    //not necessary
//    void SetE_nu(double value){E_nu = value;}
    //Correction parameters
    double aa[7];
    float bb[7][6];
};

#endif
