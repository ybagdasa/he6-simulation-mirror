/********************************************************************
   6He simulation Event Generator
   Author: 	Ran Hong
   Date:	May. 7th. 2013
   Version: 	v1.03
   This event generator generates 6He beta decay events, with initial
   decay position, beta momentum, recoil ion momentum. The input para-
   meters are stored in the file "simulation/Data/Input". It contains
   parameters like a, b, MOT position and size.
   This event generator reads it and the output file will end with th-
   is ID. The number of events is the only argument of the main progr-
   am. The output data will be saved in "simulation/Data". The file
   names are like "OriginalEvent##.dat", where the last 2 digits are
   numbers that identify each run. Generally, the event generator 
   reads the input file and use the parameter "fileID" to lable the 
   output file. The output file is a binary file with the format
     Event ID 			(4 byte int)
     Decay Position[x,y,z]	(2 byte short * 3)
     Electron Momentum[x,y,z]	(4 byte int * 3)
     Recoil Momentum[x,y,z]     (4 byte int * 3)
     Particle type 0:e 1:gamma  (1 byte int)

   v1.01 modifications: 1: the fileID is not read from Input, but an
   argument provided by user when run the program. Like:
   ./EventGenerator fileID number_of_events
   2: Use 2 digits for the file ID
   3: Neglect irrelevant inputs in the Input file, since it is shared
   by the whole simulation program, i.e. Iontracking, post processor

   v1.02 modification: Recoil effects taken into account

   v1.03 modification: Separate generator and steering main function.
   Allow fixed energy and normal incident. Not using global variables.

   v1.04 modification: removed several bugs in v1.03. Build library.

   NOTE: In current version, the MOT is uniform distribution
*********************************************************************/

//Std includes
#include <iostream>
#include <string>
#include <cstring>
#include <fstream>

#include <stdio.h>
#include <stdlib.h>
#include <stdint.h>
#include <math.h>

#include "EventGenerator.h"
#include "TH1.h"
#include "TFile.h"
#include "TCanvas.h"

#define PI 3.14159265358

#ifndef CSPEED 
#define CSPEED 299.792458
#endif


//main program

using namespace std;


int main(int argc, char **argv){
  int N_Of_Events = 10000;	//Number of events, default value = 10000
  ofstream outputfile;		//Output file stream
  char filename[1000];		//File name for file opening
  ifstream inputfile;		//Input file stream
	string command;
	string InFileName;
  char   CharBuffer[500];
  
  double E_endpoint = 3.5097;	//Endpoint energy
  double M_Li6 = 5603.051;	//Recoil 6Li ion mass in MeV
	double M_He4 = 3728.400; //Recoil 4He ion mass in MeV
	double M_r = M_Li6; //By default
  double m_e = 0.510998910;	//Electron mass in MeV
  double Z = 3.0;
  double a = -0.333333;		//Beta-antineutrino correlation
  double b = 0.0;			//Fierz interference
  
  //Parameters from the input file
  int fileID = 0;		//File/Generator id
	int RunID = 0;  //RunID/BetaID
  double MOT_pos[3] = {0.0,0.0,0.0};	//MOT position
  double DEC_pos[3] = {0.0,0.0,0.0};	//Decay position
  double MOT_rad[3]    ={0.0,0.0,0.0};	//MOT radius
	double alpha = 0.0; //Polar direction of electron solid angle
	double beta = 0.0; //Azimuthal direction of electron solid angle
  double Aperture   = 0.8;	//Aperture angle of the electron solid angle
  double Fixed_Energy = 0.0;	//Fixed Energy? <0.001: Random, otherwise fixed
  string SourceType;  //MOT or Diffused
  double D_radius = 40;		//Diffused source radius
  double D_height = 40;		//Diffused source height
  
  //Kinematics parameters
  double P_e[3] = {0.0,0.0,1.0};	//Electron momentum
  double E_r = 0.0;		//Recoil ion energy in MeV;
  double P_r[3] = {0.0,0.0,0.0};	//Recoil ion momentum
  double E_e = 0.0; 
  string ParticleType;
  string Isotope;
  uint8_t Particle;
  
  //Radiative correction
  double Omega = 0.000001;
  double HardRatio = 0.02955;
  //Output structs
  int P_e_Out[3];
  int V_Ion[3];
  short Dec_pos_Out[3];

  //Check input argument
  if (argc<2 || argc>2){
    cout << "Usage: EventGenerator inputfile.in\n";
    return -1;
  }
  else{
    InFileName = argv[1];
  }

  char* DATA_DIRECTORY = getenv("HE6_SIMULATION_DATA_DIRECTORY");
  if ( DATA_DIRECTORY==NULL ){
    cout << "The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    return -1;
  }
  inputfile.open(InFileName.c_str(),ios::in);
  if(!inputfile.is_open()){
    cout << "Can't open input file "<<filename<<" !" <<endl;
    return -1;
  }

  while(inputfile>>command){

    if(command.compare("SetGeneratorID")==0){
      inputfile>>fileID;
    }else if(command.compare("SetRunID")==0){
      inputfile>>RunID;
    }else if(command.compare("SetSourceType")==0){
      inputfile>>SourceType;
      if (SourceType.compare("MOT")!=0 && SourceType.compare("Diffuse")!=0){
	cout << "Unsupported source type "<<SourceType.c_str()<<endl;
	cout << "Set to default: MOT\n";
	SourceType = "MOT";
      }		
    }else if(command.compare("SetIsotope")==0){
      inputfile>>Isotope;
      if (Isotope.compare("He6")!=0 && Isotope.compare("Bi207")!=0 && Isotope.compare("Sr90")!=0 && Isotope.compare("He4")!=0){
	cout <<"Unsupported isotope "<<Isotope.c_str()<<endl;
	cout <<"Set to default: He6";
	Isotope = "He6";
      }
      else if (Isotope.compare("He4")==0){
	M_r = M_He4;
	Z = 2;
      }
      cout<<"Isotope = " <<Isotope<<endl;
      cout<<"Ion Mass = "<<M_r<<endl;	
    }else if(command.compare("SetSolidAngle")==0){
      inputfile>>alpha>>beta>>Aperture;
      if (Aperture<0.0000001) {cout << "Normal Incident"<<endl;}
      else {cout << "Aperture= "<<Aperture<<" deg"<<endl;}
      Aperture = Aperture/180.0*PI;
    }else if(command.compare("SetMOT_pos")==0){
      inputfile >> MOT_pos[0] >> MOT_pos[1] >> MOT_pos[2];
      cout << "MOT_pos= {" <<MOT_pos[0] << " , "<<MOT_pos[1] <<" , " <<MOT_pos[2] <<"}"<<endl;
    }else if(command.compare("SetMOT_sigma")==0){
      inputfile >> MOT_rad[0] >> MOT_rad[1] >> MOT_rad[2];
      cout << "MOT_rad= {" <<MOT_rad[0] << " , "<<MOT_rad[1] <<" , " <<MOT_rad[2] <<"}"<<endl;
    }else if(command.compare("SetD_radius")==0){
      inputfile>>D_radius;	
      cout << "D_radius= "<<D_radius<<endl;
    }else if(command.compare("SetD_height")==0){
      inputfile>>D_height;
      cout << "D_height= "<<D_height<<endl;
    }else if(command.compare("SetFixed_Energy")==0){
      inputfile>>Fixed_Energy;
      if (Fixed_Energy<0.001){cout << "Random Energy according to the spectrum"<<endl;}
      else{cout << "Energy of the electrons are fixed, Fixed Energy = " <<Fixed_Energy <<endl;}
    }else if(command.compare("Set_b")==0){
      inputfile>>b;
      cout << "b= "<<b<<endl;
    }else if(command.compare("Set_a")==0){
      inputfile>>a;
      cout << "a= "<<a<<endl;	
    }else if(command.compare("Set_Omega")==0){
      inputfile>>Omega;
      cout << "Omega= "<<Omega<<endl;	
    }else if(command.compare("Set_HardRatio")==0){
      inputfile>>HardRatio;
      cout << "HardRatio= "<<HardRatio<<endl;	
    }else if(command.compare("SetNEvents")==0){
      inputfile>>N_Of_Events;
    }else if(command.compare(0,1,"#")==0){		//Comment line, ignore up to 500 characters
      inputfile.getline(CharBuffer,500);
    }else {
      cout<<"Unrecognizable command. Aborting!"<<endl;
      return -1;
    }
  }
  inputfile.close();

  //Set up the output file
  if(Isotope.compare("He4")==0){
    sprintf(filename,"%s/IonInfoFile%03d%02d.dat",DATA_DIRECTORY,fileID,RunID);
  }
  else{
    sprintf(filename,"%s/OriginalEvent%03d.dat",DATA_DIRECTORY,fileID);
  }

  outputfile.open(filename,ios::out | ios::binary);

  //Initialize generator

  EventGenerator* ThisGenerator = new EventGenerator(m_e,M_r,Z,E_endpoint,Fixed_Energy,0,0,Aperture,SourceType,Isotope);
  ThisGenerator->SetCorrelation(a,b);
  if (SourceType.compare("MOT")==0)
    ThisGenerator->SetMOTSourceParameter(MOT_rad,MOT_pos);
  else if (SourceType.compare("Diffuse")==0)
    ThisGenerator->SetDiffuseSourceParameter(D_radius,D_height,MOT_pos);
  ThisGenerator->SetConstants(m_e,M_r,Z,E_endpoint,Fixed_Energy,Omega,HardRatio);
  ThisGenerator->SetSpectrumMax();

  //Output histograms
  TH1* hBetaEnergy = new TH1D("hBetaEnergy","Beta Energy;keV",500,0,4000);//keV
  TH1* hIonEnergy = new TH1D("hIonEnergy","Recoil Ion Energy;eV",500,0,1500);//eV
  //Generating random events
  bool CoinParExist;
  for (int i=0; i<N_Of_Events; i++){
    ThisGenerator->Generate(DEC_pos,P_e,P_r,ParticleType,CoinParExist);	
    if (ParticleType.compare("Electron")==0){
      E_e = sqrt((P_e[0]*P_e[0] + P_e[1]*P_e[1] + P_e[2]*P_e[2]) + m_e*m_e) - m_e;
      Particle = 0;//Electron
    }
    else if (ParticleType.compare("Photon")==0){
      E_e = sqrt(P_e[0]*P_e[0] + P_e[1]*P_e[1] + P_e[2]*P_e[2]);
      Particle = 1;//Photon
    }
    else if (ParticleType.compare("None")==0){
      Particle = -1; //no particle
    }
    else{
      cout<<"Unrecognizable ParticleType "<<ParticleType<<endl;
      return -1;
    } 

    E_r = (P_r[0]*P_r[0] + P_r[1]*P_r[1] + P_r[2]*P_r[2])/2.0/M_r;

    //Write to output file
    //outputfile << i <<"\t" << E_e  << "\t" << P_e[0] <<"\t" <<P_e[1] << "\t" << P_e[2];
    //outputfile <<"\t" <<P_r[0] <<"\t" <<P_r[1] <<"\t" << P_r[2]<<endl;

    //Convert to output format
    for (int j=0;j<3;j++){
      P_e_Out[j] = int(floor(P_e[j]*1000000.0));
      Dec_pos_Out[j] = short(floor(DEC_pos[j]*1000.0));
      V_Ion[j] = int(floor(P_r[j]/M_r*CSPEED*100.0*100000.0));
    }
    //Output
    if(Isotope.compare("He4")==0){
      outputfile.write((char *)Dec_pos_Out,3*sizeof(short));
      outputfile.write((char *)&V_Ion[0],3);    
      outputfile.write((char *)&V_Ion[1],3);
      outputfile.write((char *)&V_Ion[2],3);	
    }
    else{
      outputfile.write((char *)&i,sizeof(int));
      //    outputfile.write((char *)&E_e,sizeof(double));
      outputfile.write((char *)Dec_pos_Out,3*sizeof(short));
      outputfile.write((char *)P_e_Out,3*sizeof(int));
      outputfile.write((char *)V_Ion,3*sizeof(int));
      outputfile.write((char *)&Particle,sizeof(uint8_t));
      //cout<< i<<"\n"<<Dec_pos_Out[0]<<"\t"<<Dec_pos_Out[1]<<"\t"<<Dec_pos_Out[2]<<"\n"<<P_e_Out[0]<<"\t"<<P_e_Out[1]<<"\t"<<P_e_Out[2]<<"\n"<<V_Ion[0]<<"\t"<<V_Ion[1]<<"\t"<<V_Ion[2]<<"\n"<<Particle<<"\n\n";
      //cout<<"pmag = "<<sqrt(P_e[0]*P_e[0]+P_e[1]*P_e[1]+P_e[2]*P_e[2])<<endl;

      // Fill Histograms
      if (ParticleType.compare("Electron")==0){
	hBetaEnergy->Fill(E_e*1000.0);
	hIonEnergy->Fill(E_r*1000000.0);
      }
    }
  }
  cout << "Event generating finished! \n";
  cout << N_Of_Events <<" events are generated.\n";
  cout << "Events are written to "<<filename<<endl;

  outputfile.close();

  if(Isotope.compare("He4")!=0){
    //Output histograms and canvas
    sprintf(filename,"%s/../Spectra/OriginalEventHistograms%03d.pdf(",DATA_DIRECTORY,fileID);
    TCanvas * canv = new TCanvas("canv","Beta Energy",0,0,800,600);
    hBetaEnergy->Draw();
    canv->SaveAs(filename);
    delete canv;
    canv = new TCanvas("canv","Ion Energy",0,0,800,600);
    hIonEnergy->Draw();
    sprintf(filename,"%s/../Spectra/OriginalEventHistograms%03d.pdf)",DATA_DIRECTORY,fileID);
    canv->SaveAs(filename);
    delete canv;
    sprintf(filename,"%s/../Histograms/OriginalEventHistograms%03d.root",DATA_DIRECTORY,fileID);
    TFile* histfile = new TFile(filename,"RECREATE");
    hBetaEnergy->Write();
    hIonEnergy->Write();
    histfile->Close();
    delete histfile;
  }
  delete hBetaEnergy;
  delete hIonEnergy;

  return 0;
}



