#ifndef EventDataStruct_h
#define EventDataStruct_h

#include <stdint.h>

typedef struct InBetaDataStruct{
  unsigned int ScintEnergy;     //in eV, only 3 bytes will be filled
  unsigned short  MWPCEnergy;   //in eV
//  short PosMWPCEntrance[3];     //in um,z component 10um
//  short PosMWPCExit[3];         //in um,z component 10um
  unsigned short ExitAngle_Be;           //1/100 deg
  int InitP[3];			//Initial momentum
  short DECPos[3];		//Decay Position
  uint16_t  Hits;        	//Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
  uint8_t ParticleID;		//0:electron 1:photon
  uint8_t MWPCFiredCell;        //Number of fired cells/wires
  uint8_t MWPCCellIndex[20];    //Fired cell index
  unsigned short MWPCCellIonNumber[20]; //Total ion number in a cell
  short MWPCCellXpos[20];       //Average y position
  unsigned short MWPCCellXvar[20];      //y position variation
  short ScintTimeCorrection;	//Time correction due to beta velocity
  short MCPTime;		//Time of Beta hit MCP
}InBetaDataStruct;

typedef struct InIonDataStruct{
  unsigned int TOF;		//in ps, only 3 bytes will be filled
  int HitPos[2];		//in um, only 3 bytes will be filled
  unsigned short HitAngle;	//in 0.01 deg
  unsigned short IonEnergy;	//in eV
  uint8_t Status;		//Keep ion(0) or not(1)
}InIonDataStruct;

typedef struct OutDataStruct{
  unsigned int ScintEnergy;     //in eV, only 3 bytes will be written
  unsigned short  MWPCEnergy;   //in eV
  short MWPCHitPos[2];	        //in um
  unsigned short MWPCAnodeSignal1;	//any unit
  unsigned short MWPCAnodeSignal2;	//any unit
  unsigned short MWPCCathodeSignal[12]; //any unit
  unsigned short ExitAngle_Be;           //1/100 deg
  int InitP[3];
  short DECPos[3];
  uint16_t  Hits;        	//Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
  uint8_t ParticleID;
  unsigned int TOF;		//in ps, only 3 bytes will be written
  int HitPos[2];
  unsigned short HitAngle;
  unsigned short IonEnergy;
  uint8_t IonStatus;
  short TOFCorrection;	//Time correction due to beta velocity
  short MCPTime;	//Time when bet hit MCP
}OutDataStruct;

typedef struct InBetaDataStruct_D{
  double ScintEnergy;     //in eV, only 3 bytes will be filled
  double  MWPCEnergy;   //in eV
  int MWPCFiredCell;
  int MWPCCellIndex[20];
  int MWPCCellIonNumber[20];
  double MWPCCellXpos[20];
  double MWPCCellXvar[20];
//  double PosMWPCEntrance[3];     //in um,z component 10um
//  double PosMWPCExit[3];         //in um,z component 10um
//  double ExitAngle_Be;           //1/100 deg
//  uint8_t  Hits;        	//Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
  double ScintTimeCorrection;	//Time correction due to beta velocity
  double MCPTime;		//Time when bet hit MCP
}InBetaDataStruct_D;

typedef struct InIonDataStruct_D{
  double TOF;		//in ps, only 3 bytes will be filled
  double HitPos[2];		//in um, only 3 bytes will be filled
  double HitAngle;	//in 0.01 deg
  double IonEnergy;	//in eV
//  uint8_t Status;		//Keep ion(0) or not(1)
}InIonDataStruct_D;

typedef struct OutDataStruct_D{
  double ScintEnergy;     //in eV, only 3 bytes will be written
  double MWPCEnergy;   //in eV
  double MWPCAnodeSignal1;	//in mV
  double MWPCAnodeSignal2;	//in mV
  double MWPCCathodeSignal[12]; //in mV
  double MWPCHitPos[2];	        //in um
//  double ExitAngle_Be;           //1/100 deg
//  uint8_t  Hits;        	//Hits bitmap: 0 MCP Wall Ele Col Win MWPC Sci
  double TOF;		//in ps, only 3 bytes will be written
  double HitPos[2];
  double HitAngle;
  double IonEnergy;
  uint8_t IonStatus;
  double TOFCorrection;	//Time correction due to beta velocity
  double MCPTime;		//Time when bet hit MCP
}OutDataStruct_D;

typedef struct MWPCSignalStruct{
  double CathodeX[12];
  double CathodeY[12];
}MWPCSignalStruct;

#endif
