#ifndef PROCESSOR_H
#define PROCESSOR_H

//Std includes
#include <iostream>
#include <fstream>
#include <string>
#include <cstring>

#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <stdint.h>

//Root includes
#include "TROOT.h"
#include "TStyle.h"
#include "TRandom3.h"
#include "TFile.h"
#include "TH2.h"
#include "TCanvas.h"
#include "TTree.h"

//Project includes
#include "EventDataStruct.h"

#define N_OF_OUT 20

using namespace std;

class Processor{
private:
  //Input and Output
  int N_Out;  //Number of Outputs, up to N_OF_OUT
  int GeneratorID;
  int BetafileID;
  int IonfileID;
  int OutputfileID;
  bool RootOutputRequested;
  bool DatOutputRequested;
  bool IonExists, BetaExists;
  //DataStructs
  //compressed
  InBetaDataStruct InBetaData;
  InIonDataStruct InIonData;
  OutDataStruct OutData;
  
  //double precision
  InBetaDataStruct_D InBeta_Data;
  InIonDataStruct_D InIon_Data;
  OutDataStruct_D Out_Data[N_OF_OUT];
  
	//Input files
  ifstream InfoInput;
  ifstream BetaInput;
  ifstream IonInput;
  TFile* BetaInputRoot;
  TFile* IonInputRoot;
  TTree* BetaInputTree;
  TTree* IonInputTree;
  //Output files
  ofstream FinalOutput[N_OF_OUT];
  TFile* FinalOutputRoot[N_OF_OUT];
	TTree* ProcessorTree[N_OF_OUT];

  //Data directory
  char* DATA_DIRECTORY;
  char* SIM_DIRECTORY;
  char* CAL_DIRECTORY;
  string CalDirectory;

  //Detector response parameters
  double ScintWidth[N_OF_OUT];
  double MWPC_M[N_OF_OUT][18][17];
  MWPCSignalStruct MWPC_Signal[N_OF_OUT][257][18];
  string GainFileNames[N_OF_OUT];
  string SignalFileNames[N_OF_OUT];
  double MWPCAnodeGainFactor[N_OF_OUT];
  double MWPCAnodeNoise[N_OF_OUT];    
  double MWPCAnodeBroadenFactor[N_OF_OUT];
  double MWPCCathodeNoise[N_OF_OUT];   
  double TOFWidthCorrection[N_OF_OUT];
  double TOFCorrection[N_OF_OUT];
  double MCPTime[N_OF_OUT];
  double TOFWidth[N_OF_OUT];
  double MCPEfficiency[N_OF_OUT];
  double MCPWidth[N_OF_OUT];
  double MaskRadius; //Mask radius [mm]
  double MaskPitch; //From grid line center to grid line center [mm]
  double MaskGridThickness; //Thickness of grid line [mm]
  double MCPRot;//MCP rotation angle relative to the Field simulation coordinate system
/*  bool ScintOn; //Allow to turn off features
  bool MWPCOn;
  bool TOFOn;
  bool MCPOn;*/
  //Test Histogram
//  TH2* TestHist; 
  TH1* TestHist;
  
 
public:
  Processor(int N,int GenID,int BetaID,int IonID,int OutID,bool BetaFileExists=true,bool IonFileExists=true);
  ~Processor();
  void GetDirectories();
  void SetRootOutput(bool Switch) {RootOutputRequested=Switch;}
  void SetDatOutput(bool Switch) {DatOutputRequested=Switch;}
  void SetIonExists(bool Switch) {IonExists=Switch;}
  void SetBetaExists(bool Switch) {BetaExists=Switch;}
  void InitializeRootTrees();
  void LoadInputTrees();  
  //Load MWPC responses
  int LoadMWPCResponse(int i,string GainFile,string SignalFile);
  //Input Output
  void OpenInputFiles();
  void CloseInputFiles();
  int ReadInputFiles();
  void OpenOutputFiles(string PurposeName);
  void CloseOutputFiles();
 // void SetInput(InBetaDataStruct_D* Beta_In,InIonDataStruct_D* Ion_In); //Set the input pointer to the input data
 // void SetOutput(OutDataStruct_D* DataOut); //Set the output pointer to the beginning of the output data
  void ApplyDetectorResponse();
  void SaveEntryToOutputStruct(int i); //ith response to Out_i
  void WriteOutputStructToFile(int i);
  void FillRootTree(int i);
  void WriteEventToFiles();
  void WriteRootTrees();
/*  //Turn features on and off
  void TurnOn(string name);
  void TurnOff(string name);*/
  //Processor
  int ApplyScintResponse();
  int ApplyMWPCResponse();
  int ApplyTOFWidth();
  int ApplyMCPResponse();
  //Set parameters
  void SetParameter(string name,int i,double val);
  //Logging
  void Logging(int Index);
};

#endif
