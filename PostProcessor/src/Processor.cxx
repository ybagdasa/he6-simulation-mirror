#include "Processor.h"
#include "GlobalConstants.h"

Processor::Processor(int N,int GenID,int BetaID,int IonID,int OutID,bool BetaFileExists,bool IonFileExists)
{
  MaskRadius = 37.5; //Mask radius [mm]
  MaskPitch = 4; //From grid line center to grid line center [mm]
  MaskGridThickness = .250; //Thickness of grid line [mm]
  MCPRot = 22.5/180.0*PI;
  //Input and Output
  N_Out = N;  //Number of Outputs, up to N_OF_OUT
  GeneratorID = GenID;
  BetafileID = BetaID;
  IonfileID = IonID;
  OutputfileID = OutID;
  RootOutputRequested = true;
  DatOutputRequested = false;
  IonExists = IonFileExists; 
  BetaExists = BetaFileExists;
  for (int i=0;i<N_OF_OUT;i++) FinalOutputRoot[i] = NULL;
  BetaInputRoot = NULL;
  IonInputRoot = NULL;	
  
  GetDirectories();
  CalDirectory = CAL_DIRECTORY;
  InitializeRootTrees();
  
  //InBetaData = NULL;
  //InIonData =NULL;
  //OutData = NULL;
  for (int i=0;i<N_OF_OUT;i++){
    ScintWidth[i] = 0.0;
//    MWPCWidth_Anode[i] = 0.0;
    TOFWidth[i] = 0.0;
    TOFWidthCorrection[i] = 0.0;
    TOFCorrection[i] = 0.0;
    MCPEfficiency[i] = 1.0;
    MCPWidth[i] = 0.0;
    MCPTime[i] = -10;
    for (int j=0;j<17;j++){
      for (int k=0;k<18;k++){
	MWPC_M[i][k][j] = 1.0;	//Default multiplication is one
      }
    }
    //No initialization for MWPC Signal
  }

}
Processor::~Processor()
{
  for (int i=0;i<N_Out;i++){
  	delete ProcessorTree[i];
  }
}

void Processor::GetDirectories(){
  //Get data directory
  DATA_DIRECTORY=getenv("HE6_SIMULATION_DATA_DIRECTORY");

  if (DATA_DIRECTORY==NULL) {
    cout << "Error:The environment variable HE6_SIMULATION_DATA_DIRECTORY is not defined! Set it up to the data directory first!\n";
    exit(1);
  }

  //Get simulation directory
  SIM_DIRECTORY=getenv("HE6_SIMULATION_DIRECTORY");

  if (SIM_DIRECTORY==NULL) {
    cout << "Error:The environment variable HE6_SIMULATION_DIRECTORY is not defined! Set it up to the simulation directory first!\n";
    exit(1);
  }

  //Get calibration directory
  CAL_DIRECTORY=getenv("HE6_CALIBRATION_DATA_DIRECTORY");

  if (CAL_DIRECTORY==NULL) {
    cout << "Error:The environment variable HE6_CALIBRATION_DATA_DIRECTORY is not defined! Set it up to the calibration data directory first!\n";
    exit(1);
  }
}
/************************************************************************************/
void Processor::InitializeRootTrees(){
  for (int i=0;i<N_Out;i++){
  	ProcessorTree[i] = new TTree("ProcessorTree","The Processed Events Dataset");
  	//Creat Branches
  	if(BetaExists){
    	ProcessorTree[i]->Branch("ScintEnergyPP",&OutData.ScintEnergy,"ScintEnergyPP/i");
    	ProcessorTree[i]->Branch("MWPCEnergyPP",&OutData.MWPCEnergy,"MWPCEnergyPP/s");
    	ProcessorTree[i]->Branch("MWPCHitPos",&OutData.MWPCHitPos[0],"X/S:Y/S");	  	
    	ProcessorTree[i]->Branch("MWPCAnodeSignal1",&OutData.MWPCAnodeSignal1 ,"MWPCAnodeSignal1/s");
  	  ProcessorTree[i]->Branch("MWPCAnodeSignal2",&OutData.MWPCAnodeSignal2 ,"MWPCAnodeSignal2/s");
  	  ProcessorTree[i]->Branch("MWPCCathodeSignal",&OutData.MWPCCathodeSignal[0] ,"MWPCCathodeSignal[12]/s");
  	  //ProcessorTree[i]->Branch("",&OutData.TOFCorrection,"");
    }
    if(IonExists){	  	
    	ProcessorTree[i]->Branch("MCPTimePP",&OutData.MCPTime,"MCPTimePP/S");
    	ProcessorTree[i]->Branch("TOFPP",&OutData.TOF ,"TOFPP/i");
    	ProcessorTree[i]->Branch("HitPosPP",&OutData.HitPos[0] ,"X/I:Y/I");
    	ProcessorTree[i]->Branch("IonStatusPP",&OutData.IonStatus ,"IonStatusPP/b");
    	//IonAngle and IonEnergy are in IonTree and are simply copied over to the outdata struct
    }	  	
  } 
}
/************************************************************************************/
void Processor::FillRootTree(int i){
	FinalOutputRoot[i]->cd();
	ProcessorTree[i]->Fill();
}
/************************************************************************************/
void Processor::WriteRootTrees(){
	LoadInputTrees();
	for (int i=0;i<N_Out;i++){
		FinalOutputRoot[i]->cd();
		ProcessorTree[i]->Write("", TObject::kOverwrite);
		if(BetaExists) BetaInputTree->Write("", TObject::kOverwrite);
		if(IonExists) IonInputTree->Write("", TObject::kOverwrite);
		cout<<"Root trees written to file "<<FinalOutputRoot[i]->GetName()<<"."<<endl;
	}
	
}
/************************************************************************************/
void Processor::LoadInputTrees(){
	if(BetaExists){
  	TTree* dummyBeta = (TTree*)BetaInputRoot->Get("BetaTree");
	  BetaInputTree = dummyBeta->CloneTree();
	}
	if(IonExists){
	  TTree* dummyIon = (TTree*)IonInputRoot->Get("IonTree");
	  IonInputTree = dummyIon->CloneTree();
	}
}

/************************************************************************************/
//Input Output

void Processor::OpenInputFiles(){
  char filename[100];

  //Beta Event file
  if (BetaExists) {
    sprintf(filename,"%s/BetaEventFile%03d%02d.dat",DATA_DIRECTORY,GeneratorID,BetafileID);
    BetaInput.open(filename,ios::in | ios::binary);
    if(!BetaInput.is_open()) {
      cout << "Error:Cannot open beta event file "<<filename<<" !" <<endl;
      exit(1);
    }
    else cout << "Beta input file "<<filename<<" opened.\n";
  }
  //Ion Event file
  if (IonExists) {
    sprintf(filename,"%s/IonEventFile%03d%02d%02d.dat",DATA_DIRECTORY,GeneratorID,BetafileID,IonfileID);
    IonInput.open(filename,ios::in | ios::binary);
    if(!IonInput.is_open()) {
      cout << "Error:Cannot open ion event file "<<filename<<" !" <<endl;
      exit(1);
    }
    else cout << "Ion input file "<<filename<<" opened.\n";
  }
  //Root Event files
  if(RootOutputRequested) {
    if (BetaExists) {  
    	sprintf(filename,"%s/BetaEventFile%03d%02d.root",DATA_DIRECTORY,GeneratorID,BetafileID);
  		BetaInputRoot = new TFile (filename,"READ");
  		if(BetaInputRoot->IsZombie()){
  			cout << "Error:Cannot open root beta event file "<<filename<<" !" <<endl;
      	exit(1);
    	}
    	else cout << "Root input file "<<filename<<" opened.\n";
    
  	}
  	if (IonExists) {
  	  sprintf(filename,"%s/IonEventFile%03d%02d%02d.root",DATA_DIRECTORY,GeneratorID,BetafileID,IonfileID);
  	  IonInputRoot = new TFile (filename,"READ");
  		if(IonInputRoot->IsZombie()){
  			cout << "Error:Cannot open root ion event file "<<filename<<" !" <<endl;
      	exit(1);
    	}
    	else cout << "Root input file "<<filename<<" opened.\n";
    }
	}	
} 

/************************************************************************************/

void Processor::CloseInputFiles(){
	if(BetaExists && BetaInput.is_open()) BetaInput.close();
	if(IonExists && IonInput.is_open()) IonInput.close();
	if(BetaInputRoot!=NULL){
  	if (BetaInputRoot->IsOpen()) BetaInputRoot->Close();
  	delete BetaInputRoot;
  	BetaInputRoot=NULL;
  }
  if(IonInputRoot!=NULL){
  	if (IonInputRoot->IsOpen()) IonInputRoot->Close();
  	delete IonInputRoot;
  	IonInputRoot=NULL;
  }
	cout<<"Input files closed."<<endl;
}

/************************************************************************************/

int Processor::ReadInputFiles(){
		//Check EOF
		if(BetaExists && IonExists) {
      InBetaData.ScintEnergy = 0;
      BetaInput.read((char *)&(InBetaData.ScintEnergy),sizeof(int));
      InIonData.TOF = 0;
      IonInput.read((char *)&(InIonData.TOF),sizeof(int));
      //  cout<<InBetaData.ScintEnergy<<" "<<InIonData.TOF<<endl;
      if(IonInput.eof() && BetaInput.eof()) {
				return 0;//loop ends here
      } else if(IonInput.eof() && (!BetaInput.eof())) {
				cout << "Beta event file and Ion event file do not match!\n";
				exit(1);
      } else if((!IonInput.eof()) && BetaInput.eof()) {
				cout << "Beta event file and Ion event file do not match!\n";
				exit(1);
      } else {}
  	}
    else if (IonExists) {
      InIonData.TOF = 0;
      IonInput.read((char *)&(InIonData.TOF),sizeof(int));
      if(IonInput.eof()) return 0;//loop ends here
    }
    else {//Beta only
      InBetaData.ScintEnergy = 0;
      BetaInput.read((char *)&(InBetaData.ScintEnergy),sizeof(int));
      if(BetaInput.eof()) return 0;//loop ends here

    }
		//Read from file
    if(BetaExists) {
      //Read the Beta event file
      BetaInput.read((char *)&(InBetaData.MWPCEnergy),sizeof(unsigned short));
      //	BetaInput.read((char *)(InBetaData.PosMWPCEntrance),3*sizeof(short));
      //	BetaInput.read((char *)(InBetaData.PosMWPCExit),3*sizeof(short));
      BetaInput.read((char *)&(InBetaData.MWPCFiredCell),sizeof(uint8_t));
      int kk = InBetaData.MWPCFiredCell;
      //	cout <<InBetaData.ScintEnergy<<" "<< InBetaData.MWPCEnergy<<" " << kk<<endl;
      if (kk>0) {
				BetaInput.read((char *)(InBetaData.MWPCCellIndex),kk*sizeof(uint8_t));
				BetaInput.read((char *)(InBetaData.MWPCCellIonNumber),kk*sizeof(unsigned short));
				BetaInput.read((char *)(InBetaData.MWPCCellXpos),kk*sizeof(short));
				BetaInput.read((char *)(InBetaData.MWPCCellXvar),kk*sizeof(unsigned short));
      }
      BetaInput.read((char *)&(InBetaData.ExitAngle_Be),sizeof(unsigned short));
      BetaInput.read((char *)(InBetaData.InitP),3*sizeof(int));
      BetaInput.read((char *)(InBetaData.DECPos),3*sizeof(short));
      BetaInput.read((char *)&(InBetaData.Hits),sizeof(uint16_t));
      BetaInput.read((char *)&(InBetaData.ParticleID),sizeof(uint8_t));
      BetaInput.read((char *)&(InBetaData.ScintTimeCorrection),sizeof(short));
      BetaInput.read((char *)&(InBetaData.MCPTime),sizeof(short));

      //    cout<<"*************"<<InBetaData.ScintEnergy<<endl;
      //Save data to local double variables for calculation
      InBeta_Data.ScintEnergy = double(InBetaData.ScintEnergy)/1000.0;	//keV
      InBeta_Data.MWPCEnergy = double(InBetaData.MWPCEnergy)/1000.0;		//keV
      InBeta_Data.MWPCFiredCell = InBetaData.MWPCFiredCell;
      for (int i=0; i<kk; i++) {
				InBeta_Data.MWPCCellIndex[i] = InBetaData.MWPCCellIndex[i];
				InBeta_Data.MWPCCellIonNumber[i] = InBetaData.MWPCCellIonNumber[i];
				InBeta_Data.MWPCCellXpos[i] = double(InBetaData.MWPCCellXpos[i])/1000.0; //mm
				InBeta_Data.MWPCCellXvar[i] = double(InBetaData.MWPCCellXvar[i])/1000.0; //mm
      }
      InBeta_Data.ScintTimeCorrection = double(InBetaData.ScintTimeCorrection/1000.0);//ns
      InBeta_Data.MCPTime = double(InBetaData.MCPTime/1000.0);//ns
      /*	for(int i=0;i<2;i++){
		InBeta_Data.PosMWPCEntrance[i] = double(InBetaData.PosMWPCEntrance[i])/1000.0;	//mm
		InBeta_Data.PosMWPCExit[i] = double(InBetaData.PosMWPCExit[i])/1000.0;	//mm
		}
		InBeta_Data.PosMWPCEntrance[2] = double(InBetaData.PosMWPCEntrance[2])/100.0;	//mm
		InBeta_Data.PosMWPCExit[2] = double(InBetaData.PosMWPCExit[2])/100.0;	//mm
       */
      //    ExitAngle_Be = double(InBetaData.ExitAngle_Be)/100.0;
      //Hit map does not need to process, will be written to event file directly
    }

    if(IonExists) {
      //Read the Ion event file
      for(int i=0; i<2; i++) {
				InIonData.HitPos[i]=0;
				IonInput.read((char*)(&(InIonData.HitPos[i])),sizeof(int));
				//The integer in the file occupies 3 byte, need to take care of the sign when converting it to int
				//Not anymore
				//	if(((InIonData.HitPos[i]>>23) & 0x1) != 0) InIonData.HitPos[i] |= 0xff000000;
      }
      IonInput.read((char *)&(InIonData.HitAngle),sizeof(unsigned short));
      IonInput.read((char *)&(InIonData.IonEnergy),sizeof(unsigned short));
      IonInput.read((char *)&(InIonData.Status),sizeof(uint8_t));

      //Save data to local double variables for calculation
      InIon_Data.TOF = double(InIonData.TOF)/1000.0; //ns
      for(int i=0; i<2; i++) {
				InIon_Data.HitPos[i] = double(InIonData.HitPos[i])/1000.0;	//mm
      }
      InIon_Data.HitAngle = double(InIonData.HitAngle)/100.0;
      InIon_Data.IonEnergy = double(InIonData.IonEnergy)/1000.0;	//keV
    }
    return 1;
}

/************************************************************************************/
void Processor::OpenOutputFiles(string PurposeName){
  char filename[100];
  char rootFilename[100];
  for (int i=0; i<N_Out; i++) {
    if(PurposeName.compare("Normal")==0) { //Normal He6 Decay Event
      sprintf(filename,"%s/EventFile%03d%02d%02d%02d.dat",DATA_DIRECTORY,GeneratorID,BetafileID,IonfileID,OutputfileID+i);
      sprintf(rootFilename, "%s/EventFile%03d%02d%02d%02d.root",DATA_DIRECTORY,GeneratorID,BetafileID,IonfileID,OutputfileID+i);
    }
    else {
      sprintf(filename,"%s/%s_EventFile%03d%02d%02d%02d.dat",DATA_DIRECTORY, PurposeName.c_str(), GeneratorID, BetafileID, IonfileID, OutputfileID+i);
      sprintf(rootFilename,"%s/%s_EventFile%03d%02d%02d%02d.root",DATA_DIRECTORY, PurposeName.c_str(), GeneratorID, BetafileID, IonfileID, OutputfileID+i);
    }
    if(DatOutputRequested)
      FinalOutput[i].open(filename,ios::out | ios::binary);
    if(RootOutputRequested)
      FinalOutputRoot[i]=new TFile(rootFilename,"recreate");//FinalOutputRoot[i] now holds as its data the address of a TFile instance.
	}
}

/************************************************************************************/
void Processor::CloseOutputFiles(){
	
	for (int i=0; i<N_Out; i++){
		if (FinalOutput[i].is_open())	FinalOutput[i].close();
 		if(FinalOutputRoot[i]!=NULL){
		  if (FinalOutputRoot[i]->IsOpen()) FinalOutputRoot[i]->Close();
			delete FinalOutputRoot[i];
			FinalOutputRoot[i]=NULL;
		}
  }
  cout << "Output files closed!\n";
}
  
/************************************************************************************
void Processor::SetInput(InBetaDataStruct_D* Beta_In,InIonDataStruct_D* Ion_In)
{
  InBetaData = Beta_In;
  InIonData = Ion_In;
}
void Processor::SetOutput(OutDataStruct_D* DataOut)
{
  OutData = DataOut;
}
************************************************************************************/

void Processor::ApplyDetectorResponse(){
  if(BetaExists) {
    ApplyScintResponse();
    ApplyMWPCResponse();
  }
  if(IonExists) {
    ApplyTOFWidth();
    ApplyMCPResponse();
  }
}

/************************************************************************************/

void Processor::WriteEventToFiles(){
	for (int i=0;i<N_Out;i++) { //Output file loop
		SaveEntryToOutputStruct(i);
		WriteOutputStructToFile(i);
		FillRootTree(i);
	}
}
/************************************************************************************/
void Processor::SaveEntryToOutputStruct(int i){
	if(BetaExists) {
		//Beta
		OutData.ScintEnergy = (unsigned int)(Out_Data[i].ScintEnergy*1000.0);	//eV
		if (Out_Data[i].MWPCEnergy*1000.0>65535) OutData.MWPCEnergy = 65535;
		else OutData.MWPCEnergy = (unsigned short)(Out_Data[i].MWPCEnergy*1000.0);		//eV
		OutData.MWPCHitPos[0] = short(floor(Out_Data[i].MWPCHitPos[0]*1000.0));
		OutData.MWPCHitPos[1] = short(floor(Out_Data[i].MWPCHitPos[1]*1000.0));		//um
		if (Out_Data[i].MWPCAnodeSignal1*2000.0>65535) OutData.MWPCAnodeSignal1 = 65535;
		//	  cout <<Out_Data[i].MWPCAnodeSignal1<<endl;
		else OutData.MWPCAnodeSignal1 = (unsigned short)(Out_Data[i].MWPCAnodeSignal1*2000.0);		//arbi. unit
		if (Out_Data[i].MWPCAnodeSignal2*2000.0>65535) OutData.MWPCAnodeSignal2 = 65535;
		else OutData.MWPCAnodeSignal2 = (unsigned short)(Out_Data[i].MWPCAnodeSignal2*2000.0);		//arbi. unit
		for (int j=0; j<12; j++) {
			if (Out_Data[i].MWPCCathodeSignal[j]*8000.0>65535) OutData.MWPCCathodeSignal[j] = 65535;
			else OutData.MWPCCathodeSignal[j] = (unsigned short)(Out_Data[i].MWPCCathodeSignal[j]*8000.0);		//uV
				   // cout << OutData.MWPCCathodeSignal[j]<< " ";
		}
			  //cout << endl;
		OutData.ExitAngle_Be = InBetaData.ExitAngle_Be;
		for (int ii=0; ii<3; ii++) {
			OutData.InitP[ii] = InBetaData.InitP[ii];
		}
		for (int ii=0; ii<3; ii++) {
			OutData.DECPos[ii] = InBetaData.DECPos[ii];
		}
		OutData.Hits = InBetaData.Hits;
		OutData.ParticleID = InBetaData.ParticleID;
		if(Out_Data[i].TOFCorrection>=0)OutData.TOFCorrection = Out_Data[i].TOFCorrection*1000;
		if(Out_Data[i].MCPTime>=0)OutData.MCPTime = Out_Data[i].MCPTime*1000;
  }
  if(IonExists) {
		//Ion
		OutData.TOF = (unsigned int)(Out_Data[i].TOF*1000.0);
		OutData.HitPos[0] = int(floor(Out_Data[i].HitPos[0]*1000.0));       //in um
		OutData.HitPos[1] = int(floor(Out_Data[i].HitPos[1]*1000.0));
		OutData.HitAngle = (unsigned short)(Out_Data[i].HitAngle*100.0);        //0.01deg
		//cout<<"Ion energy "<<i<<":"<<Out_Data[i].IonEnergy*1000.0<<endl;
		OutData.IonEnergy = (unsigned short)(Out_Data[i].IonEnergy*1000.0);        //eV
		if(InIonData.Status==1) OutData.IonStatus = InIonData.Status;
		else OutData.IonStatus = Out_Data[i].IonStatus;
  }
}
  
/************************************************************************************/  

void Processor::WriteOutputStructToFile(int i){
 	if(BetaExists) {
		if(DatOutputRequested){
			FinalOutput[i].write((char*)&(OutData.ScintEnergy),sizeof(int));
			FinalOutput[i].write((char*)&(OutData.MWPCEnergy),sizeof(unsigned short));
			FinalOutput[i].write((char*)(OutData.MWPCHitPos),2*sizeof(short));
			FinalOutput[i].write((char*)&(OutData.MWPCAnodeSignal1),sizeof(unsigned short));
			FinalOutput[i].write((char*)&(OutData.MWPCAnodeSignal2),sizeof(unsigned short));
			FinalOutput[i].write((char*)(OutData.MWPCCathodeSignal),12*sizeof(unsigned short));
			FinalOutput[i].write((char*)&(OutData.ExitAngle_Be),sizeof(unsigned short));
			FinalOutput[i].write((char*)(OutData.InitP),3*sizeof(int));
			FinalOutput[i].write((char*)(OutData.DECPos),3*sizeof(short));
			FinalOutput[i].write((char*)&(OutData.Hits),sizeof(uint16_t));
			FinalOutput[i].write((char*)&(OutData.ParticleID),sizeof(uint8_t));
		}
	}
	if(IonExists) {
		if(DatOutputRequested){
			FinalOutput[i].write((char*)&(OutData.TOF),sizeof(int));
			FinalOutput[i].write((char*)&(OutData.HitPos[0]),sizeof(int));
			FinalOutput[i].write((char*)&(OutData.HitPos[1]),sizeof(int));
			FinalOutput[i].write((char*)&(OutData.HitAngle),sizeof(unsigned short));
			FinalOutput[i].write((char*)&(OutData.IonEnergy),sizeof(unsigned short));
			FinalOutput[i].write((char*)&(OutData.IonStatus),sizeof(uint8_t));
		}
		//cout<<"Xhit = "<< OutData.HitPos[0]<<"\tYhit = "<< OutData.HitPos[1]<<endl;
	}
}

/************************************************************************************/   
/*
//Turn features on and off
void Processor::TurnOn(string name)
{
  if (name.compare("Scint")==0) ScintOn = true; 
  else if (name.compare("MWPC")==0) MWPCOn = true; 
  else if (name.compare("TOF")==0) TOFOn = true;
  else if (name.compare("MCP")==0) MCPOn = true; 
}
void Processor::TurnOff(string name)
{
  if (name.compare("Scint")==0) ScintOn = false; 
  else if (name.compare("MWPC")==0) MWPCOn = false; 
  else if (name.compare("TOF")==0) TOFOn = false; 
  else if (name.compare("MCP")==0) MCPOn = false; 
}*/

int Processor::LoadMWPCResponse(int i,string GainFile,string SignalFile){
  GainFileNames[i] = GainFile;
  SignalFileNames[i] = SignalFile;
  string GainFileName = CalDirectory + "/" + GainFile;
  string SignalFileName = CalDirectory + "/" + SignalFile;
  cout << "Loading MWPC Field file "<<GainFileName<<" for output "<<i<<endl;
  ifstream fieldfilein;
  ifstream signalfilein;
  double x,y,RelGain;
  //Load Gain map from calibration
  fieldfilein.open(GainFileName.c_str(),ios::in);
  if(!fieldfilein.is_open()){
    cout << "MWPC Gain file "<<GainFileName<<" does not exist!"<<endl;
    return -1;
  }
  for (int j=0;j<17;j++){
    for (int k=0;k<18;k++){
      fieldfilein >> x >> y >> RelGain;
      //MWPC_M[i][y][x]!!!!!!!!!!!!!!!!!
      MWPC_M[i][k][j] = RelGain;	//Set multiplication factor
//      cout << x<<" "<<y<<" "<< MWPC_M[i][k][j]<<endl;
//      TestHist->Fill(x*10,y*10,MWPC_M[i][k][j]);
    }
  }
  fieldfilein.close();
  //Load Signal Map
  signalfilein.open(SignalFileName.c_str(),ios::in);
  cout << "Loading MWPC Signal file "<<SignalFileName<<" for output "<<i<<endl;
  for (int j=0;j<257;j++){
    for (int k=0;k<18;k++){
      signalfilein >> x >> y;
//      cout <<x<<" "<<y<<" ";
      for (int l=0;l<6;l++){
	signalfilein >> MWPC_Signal[i][j][k].CathodeX[l];
//	cout << MWPC_Signal[i][j][k].CathodeX[l]<< " ";
      }
      for (int l=0;l<6;l++){
	signalfilein >> MWPC_Signal[i][j][k].CathodeY[l];
//	cout << MWPC_Signal[i][j][k].CathodeY[l]<< " ";
      }
//      cout <<endl;
    }
  }
  signalfilein.close();
/*
  static TRandom3 rand_gen(0);
  double aux;   //Auxiliary number
  for (int ii=0;ii<10000;ii++){
    double Xpos = rand_gen.Uniform(-20.0,20.0);
    //double Xpos = -5.0+0.001*ii;
    int Xlow,Xhigh;
    double weight;
    if(Xpos < -19.0){
      Xlow=Xhigh=0;
      weight = 1.0;
    }
    else if(Xpos > 19.0){
      Xlow=Xhigh=38;
      weight = 1.0;
    }
    else {
      Xlow = int(Xpos+19.0);
      Xhigh = Xlow+1;
      weight = double(Xhigh)-Xpos-19.0;
    }
    double QMWPC_cathode_X = 0.0;
    double QMWPC_cathode_Y = 0.0;
    double Q_read;
    double xpos = 0.0;
    double ypos = 0.0;
    for (int l=0;l<6;l++){
      Q_read = (weight*MWPC_Signal[i][5][Xlow].CathodeX[l]+(1-weight)*MWPC_Signal[i][5][Xhigh].CathodeX[l]);
//      Q_read = MWPC_Signal[i][5][Xlow].CathodeX[l];
      QMWPC_cathode_X+=Q_read;
      xpos += (-20.0+8.0*l)*Q_read;
    }
    for (int l=6;l<12;l++){
      Q_read = (weight*MWPC_Signal[i][5][Xlow].CathodeY[l-6]+(1-weight)*MWPC_Signal[i][5][Xhigh].CathodeY[l-6]);
      QMWPC_cathode_Y+=Q_read;
      ypos += (-20.0+8.0*(l-6))*Q_read;
    }
//    TestHist->Fill(rand_gen.Uniform(xpos/QMWPC_cathode_X-0.5,xpos/QMWPC_cathode_X+0.5));
    TestHist->Fill(xpos/QMWPC_cathode_X);
//    cout<<xpos/QMWPC_cathode_X<<endl;
//    cout <<xpos/QMWPC_cathode_X<<" "<<ypos/QMWPC_cathode_Y<<endl;
  }*/

/*  for (int j=0;j<20;j++){
    for (int k=0;k<39;k++){
      double QMWPC_cathode_X = 0.0;
      double QMWPC_cathode_Y = 0.0;
      double Q_read;
      double xpos = 0.0;
      double ypos = 0.0;
      for (int l=0;l<6;l++){
	Q_read = MWPC_Signal[i][j][k].CathodeX[l];
	QMWPC_cathode_X+=Q_read;
	xpos += (-20.0+8.0*l)*Q_read;
      }
      for (int l=0;l<6;l++){
	Q_read = MWPC_Signal[i][j][k].CathodeY[l];
	QMWPC_cathode_Y+=Q_read;
	ypos += (-20.0+8.0*l)*Q_read;
      }
      TestHist->Fill(xpos/QMWPC_cathode_X,ypos/QMWPC_cathode_Y);
      cout <<xpos/QMWPC_cathode_X<<" "<<ypos/QMWPC_cathode_Y<<endl;
    }
  }*/
  cout << "Loading done.\n";

  return 0;
}
//Processor
int Processor::ApplyScintResponse()
{
  //Random number generator
  static TRandom3 rand_gen(0);
  double aux;   //Auxiliary number

  //TOF width corrections
  double DecayNorm = 0.41;
  double DecayLength = 530.123;

  for (int i=0;i<N_Out;i++){
    double Photon_rate = 1.0/pow(ScintWidth[i],2.0);	//Number of photons for 0.93561 MeV electron
    double Energy_res = 0.09;	//Energy resolution,
    
    //Calculate photon number and the energy resolution of the Scintillator
    //The energy unit here is keV
    if (InBeta_Data.ScintEnergy>0.001){	//Makesure not devided by zero
      Energy_res = 1.0/sqrt(Photon_rate * InBeta_Data.ScintEnergy/935.61) * InBeta_Data.ScintEnergy;
      Out_Data[i].ScintEnergy = rand_gen.Gaus(InBeta_Data.ScintEnergy, Energy_res);
      if (Out_Data[i].ScintEnergy<0)Out_Data[i].ScintEnergy=0.0;
//      cout << Out_Data[i].MWPCEnergy<<endl;
    }else{
      Out_Data[i].ScintEnergy = 0.0;
    }
    TOFWidthCorrection[i]=DecayNorm*exp(-Out_Data[i].ScintEnergy/DecayLength);
    TOFCorrection[i]=InBeta_Data.ScintTimeCorrection;
    if (InBeta_Data.ScintTimeCorrection>0 && InBeta_Data.MCPTime>0)MCPTime[i] = InBeta_Data.MCPTime - InBeta_Data.ScintTimeCorrection;
    else MCPTime[i]=-10;
    Out_Data[i].MCPTime=MCPTime[i];
    Out_Data[i].TOFCorrection=TOFCorrection[i];
//    Out_Data[i].TOFCorrection = rand_gen.Gaus(TOFCorrection[i],TOFWidth[i]+TOFWidthCorrection[i]);
  }
  return 0;
}

int Processor::ApplyMWPCResponse()
{
  //Random number generator
  static TRandom3 rand_gen(0);
  double aux;   //Auxiliary number
  int WireIndex;
  int NWire = InBeta_Data.MWPCFiredCell;
  int XIndex;
  int Xlow,Xhigh;	//for cathode determination, interpolate
  int InitialElectron;
  int FinalElectron;
  double XposCenter;
  double XposCenter2;
  double YposCenter;	//for test
  double Xwidth;
  double Xpos;
  double M;
  double std_M = 10000;
  double std_AnodeAmp = 2.916e-6;	//Scaling factor to match the output format
//  double AnodeNoise = 0.015;	//After Scaling
//  double CathodeNoise = 0.02;	//After Scaling
  double weight = 0.0;
  double SigRatio = 0.0;
  double N_tot;
//  double AnodeGainFactor = 0.15;

  for (int i=0;i<N_Out;i++){
    double AnodeAmp = std_AnodeAmp*std_M/MWPCAnodeGainFactor[i];	//Scaling factor to match the output format
    double CathodeAmp = AnodeAmp*1.0;	
    Out_Data[i].MWPCAnodeSignal1=0.0;
    Out_Data[i].MWPCAnodeSignal2=0.0;
    for (int j=0;j<12;j++){
      Out_Data[i].MWPCCathodeSignal[j]=0.0;
    }
    XposCenter2=0;
    YposCenter=0;
    N_tot = 0.0;
    for (int j=0;j<NWire;j++){	//Loop triggered wires
      WireIndex = InBeta_Data.MWPCCellIndex[j];
      SigRatio = double(WireIndex+1)/21.0;
      InitialElectron = InBeta_Data.MWPCCellIonNumber[j];
      XposCenter = InBeta_Data.MWPCCellXpos[j];
      Xwidth = sqrt(12.0*pow(InBeta_Data.MWPCCellXvar[j],2.0));	//assuming uniform distibution
      double ATotCharge = 0.0;
      for (int k=0;k<InitialElectron;k++){
	Xpos = rand_gen.Uniform(XposCenter-Xwidth/2.0,XposCenter+Xwidth/2.0);
	XIndex = int((Xpos+16)/2.0);
	
	//If beyond calibrated area, set it to the edge
	if(Xpos < -16.0){
	  Xlow=Xhigh=0;
	  weight = 1.0;
	}
	else if(Xpos > 16.0){
	  Xlow=Xhigh=16;
	  weight = 1.0;
	}
	else {
	  Xlow = XIndex;
	  Xhigh = XIndex+1;
	  weight = (double(Xhigh)*2.0-16.0-Xpos)/2.0;
	}
	//Generate Multiplication
	if (WireIndex<1) WireIndex=1;	//the last 1 wire are not calibrated, so set it to the edge
	if (WireIndex>18) WireIndex=18;
	M = rand_gen.Exp(MWPC_M[i][WireIndex-1][Xlow]*weight+MWPC_M[i][WireIndex-1][Xhigh]*(1-weight))*MWPCAnodeGainFactor[i];
//	M = (MWPC_M[i][WireIndex][Xlow]*weight+MWPC_M[i][WireIndex][Xhigh]*(1-weight))*0.5;
//	M = 350428.0;
//	M = rand_gen.Exp(MWPC_M[i][WireIndex][XIndex]);
	if (M<1.0) M=1.0;
//	M=1.0;
	ATotCharge+=M;
	//Calculate center of X Y hit
        YposCenter += (-19.0+2.0*WireIndex);
        XposCenter2 +=InBeta_Data.MWPCCellXpos[j];
	N_tot = N_tot+1.0;;

	//Output cathode signals
	if (Xpos>-16.0625 && Xpos<16.0625 && WireIndex>=1 && WireIndex<=18){
	  XIndex = int((Xpos+16.0625)/0.125);
	  for (int l=0;l<6;l++){
	    Out_Data[i].MWPCCathodeSignal[l] += M*0.5*MWPC_Signal[i][XIndex][WireIndex-1].CathodeX[l];
//	    cout <<Out_Data[i].MWPCCathodeSignal[l]<<endl;
	  }
	}else if (Xpos<=-16.0625 && Xpos>-20.0 && WireIndex>=2 && WireIndex<=17){
	  XIndex = int((Xpos+8.0+16.0625)/0.125);
	  for (int l=0;l<5;l++){
	    Out_Data[i].MWPCCathodeSignal[l] += M*0.5*MWPC_Signal[i][XIndex][WireIndex-1].CathodeX[l+1];
	  }
	  Out_Data[i].MWPCCathodeSignal[5]=0.0;
	}else if (Xpos>=16.0625 && Xpos<20.0 && WireIndex>=2 && WireIndex<=17){
	  XIndex = int((Xpos-8.0+16.0625)/0.125);
	  for (int l=1;l<6;l++){
	    Out_Data[i].MWPCCathodeSignal[l] += M*0.5*MWPC_Signal[i][XIndex][WireIndex-1].CathodeX[l-1];
	  }
/*	  if (Out_Data[i].MWPCCathodeSignal[5]>Out_Data[i].MWPCCathodeSignal[4]){
	    cout <<Out_Data[i].MWPCCathodeSignal[4] <<" "<<Out_Data[i].MWPCCathodeSignal[5]<<endl;
	  }*/
	  Out_Data[i].MWPCCathodeSignal[0]=0.0;
	}
	if ( WireIndex>=1 && WireIndex<=18){
	  for (int l=6;l<12;l++){
	    Out_Data[i].MWPCCathodeSignal[l] += M*0.5*MWPC_Signal[i][XIndex][WireIndex-1].CathodeY[l-6];
	  }
	}else if ( WireIndex<1){
	  for (int l=6;l<11;l++){
	    Out_Data[i].MWPCCathodeSignal[l] += M*0.5*MWPC_Signal[i][XIndex][WireIndex+4-1].CathodeY[l-6+1];
	  }
	  Out_Data[i].MWPCCathodeSignal[11]=0.0;
	}else if ( WireIndex>18){
	  for (int l=7;l<12;l++){
	    Out_Data[i].MWPCCathodeSignal[l] += M*0.5*MWPC_Signal[i][XIndex][WireIndex-4-1].CathodeY[l-6-1];
	  }
	  Out_Data[i].MWPCCathodeSignal[6]=0.0;
	}

/*	if(Xpos < -19.0){
	  Xlow=Xhigh=0;
	  weight = 1.0;
	}
	else if(Xpos > 19.0){
	  Xlow=Xhigh=38;
	  weight = 1.0;
	}
	else {
	  Xlow = int(Xpos+19.0);
	  Xhigh = Xlow+1;
	  weight = double(Xhigh)-Xpos-19.0;
	}
	for (int l=0;l<6;l++){
	  Out_Data[i].MWPCCathodeSignal[l] += M*0.5*(weight*MWPC_Signal[i][WireIndex][Xlow].CathodeX[l]+(1-weight)*MWPC_Signal[i][WireIndex][Xhigh].CathodeX[l]);
	}
	for (int l=6;l<12;l++){
	  Out_Data[i].MWPCCathodeSignal[l] += M*0.5*(weight*MWPC_Signal[i][WireIndex][Xlow].CathodeY[l-6]+(1-weight)*MWPC_Signal[i][WireIndex][Xhigh].CathodeY[l-6]);
	}*/
      }
      //Broadening due to some unknown reason
      ATotCharge = rand_gen.Gaus(ATotCharge,ATotCharge*MWPCAnodeBroadenFactor[i]);
      Out_Data[i].MWPCAnodeSignal1+=ATotCharge*SigRatio;
      Out_Data[i].MWPCAnodeSignal2+=ATotCharge*(1.0-SigRatio);
    }
    //Scale output and add noise
    Out_Data[i].MWPCAnodeSignal1*=AnodeAmp;
    Out_Data[i].MWPCAnodeSignal2*=AnodeAmp;
//    cout << Out_Data[i].MWPCAnodeSignal1+Out_Data[i].MWPCAnodeSignal2<<endl;
    for (int l=0;l<12;l++){
      Out_Data[i].MWPCCathodeSignal[l] *= CathodeAmp;
    }
    Out_Data[i].MWPCEnergy = InBeta_Data.MWPCEnergy;
    //Adding  noise
    Out_Data[i].MWPCAnodeSignal1=rand_gen.Gaus(Out_Data[i].MWPCAnodeSignal1,MWPCAnodeNoise[i]);
    Out_Data[i].MWPCAnodeSignal2=rand_gen.Gaus(Out_Data[i].MWPCAnodeSignal2,MWPCAnodeNoise[i]);
    if (Out_Data[i].MWPCAnodeSignal1<0.0)Out_Data[i].MWPCAnodeSignal1=0.0;
    if (Out_Data[i].MWPCAnodeSignal2<0.0)Out_Data[i].MWPCAnodeSignal2=0.0;
    for (int l=0;l<12;l++){
      Out_Data[i].MWPCCathodeSignal[l] = rand_gen.Gaus(Out_Data[i].MWPCCathodeSignal[l],MWPCCathodeNoise[i]);
      if (Out_Data[i].MWPCCathodeSignal[l]<0.0)Out_Data[i].MWPCCathodeSignal[l]=0.0;
    }
    YposCenter/=N_tot;
    XposCenter2/=N_tot;
    if (InBeta_Data.MWPCEnergy==0.0){
      Out_Data[i].MWPCHitPos[0] = -26;
      Out_Data[i].MWPCHitPos[1] = -26;
    }else{
      Out_Data[i].MWPCHitPos[0] = XposCenter2;
      Out_Data[i].MWPCHitPos[1] = YposCenter;
    }
//    if (Out_Data[i].MWPCAnodeSignal1+Out_Data[i].MWPCAnodeSignal2>0.3)
    double CathodeXTot=0;
    for (int l=0;l<6;l++){
      CathodeXTot+=Out_Data[i].MWPCCathodeSignal[l];
    }
//    cout << CathodeXTot<<endl;
//    TestHist->Fill(CathodeXTot);
    //TestHist->Fill(InBeta_Data.MWPCEnergy);
//    TestHist->Fill(Out_Data[i].MWPCAnodeSignal1+Out_Data[i].MWPCAnodeSignal2);
    
//    TestHist->Fill(Out_Data[i].MWPCCathodeSignal[3]);
    //simple reconstruction for check
/*    double QMWPC_cathode_X = 0.0;
    double QMWPC_cathode_Y = 0.0;
    double Q_read;
    double xpos = 0.0;
    double ypos = 0.0;
    for (int l=0;l<6;l++){
      Q_read = Out_Data[i].MWPCCathodeSignal[l];
      QMWPC_cathode_X+=Q_read;
      xpos += (-20.0+8.0*l)*Q_read;
    }
    for (int l=6;l<12;l++){
      Q_read = Out_Data[i].MWPCCathodeSignal[l];
      QMWPC_cathode_Y+=Q_read;
      ypos += (-20.0+8.0*(l-6))*Q_read;
    }*/
//    TestHist->Fill(xpos/QMWPC_cathode_X);
//    TestHist->Fill(ypos/QMWPC_cathode_Y);
//    double YPOS = (Out_Data[i].MWPCAnodeSignal1-Out_Data[i].MWPCAnodeSignal2)/(Out_Data[i].MWPCAnodeSignal1+Out_Data[i].MWPCAnodeSignal2)*21.0;
    
//    TestHist->Fill(xpos/QMWPC_cathode_X,ypos/QMWPC_cathode_Y);
//    TestHist->Fill(xpos/QMWPC_cathode_X,YPOS);
//    double sig =  Out_Data[i].MWPCAnodeSignal1+Out_Data[i].MWPCAnodeSignal2;
    //double sig = Out_Data[i].MWPCCathodeSignal[3]; 
//    double sig = Out_Data[i].MWPCAnodeSignal1; 
//    if (sig >0.2)TestHist->Fill(sig);
//    double YPOS = (Out_Data[i].MWPCAnodeSignal1-Out_Data[i].MWPCAnodeSignal2)/(Out_Data[i].MWPCAnodeSignal1+Out_Data[i].MWPCAnodeSignal2)*21.0;
//    TestHist->Fill(YPOS);
/*    if (Out_Data[i].MWPCAnodeSignal>0){
      double sig =  rand_gen.Gaus(Out_Data[i].MWPCAnodeSignal,700000);
      TestHist->Fill(sig);
    }*/
//    TestHist->Fill(Out_Data[i].MWPCAnodeSignal);

  }
  return 0;
}

int Processor::ApplyTOFWidth()
{
  //Random number generator
  static TRandom3 rand_gen(0);
  double aux;   //Auxiliary number

  //ApplyTOFWidth needs to be excuted after scintillator response is processed
  for (int i=0;i<N_Out;i++){
    //If the beta hits both MCP and scintillator, use this as the TOF at 50% of the time
    if (MCPTime[i]>-9){
      aux = rand_gen.Rndm();
      if (aux>0.5){
	Out_Data[i].TOF = rand_gen.Gaus(MCPTime[i],TOFWidth[i]+TOFWidthCorrection[i]); 
	Out_Data[i].TOF+=5;//Plus 5 to avoid negative TOF
	continue;
      }
    }
    //Add Gaussian width of TOF
    Out_Data[i].TOF = rand_gen.Gaus(InIon_Data.TOF,TOFWidth[i]+TOFWidthCorrection[i]) - TOFCorrection[i];
    //    cout << InIon_Data.TOF<<" "<< Out_Data[i].TOF<<endl;
    
  }
  return 0;
}

int Processor::ApplyMCPResponse()
{
  //Random number generator
  static TRandom3 rand_gen(0);
  double aux;   //Auxiliary number
  double x, y, xg, yg, xlim_m, xlim_p, ylim_m, ylim_p;
  double MaskR2 = MaskRadius*MaskRadius;
  xlim_m = (MaskPitch - MaskGridThickness)/2.0;
  xlim_p = (MaskPitch + MaskGridThickness)/2.0;
  ylim_m = xlim_m;
  ylim_p = xlim_p;
  
  for (int i=0;i<N_Out;i++){

    double x0 = InIon_Data.HitPos[0];
    double y0 = InIon_Data.HitPos[1];
    //Apply rotation
    x = x0*cos(MCPRot)-y0*sin(MCPRot); 
    y = x0*sin(MCPRot)+y0*cos(MCPRot);
    //Apply Grid Hardware Cut
    xg = fabs(fmod(x,MaskPitch));
    yg = fabs(fmod(y,MaskPitch));

    if(xg > xlim_m && xg < xlim_p) Out_Data[i].IonStatus = 2; //Don't keep if on grid line
    else if (yg > ylim_m && yg < ylim_p) Out_Data[i].IonStatus = 2; //Don't keep if on grid line
    else if (x*x + y*y > MaskR2) Out_Data[i].IonStatus = 2; //Don't keep if outside of mask radius
    else Out_Data[i].IonStatus = 0;

    //Add Gaussian width of MCP pos
    (Out_Data[i].HitPos)[0] = rand_gen.Gaus(x,MCPWidth[i]);
    (Out_Data[i].HitPos)[1] = rand_gen.Gaus(y,MCPWidth[i]);
    //copy ion hit angle and energy
    Out_Data[i].IonEnergy = InIon_Data.IonEnergy;
    Out_Data[i].HitAngle = InIon_Data.HitAngle;

  }
  return 0;
}

//Set parameters
void Processor::SetParameter(string name,int i,double val)
{
  if (name.compare("ScintWidth")==0) ScintWidth[i] = val; 
  else if (name.compare("MWPCAnodeGainFactor")==0) MWPCAnodeGainFactor[i] = val; 
  else if (name.compare("MWPCAnodeNoise")==0) MWPCAnodeNoise[i] = val; 
  else if (name.compare("MWPCAnodeBroadenFactor")==0)  MWPCAnodeBroadenFactor[i] = val; 
  else if (name.compare("MWPCCathodeNoise")==0) MWPCCathodeNoise[i] = val; 
  else if (name.compare("TOFWidth")==0) TOFWidth[i] = val;
  else if (name.compare("MCPWidth")==0) MCPWidth[i] = val;
  //else if (name.compare("MaskRadius")==0) MaskRadius[i] = val;
  //else if (name.compare("MaskPitch")==0) MaskPitch[i] = val;
  //else if (name.compare("MaskGridThickness")==0) MaskGridThickness[i] = val; 
   
}

//Logging
void Processor::Logging(int Index)
{
  char filename[100];
  char Betaname[100];
  char Ionname[100];
  ofstream LogOut;
  ifstream BetaIn;
  ifstream IonIn;
  string BufferStr;

  sprintf(Betaname,"%s/Setup/BetaSetup%03d%02d.log",DATA_DIRECTORY,GeneratorID, BetafileID);
  sprintf(Ionname,"%s/Setup/IonSetup%03d%02d%02d.log",DATA_DIRECTORY,GeneratorID, BetafileID, IonfileID);
  sprintf(filename,"%s/Setup/SetupFile%03d%02d%02d%02d.log",DATA_DIRECTORY,GeneratorID, BetafileID, IonfileID, OutputfileID+Index);
  LogOut.open(filename,ios::out);	//Open Log file

  // Copy log from Beta Log file
  BetaIn.open(Betaname,ios::in);	//Open Beta Log file
  getline(BetaIn,BufferStr);
  LogOut << "Beta Setup: " << BufferStr << endl;
  BetaIn.close();

  // Copy log from Ion Log file
  IonIn.open(Ionname,ios::in);		//Open Ion Log file
  getline(IonIn,BufferStr);
  LogOut << "Ion Setup: " << BufferStr << endl;
  IonIn.close();

  // Log Detector Responses from PostProcessor
  LogOut << "Detector Response " << ScintWidth[Index] << " " << GainFileNames[Index] << " " << SignalFileNames[Index]  << " " << MWPCAnodeGainFactor[Index] <<" "<< MWPCAnodeBroadenFactor[Index] <<" " << TOFWidth[Index] << " " << MCPWidth[Index] << endl;
  LogOut.close();	//Close output file 
}




