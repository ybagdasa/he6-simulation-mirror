#ifndef GLOBALCONSTANTS_H
#define GLOBALCONSTANTS_H

#define PI 3.14159265
#define CSPEED 299.792458			//[mm/ns]
#define CSPEED2 89875.5179		//[mm/ns]^2

#define MASS_E 0.510998928			//[MeV]
#define MASS_P 938.272046			//[MeV]
#define MASS_HE6 5606.556		//[MeV] 6He atomic mass (from TRIUMF 2012)
#define MASS_LI6 5603.051			//[MeV] 6Li atomic mass
#define MASS_HE4 3728.400 		//[MeV] 4He atomic mass
#define MASS_HE3 2809.413
#define AMU 931.494061			//[MeV]

#endif

