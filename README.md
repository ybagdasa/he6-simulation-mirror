He6-Simulation
==============

Simulation project for He6 experiment

Developing notes:

***************************************************************************************************
Event Generator

  The source code EventGenerator.cpp should stay in the folder ./src. Before building, make sure the
  directory ./lib exists. 

  A library associated with the core generator is built, so that the Beta Tracking program can use 
  and generate events internally, saving disk space when generating large quantity of data. The ROOT
  libraries need to be linked to this library when building.

  After the program is built, source the setup.sh
  $. setup.sh  (for bash)
  Otherwise the program won't find the lib.

***************************************************************************************************
Beta particle tracking

  In the CMakeList.txt, the "include_directories(${PROJECT_SOURCE_DIR}/../../EventGenerator/include)"
  line is needed for including the EventGenerator.h.
  Also, "-L../EventGenerator/lib EventGenerator" needed to be added to the target_link_libraries.
  -LXXXX is a black magic. It just works and I don't know the details.

  After the program is built, also source the setup.sh if you never source it or the linked one in
  the EventGenerator folder. The shell script in this folder is a symbolic link to the one in the
  EventGenerator folder. 

  To build this program, run cmake first and then make. The cmake command is so long that I wrote
  them into a shell script. However, the parameters are system and Geant4 installation dependent.
  Use the right version of cmake shell script according to their names and make sure that the 
  command in it fits your user name, source directory and Geant4 installation directory.

***************************************************************************************************
Recoil ion tracking

***************************************************************************************************
Post processing

***************************************************************************************************
Analyzer

